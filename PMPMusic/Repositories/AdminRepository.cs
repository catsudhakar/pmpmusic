﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PMPMusic.Models;
using PMPMusic.ViewModel;
using Microsoft.AspNetCore.Identity;

namespace PMPMusic.Repositories
{
    public class AdminRepository : IAdminRepository
    {
        private readonly ApplicationDBContext _applicationDbContext;
        private readonly UserManager<ApplicationUser> _userManager;


        public AdminRepository(ApplicationDBContext applicationDbContext, UserManager<ApplicationUser> userManager)
        {
            _applicationDbContext = applicationDbContext;
            _userManager = userManager;
        }
        public async Task<List<UserDetailsVM>> GetUserDetails()
        {

            try
            {
                var Users = _applicationDbContext.AspNetUsers.Where(x => x.IsActive == true).Select(f => new UserDetailsVM
                {
                    UserId = f.Id,
                    Email = f.Email,
                    FirstName = f.FirstName,
                    LastName = f.LastName,
                    ProfileLink = f.ProfileLink,
                    RegisteredOn = f.RegisteredOn,
                    IsFeaturedUser = f.IsFeaturedUser,
                    IsVerifyedUser = f.IsVerifyedUser

                }).OrderByDescending(m => m.RegisteredOn).ToList();

                return Users;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task UpdateFeaturedUser(List<SelectedUserDetailsVM> Users)
        {
            try
            {
                if (Users.Count > 0)
                {
                    foreach (var item in Users)
                    {
                        var user = _applicationDbContext.AspNetUsers.Where(x => x.Id == item.UserId).FirstOrDefault();
                        //if (item.IsFeaturedUser)
                        //    user.IsFeaturedUser = false;
                        //else
                        //    user.IsFeaturedUser = true;

                            user.IsFeaturedUser = item.IsFeaturedUser;
                            user.IsVerifyedUser = item.IsVerifyedUser;
                       

                    }
                    await _applicationDbContext.SaveChangesAsync();
                }



            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public async Task UpdateTrendingFeed(List<PostFeedVM> Users)
        {
            try
            {
                if (Users.Count > 0)
                {
                    foreach (var item in Users)
                    {

                        var user = _applicationDbContext.PostFeed.Where(x => x.Id == item.Id).FirstOrDefault();
                        if (user != null)
                        {
                            if (item.isTrending)
                                user.IsTrendingFeed = false;
                            else
                                user.IsTrendingFeed = true;
                        }

                    }
                    await _applicationDbContext.SaveChangesAsync();
                }



            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
