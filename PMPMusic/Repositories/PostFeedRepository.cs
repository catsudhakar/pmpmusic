﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PMPMusic.ViewModel;
using System.IO;
using PMPMusic.Services;
using PMPMusic.Models;
using Microsoft.AspNetCore.Identity;
using Amazon.S3.Model;
using Amazon.S3;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;


namespace PMPMusic.Repositories
{
    public class PostFeedRepository : IPostFeedRepository
    {

        private readonly ICloudService _amazonService;
        private readonly ApplicationDBContext _applicationDbContext;
        private readonly UserManager<ApplicationUser> _userManager;


        public PostFeedRepository(ApplicationDBContext applicationDbContext, ICloudService amazonService, UserManager<ApplicationUser> userManager)
        {
            _applicationDbContext = applicationDbContext;
            _amazonService = amazonService;
            _userManager = userManager;
        }
        public async Task<Array> AddPostFeed(PostFeedVM model, string userId)
        {
            Guid postId = Guid.NewGuid();
            var imageUrl = "";
            var audioUrl = "";
            //var videoUrl = "";

            if (model.ImageSrc != null)
            {
                var imageStream = new MemoryStream(Convert.FromBase64String(model.ImageSrc));
                imageUrl = await _amazonService.UploadMedia(imageStream, model.ImageType, postId.ToString());
                var postImage = Convert.FromBase64String(model.ImageSrc);
            }


            if (model.AudioSrc != null)
            {
                var audioStream = new MemoryStream(Convert.FromBase64String(model.AudioSrc));
                audioUrl = await _amazonService.UploadMedia(audioStream, model.AudioType, postId.ToString());
                var postAudio = Convert.FromBase64String(model.AudioSrc);
            }

            //if (model.VideoSrc != null)
            //{
            //    var videoStream = new MemoryStream(Convert.FromBase64String(model.VideoSrc));
            //    videoUrl = await _amazonService.UploadMedia(videoStream, model.VideoType, postId.ToString());
            //    var postVideo = Convert.FromBase64String(model.VideoSrc);
            //}




            //For now the user is fixed to default (Mirza)
            //var user = await _userManager.FindByEmailAsync("contactazhar.cat@gmail.com");
            var user = await _userManager.FindByIdAsync(userId);
            string imageName = null;
            string audioName = null;
            //string videoName = null;

            if (model.ImageFileName != null)
            {
                imageName = $"{model.ImageFileName.Split('.')[0]}";

            }
            if (model.AudioFileName != null)
            {
                audioName = $"{model.AudioFileName.Split('.')[0]}";

            }
            //if (model.VideoFileName != null)
            //{
            //    videoName = $"{model.VideoFileName.Split('.')[0]}";

            //}

            var post = new PostFeed
            {
                Id = postId,
                Title = model.Title,
                ImageLink = imageUrl,
                Description = model.Description,
                AudioLink = audioUrl,
                // VideoLink = videoUrl,
                VideoLink = model.VideoFileName,
                UpdatedBy = userId,
                CreatedOn = DateTime.UtcNow,

                IsActive = true,
                User = user,
                ExpireDate = DateTime.UtcNow.AddMonths(1),
                UpdatedOn = DateTime.UtcNow,
                ImageName = imageName,
                AudioName = audioName,
               // VideoName = videoName,
                SaveFeed = true

            };
            _applicationDbContext.PostFeed.Add(post);
            await _applicationDbContext.SaveChangesAsync();
            // var ImgAudVidArray = new string[] { imageUrl, audioUrl, videoUrl };
            var ImgAudVidArray = new string[] { imageUrl, audioUrl };
            return ImgAudVidArray;
        }

        public async Task<IEnumerable<PostFeedVM>> GetAllFeeds(string userId)
        {
            try
            {
                var feeds = await _applicationDbContext.PostFeed.Where(x => x.User.Id == userId && x.ExpireDate >= DateTime.UtcNow && x.IsActive == true).Select(f => new PostFeedVM
                {
                    Id = f.Id,
                    Title = f.Title,
                    Description = f.Description,
                    AudioLink = f.AudioLink,
                    ImageLink = f.ImageLink,
                    VideoLink = f.VideoLink,
                    ExpireDate = f.ExpireDate,
                    AudioFileName = f.AudioName,
                    ImageFileName = f.ImageName,
                    VideoFileName = f.VideoName,
                    FeedExpireIN = GetExpireTime(f.ExpireDate),
                    CreatedOn = f.CreatedOn,
                    SaveFeed = f.SaveFeed,
                    SaveFeedStatus = _applicationDbContext.SavedFeeds.Where(x => x.PostId == f.Id.ToString() && x.CreatedBy == userId && x.SaveFeed == true).Count(x => x.SaveFeed),
                    UserId = userId,
                    CreatedBy = f.User.Id,
                    IsFollow = _applicationDbContext.FollowFeeds.Where(x => (x.FollowingUserId.ToString() == f.User.Id && x.CreatedBy == userId) || (x.FollowingUserId.ToString() == userId && x.CreatedBy == f.User.Id) && x.IsFollow == true).Count(),
                    IsLikes = _applicationDbContext.LikeFeed.Where(x => x.PostId == f.Id && x.CreatedBy == userId && x.IsLike == true).Count(x => x.IsLike),
                    LikeCount = _applicationDbContext.LikeFeed.Where(x => x.PostId == f.Id && x.IsLike == true).Count(x => x.IsLike),
                    CreatedUserName = f.User.FirstName + " " + f.User.LastName,
                    UserProfileUrl = "/profile?ProfileName=" + f.User.FirstName + "-" + f.User.LastName,
                    UserProfile = f.User.ProfileLink
                }).OrderByDescending(m => m.CreatedOn).ToListAsync();

                return feeds;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        public async Task<IEnumerable<PostFeedVM>> getTotalActiveFeeds(string userId)
        {
            try
            {
                var feeds = await _applicationDbContext.PostFeed.Where(x => x.ExpireDate >= DateTime.UtcNow && x.IsActive == true).Select(f => new PostFeedVM
                {
                    Id = f.Id,
                    Title = f.Title,
                    Description = f.Description,
                    AudioLink = f.AudioLink,
                    ImageLink = f.ImageLink,
                    VideoLink = f.VideoLink,
                    ExpireDate = f.ExpireDate,
                    AudioFileName = f.AudioName,
                    ImageFileName = f.ImageName,
                    VideoFileName = f.VideoName,
                    FeedExpireIN = GetExpireTime(f.ExpireDate),
                    CreatedOn = f.CreatedOn,
                    SaveFeed = f.SaveFeed,
                    SaveFeedStatus = _applicationDbContext.SavedFeeds.Where(x => x.PostId == f.Id.ToString() && x.CreatedBy == userId && x.SaveFeed == true).Count(x => x.SaveFeed),
                    UserId = userId,
                    CreatedBy = f.User.Id,
                    IsFollow = _applicationDbContext.FollowFeeds.Where(x => (x.FollowingUserId.ToString() == f.User.Id && x.CreatedBy == userId) || (x.FollowingUserId.ToString() == userId && x.CreatedBy == f.User.Id) && x.IsFollow == true).Count(x => x.IsFollow == true),
                    IsLikes = _applicationDbContext.LikeFeed.Where(x => x.PostId == f.Id && x.CreatedBy == userId && x.IsLike == true).Count(x => x.IsLike),
                    LikeCount = _applicationDbContext.LikeFeed.Where(x => x.PostId == f.Id && x.IsLike == true).Count(x => x.IsLike),
                    CreatedUserName = f.User.FirstName + " " + f.User.LastName,
                    UserProfileUrl = "/profile?ProfileName=" + f.User.FirstName + "-" + f.User.LastName,
                    UserProfile = f.User.ProfileLink
                }).OrderByDescending(m => m.CreatedOn).ToListAsync();
                return feeds;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public async Task<IEnumerable<PostFeedVM>> getVideos(string userId)
        {
            try
            {
                var feeds = await _applicationDbContext.PostFeed.Where(x => x.ExpireDate >= DateTime.UtcNow && x.IsActive == true && x.User.Id == userId && (x.VideoLink != "" && x.VideoLink != null)).Select(f => new PostFeedVM
                {
                    Id = f.Id,
                    Title = f.Title,
                    Description = f.Description,
                    AudioLink = f.AudioLink,
                    ImageLink = f.ImageLink,
                    VideoLink = f.VideoLink,
                    ExpireDate = f.ExpireDate,
                    AudioFileName = f.AudioName,
                    ImageFileName = f.ImageName,
                    VideoFileName = f.VideoName,
                    FeedExpireIN = GetExpireTime(f.ExpireDate),
                    CreatedOn = f.CreatedOn,
                    SaveFeed = f.SaveFeed,
                    UserId = userId,
                    CreatedBy = f.User.Id,
                    CreatedUserName = f.User.FirstName + " " + f.User.LastName,
                    UserProfileUrl = "/profile?ProfileName=" + f.User.FirstName + "-" + f.User.LastName,
                    UserProfile = f.User.ProfileLink
                }).OrderByDescending(m => m.CreatedOn).ToListAsync();
                return feeds;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private string getVideoFileNmae(string VideoLink)
        {
            string name = "";
            if (!string.IsNullOrEmpty(VideoLink))
            {
                int index3 = VideoLink.LastIndexOf('/');
                if (index3 != -1)
                {
                    name = VideoLink.Substring(index3 + 1);
                }
            }
            return name;
        }


        private string GetExpireTime(DateTime ExpireDate)
        {
            int TotalHours = 0;
            DateTime startTime = DateTime.Now;

            DateTime endTime = ExpireDate;// startTime.AddHours(1);

            TimeSpan span = endTime.Subtract(startTime);
            //Console.WriteLine("Time Difference (seconds): " + span.Seconds);
            //Console.WriteLine("Time Difference (minutes): " + span.Minutes);
            //Console.WriteLine("Time Difference (hours): " + span.Hours);
            //Console.WriteLine("Time Difference (days): " + span.Days);

            if (span.Days > 0)
            {
                TotalHours = (span.Days * 24) + span.Hours;
            }
            else
            {
                TotalHours = span.Hours;
            }
            //return TotalHours.ToString() + ":" + span.Minutes.ToString();
            return span.Days.ToString() + ":" + span.Hours.ToString();
        }

        public async Task AddNewComment(PostCommentVM model)
        {

            if (model.Id == Guid.Empty)
            {
                PostComment postComment = new PostComment
                {
                    Id = Guid.NewGuid(),
                    PostId = model.PostId,
                    Comment = model.PostComment,
                    CreatedBy = new Guid(model.CreatedBy),
                    CreatedOn = DateTime.UtcNow

                };

                _applicationDbContext.PostComment.Add(postComment);
                await _applicationDbContext.SaveChangesAsync();
            }

        }



        public async Task<bool> LikeDislikeFeed(LikeFeedVM model)
        {
            bool likeStatus = false;
            var likefeed = await _applicationDbContext.LikeFeed.Where(x => x.PostId == model.PostId && x.CreatedBy == model.CreatedBy).FirstOrDefaultAsync();//SingleOrDefaultAsync();
            if (likefeed != null)
            {
                if (likefeed.IsLike == false)
                {
                    likefeed.IsLike = true;
                    likeStatus = true;
                }
                else
                {
                    likefeed.IsLike = false;
                    likeStatus = false;
                }
                likefeed.UpdatedDate = DateTime.UtcNow;
                await _applicationDbContext.SaveChangesAsync();
            }
            else
            {
                LikeFeed postComment = new LikeFeed
                {
                    Id = Guid.NewGuid(),
                    PostId = model.PostId,
                    CreatedBy = model.CreatedBy,
                    UpdatedDate = DateTime.UtcNow,
                    IsLike = model.IsLike

                };

                _applicationDbContext.LikeFeed.Add(postComment);
                await _applicationDbContext.SaveChangesAsync();
                likeStatus = true;
            }
            return likeStatus;
        }

        public async Task<MainVM> GetAllFeedDetails(string userId)
        {
            //userId = "996d7e2f - 280b - 4d3d - 9a57 - dfde0df2a445";
            //Guid UserGuid=new Guid(userId);

            MainVM mainVM = new MainVM();

            mainVM.PostFeeds = await _applicationDbContext.PostFeed.Where(x => x.User.Id == userId && x.ExpireDate >= DateTime.UtcNow && x.IsActive == true).Select(f => new PostFeedVM
            {
                Id = f.Id,
                Title = f.Title,
                Description = f.Description,
                AudioLink = f.AudioLink,
                ImageLink = f.ImageLink,
                VideoLink = f.VideoLink,
                ExpireDate = f.ExpireDate,
                AudioFileName = f.AudioName,
                ImageFileName = f.ImageName,
                // VideoFileName = f.VideoName,
                VideoFileName = getVideoFileNmae(f.VideoLink),
                FeedExpireIN = GetExpireTime(f.ExpireDate),
                CreatedOn = f.CreatedOn,
                SaveFeed = f.SaveFeed
                //SaveFeedStatus = _applicationDbContext.SavedFeeds.Where(x => x.PostId == f.Id.ToString() && x.CreatedBy == userId && x.SaveFeed == true).Count(x => x.SaveFeed),

            }).OrderByDescending(m => m.CreatedOn).ToListAsync();

            mainVM.PostFeedsLatest = await _applicationDbContext.PostFeed.Where(x => x.User.Id == userId && x.ExpireDate >= DateTime.UtcNow && x.IsActive == true).Select(f => new PostFeedVM
            {
                Id = f.Id,
                Title = f.Title,
                Description = f.Description,
                AudioLink = f.AudioLink,
                ImageLink = f.ImageLink,
                VideoLink = f.VideoLink,
                ExpireDate = f.ExpireDate,
                AudioFileName = f.AudioName,
                ImageFileName = f.ImageName,
                // VideoFileName = f.VideoName,
                VideoFileName = getVideoFileNmae(f.VideoLink),
                FeedExpireIN = GetExpireTime(f.ExpireDate),
                CreatedOn = f.CreatedOn,
                SaveFeed = f.SaveFeed
                //SaveFeedStatus = _applicationDbContext.SavedFeeds.Where(x => x.PostId == f.Id.ToString() && x.CreatedBy == userId && x.SaveFeed == true).Count(x => x.SaveFeed),
            }).OrderByDescending(m => m.CreatedOn).ToListAsync();//.Take(5).

            mainVM.UserDetails = _applicationDbContext.AspNetUsers.Where(x => x.Id == userId).SingleOrDefault();

            return mainVM;

        }

        public async Task<MainVM> GetFollowedFeedDetails(string userId)
        {
            MainVM mainVM = new MainVM();
            try
            {
                mainVM.FollowFeedDetails = await _applicationDbContext.FollowFeeds.
                Join(_applicationDbContext.AspNetUsers, fuser => (fuser.CreatedBy != userId) ? (fuser.FollowingUserId).ToString() : ((fuser.FollowingUserId.ToString() == userId) ? (fuser.CreatedBy).ToString() : (fuser.FollowingUserId).ToString()), user => user.Id.ToString(),
                (fuser, user) => new { fuser, user })
                .Where(m => ((m.fuser.CreatedBy == userId) || (m.fuser.FollowingUserId.ToString() == userId)) && m.fuser.IsFollow == true)
                .Select(m => new PostFeedFollowedVM
                {
                    CreatedOn = m.fuser.FollowDate,
                    FollowedId = m.fuser.Id,
                    FollowingUser = (m.fuser.CreatedBy != userId) ? (m.fuser.FollowingUser.FirstName + ' ' + m.fuser.FollowingUser.LastName) : (m.user.FirstName + " " + m.user.LastName),
                    //FollowingUserPUrl = "/profile?ProfileName=" + m.user.FirstName + "-" + m.user.LastName,
                    FollowingUserPUrl = (m.fuser.CreatedBy != userId) ? "/profile?ProfileName=" + (m.fuser.FollowingUser.FirstName + '-' + m.fuser.FollowingUser.LastName) : "/profile?ProfileName=" + (m.user.FirstName + "-" + m.user.LastName),
                    //FollowingUserPLink = m.user.ProfileLink,
                    FollowingUserPLink = (m.fuser.CreatedBy != userId) ? (m.fuser.FollowingUser.ProfileLink) : (m.user.ProfileLink),
                    FollowDate = m.fuser.FollowDate,
                    CreatedBy = m.fuser.FollowingUser.FirstName + ' ' + m.fuser.FollowingUser.LastName,
                    FollowingUserId = (m.fuser.CreatedBy != userId) ? m.fuser.FollowingUserId.ToString() : m.fuser.CreatedBy

                }).OrderByDescending(m => m.CreatedOn).ToListAsync();

                //mainVM.FollowFeedDetails = mainVM.FollowFeedDetails.GroupBy(x => x.FollowingUserId).Select(x => x.First()).ToList();
                // mainVM.UserDetails = _applicationDbContext.AspNetUsers.Where(x => x.Id == userId).SingleOrDefault();

                //mainVM.FollowFeedDetails = await _applicationDbContext.PostFeed.
                //Join(_applicationDbContext.FollowFeeds, postfeed => (postfeed.User.Id).ToString(), feed => feed.FollowingUserId.ToString(),
                //(postfeed, feed) => new { postfeed, feed }).
                //Join(_applicationDbContext.AspNetUsers, fuser => fuser.feed.FollowingUserId.ToString(), user => user.Id.ToString(),
                //(fuser, user) => new { fuser, user })
                //.Where(m => m.fuser.feed.CreatedBy == userId && m.fuser.feed.IsFollow == true && m.fuser.postfeed.ExpireDate >= DateTime.UtcNow && m.fuser.postfeed.IsActive == true)
                //.Select(m => new PostFeedFollowedVM
                //{
                //    Id = m.fuser.postfeed.Id,
                //    Title = m.fuser.postfeed.Title,
                //    Description = m.fuser.postfeed.Description,
                //    AudioLink = m.fuser.postfeed.AudioLink,
                //    ImageLink = m.fuser.postfeed.ImageLink,
                //    VideoLink = m.fuser.postfeed.VideoLink,
                //    ExpireDate = m.fuser.postfeed.ExpireDate,
                //    AudioFileName = m.fuser.postfeed.AudioName,
                //    ImageFileName = m.fuser.postfeed.ImageName,
                //    VideoFileName = m.fuser.postfeed.VideoName,
                //    FeedExpireIN = GetExpireTime(m.fuser.postfeed.ExpireDate),
                //    CreatedOn = m.fuser.feed.FollowDate,
                //    SaveFeed = m.fuser.postfeed.SaveFeed,
                //    FollowedId = m.fuser.feed.Id,
                //    PostId = m.fuser.postfeed.Id,
                //    IsFollow = _applicationDbContext.FollowFeeds.Where(x => (x.FollowingUserId.ToString() == m.fuser.postfeed.User.Id && x.CreatedBy == userId) || (x.FollowingUserId.ToString() == userId && x.CreatedBy == m.fuser.postfeed.User.Id) && x.IsFollow == true).Count(x => x.IsFollow),
                //    FollowingUser = m.user.FirstName + " " + m.user.LastName,
                //    FollowingUserPUrl = "/profile?ProfileName=" + m.user.FirstName + "-" + m.user.LastName,
                //    FollowingUserPLink = m.user.ProfileLink,
                //    FollowDate = m.fuser.feed.FollowDate,
                //    CreatedBy = m.fuser.feed.CreatedBy,
                //    FollowingUserId = m.fuser.feed.FollowingUserId.ToString()

                //}).OrderByDescending(m => m.CreatedOn).ToListAsync();

                //mainVM.UserDetails = _applicationDbContext.AspNetUsers.Where(x => x.Id == userId).SingleOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return mainVM;

        }

        public async Task<MainVM> GetSaveFeedDetails(string userId)
        {
            try
            {
                MainVM mainVM = new MainVM();
                mainVM.SavedFeedDetails = await _applicationDbContext.SavedFeeds.
                Join(_applicationDbContext.PostFeed, sFeed => sFeed.PostId.ToString(), pFeed => pFeed.Id.ToString(),
                (sFeed, pFeed) => new { sFeed, pFeed }).
                Join(_applicationDbContext.AspNetUsers, psFeed => psFeed.pFeed.User.Id.ToString(), user => user.Id.ToString(),
                (psFeed, user) => new { psFeed, user })
                .Where(m => m.psFeed.sFeed.User.Id.ToString() == userId && m.psFeed.sFeed.SaveFeed == true && m.psFeed.pFeed.IsActive == true)
                .Select(m => new PostFeedSavedVM
                {
                    Id = m.psFeed.pFeed.Id,
                    Title = m.psFeed.pFeed.Title,
                    Description = m.psFeed.pFeed.Description,
                    AudioLink = m.psFeed.pFeed.AudioLink,
                    ImageLink = m.psFeed.pFeed.ImageLink,
                    ExpireDate = m.psFeed.pFeed.ExpireDate,
                    ImageFileName = m.psFeed.pFeed.ImageName,
                    FeedExpireIN = GetExpireTime(m.psFeed.pFeed.ExpireDate),
                    CreatedOn = m.psFeed.pFeed.CreatedOn,
                    SaveFeed = m.psFeed.pFeed.SaveFeed,
                    SavedFeedId = m.psFeed.pFeed.Id,
                    PostId = m.psFeed.pFeed.Id,
                    CreatedBy = m.psFeed.pFeed.User.Id,
                    SavedFeedUser = m.user.FirstName + " " + m.user.LastName,
                    SavedFeedUserPUrl = "/profile?ProfileName=" + m.user.FirstName + "-" + m.user.LastName,
                    SavedFeedUserPLink = m.user.ProfileLink,
                    SavedFeedDate = m.psFeed.sFeed.CreatedOn,
                    UapdatedDate = m.psFeed.sFeed.UpdatedOn
                }).OrderByDescending(m => m.UapdatedDate).ToListAsync();
                return mainVM;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //MainVM mainVM = new MainVM();
            //mainVM.SavedFeedDetails = await _applicationDbContext.PostFeed.
            //Join(_applicationDbContext.AspNetUsers, pFeed => pFeed.User.Id, user => user.Id,
            //(pFeed, user) => new { pFeed, user })
            //.Where(m => m.pFeed.User.Id == userId && m.pFeed.ExpireDate >= DateTime.UtcNow && m.pFeed.SaveFeed == true && m.pFeed.IsActive == true)
            //.Select(m => new PostFeedSavedVM
            //{
            //    Id = m.pFeed.Id,
            //    Title = m.pFeed.Title,
            //    Description = m.pFeed.Description,
            //    AudioLink = m.pFeed.AudioLink,
            //    ImageLink = m.pFeed.ImageLink,
            //    VideoLink = m.pFeed.VideoLink,
            //    ExpireDate = m.pFeed.ExpireDate,
            //    AudioFileName = m.pFeed.AudioName,
            //    ImageFileName = m.pFeed.ImageName,
            //    VideoFileName = m.pFeed.VideoName,
            //    FeedExpireIN = GetExpireTime(m.pFeed.ExpireDate),
            //    CreatedOn = m.pFeed.CreatedOn,
            //    SaveFeed = m.pFeed.SaveFeed,
            //    SavedFeedId = m.pFeed.Id,
            //    PostId = m.pFeed.Id,
            //    CreatedBy = m.pFeed.User.Id,
            //    IsFollow = _applicationDbContext.FollowFeeds.Where(x => (x.FollowingUserId.ToString() == m.pFeed.User.Id && x.CreatedBy == userId) || (x.FollowingUserId.ToString() == userId && x.CreatedBy == m.pFeed.User.Id) && x.IsFollow == true).Count(x => x.IsFollow),
            //    SavedFeedUser = m.user.FirstName + " " + m.user.LastName,
            //    SavedFeedUserPUrl = "/profile?ProfileName=" + m.user.FirstName + "-" + m.user.LastName,
            //    SavedFeedUserPLink = m.user.ProfileLink,
            //    SavedFeedDate = m.pFeed.CreatedOn
            //}).OrderByDescending(m => m.CreatedOn).ToListAsync();

            //mainVM.UserDetails = _applicationDbContext.AspNetUsers.Where(x => x.Id == userId).SingleOrDefault();
            //return mainVM;
        }

        public async Task<string> SaveFeed(PostFeedVM model, string userId)
        {
            var feed = new PostFeed();
            try
            {
                string status = "unsaved";

                var savefeed = await _applicationDbContext.SavedFeeds.Where(x => x.PostId == model.Id.ToString() && x.CreatedBy == userId).SingleOrDefaultAsync();
                if (savefeed != null)
                {
                    if (savefeed.SaveFeed == false)
                    {
                        savefeed.CreatedBy = userId;
                        savefeed.SaveFeed = true;
                        savefeed.UpdatedOn = DateTime.UtcNow;
                        status = "saved";
                    }
                    else
                    {
                        savefeed.CreatedBy = userId;
                        savefeed.SaveFeed = false;
                        savefeed.UpdatedOn = DateTime.UtcNow;
                        status = "unsaved";
                    }
                    _applicationDbContext.SavedFeeds.Update(savefeed);
                    await _applicationDbContext.SaveChangesAsync();
                }
                else
                {
                    SavedFeed savefd = new SavedFeed
                    {
                        Id = Guid.NewGuid(),
                        PostId = model.Id.ToString(),
                        SaveFeed = true,
                        CreatedBy = userId,
                        CreatedOn = DateTime.UtcNow,
                        UpdatedOn = DateTime.UtcNow
                    };
                    _applicationDbContext.SavedFeeds.Add(savefd);
                    await _applicationDbContext.SaveChangesAsync();
                    status = "saved";
                }
                return status;

                //if (model.Id == Guid.Empty)
                //{
                //    feed.Id = Guid.NewGuid();
                //}
                //else
                //{
                //    feed = await _applicationDbContext.PostFeed.Where(x => x.Id == model.Id).FirstOrDefaultAsync();
                //    feed.SaveFeed = model.SaveFeed;
                //    //_applicationDbContext.PostFeed.Add(feed);
                //    await _applicationDbContext.SaveChangesAsync();
                //}
                //model.Id = feed.Id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public async Task<UserTagsVM> getUserTags(string userId)
        {
            var userTags = new UserTags();
            try
            {
                userTags = await _applicationDbContext.UserTags
                    .Where(x => x.User.Id == userId).FirstOrDefaultAsync();
                var getUserTags = new UserTagsVM();
                if (userTags != null)
                {
                    getUserTags.Id = userTags.Id;
                    getUserTags.MainTag = userTags.MainTag;
                    getUserTags.Tag1 = userTags.Tag1;
                    getUserTags.Tag2 = userTags.Tag2;
                    getUserTags.Tag3 = userTags.Tag3;
                    getUserTags.UserBiography = userTags.UserBiography;
                    getUserTags.totalFeedUserLikes = _applicationDbContext.LikeFeed.Join(_applicationDbContext.PostFeed, lFeed => lFeed.PostId, pFeed => pFeed.Id,
                        (lFeed, pFeed) => new { lFeed, pFeed }).Where(x => x.lFeed.IsLike == true &&
                          x.lFeed.CreatedBy == userId).Count(x => x.lFeed.IsLike);
                    getUserTags.totalFeedViews = _applicationDbContext.PostFeed.Where(x => x.ExpireDate >= DateTime.UtcNow && x.IsActive == true).Count();
                }
                return getUserTags;
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public async Task<RegisterViewModel> editProfile(RegisterViewModel model, string userId)
        {
            var editUser = new ApplicationUser();
            editUser = await _applicationDbContext.AspNetUsers.Where(x => x.Id == userId).FirstOrDefaultAsync();
            try
            {
                if (model == null)
                {
                    model = new RegisterViewModel();
                    model.FirstName = (editUser.FirstName).Replace("-", " ");
                    //model.LastName = editUser.LastName;
                    model.Email = editUser.Email;
                    model.ProfileLink = editUser.ProfileLink;
                    model.Password = editUser.PasswordHash;
                    //model.User.UserName = editUser.UserName;
                    model.Country = editUser.Country;
                    model.Id = editUser.Id;
                    //_applicationDbContext.AspNetUsers.Add(editUser);
                    //await _applicationDbContext.SaveChangesAsync();

                }
                else
                {
                    string[] userName = model.FirstName.Split(' ');
                    string Fname = string.Empty;
                    int i = 0;
                    foreach (string users in userName)
                    {
                        if (users != "")
                        {
                            if (i == 0)
                                Fname = users;
                            else
                                Fname = Fname + "-" + users;
                            i++;
                        }
                    }

                    //var userName = model.FirstName.TrimEnd();
                    editUser.FirstName = Fname;
                    editUser.LastName = "";
                    //editUser.Email = model.Email;
                    var imageUrl = "";
                    if (model.ImageSrc != null)
                    {
                        if (model.ImageType == "image/svg+xml")
                        {
                            model.ImageType = "image/svg";
                        }
                        var imageStream = new MemoryStream(Convert.FromBase64String(model.ImageSrc));
                        imageUrl = await _amazonService.UploadMedia(imageStream, model.ImageType, model.Id.ToString());
                        var postImage = Convert.FromBase64String(model.ImageSrc);
                    }
                    else
                    {
                        imageUrl = model.ProfileLink;
                    }
                    editUser.ProfileLink = imageUrl;
                    editUser.UserName = Fname;
                    editUser.Country = model.Country;
                    await _applicationDbContext.SaveChangesAsync();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return model;
        }

        public async Task<RegisterViewModel> editAccountInfo(RegisterViewModel model, string userId)
        {
            var editUser = new ApplicationUser();
            editUser = await _applicationDbContext.AspNetUsers.Where(x => x.Id == userId).FirstOrDefaultAsync();
            try
            {
                if (editUser != null)
                {
                    editUser.Email = model.Email;

                    if (!model.Password.Contains("AQAAAAEAACc") && editUser.PasswordHash != model.Password)
                    {
                        var user = await _userManager.FindByEmailAsync(editUser.Email);
                        var removePassword = _userManager.RemovePasswordAsync(user);
                        if (removePassword.Result.ToString() == "Succeeded")
                        {
                            var AddPassword = _userManager.AddPasswordAsync(user, model.Password);
                            if (AddPassword.Result.ToString() == "Succeeded")
                            {
                                string Message = "Password reset successful!";
                            }
                        }
                    }
                    await _applicationDbContext.SaveChangesAsync();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return model;
        }


        public async Task<UserTagsVM> addUserTags(UserTagsVM model, string userId)
        {
            var userTags = new UserTags();

            try
            {
                userTags = await _applicationDbContext.UserTags.Where(x => x.User.Id == userId).FirstOrDefaultAsync();
                if (userTags == null)
                {
                    userTags = new UserTags();
                    var user = await _applicationDbContext.AspNetUsers.Where(x => x.Id == userId).FirstOrDefaultAsync();
                    userTags.Id = Guid.NewGuid();
                    userTags.MainTag = model.MainTag;
                    userTags.Tag1 = model.Tag1;
                    userTags.Tag2 = model.Tag2;
                    userTags.Tag3 = model.Tag3;
                    userTags.User = user;
                    _applicationDbContext.Add(userTags);
                    await _applicationDbContext.SaveChangesAsync();
                }
                else if (userTags.Id == model.Id)
                {
                    userTags.MainTag = model.MainTag;
                    userTags.Tag1 = model.Tag1;
                    userTags.Tag2 = model.Tag2;
                    userTags.Tag3 = model.Tag3;
                    await _applicationDbContext.SaveChangesAsync();
                }

                //return model;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }

        public async Task<UserTagsVM> editBiography(UserTagsVM model, string userId)
        {
            var editBiography = new UserTags();
            try
            {
                var user = await _applicationDbContext.AspNetUsers.Where(x => x.Id == userId).FirstOrDefaultAsync();
                editBiography = await _applicationDbContext.UserTags.Where(x => x.User == user).FirstOrDefaultAsync();
                if (editBiography != null)
                {
                    editBiography = await _applicationDbContext.UserTags.Where(x => x.Id == model.Id).FirstOrDefaultAsync();
                    editBiography.UserBiography = model.UserBiography;
                    await _applicationDbContext.SaveChangesAsync();
                }
                else
                {
                    editBiography = new UserTags();
                    editBiography.UserBiography = model.UserBiography;
                    editBiography.Id = Guid.NewGuid();
                    editBiography.User = user;
                    _applicationDbContext.UserTags.Add(editBiography);
                    await _applicationDbContext.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }


        public async Task<string> FollowFeed(FollowFeedVM model)
        {
            string status = "unfollow";

            var userFollow = await _applicationDbContext.FollowFeeds.Where(x => (x.FollowingUserId == model.FollowingUserId && x.CreatedBy == model.FollowingUser) || (x.FollowingUserId == new Guid(model.FollowingUser) && x.CreatedBy == model.FollowingUserId.ToString())).SingleOrDefaultAsync();
            if (userFollow != null)
            {
                if (userFollow.IsFollow == false)
                {
                    userFollow.CreatedBy = model.FollowingUser;
                    userFollow.IsFollow = true;
                    userFollow.FollowingUserId = model.FollowingUserId;
                    status = "followed";
                }
                else
                {
                    userFollow.CreatedBy = model.FollowingUser;
                    userFollow.IsFollow = false;
                    userFollow.FollowingUserId = model.FollowingUserId;
                    status = "unfollow";
                }
                _applicationDbContext.FollowFeeds.Update(userFollow);
                await _applicationDbContext.SaveChangesAsync();
            }
            else
            {
                if (model.Id == Guid.Empty)
                {
                    FollowFeed followFeed = new FollowFeed
                    {
                        Id = Guid.NewGuid(),
                        PostId = model.PostId,
                        IsFollow = true,
                        FollowingUserId = model.FollowingUserId,
                        CreatedBy = model.FollowingUser,
                        FollowDate = DateTime.UtcNow
                    };
                    _applicationDbContext.FollowFeeds.Add(followFeed);
                    await _applicationDbContext.SaveChangesAsync();
                    status = "followed";
                }
            }
            return status;
            //int count = 0;
            //try
            //{
            //    count = _applicationDbContext.FollowFeeds.Where(x => x.PostId == model.PostId && x.CreatedBy == model.FollowingUser).Count();
            //    if (count == 0)
            //    {
            //        if (model.Id == Guid.Empty)
            //        {
            //            //var followingUser = await _applicationDbContext.AspNetUsers.Where(x => x.Id == model.FollowingUser).FirstOrDefaultAsync();
            //            FollowFeed followFeed = new FollowFeed
            //            {
            //                Id = Guid.NewGuid(),
            //                PostId = model.PostId,
            //                CreatedBy = model.FollowingUser,
            //                FollowDate = DateTime.UtcNow
            //            };
            //            _applicationDbContext.FollowFeeds.Add(followFeed);
            //            await _applicationDbContext.SaveChangesAsync();
            //            return "";
            //        }
            //    }
            //    else
            //    {
            //        return "Followed";
            //    }
            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}
            //return "null";
        }

        public async Task<MusicCatalogVM> addMusicCatalog(MusicCatalogVM model, string userId)
        {
            var musicCatalog = new MusicCatalog();

            try
            {
                musicCatalog = await _applicationDbContext.MusicCatalog.Where(x => x.Id.ToString() == model.Id && x.CatalogId == model.CatalogId).FirstOrDefaultAsync();
                if (musicCatalog == null)
                {
                    musicCatalog = new MusicCatalog();
                    musicCatalog.Id = Guid.NewGuid();
                    musicCatalog.CatalogName = model.CatalogName;
                    musicCatalog.CreatedBy = userId;
                    if (model.CatalogId != null)
                    {
                        musicCatalog.CatalogId = model.CatalogId;
                    }
                    else
                    {
                        musicCatalog.CatalogId = Guid.NewGuid().ToString();
                    }

                    var imageUrl = "";
                    if (model.ImageSrc != null)
                    {
                        if (model.ImageType == "image/svg+xml")
                        {
                            model.ImageType = "image/svg";
                        }
                        var imageStream = new MemoryStream(Convert.FromBase64String(model.ImageSrc));
                        imageUrl = await _amazonService.UploadMedia(imageStream, model.ImageType, musicCatalog.Id.ToString());
                        var postImage = Convert.FromBase64String(model.ImageSrc);
                    }
                    else
                    {
                        imageUrl = model.ImageLink;
                    }
                    var audioUrl = "";
                    if (model.AudioSrc != null)
                    {
                        var audioStream = new MemoryStream(Convert.FromBase64String(model.AudioSrc));
                        audioUrl = await _amazonService.UploadMedia(audioStream, model.AudioType, musicCatalog.Id.ToString());
                        var postAudio = Convert.FromBase64String(model.AudioSrc);
                    }
                    else
                    {
                        audioUrl = model.SongLink;
                    }

                    string imageName = null;
                    string audioName = null;

                    if (model.ImageName != null)
                    {
                        imageName = $"{model.ImageName.Split('.')[0]}";

                    }
                    else
                    {
                        imageName = imageName = $"{model.ImageName.Split('.')[0]}";
                    }
                    if (model.SongName != null)
                    {
                        audioName = $"{model.SongName.Split('.')[0]}";

                    }
                    else
                    {
                        audioName = $"{model.SongName.Split('.')[0]}";
                    }

                    musicCatalog.ImageLink = imageUrl;
                    musicCatalog.ImageName = imageName;
                    musicCatalog.SongLink = audioUrl;
                    musicCatalog.SongName = audioName;
                    musicCatalog.CreatedOn = DateTime.UtcNow;
                    musicCatalog.MusicTag1 = model.MusicTag1;
                    musicCatalog.MusicTag2 = model.MusicTag2;
                    musicCatalog.MusicTag3 = model.MusicTag3;

                    _applicationDbContext.Add(musicCatalog);
                    await _applicationDbContext.SaveChangesAsync();
                }
                else if (musicCatalog.Id.ToString() == model.Id && musicCatalog.CatalogId == model.CatalogId)
                {
                    musicCatalog.CatalogName = model.CatalogName;
                    musicCatalog.CreatedBy = userId;
                    var imageUrl = "";
                    if (model.ImageSrc != null)
                    {
                        if (model.ImageType == "image/svg+xml")
                        {
                            model.ImageType = "image/svg";
                        }
                        var imageStream = new MemoryStream(Convert.FromBase64String(model.ImageSrc));
                        imageUrl = await _amazonService.UploadMedia(imageStream, model.ImageType, model.Id.ToString());
                        var postImage = Convert.FromBase64String(model.ImageSrc);
                    }
                    else
                    {
                        imageUrl = model.ImageLink;
                    }
                    var audioUrl = "";
                   
                    if (model.AudioSrc != null)
                    {
                        var audioStream = new MemoryStream(Convert.FromBase64String(model.AudioSrc));
                        audioUrl = await _amazonService.UploadMedia(audioStream, model.AudioType, musicCatalog.Id.ToString());
                        var postAudio = Convert.FromBase64String(model.AudioSrc);
                    }
                    else
                    {
                        audioUrl = model.SongLink;
                    }
                    musicCatalog.ImageLink = imageUrl;
                    musicCatalog.ImageName = model.ImageName;
                    musicCatalog.SongLink = audioUrl;
                    musicCatalog.SongName = model.SongName;
                    musicCatalog.UpdatedBy = userId;
                    musicCatalog.UpdatedOn = DateTime.UtcNow;
                    musicCatalog.MusicTag1 = model.MusicTag1;
                    musicCatalog.MusicTag2 = model.MusicTag2;
                    musicCatalog.MusicTag3 = model.MusicTag3;
                    await _applicationDbContext.SaveChangesAsync();
                }

                //return model;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }

        public async Task<MusicCatalogVM> editMusicCatalog(MusicCatalogVM model, string userId)
        {
            //var musicCatalog = new MusicCatalog();

            try
            {
                var catalogGroup = await _applicationDbContext.MusicCatalog.Where(x => x.CatalogId == model.CatalogId).ToListAsync();
                foreach (var catalog in catalogGroup)
                {
                    if (catalog.CatalogId.ToString() == model.CatalogId)
                    {
                        catalog.CatalogName = model.CatalogName;
                        catalog.UpdatedBy = userId;
                        catalog.CatalogId = model.CatalogId;
                        var imageUrl = "";
                        if (model.ImageSrc != null)
                        {
                            if (model.ImageType == "image/svg+xml")
                            {
                                model.ImageType = "image/svg";
                            }
                            var imageStream = new MemoryStream(Convert.FromBase64String(model.ImageSrc));
                            imageUrl = await _amazonService.UploadMedia(imageStream, model.ImageType, catalog.Id.ToString());
                            var postImage = Convert.FromBase64String(model.ImageSrc);
                        }
                        else
                        {
                            imageUrl = catalog.ImageLink;
                        }
                        var audioUrl = "";
                        if (model.AudioSrc != null)
                        {
                            var audioStream = new MemoryStream(Convert.FromBase64String(model.AudioSrc));
                            audioUrl = await _amazonService.UploadMedia(audioStream, model.AudioType, catalog.Id.ToString());
                            var postAudio = Convert.FromBase64String(model.AudioSrc);
                        }
                        else
                        {
                            audioUrl = catalog.SongLink;
                        }

                        string imageName = null;
                        string audioName = null;

                        if (model.ImageName == catalog.ImageName)
                        {
                            imageName = $"{model.ImageName.Split('.')[0]}";

                        }
                        else
                        {
                            imageName = $"{catalog.ImageName.Split('.')[0]}";
                        }
                        if (model.SongName == catalog.SongName)
                        {
                            audioName = $"{model.SongName.Split('.')[0]}";

                        }
                        else
                        {
                            audioName = $"{catalog.SongName.Split('.')[0]}";
                        }

                        catalog.ImageLink = imageUrl;
                        catalog.ImageName = imageName;
                        catalog.SongLink = audioUrl;
                        catalog.SongName = audioName;
                        catalog.UpdatedOn = DateTime.UtcNow;
                        await _applicationDbContext.SaveChangesAsync();
                    }

                }



                //return model;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }

        public async Task<IEnumerable<IGrouping<string, MusicCatalog>>> getAllMusicCatalogs(string userId)
        {
            var musicCatalog = new List<MusicCatalog>();

            try
            {
                musicCatalog = await _applicationDbContext.MusicCatalog.Where(x => x.CreatedBy == userId).OrderByDescending(x => x.CreatedOn).ToListAsync();
                var groupCatalog = musicCatalog.GroupBy(x => x.CatalogId);
                return groupCatalog;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public async Task<IEnumerable<PostFeedVM>> GetAllFeeds()
        {
            try
            {
                var feeds = await _applicationDbContext.PostFeed.Where(x => x.ExpireDate >= DateTime.UtcNow && x.IsActive == true).Select(f => new PostFeedVM
                {
                    Id = f.Id,
                    Title = f.Title,
                    Description = f.Description,
                    AudioLink = f.AudioLink,
                    ImageLink = f.ImageLink,
                    VideoLink = f.VideoLink,
                    ExpireDate = f.ExpireDate,
                    AudioFileName = f.AudioName,
                    ImageFileName = f.ImageName,
                    VideoFileName = f.VideoName,
                    FeedExpireIN = GetExpireTime(f.ExpireDate),
                    CreatedOn = f.CreatedOn,
                    SaveFeed = f.SaveFeed,
                    SaveFeedStatus = _applicationDbContext.SavedFeeds.Where(x => x.PostId == f.Id.ToString() && x.CreatedBy == f.User.Id && x.SaveFeed == true).Count(x => x.SaveFeed),

                }).OrderByDescending(m => m.CreatedOn).Take(5).ToListAsync(); /*.Take(5)*/

                return feeds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<PostFeedList> GetAllHomepageFeeds(string UserId)
        {
            try
            {
                PostFeedList postFeeds = new PostFeedList();
                var feeds = await _applicationDbContext.PostFeed.
                Join(_applicationDbContext.AspNetUsers, pFeed => pFeed.User.Id, fuser => fuser.Id,
                (pFeed, fuser) => new { pFeed, fuser })
                .Where(m => m.pFeed.ExpireDate >= DateTime.UtcNow && m.pFeed.IsActive == true && m.pFeed.IsTrendingFeed == true)
                .Select(f => new PostFeedVM
                {
                    Id = f.pFeed.Id,
                    Title = f.pFeed.Title,
                    Description = f.pFeed.Description,
                    AudioLink = f.pFeed.AudioLink,
                    ImageLink = f.pFeed.ImageLink,
                    VideoLink = f.pFeed.VideoLink,
                    ExpireDate = f.pFeed.ExpireDate,
                    AudioFileName = f.pFeed.AudioName,
                    ImageFileName = f.pFeed.ImageName,
                    VideoFileName = f.pFeed.VideoName,
                    FeedExpireIN = GetExpireTime(f.pFeed.ExpireDate),
                    CreatedOn = f.pFeed.CreatedOn,
                    SaveFeed = f.pFeed.SaveFeed,
                    SaveFeedStatus = _applicationDbContext.SavedFeeds.Where(x => x.PostId == f.pFeed.Id.ToString() && x.CreatedBy == UserId && x.SaveFeed == true).Count(x => x.SaveFeed),
                    UserId = f.fuser.Id,
                    IsFollow = _applicationDbContext.FollowFeeds.Where(x => (x.FollowingUserId.ToString() == f.pFeed.User.Id && x.CreatedBy == UserId) || (x.FollowingUserId.ToString() == UserId && x.CreatedBy == f.pFeed.User.Id) && x.IsFollow == true).Count(x => x.IsFollow),
                    IsLikes = _applicationDbContext.LikeFeed.Where(x => x.PostId == f.pFeed.Id && x.CreatedBy == UserId && x.IsLike == true).Count(x => x.IsLike),
                    LikeCount = _applicationDbContext.LikeFeed.Where(x => x.PostId == f.pFeed.Id && x.IsLike == true).Count(x => x.IsLike),
                    CreatedUserName = f.fuser.FirstName,
                    IsLike = false,
                    UserProfile = f.fuser.ProfileLink,
                    UserProfileUrl = "/profile?ProfileName=" + f.fuser.FirstName + "-" + f.fuser.LastName,
                }).OrderByDescending(m => m.CreatedOn).ToListAsync(); /*.Take(5)*/

                //foreach (PostFeedVM item in feeds)
                //{
                //    item.LikeCount = GetLikeCount(item.Id);
                //    if (UserId != null)
                //    {
                //        item.IsLike = IsUserLike(UserId, item.Id);
                //    }
                //}

                postFeeds.PostFeeds = feeds;
                int TotalFeeds = 0;
                if (UserId == null)
                {
                    TotalFeeds = _applicationDbContext.PostFeed.Where(x => x.IsActive == true).Count();
                }
                else
                {
                    TotalFeeds = _applicationDbContext.PostFeed.Where(x => x.IsActive == true && x.User.Id == UserId).Count();
                }

                postFeeds.TotalFeeds = TotalFeeds;



                return postFeeds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public async Task<IEnumerable<PostFeedVM>> GetAllHomepageFeeds(string UserId)
        //{
        //    try
        //    {
        //        var feeds = await _applicationDbContext.PostFeed.
        //        Join(_applicationDbContext.AspNetUsers, pFeed => pFeed.User.Id, fuser => fuser.Id,
        //        (pFeed, fuser) => new { pFeed, fuser })
        //        .Where(m => m.pFeed.ExpireDate >= DateTime.UtcNow && m.pFeed.IsActive == true && m.pFeed.IsTrendingFeed == true)
        //        .Select(f => new PostFeedVM
        //        {
        //            Id = f.pFeed.Id,
        //            Title = f.pFeed.Title,
        //            Description = f.pFeed.Description,
        //            AudioLink = f.pFeed.AudioLink,
        //            ImageLink = f.pFeed.ImageLink,
        //            VideoLink = f.pFeed.VideoLink,
        //            ExpireDate = f.pFeed.ExpireDate,
        //            AudioFileName = f.pFeed.AudioName,
        //            ImageFileName = f.pFeed.ImageName,
        //            VideoFileName = f.pFeed.VideoName,
        //            FeedExpireIN = GetExpireTime(f.pFeed.ExpireDate),
        //            CreatedOn = f.pFeed.CreatedOn,
        //            SaveFeed = f.pFeed.SaveFeed,
        //            SaveFeedStatus = _applicationDbContext.SavedFeeds.Where(x => x.PostId == f.pFeed.Id.ToString() && x.CreatedBy == UserId && x.SaveFeed == true).Count(x => x.SaveFeed),
        //            UserId = f.fuser.Id,
        //            IsFollow = _applicationDbContext.FollowFeeds.Where(x => (x.FollowingUserId.ToString() == f.pFeed.User.Id && x.CreatedBy == UserId) || (x.FollowingUserId.ToString() == UserId && x.CreatedBy == f.pFeed.User.Id) && x.IsFollow == true).Count(x => x.IsFollow),
        //            IsLikes = _applicationDbContext.LikeFeed.Where(x => x.PostId == f.pFeed.Id && x.CreatedBy == UserId && x.IsLike == true).Count(x => x.IsLike),
        //            LikeCount = _applicationDbContext.LikeFeed.Where(x => x.PostId == f.pFeed.Id && x.IsLike == true).Count(x => x.IsLike),
        //            CreatedUserName = f.fuser.FirstName,
        //            IsLike = false,
        //            UserProfile = f.fuser.ProfileLink,
        //            UserProfileUrl = "/profile?ProfileName=" + f.fuser.FirstName + "-" + f.fuser.LastName,
        //        }).OrderByDescending(m => m.CreatedOn).ToListAsync(); /*.Take(5)*/

        //        //foreach (PostFeedVM item in feeds)
        //        //{
        //        //    item.LikeCount = GetLikeCount(item.Id);
        //        //    if (UserId != null)
        //        //    {
        //        //        item.IsLike = IsUserLike(UserId, item.Id);
        //        //    }
        //        //}

        //        return feeds;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public bool IsUserLike(string UserId, Guid FeedId)
        {
            int cnt = 0;
            try
            {
                cnt = _applicationDbContext.LikeFeed.Where(x => x.PostId == FeedId && x.FollowingUser.Id == UserId && x.IsLike == true).Count();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return cnt > 0;
        }

        public int GetLikeCount(Guid FeedId)
        {
            int cnt = 0;
            try
            {
                cnt = _applicationDbContext.LikeFeed.Where(x => x.PostId == FeedId && x.IsLike == true).Count();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return cnt;
        }

        public async Task<IEnumerable<MusicCatalog>> getUserMusicCatalogs(MusicCatalogVM model, string userId)
        {
            var musicCatalog = new List<MusicCatalog>();

            try
            {
                musicCatalog = await _applicationDbContext.MusicCatalog.Where(x => x.CreatedBy == userId && x.CatalogId == model.CatalogId).OrderByDescending(x => x.CreatedOn).ToListAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return musicCatalog;
        }

        public async Task<MusicCatalog> deleteSongFromCatalog(MusicCatalogVM model, string userId)
        {
            var musicCatalog = new MusicCatalog();

            try
            {
                musicCatalog = await _applicationDbContext.MusicCatalog.Where(x => x.CreatedBy == userId && x.CatalogId == model.CatalogId && x.Id.ToString() == model.Id).FirstOrDefaultAsync();

                _applicationDbContext.Remove(musicCatalog);
                await _applicationDbContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return musicCatalog;
        }

        public async Task<UserGallery> addGallery(UserGallery model, string userId)
        {
            var userGallery = new UserGallery();
            try
            {
                userGallery = await _applicationDbContext.UserGallery.Where(x => x.Id == model.Id).FirstOrDefaultAsync();
                if (userGallery == null)
                {
                    userGallery = new UserGallery();
                    userGallery.Id = Guid.NewGuid();
                    userGallery.CreatedBy = userId;
                    userGallery.Status = true;
                    string filePath = model.fileUrl;
                    model.fileUrl = filePath.Substring(filePath.LastIndexOf(',') + 1);
                    var imageUrl = "";
                    if (model.fileUrl != null)
                    {
                        if (model.fileType == "image/svg+xml")
                        {
                            model.fileType = "image/svg";
                        }
                        var imageStream = new MemoryStream(Convert.FromBase64String(model.fileUrl));
                        imageUrl = await _amazonService.UploadMedia(imageStream, model.fileType, userGallery.Id.ToString());
                        var postImage = Convert.FromBase64String(model.fileUrl);
                    }

                    userGallery.fileUrl = imageUrl;
                    userGallery.fileName = model.fileName;
                    userGallery.fileType = model.fileType;
                    userGallery.CreatedOn = DateTime.UtcNow;
                    userGallery.UpdatedOn = DateTime.UtcNow;
                    userGallery.UpdatedBy = userId;
                    _applicationDbContext.Add(userGallery);
                    await _applicationDbContext.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }

        public async Task<UserGallery> editUserGallery(UserGalleryVM model, string userId)
        {
            try
            {
                var userGalleryresult = await _applicationDbContext.UserGallery.Where(x => x.Id == model.Id).ToListAsync();
                foreach (var gellery in userGalleryresult)
                {
                    if (gellery.Id == model.Id)
                    {
                        gellery.UpdatedBy = userId;
                        string filePath = model.FileUrl;
                        model.FileUrl = filePath.Substring(filePath.LastIndexOf(',') + 1);
                        var imageUrl = "";
                        if (model.FileUrl != null)
                        {
                            if (model.FileType == "image/svg+xml")
                            {
                                model.FileType = "image/svg";
                            }
                            var imageStream = new MemoryStream(Convert.FromBase64String(model.FileUrl));
                            imageUrl = await _amazonService.UploadMedia(imageStream, model.FileType, gellery.Id.ToString());
                            var postImage = Convert.FromBase64String(model.FileUrl);
                        }
                        gellery.fileUrl = imageUrl;
                        gellery.fileName = model.FileName;
                        gellery.fileType = model.FileType;
                        gellery.Status = true;
                        gellery.UpdatedOn = DateTime.UtcNow;
                        await _applicationDbContext.SaveChangesAsync();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return await _applicationDbContext.UserGallery.Where(x => x.Id == model.Id).SingleOrDefaultAsync();
        }

        public async Task<List<UserGalleryVM>> getUserGalleryDetails(string userId)
        {
            var userGallery = new List<UserGalleryVM>();
            try
            {
                //userGallery = await _applicationDbContext.UserGallery.Where(x => x.User.Id == userId && x.Status == true).OrderByDescending(x => x.CreatedOn).ToListAsync();
                userGallery = await _applicationDbContext.UserGallery.
                Join(_applicationDbContext.AspNetUsers, uGallery => uGallery.CreatedBy.ToString(), gUser => gUser.Id.ToString(),
                 (uGallery, gUser) => new { uGallery, gUser }).
                 Where(x => x.uGallery.User.Id == userId && x.uGallery.Status == true).Select(f => new UserGalleryVM
                 {
                     Id = f.uGallery.Id,
                     CreatedOn = f.uGallery.CreatedOn,
                     FileUrl = f.uGallery.fileUrl,
                     FileName = f.uGallery.fileName,
                     FileType = f.uGallery.fileType,
                     CreatedBy = f.uGallery.CreatedBy,
                     UpdatedBy = f.uGallery.UpdatedBy,
                     UpdatedOn = f.uGallery.UpdatedOn,
                     Status = f.uGallery.Status,
                     UserName = (f.gUser.FirstName + " " + f.gUser.LastName).Replace("-", " "),
                     profileUrl = "/profile?ProfileName=" + f.gUser.FirstName + "-" + f.gUser.LastName,
                 }).OrderByDescending(x => x.CreatedOn).ToListAsync();

                return userGallery;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task removeUserGallery(UserGalleryVM model)
        {
            try
            {
                var result = await _applicationDbContext.UserGallery.Where(x => x.Id == model.Id).SingleOrDefaultAsync();
                if (result != null)
                {
                    result.Id = model.Id;
                    result.Status = false;
                    result.UpdatedBy = model.UpdatedBy;
                    result.UpdatedOn = DateTime.UtcNow;
                    await _applicationDbContext.SaveChangesAsync();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public async Task<List<PostCommentVM>> GetComments(string feedId)
        {
            var comments = new List<PostCommentVM>();
            Guid postId = new Guid(feedId);
            try
            {
                comments = await _applicationDbContext.PostFeed.
                Join(_applicationDbContext.PostComment, pFeed => pFeed.Id, pComment => pComment.PostId,
                (pFeed, pComment) => new { pFeed, pComment }).
                Join(_applicationDbContext.AspNetUsers, fuser => fuser.pComment.CreatedBy.ToString(), user => user.Id,
                (fuser, user) => new { fuser, user })
                .Where(m => m.fuser.pComment.PostId == postId)
                .Select(m => new PostCommentVM
                {
                    Title = m.fuser.pFeed.Title,
                    PostComment = m.fuser.pComment.Comment,
                    PostId = m.fuser.pComment.PostId,
                    Id = m.fuser.pComment.Id,
                    CreatedBy = m.user.FirstName + " " + m.user.LastName,
                    CreatedOn = m.fuser.pComment.CreatedOn
                }).OrderByDescending(m => m.CreatedOn).ToListAsync();

                return comments;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<SocialMedia> socialMediaEdit(SocialMedia model)
        {
            var editMedia = new SocialMedia();
            try
            {
                var result = await _applicationDbContext.SocialMedia.Where(x => x.CreatedBy == model.CreatedBy && x.MediaName == model.MediaName).FirstOrDefaultAsync();
                if (result == null)
                {
                    editMedia.MediaName = model.MediaName;
                    editMedia.CreatedBy = model.CreatedBy;
                    editMedia.Id = Guid.NewGuid();
                    var user = await _userManager.FindByIdAsync(model.CreatedBy);
                    editMedia.User = user;
                    editMedia.MediaLink = model.MediaLink;
                    _applicationDbContext.SocialMedia.Add(editMedia);
                    await _applicationDbContext.SaveChangesAsync();
                }
                else
                {
                    result.CreatedBy = model.CreatedBy;
                    result.MediaName = model.MediaName;
                    result.MediaLink = model.MediaLink;
                    await _applicationDbContext.SaveChangesAsync();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }


        public async Task<List<SocialMedia>> getSocialMediaLinks(string userId)
        {
            var mediaResult = new List<SocialMedia>();
            try
            {

                if (string.IsNullOrEmpty(userId))
                    userId = "";
                mediaResult = await _applicationDbContext.SocialMedia.FromSql("EXECUTE dbo.Socialmedialinks {0}", userId).OrderBy(x => x.MediaName).ToListAsync();

                //mediaResult = await _applicationDbContext.SocialMedia.Where(x => x.CreatedBy == userId).OrderBy(x=>x.MediaName).ToListAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return mediaResult;
        }



        public async Task<SocialMedia> checkSocialMedia(SocialMedia model)
        {
            var mediaResult = new SocialMedia();
            try
            {
                mediaResult = await _applicationDbContext.SocialMedia.Where(x => x.CreatedBy == model.CreatedBy && x.MediaName == model.MediaName).SingleOrDefaultAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return mediaResult;
        }

        public async Task<string> editProfileImage(ApplicationUser model)
        {
            string status = "Failed";
            var userResult = await _applicationDbContext.AspNetUsers.Where(x => x.Id == model.Id).SingleOrDefaultAsync();
            if (userResult != null)
            {
                try
                {
                    string filePath = model.ProfileLink;
                    filePath = filePath.Substring(filePath.LastIndexOf(',') + 1);
                    var imageUrl = "";
                    if (filePath != null)
                    {
                        var imageStream = new MemoryStream(Convert.FromBase64String(filePath));
                        imageUrl = await _amazonService.UploadMedia(imageStream, "image/jpeg", model.Id);
                        var postImage = Convert.FromBase64String(filePath);
                    }
                    userResult.Id = model.Id;
                    userResult.ProfileLink = imageUrl;
                    await _applicationDbContext.SaveChangesAsync();
                    status = "Success";
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return status;
        }

        public async Task<MessageVM> SendMessage(MessageVM model, string userId)
        {
            try
            {

                var message = new Message();
                message.Id = Guid.NewGuid();
                message.MessageTo = model.MessageTo;
                message.MessageFrom = userId;
                message.PostId = model.PostId;
                message.Subject = model.Subject;
                message.MessageDate = DateTime.UtcNow;
                _applicationDbContext.Add(message);
                await _applicationDbContext.SaveChangesAsync();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }

        public async Task<List<MessageVM>> GetMessageByUserId(string userId)
        {
            var messages = new List<MessageVM>();
            var sqlquery = "";

            sqlquery = @"select m.MessageFrom,(select FirstName from AspNetUsers where id=m.MessageFrom)  as FromUserName,
                  m.MessageTo,(select FirstName from AspNetUsers where id = m.MessageTo)  as ToUserName,
                    m.MessageDate,M.subject, P.Title,P.Id as FeedId
                  from message m inner join PostFeed p on m.PostId = p.Id
                  where p.IsActive = 1 and (m.MessageTo = '" + userId + "' or m.MessageFrom = '" + userId + "') order by m.MessageDate desc";


            using (var command = _applicationDbContext.Database.GetDbConnection().CreateCommand())
            {
                command.CommandText = sqlquery;
                _applicationDbContext.Database.OpenConnection();
                using (var result = command.ExecuteReader())
                {
                    if (result.HasRows)
                    {
                        while (result.Read())
                        {
                            MessageVM message = new MessageVM();
                            if (result["MessageFrom"] != null)
                                message.MessageFrom = result["MessageFrom"].ToString();

                            if (result["FromUserName"] != null)
                                message.FromUserName = result["FromUserName"].ToString();

                            if (result["MessageTo"] != null)
                                message.MessageTo = result["MessageTo"].ToString();

                            if (result["ToUserName"] != null)
                                message.ToUserName = result["ToUserName"].ToString();

                            if (result["FeedId"] != null)
                                message.FeedId = result["FeedId"].ToString();

                            if (result["Title"] != null)
                                message.FeedTitle = result["Title"].ToString();

                            if (result["Subject"] != null)
                                message.Subject = result["Subject"].ToString();

                            if (result["MessageDate"] != null)
                                message.MessageDate = Convert.ToDateTime(result["MessageDate"]);


                            messages.Add(message);


                        }
                    }
                }
            }

            return messages;
        }

        public async Task<PostFeed> getSingleFeed(PostFeed model)
        {
            PostFeed feedResult = new PostFeed();
            try
            {
                feedResult = await _applicationDbContext.PostFeed.Where(x => x.Id == model.Id).SingleOrDefaultAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return feedResult;
        }

        public async Task<string> editSingleFeed(PostFeed model, string postId)
        {
            string status = "success";
            try
            {
                var feedsResult = await _applicationDbContext.PostFeed.Where(x => x.Id == model.Id && x.User.Id == postId).SingleOrDefaultAsync();
                if (feedsResult != null)
                {
                    var imageUrl = ""; var videoUrl = ""; var audioUrl = "";
                    string imageNames = null, audioName = null, videoName = null;
                    string imageType = "image/", audioType = "audio/", videoType = "video/";
                    feedsResult.Id = model.Id;
                    if (model.ImageLink != null && feedsResult.ImageLink != model.ImageLink)
                    {
                        if (model.ImageName != null)
                        {
                            imageNames = $"{model.ImageName.Split('.')[0]}";
                            imageType = imageType + $"{model.ImageName.Split('.')[1]}";
                            feedsResult.ImageName = imageNames;
                        }
                        var imageStream = new MemoryStream(Convert.FromBase64String(model.ImageLink));
                        imageUrl = await _amazonService.UploadMedia(imageStream, imageType, model.Id.ToString());
                        var postImage = Convert.FromBase64String(model.ImageLink);
                        feedsResult.ImageLink = imageUrl;
                    }

                    if (model.AudioLink != null && feedsResult.AudioLink != model.AudioLink)
                    {
                        if (model.AudioName != null)
                        {
                            audioName = $"{model.AudioName.Split('.')[0]}";
                            audioType = audioType + $"{model.AudioName.Split('.')[1]}";
                            feedsResult.AudioName = audioName;
                        }
                        var audioStream = new MemoryStream(Convert.FromBase64String(model.AudioLink));
                        audioUrl = await _amazonService.UploadMedia(audioStream, audioType, model.Id.ToString());
                        var postAudio = Convert.FromBase64String(model.AudioLink);
                        feedsResult.AudioLink = audioUrl;
                    }

                    if (model.VideoLink != null && feedsResult.VideoLink != model.VideoLink)
                    {
                        if (model.VideoName != null)
                        {
                            videoName = $"{model.VideoName.Split('.')[0]}";
                            videoType = videoType + $"{model.VideoName.Split('.')[1]}";
                            feedsResult.VideoName = videoName;
                        }
                        var videoStream = new MemoryStream(Convert.FromBase64String(model.VideoLink));
                        videoUrl = await _amazonService.UploadMedia(videoStream, videoType, model.Id.ToString());
                        var postVideo = Convert.FromBase64String(model.VideoLink);
                        feedsResult.VideoLink = videoUrl;
                    }

                    feedsResult.Title = model.Title;
                    feedsResult.Description = model.Description;
                    feedsResult.UpdatedBy = postId;
                    feedsResult.UpdatedOn = DateTime.UtcNow;
                    _applicationDbContext.Update(feedsResult);
                    await _applicationDbContext.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                status = "Failed";
                throw ex;
            }
            return status;
        }

        public async Task<string> addSubfeeds(SubFeeds model, string postId)
        {
            string status = "Feed submited successfully.";
            var subFeeds = new SubFeeds();
            try
            {
                string fileType = null;
                if (model.FileType != null)
                {
                    fileType = $"{model.FileType.Split('/')[0]}";
                }
                if (model.FileUrl != null && !model.FileUrl.Contains("https://"))
                {
                    if (model.FileName != null)
                    {
                        model.FileName = $"{model.FileName.Split('.')[0]}";
                    }
                    var imageStream = new MemoryStream(Convert.FromBase64String(model.FileUrl));
                    var imageUrl = await _amazonService.UploadMedia(imageStream, model.FileType, Guid.NewGuid().ToString());
                    var postImage = Convert.FromBase64String(model.FileUrl);
                    model.FileUrl = imageUrl;
                }

                var user = await _applicationDbContext.AspNetUsers.Where(x => x.Id == postId).SingleOrDefaultAsync();
                int subFeedresult = _applicationDbContext.SubFeeds.Where(x => x.MainFeedId == model.MainFeedId && x.User.Id == postId).Count();
                if (subFeedresult <= 10)
                {
                    subFeeds.Id = Guid.NewGuid();
                    subFeeds.CategoryId = model.CategoryId;//musiccatelog table id column or gallery table id or post feed id
                    subFeeds.MainFeedId = model.MainFeedId;
                    subFeeds.FileName = model.FileName;
                    subFeeds.FileType = fileType;
                    subFeeds.FileUrl = model.FileUrl;
                    subFeeds.User = user;
                    subFeeds.CreatedOn = DateTime.UtcNow;
                    subFeeds.IsActive = true;
                    _applicationDbContext.Add(subFeeds);
                    await _applicationDbContext.SaveChangesAsync();
                }
                else
                {
                    status = "you already submited to this feed!";
                }
            }
            catch (Exception ex)
            {
                status = "Failed! pls try again";
                throw ex;
            }
            return status;
        }

        public async Task<IEnumerable<UserDetailsVM>> getFeaturebrowseusers()
        {
            var Result = new List<UserDetailsVM>();
            try
            {
                return await _applicationDbContext.AspNetUsers
                       .Where(x => x.IsFeaturedUser == true)
                       .Select(r => new UserDetailsVM
                       {
                           FirstName = r.UserName,
                           ProfileLink = r.ProfileLink,
                           UserId = r.Id,
                           Country = r.Country,
                           LastName = r.FirstName + "-" + r.LastName,
                           Email = r.Email,
                           RegisteredOn = r.RegisteredOn,
                           IsFeaturedUser = r.IsFeaturedUser
                       }).OrderByDescending(r => r.RegisteredOn).ToListAsync();
            }
            catch (Exception ex)
            {
                return Result;
                throw ex;
            }

        }

        public async Task<MainVM> GetCustomiseFeeds(List<SelectedCategoriesVM> selectedCat, string userId)
        {
            List<string> tagList = new List<string>();
            foreach (SelectedCategoriesVM item in selectedCat)
            {
                tagList.Add(item.Text);
            }
            MainVM mainVM = new MainVM();
            mainVM.PostFeeds = await _applicationDbContext.PostFeed.
            Join(_applicationDbContext.AspNetUsers, pFeed => pFeed.User.Id, user => user.Id,
            (pFeed, user) => new { pFeed, user }).
            Join(_applicationDbContext.UserTags, pauser => pauser.user.Id, uTags => uTags.User.Id,
             (pauser, uTags) => new { pauser, uTags })
            .Where(m => m.pauser.pFeed.ExpireDate >= DateTime.UtcNow && m.pauser.pFeed.IsActive == true && /*m.uTags.User.Id == userId &&*/
             (tagList.Contains(m.uTags.MainTag) || tagList.Contains(m.uTags.Tag1) || tagList.Contains(m.uTags.Tag2) || tagList.Contains(m.uTags.Tag3)))
            .Select(m => new PostFeedVM
            {
                Id = m.pauser.pFeed.Id,
                Title = m.pauser.pFeed.Title,
                Description = m.pauser.pFeed.Description,
                AudioLink = m.pauser.pFeed.AudioLink,
                ImageLink = m.pauser.pFeed.ImageLink,
                VideoLink = m.pauser.pFeed.VideoLink,
                ExpireDate = m.pauser.pFeed.ExpireDate,
                AudioFileName = m.pauser.pFeed.AudioName,
                ImageFileName = m.pauser.pFeed.ImageName,
                VideoFileName = m.pauser.pFeed.VideoName,
                FeedExpireIN = GetExpireTime(m.pauser.pFeed.ExpireDate),
                CreatedOn = m.pauser.pFeed.CreatedOn,
                SaveFeed = m.pauser.pFeed.SaveFeed,
                SaveFeedStatus = _applicationDbContext.SavedFeeds.Where(x => x.PostId == m.pauser.pFeed.Id.ToString() && x.CreatedBy == userId && x.SaveFeed == true).Count(x => x.SaveFeed),
                UserId = userId,
                CreatedBy = m.pauser.pFeed.User.Id,
                IsFollow = _applicationDbContext.FollowFeeds.Where(x => (x.FollowingUserId.ToString() == m.pauser.pFeed.User.Id && x.CreatedBy == userId) || (x.FollowingUserId.ToString() == userId && x.CreatedBy == m.pauser.pFeed.User.Id) && x.IsFollow == true).Count(x => x.IsFollow),
                IsLikes = _applicationDbContext.LikeFeed.Where(x => x.PostId == m.pauser.pFeed.Id && x.CreatedBy == userId && x.IsLike == true).Count(x => x.IsLike),
                LikeCount = _applicationDbContext.LikeFeed.Where(x => x.PostId == m.pauser.pFeed.Id && x.IsLike == true).Count(x => x.IsLike),
                CreatedUserName = m.pauser.user.FirstName + " " + m.pauser.user.LastName,
                UserProfileUrl = "/profile?ProfileName=" + m.pauser.user.FirstName + "-" + m.pauser.user.LastName,
                isTrending = m.pauser.pFeed.IsTrendingFeed,
                UserProfile = m.pauser.user.ProfileLink
            }).OrderByDescending(m => m.CreatedOn).ToListAsync();


            //------------Adding custom search tag history-------------//
            int i = 0;
            string searchTags = string.Empty;
            foreach (string stag in tagList)
            {
                if (i == 0)
                    searchTags = stag;
                else
                    searchTags = searchTags + "," + stag;
                i++;
            }
            var sTag = new SearchSkillsTags();
            try
            {
                var searchTag = await _applicationDbContext.SearchSkillsTags.Where(x => x.CreatedBy == userId).SingleOrDefaultAsync();
                if (searchTag == null)
                {
                    sTag.Id = Guid.NewGuid();
                    sTag.CreatedBy = userId;
                    sTag.SearchOn = DateTime.UtcNow;
                    sTag.SearchTags = searchTags;
                    _applicationDbContext.SearchSkillsTags.Add(sTag);
                    await _applicationDbContext.SaveChangesAsync();
                }
                else
                {
                    searchTag.CreatedBy = userId;
                    searchTag.SearchOn = DateTime.UtcNow;
                    searchTag.SearchTags = searchTags;
                    _applicationDbContext.SearchSkillsTags.Update(searchTag);
                    await _applicationDbContext.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return mainVM;
        }

        //public async Task<MainVM> GetFeedsByTags(List<string> selectedTags)
        //{
        //    //List<string> tagList = new List<string>();
        //    MusicCatalogVM mainVM = new MusicCatalogVM();
        //    try
        //    {
        //        var postFeeds = await _applicationDbContext.MusicCatalog
        //                      .Where(x => selectedTags.Contains(x.MusicTag1))
        //                      .Select(m => new MusicCatalogVM
        //                      {
        //                          CatalogName = m.CatalogName
        //                      }).ToListAsync();
        //    }

        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //    return mainVM;
        //}

        public async Task<IList<MusicCatalogVM>> GetFeedsByTags(List<string> selectedTags)
        {
            var musicCatalogVM = new List<MusicCatalogVM>();

            try
            {
                musicCatalogVM = await _applicationDbContext.MusicCatalog
                              .Where(x => selectedTags.Contains(x.MusicTag1) ||
                                    selectedTags.Contains(x.MusicTag2) || selectedTags.Contains(x.MusicTag3))
                              .Select(m => new MusicCatalogVM
                              {
                                  CatalogName = m.CatalogName,
                                  CreatedBy = m.User.FirstName,
                                  MusicTag1 = m.MusicTag1,
                                  MusicTag2 = m.MusicTag2,
                                  MusicTag3 = m.MusicTag3,
                                  ImageLink = m.ImageLink,
                                  ImageName = m.ImageName,
                                  SongLink = m.SongLink,
                                  SongName = m.SongName,
                                  Id = m.Id.ToString(),
                              }).ToListAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return musicCatalogVM;
        }

        public async Task<MainVM> feedSeachByUser(string seachType, string searchString, string userId)
        {
            MainVM mainVM = new MainVM();
            if (searchString != null && searchString.Trim() != "")
            {
                if (seachType == "user")
                {
                    mainVM.PostFeeds = await _applicationDbContext.PostFeed.
                    Join(_applicationDbContext.AspNetUsers, pFeed => pFeed.User.Id, user => user.Id,
                    (pFeed, user) => new { pFeed, user })
                    .Where(m => (m.user.FirstName + " " + m.user.LastName).Contains(searchString) && m.pFeed.ExpireDate >= DateTime.UtcNow && m.pFeed.IsActive == true)
                    .Select(m => new PostFeedVM
                    {
                        Id = m.pFeed.Id,
                        Title = m.pFeed.Title,
                        Description = m.pFeed.Description,
                        AudioLink = m.pFeed.AudioLink,
                        ImageLink = m.pFeed.ImageLink,
                        VideoLink = m.pFeed.VideoLink,
                        ExpireDate = m.pFeed.ExpireDate,
                        AudioFileName = m.pFeed.AudioName,
                        ImageFileName = m.pFeed.ImageName,
                        VideoFileName = m.pFeed.VideoName,
                        FeedExpireIN = GetExpireTime(m.pFeed.ExpireDate),
                        CreatedOn = m.pFeed.CreatedOn,
                        SaveFeed = m.pFeed.SaveFeed,
                        SaveFeedStatus = _applicationDbContext.SavedFeeds.Where(x => x.PostId == m.pFeed.Id.ToString() && x.CreatedBy == userId && x.SaveFeed == true).Count(x => x.SaveFeed),
                        UserId = userId,
                        CreatedBy = m.pFeed.User.Id,
                        IsFollow = _applicationDbContext.FollowFeeds.Where(x => (x.FollowingUserId.ToString() == m.pFeed.User.Id && x.CreatedBy == userId) || (x.FollowingUserId.ToString() == userId && x.CreatedBy == m.pFeed.User.Id) && x.IsFollow == true).Count(x => x.IsFollow),
                        IsLikes = _applicationDbContext.LikeFeed.Where(x => x.PostId == m.pFeed.Id && x.CreatedBy == userId && x.IsLike == true).Count(x => x.IsLike),
                        LikeCount = _applicationDbContext.LikeFeed.Where(x => x.PostId == m.pFeed.Id && x.IsLike == true).Count(x => x.IsLike),
                        CreatedUserName = m.user.FirstName + " " + m.user.LastName,
                        UserProfileUrl = "/profile?ProfileName=" + m.user.FirstName + "-" + m.user.LastName,
                        isTrending = m.pFeed.IsTrendingFeed,
                        UserProfile = m.user.ProfileLink
                    }).OrderByDescending(m => m.CreatedOn).ToListAsync();
                }

                else if (seachType == "title")
                {
                    mainVM.PostFeeds = await _applicationDbContext.PostFeed.
                   Join(_applicationDbContext.AspNetUsers, pFeed => pFeed.User.Id, user => user.Id,
                   (pFeed, user) => new { pFeed, user })
                   .Where(m => m.pFeed.Title.Contains(searchString) && m.pFeed.ExpireDate >= DateTime.UtcNow && m.pFeed.IsActive == true)
                   .Select(m => new PostFeedVM
                   {
                       Id = m.pFeed.Id,
                       Title = m.pFeed.Title,
                       Description = m.pFeed.Description,
                       AudioLink = m.pFeed.AudioLink,
                       ImageLink = m.pFeed.ImageLink,
                       VideoLink = m.pFeed.VideoLink,
                       ExpireDate = m.pFeed.ExpireDate,
                       AudioFileName = m.pFeed.AudioName,
                       ImageFileName = m.pFeed.ImageName,
                       VideoFileName = m.pFeed.VideoName,
                       FeedExpireIN = GetExpireTime(m.pFeed.ExpireDate),
                       CreatedOn = m.pFeed.CreatedOn,
                       SaveFeed = m.pFeed.SaveFeed,
                       SaveFeedStatus = _applicationDbContext.SavedFeeds.Where(x => x.PostId == m.pFeed.Id.ToString() && x.CreatedBy == userId && x.SaveFeed == true).Count(x => x.SaveFeed),
                       UserId = userId,
                       CreatedBy = m.pFeed.User.Id,
                       IsFollow = _applicationDbContext.FollowFeeds.Where(x => (x.FollowingUserId.ToString() == m.pFeed.User.Id && x.CreatedBy == userId) || (x.FollowingUserId.ToString() == userId && x.CreatedBy == m.pFeed.User.Id) && x.IsFollow == true).Count(x => x.IsFollow),
                       IsLikes = _applicationDbContext.LikeFeed.Where(x => x.PostId == m.pFeed.Id && x.CreatedBy == userId && x.IsLike == true).Count(x => x.IsLike),
                       LikeCount = _applicationDbContext.LikeFeed.Where(x => x.PostId == m.pFeed.Id && x.IsLike == true).Count(x => x.IsLike),
                       CreatedUserName = m.user.FirstName + " " + m.user.LastName,
                       UserProfileUrl = "/profile?ProfileName=" + m.user.FirstName + "-" + m.user.LastName,
                       isTrending = m.pFeed.IsTrendingFeed,
                       UserProfile = m.user.ProfileLink
                   }).OrderByDescending(m => m.CreatedOn).ToListAsync();
                }
                else if (seachType == "description")
                {
                    mainVM.PostFeeds = await _applicationDbContext.PostFeed.
                   Join(_applicationDbContext.AspNetUsers, pFeed => pFeed.User.Id, user => user.Id,
                   (pFeed, user) => new { pFeed, user })
                   .Where(m => m.pFeed.Description.Contains(searchString) && m.pFeed.ExpireDate >= DateTime.UtcNow && m.pFeed.IsActive == true)
                   .Select(m => new PostFeedVM
                   {
                       Id = m.pFeed.Id,
                       Title = m.pFeed.Title,
                       Description = m.pFeed.Description,
                       AudioLink = m.pFeed.AudioLink,
                       ImageLink = m.pFeed.ImageLink,
                       VideoLink = m.pFeed.VideoLink,
                       ExpireDate = m.pFeed.ExpireDate,
                       AudioFileName = m.pFeed.AudioName,
                       ImageFileName = m.pFeed.ImageName,
                       VideoFileName = m.pFeed.VideoName,
                       FeedExpireIN = GetExpireTime(m.pFeed.ExpireDate),
                       CreatedOn = m.pFeed.CreatedOn,
                       SaveFeed = m.pFeed.SaveFeed,
                       SaveFeedStatus = _applicationDbContext.SavedFeeds.Where(x => x.PostId == m.pFeed.Id.ToString() && x.CreatedBy == userId && x.SaveFeed == true).Count(x => x.SaveFeed),
                       UserId = userId,
                       CreatedBy = m.pFeed.User.Id,
                       IsFollow = _applicationDbContext.FollowFeeds.Where(x => (x.FollowingUserId.ToString() == m.pFeed.User.Id && x.CreatedBy == userId) || (x.FollowingUserId.ToString() == userId && x.CreatedBy == m.pFeed.User.Id) && x.IsFollow == true).Count(x => x.IsFollow),
                       IsLikes = _applicationDbContext.LikeFeed.Where(x => x.PostId == m.pFeed.Id && x.CreatedBy == userId && x.IsLike == true).Count(x => x.IsLike),
                       LikeCount = _applicationDbContext.LikeFeed.Where(x => x.PostId == m.pFeed.Id && x.IsLike == true).Count(x => x.IsLike),
                       CreatedUserName = m.user.FirstName + " " + m.user.LastName,
                       UserProfileUrl = "/profile?ProfileName=" + m.user.FirstName + "-" + m.user.LastName,
                       isTrending = m.pFeed.IsTrendingFeed,
                       UserProfile = m.user.ProfileLink
                   }).OrderByDescending(m => m.CreatedOn).ToListAsync();
                }
            }
            else
            {
                mainVM.PostFeeds = await _applicationDbContext.PostFeed.
               Join(_applicationDbContext.AspNetUsers, pFeed => pFeed.User.Id, user => user.Id,
               (pFeed, user) => new { pFeed, user })
               .Where(m => m.pFeed.ExpireDate >= DateTime.UtcNow && m.pFeed.IsActive == true)
               .Select(m => new PostFeedVM
               {
                   Id = m.pFeed.Id,
                   Title = m.pFeed.Title,
                   Description = m.pFeed.Description,
                   AudioLink = m.pFeed.AudioLink,
                   ImageLink = m.pFeed.ImageLink,
                   VideoLink = m.pFeed.VideoLink,
                   ExpireDate = m.pFeed.ExpireDate,
                   AudioFileName = m.pFeed.AudioName,
                   ImageFileName = m.pFeed.ImageName,
                   VideoFileName = m.pFeed.VideoName,
                   FeedExpireIN = GetExpireTime(m.pFeed.ExpireDate),
                   CreatedOn = m.pFeed.CreatedOn,
                   SaveFeed = m.pFeed.SaveFeed,
                   SaveFeedStatus = _applicationDbContext.SavedFeeds.Where(x => x.PostId == m.pFeed.Id.ToString() && x.CreatedBy == userId && x.SaveFeed == true).Count(x => x.SaveFeed),
                   UserId = userId,
                   CreatedBy = m.pFeed.User.Id,
                   IsFollow = _applicationDbContext.FollowFeeds.Where(x => (x.FollowingUserId.ToString() == m.pFeed.User.Id && x.CreatedBy == userId) || (x.FollowingUserId.ToString() == userId && x.CreatedBy == m.pFeed.User.Id) && x.IsFollow == true).Count(x => x.IsFollow),
                   IsLikes = _applicationDbContext.LikeFeed.Where(x => x.PostId == m.pFeed.Id && x.CreatedBy == userId && x.IsLike == true).Count(x => x.IsLike),
                   LikeCount = _applicationDbContext.LikeFeed.Where(x => x.PostId == m.pFeed.Id && x.IsLike == true).Count(x => x.IsLike),
                   CreatedUserName = m.user.FirstName + " " + m.user.LastName,
                   UserProfileUrl = "/profile?ProfileName=" + m.user.FirstName + "-" + m.user.LastName,
                   isTrending = m.pFeed.IsTrendingFeed,
                   UserProfile = m.user.ProfileLink
               }).OrderByDescending(m => m.CreatedOn).ToListAsync();
            }

            return mainVM;
        }

        public async Task<string> getUserSearchTags(string userId)
        {
            var musicCatalog = string.Empty;
            try
            {
                musicCatalog = await _applicationDbContext.SearchSkillsTags.Where(x => x.CreatedBy == userId).Select(x => x.SearchTags).SingleOrDefaultAsync();

                return musicCatalog;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        public async Task<IEnumerable<MusicCatalog>> getSubmitedfeedCatelogs(string userId)
        {
            var musicCatalog = new List<MusicCatalog>();

            try
            {
                musicCatalog = await _applicationDbContext.MusicCatalog.Where(x => x.CreatedBy == userId).OrderByDescending(x => x.CreatedOn).OrderBy(x => x.CatalogId).ToListAsync();
                return musicCatalog;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        public async Task<IEnumerable<SubmitedFeedHistoryVM>> getUserCatelogsById(string subuser, string feedId, string Type, string userId, string subHstry)
        {
            var subfeed = new List<SubmitedFeedHistoryVM>();
            try
            {
                if (subHstry == "FromHstry")
                {
                    subfeed = await _applicationDbContext.SubFeeds.Select(x => new SubmitedFeedHistoryVM
                    {
                        Id = x.Id,
                        CategoryId = x.CategoryId,
                        MainFeedId = x.MainFeedId,
                        fileName = x.FileName,
                        fileType = x.FileType,
                        fileUrl = x.FileUrl,
                        Firstname = x.User.FirstName + " " + x.User.LastName,
                        CreatedBy = x.User.Id,
                        CreatedOn = x.CreatedOn,
                        ProfileUrl = "/profile?ProfileName=" + x.User.FirstName + '-' + x.User.LastName,
                    }).Where(x => x.CreatedBy == userId.ToString() && x.fileType == Type && x.MainFeedId == feedId).OrderByDescending(x => x.CreatedOn).ToListAsync();
                }
                else if (subHstry == "ToHstry")
                {
                    subfeed = await _applicationDbContext.SubFeeds.
                   Join(_applicationDbContext.PostFeed, sFeed => sFeed.MainFeedId, pFeed => pFeed.Id.ToString(),
                   (sFeed, pFeed) => new { sFeed, pFeed }).
                   Where(m => m.sFeed.MainFeedId == feedId && m.sFeed.FileType == Type && m.pFeed.User.Id == userId)
                    .Select(x => new SubmitedFeedHistoryVM
                    {
                        Id = x.sFeed.Id,
                        CategoryId = x.sFeed.CategoryId,
                        MainFeedId = x.sFeed.MainFeedId,
                        fileName = x.sFeed.FileName,
                        fileType = x.sFeed.FileType,
                        fileUrl = x.sFeed.FileUrl,
                        Firstname = x.sFeed.User.FirstName + " " + x.sFeed.User.LastName,
                        CreatedBy = x.sFeed.User.Id,
                        CreatedOn = x.sFeed.CreatedOn,
                        ProfileUrl = "/profile?ProfileName=" + x.sFeed.User.FirstName + '-' + x.sFeed.User.LastName,
                    }).OrderByDescending(x => x.CreatedOn).ToListAsync();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return subfeed;
        }



        public async Task<IEnumerable<SubmitedFeedHistoryVM>> getSubmitedFeedHistory(string userId, string feedHistrytype)
        {
            var subFeedHistry = new List<SubmitedFeedHistoryVM>();
            try
            {
                if (feedHistrytype == "subFromHistry")
                {
                    subFeedHistry = await _applicationDbContext.SubFeeds.
                    Join(_applicationDbContext.PostFeed, sFeed => sFeed.MainFeedId, pFeed => pFeed.Id.ToString(),
                    (sFeed, pFeed) => new { sFeed, pFeed }).
                    //Join(_applicationDbContext.MusicCatalog, spFeed => spFeed.sFeed.CategoryId.ToString(), mCatelog => mCatelog.Id.ToString(),
                    //(spFeed, mCatelog) => new { spFeed, mCatelog }).
                    Join(_applicationDbContext.AspNetUsers, spmFeed => spmFeed.sFeed.User.Id.ToString(), user => user.Id.ToString(),
                    (spmFeed, user) => new { spmFeed, user }).
                    Join(_applicationDbContext.AspNetUsers, spmnFeed => spmnFeed.spmFeed.pFeed.User.Id.ToString(), FromUser => FromUser.Id.ToString(),
                   (spmnFeed, FromUser) => new { spmnFeed, FromUser })
                    .Where(m => m.spmnFeed.spmFeed.sFeed.User.Id == userId)
                    .Select(m => new SubmitedFeedHistoryVM
                    {
                        Id = m.spmnFeed.spmFeed.pFeed.Id,
                        Title = m.spmnFeed.spmFeed.pFeed.Title,
                        Description = m.spmnFeed.spmFeed.pFeed.Description,
                        ImageLink = m.spmnFeed.spmFeed.pFeed.ImageLink,
                        AudioLink = m.spmnFeed.spmFeed.pFeed.AudioLink,
                        VideoLink = m.spmnFeed.spmFeed.pFeed.VideoLink,
                        ExpireDate = m.spmnFeed.spmFeed.pFeed.ExpireDate,
                        ImageFileName = m.spmnFeed.spmFeed.pFeed.ImageName,
                        AudioFileName = m.spmnFeed.spmFeed.pFeed.AudioName,
                        VideoFileName = m.spmnFeed.spmFeed.pFeed.VideoName,

                        UserId = m.spmnFeed.user.Id,
                        Firstname = m.spmnFeed.user.FirstName + " " + m.spmnFeed.user.LastName,
                        LastName = m.spmnFeed.user.LastName,

                        ProfilLink = m.FromUser.ProfileLink,
                        ProfileUrl = "/profile?ProfileName=" + m.FromUser.FirstName + '-' + m.FromUser.LastName,
                        toUserId = m.FromUser.Id,
                        toUserName = m.FromUser.FirstName + " " + m.FromUser.LastName,

                        CategoryId = m.spmnFeed.spmFeed.sFeed.CategoryId,
                        MainFeedId = m.spmnFeed.spmFeed.sFeed.MainFeedId,
                        CreatedOn = m.spmnFeed.spmFeed.sFeed.CreatedOn,

                        CatalogId = m.spmnFeed.spmFeed.sFeed.CategoryId.ToString(),
                        CatalogName = "",
                        fileName = m.spmnFeed.spmFeed.sFeed.FileName,
                        fileUrl = m.spmnFeed.spmFeed.sFeed.FileUrl,
                        fileType = m.spmnFeed.spmFeed.sFeed.FileType,
                    }).OrderByDescending(m => m.CreatedOn).ToListAsync();
                }
                else if (feedHistrytype == "subToHistry")
                {
                    subFeedHistry = await _applicationDbContext.SubFeeds.
                   Join(_applicationDbContext.PostFeed, sFeed => sFeed.MainFeedId, pFeed => pFeed.Id.ToString(),
                   (sFeed, pFeed) => new { sFeed, pFeed }).
                   //Join(_applicationDbContext.MusicCatalog, spFeed => spFeed.sFeed.CategoryId.ToString(), mCatelog => mCatelog.Id.ToString(),
                   //(spFeed, mCatelog) => new { spFeed, mCatelog }).
                   Join(_applicationDbContext.AspNetUsers, spmFeed => spmFeed.pFeed.User.Id.ToString(), user => user.Id.ToString(),
                   (spmFeed, user) => new { spmFeed, user })
                    .Join(_applicationDbContext.AspNetUsers, spmnFeed => spmnFeed.spmFeed.sFeed.User.Id.ToString(), toUser => toUser.Id.ToString(),
                   (spmnFeed, toUser) => new { spmnFeed, toUser })
                   .Where(m => m.spmnFeed.spmFeed.pFeed.User.Id == userId)
                   .Select(m => new SubmitedFeedHistoryVM
                   {
                       Id = m.spmnFeed.spmFeed.pFeed.Id,
                       Title = m.spmnFeed.spmFeed.pFeed.Title,
                       Description = m.spmnFeed.spmFeed.pFeed.Description,
                       ImageLink = m.spmnFeed.spmFeed.pFeed.ImageLink,
                       AudioLink = m.spmnFeed.spmFeed.pFeed.AudioLink,
                       VideoLink = m.spmnFeed.spmFeed.pFeed.VideoLink,
                       ExpireDate = m.spmnFeed.spmFeed.pFeed.ExpireDate,
                       ImageFileName = m.spmnFeed.spmFeed.pFeed.ImageName,
                       AudioFileName = m.spmnFeed.spmFeed.pFeed.AudioName,
                       VideoFileName = m.spmnFeed.spmFeed.pFeed.VideoName,

                       UserId = m.spmnFeed.user.Id,
                       Firstname = m.spmnFeed.user.FirstName + " " + m.spmnFeed.user.LastName,
                       LastName = m.spmnFeed.user.LastName,
                       ProfilLink = m.spmnFeed.user.ProfileLink,
                       ProfileUrl = "/profile?ProfileName=" + m.spmnFeed.user.FirstName + '-' + m.spmnFeed.user.LastName,

                       toUserId = m.toUser.Id,
                       toUserName = m.toUser.FirstName + " " + m.toUser.LastName,

                       CategoryId = m.spmnFeed.spmFeed.sFeed.CategoryId,
                       MainFeedId = m.spmnFeed.spmFeed.sFeed.MainFeedId,
                       CreatedOn = m.spmnFeed.spmFeed.sFeed.CreatedOn,

                       CatalogId = m.spmnFeed.spmFeed.sFeed.CategoryId.ToString(),
                       CatalogName = "",
                       fileName = m.spmnFeed.spmFeed.sFeed.FileName,
                       fileUrl = m.spmnFeed.spmFeed.sFeed.FileUrl,
                       fileType = m.spmnFeed.spmFeed.sFeed.FileType,
                   }).OrderByDescending(m => m.CreatedOn).ToListAsync();
                }

                subFeedHistry = subFeedHistry.GroupBy(x => x.Id).Select(x => x.First()).ToList();
                return subFeedHistry;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public async Task<IEnumerable<PostFeedVM>> GetFeedsForTreding()
        {
            try
            {
                var feeds = await _applicationDbContext.PostFeed.
                Join(_applicationDbContext.AspNetUsers, pFeed => pFeed.User.Id, fuser => fuser.Id,
                (pFeed, fuser) => new { pFeed, fuser })
                .Where(m => m.pFeed.ExpireDate >= DateTime.UtcNow && m.pFeed.IsActive == true)
                .Select(f => new PostFeedVM
                {
                    Id = f.pFeed.Id,
                    Title = f.pFeed.Title,
                    Description = f.pFeed.Description,
                    AudioLink = f.pFeed.AudioLink,
                    ImageLink = f.pFeed.ImageLink,
                    VideoLink = f.pFeed.VideoLink,
                    ExpireDate = f.pFeed.ExpireDate,
                    AudioFileName = f.pFeed.AudioName,
                    ImageFileName = f.pFeed.ImageName,
                    VideoFileName = f.pFeed.VideoName,
                    FeedExpireIN = GetExpireTime(f.pFeed.ExpireDate),
                    CreatedOn = f.pFeed.CreatedOn,
                    SaveFeed = f.pFeed.SaveFeed,
                    UserId = f.fuser.Id,
                    CreatedUserName = f.fuser.FirstName,
                    IsLike = false,
                    UserProfile = f.fuser.ProfileLink,
                    isTrending = f.pFeed.IsTrendingFeed

                    //LikeCount=GetLikeCount(f.pFeed.Id)


                }).OrderByDescending(m => m.CreatedOn).ToListAsync(); /*.Take(5)*/

                return feeds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<string> AddSongsToPlayer(MusicCatalogVM model, string userId)
        {
            string status = "Song Added to play List.";
            var songPList = new SongPlayList();

            try
            {
                var user = await _applicationDbContext.AspNetUsers.Where(x => x.Id == userId).SingleOrDefaultAsync();
                var addSongs = await _applicationDbContext.SongPlayList.Where(x => x.CatelogId == model.CatalogId && x.CreatedBy == userId && x.SongUrl == model.SongLink).SingleOrDefaultAsync();
                if (addSongs == null)
                {
                    songPList.Id = Guid.NewGuid();
                    songPList.CatelogId = model.CatalogId;
                    songPList.User = user;
                    songPList.SongName = model.SongName;
                    songPList.SongUrl = model.SongLink;
                    songPList.CreatedOn = DateTime.UtcNow;
                    songPList.UpdatedBy = userId;
                    songPList.UpdatedOn = DateTime.UtcNow;
                    _applicationDbContext.Add(songPList);
                    await _applicationDbContext.SaveChangesAsync();
                }
                else
                {
                    status = "Song already added To play List";
                }
            }
            catch (Exception ex)
            {
                status = "Failed! pls try again";
                throw ex;
            }
            return status;
        }

        public async Task<IEnumerable<SongsPlayListVM>> getSongsPlayList(string userId)
        {
            var playList = new List<SongsPlayListVM>();
            try
            {
                playList = await _applicationDbContext.SongPlayList.
                Join(_applicationDbContext.AspNetUsers, sPList => sPList.User.Id, user => user.Id,
                (sPList, user) => new { sPList, user })
                .Where(m => m.sPList.User.Id == userId)
                .Select(r => new SongsPlayListVM
                {
                    Id = r.sPList.Id,
                    Title = r.sPList.SongName,
                    Url = r.sPList.SongUrl,
                    Artist = r.user.FirstName + ' ' + r.user.LastName
                }).OrderByDescending(s => s.CreatedOn).ToListAsync();

                return playList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //-----------Messages-----------
        public List<UserMessagesVM> GetMessageUsers(string userId)
        {
            try
            {
                List<UserMessagesVM> messages = new List<UserMessagesVM>();
                var sqlquery = "";
                sqlquery = String.Format(@"select* from (select a.Id, REPLACE(a.FirstName, '-', ' ') as UserName, a.ProfileLink,
                (select top 1 m.MessageDate from Message m where m.MessageFrom = a.Id and m.MessageTo='{0}' and m.isDelete=0 order by MessageDate desc) as MessageDate,
                (select top 1 m.MessageFrom from Message m where m.MessageFrom = a.Id and m.MessageTo='{0}' and m.isDelete=0 order by MessageDate desc) as MessageFrom,
                (select top 1 m.MessageTo from Message m where m.MessageFrom = a.Id and m.MessageTo='{0}' and m.isDelete=0 order by MessageDate desc) as MessageTo,
                (select top 1 m.PostId from Message m where m.MessageFrom = a.Id and m.MessageTo='{0}' and m.isDelete=0 order by MessageDate desc) as PostId,
                (select top 1 m.Subject from Message m where m.MessageFrom = a.Id and m.MessageTo='{0}' and m.isDelete=0 order by MessageDate desc) as Subject,
                (select Count(m.Subject) from Message m where m.MessageFrom = a.Id and m.MessageTo='{0}' and m.isSeenStatus=0 and m.isDelete=0 ) as cnt,
                (select Count(m.Subject) from Message m where m.MessageTo='{0}' and m.isSeenStatus=0 and m.isDelete=0 ) as totalcnt
                from dbo.aspnetUsers a where a.EmailConfirmed = 1 and a.ProfileLink is not null ) as t where Id not in ('{0}') order by MessageDate desc", userId);
                using (var command = _applicationDbContext.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = sqlquery;
                    _applicationDbContext.Database.OpenConnection();
                    using (var result = command.ExecuteReader())
                    {
                        if (result.HasRows)
                        {
                            while (result.Read())
                            {
                                UserMessagesVM message = new UserMessagesVM();
                                message.Id = result["Id"].ToString();
                                message.MessageFrom = result["UserName"].ToString();
                                message.UserImage = result["ProfileLink"].ToString();

                                if (result["MessageDate"] != null && result["MessageDate"].ToString() != "")
                                    message.MessageDate = Convert.ToDateTime(result["MessageDate"]);
                                message.FromUserId = result["MessageFrom"].ToString();
                                message.ToUserId = result["MessageTo"].ToString();
                                message.Subject = result["Subject"].ToString();
                                message.PostId = result["PostId"].ToString();
                                message.rmsgCount = Convert.ToInt32(result["cnt"]);
                                message.totalCount = Convert.ToInt32(result["totalcnt"]);
                                messages.Add(message);
                            }
                        }
                    }
                }
                return messages;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public async Task<List<UserMessagesVM>> getMessageUsersbyId(string FromUser, string ToUser)
        {
            List<UserMessagesVM> messages = new List<UserMessagesVM>();
            try
            {
                var msgStatus = await _applicationDbContext.Message.Where(m => m.isSeenStatus == false && m.MessageTo == ToUser && m.MessageFrom == FromUser).ToListAsync();
                foreach (var ismsg in msgStatus)
                {
                    ismsg.isSeenStatus = true;
                    _applicationDbContext.Message.Update(ismsg);
                    await _applicationDbContext.SaveChangesAsync();
                }

                messages = await _applicationDbContext.Message
                .Where(m => m.isDelete == false && ((m.MessageTo == ToUser && m.MessageFrom == FromUser) || (m.MessageFrom == ToUser && m.MessageTo == FromUser)))
               .Select(f => new UserMessagesVM
               {
                   Id = f.Id.ToString(),
                   FromUserId = f.MessageFrom,
                   ToUserId = f.MessageTo,
                   MessageDate = f.MessageDate,
                   PostId = f.PostId.ToString(),
                   Subject = f.Subject,
                   MessageFrom = f.FormUser.FirstName.Replace("-", " "),
                   MessageTo = _applicationDbContext.AspNetUsers.Where(x => x.Id == f.MessageTo).Select(x => x.FirstName).SingleOrDefault(),
                   UserImage = f.FormUser.ProfileLink,
                   ToUserImage = _applicationDbContext.AspNetUsers.Where(x => x.Id == f.MessageTo).Select(x => x.ProfileLink).SingleOrDefault()
               }).OrderBy(m => m.MessageDate).ToListAsync();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return messages;
        }
        public async Task<string> deleteUserMessage(string usermsg, string UserId)
        {
            string status = "failed";
            try
            {
                var msgDelete = await _applicationDbContext.Message.Where(m => m.Id == new Guid(usermsg) && m.MessageFrom == UserId).SingleOrDefaultAsync();
                if (msgDelete != null)
                {
                    msgDelete.MessageFrom = UserId;
                    msgDelete.Id = new Guid(usermsg);
                    msgDelete.isDelete = true;
                    _applicationDbContext.Message.Update(msgDelete);
                    await _applicationDbContext.SaveChangesAsync();
                    status = "success";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return status;
        }

        public async Task<string> deleteUserFeed(string feedId, string UserId)
        {
            string status = "failed";
            try
            {
                var feedDel = await _applicationDbContext.PostFeed.Where(m => m.Id == new Guid(feedId) && m.User.Id == UserId).SingleOrDefaultAsync();
                if (feedDel != null)
                {
                    feedDel.Id = new Guid(feedId);
                    feedDel.IsActive = false;
                    _applicationDbContext.PostFeed.Update(feedDel);
                    await _applicationDbContext.SaveChangesAsync();
                    status = "success";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return status;
        }

        public async Task<bool> getFollowingStatus(string profileUser, string UserId)
        {
            bool status = false;
            try
            {
                status = await _applicationDbContext.FollowFeeds.Where(x => (x.FollowingUserId.ToString() == profileUser && x.CreatedBy == UserId) || (x.FollowingUserId.ToString() == UserId && x.CreatedBy == profileUser)).Select(x => x.IsFollow).SingleOrDefaultAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return status;
        }

        public async Task<PostFeedVM> GetFeedById(string FeedId, string userId)
        {
            try
            {
                Guid gfeedId = new Guid(FeedId);
                var feeds = await _applicationDbContext.PostFeed.Where(x => x.Id == gfeedId).Select(f => new PostFeedVM
                {
                    Id = f.Id,
                    Title = f.Title,
                    Description = f.Description,
                    AudioLink = f.AudioLink,
                    ImageLink = f.ImageLink,
                    VideoLink = f.VideoLink,
                    ExpireDate = f.ExpireDate,
                    AudioFileName = f.AudioName,
                    ImageFileName = f.ImageName,
                    VideoFileName = f.VideoName,
                    FeedExpireIN = GetExpireTime(f.ExpireDate),
                    CreatedOn = f.CreatedOn,
                    SaveFeed = f.SaveFeed,
                    SaveFeedStatus = _applicationDbContext.SavedFeeds.Where(x => x.PostId == f.Id.ToString() && x.CreatedBy == f.User.Id && x.SaveFeed == true).Count(x => x.SaveFeed),
                    UserId = userId,
                    CreatedBy = f.User.Id,
                    IsFollow = _applicationDbContext.FollowFeeds.Where(x => x.IsFollow == true && ((x.FollowingUserId == new Guid(f.User.Id) && x.CreatedBy == userId) || (x.CreatedBy == f.User.Id.ToString() && x.FollowingUserId == new Guid(userId)))).Count(),
                    IsLikes = _applicationDbContext.LikeFeed.Where(x => x.PostId == f.Id && x.CreatedBy == userId && x.IsLike == true).Count(x => x.IsLike),
                    LikeCount = _applicationDbContext.LikeFeed.Where(x => x.PostId == f.Id && x.IsLike == true).Count(x => x.IsLike),
                    CreatedUserName = f.User.FirstName + " " + f.User.LastName,
                    UserProfileUrl = "/profile?ProfileName=" + f.User.FirstName + "-" + f.User.LastName,
                    UserProfile = f.User.ProfileLink

                }).SingleOrDefaultAsync();

                return feeds;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }



    }
}
