﻿using PMPMusic.Models;
using PMPMusic.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PMPMusic.Repositories
{
    public interface IAdminRepository
    {
        Task<List<UserDetailsVM>> GetUserDetails();

        Task UpdateFeaturedUser(List<SelectedUserDetailsVM> Users);

        Task UpdateTrendingFeed(List<PostFeedVM> Users);

    }
}

