﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using PMPMusic.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace PMPMusic.Repositories
{
    public class CommonRepository : ICommonRepository
    {
        private static bool _databaseChecked;
        private IHostingEnvironment _envronment;
        private ApplicationDBContext _applicationDbContext { get; set; }

        public CommonRepository(ApplicationDBContext applicationDbContext, IHostingEnvironment envronment)
        {
            _applicationDbContext = applicationDbContext;
            _envronment = envronment;
        }

        public string ReadHtmlTemplate(string templateName, string toUserName, string fromUserName, string callbackUrl)
        {
            //var path = $"{_envronment.WebRootPath}emailTemplates/ { templateName} .html";
            var path = _envronment.WebRootPath + "/emailTemplates/" + templateName + ".html";
            string html = File.ReadAllText(path);
            html = html.Replace("#toUserName#", toUserName);
            html = html.Replace("#fromUserName#", fromUserName);
            html = html.Replace("#callbackUrl#", callbackUrl);
            return html;
        } 

        public void EnsureDatabaseCreated(ApplicationDBContext context)
        {
            if (!_databaseChecked)
            {
                _databaseChecked = true;
                //context.Database.AsRelational().ApplyMigrations();
                context.Database.MigrateAsync();
            }
        }
    }
}
