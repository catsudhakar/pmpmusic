﻿using PMPMusic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PMPMusic.Repositories
{
    public interface ICommonRepository
    {
        string ReadHtmlTemplate(string templateName, string toUserName, string fromUserName, string callbackUrl);
        void EnsureDatabaseCreated(ApplicationDBContext context);

       

    }
}
