﻿using PMPMusic.Models;
using PMPMusic.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PMPMusic.Repositories
{
    public interface IPostFeedRepository
    {
        Task<Array> AddPostFeed(PostFeedVM postFeed, string userId);
        Task<IEnumerable<PostFeedVM>> GetAllFeeds(string userId);
        Task<MainVM> GetAllFeedDetails(string userId);
        Task<IEnumerable<PostFeedVM>> getTotalActiveFeeds(string userId);

        Task AddNewComment(PostCommentVM model);

        Task<bool> LikeDislikeFeed(LikeFeedVM model);

        Task<RegisterViewModel> editProfile(RegisterViewModel model, string userId);

        Task<UserTagsVM> editBiography(UserTagsVM model, string userId);

        Task<string> SaveFeed(PostFeedVM model,string userId);
        Task<MainVM> GetSaveFeedDetails(string userId);

        Task<UserTagsVM> getUserTags(string userId);
        Task<UserTagsVM> addUserTags(UserTagsVM model, string userId);

        Task<IEnumerable<IGrouping<string, MusicCatalog>>> getAllMusicCatalogs(string userId);
        Task<IEnumerable<MusicCatalog>> getUserMusicCatalogs(MusicCatalogVM model, string userId);
        Task<MusicCatalog> deleteSongFromCatalog(MusicCatalogVM model, string userId);
        Task<MusicCatalogVM> addMusicCatalog(MusicCatalogVM model, string userId);
        Task<MusicCatalogVM> editMusicCatalog(MusicCatalogVM model, string userId);

        Task<string> FollowFeed(FollowFeedVM model);
        Task<MainVM> GetFollowedFeedDetails(string userId);

        Task<IEnumerable<PostFeedVM>> GetAllFeeds();
        //Task<IEnumerable<PostFeedVM>> GetAllHomepageFeeds(string userId);
        Task<PostFeedList> GetAllHomepageFeeds(string userId);

        Task<IEnumerable<PostFeedVM>> GetFeedsForTreding();

        Task<UserGallery> addGallery(UserGallery model, string userId);
        Task<UserGallery> editUserGallery(UserGalleryVM model, string userId);
        Task<List<UserGalleryVM>> getUserGalleryDetails(string userId);
        Task removeUserGallery(UserGalleryVM model);

        Task<List<PostCommentVM>> GetComments(string feedId);

        Task<SocialMedia> socialMediaEdit(SocialMedia model);
        Task<List<SocialMedia>> getSocialMediaLinks(string userId);
        Task<SocialMedia> checkSocialMedia(SocialMedia model);

        Task<string> editProfileImage(ApplicationUser model);
        Task<IEnumerable<UserDetailsVM>> getFeaturebrowseusers();

        Task<MessageVM> SendMessage(MessageVM model, string userId);
        Task<List<MessageVM>> GetMessageByUserId(string userId);

        Task<PostFeed> getSingleFeed(PostFeed model);
        Task<string> editSingleFeed(PostFeed model,string postId);
        Task<string> addSubfeeds(SubFeeds model, string postId);

        Task<MainVM> GetCustomiseFeeds(List<SelectedCategoriesVM> selectedCat, string userId);

      
        Task<IList<MusicCatalogVM>> GetFeedsByTags(List<string> selectedTags);

        Task<IEnumerable<MusicCatalog>> getSubmitedfeedCatelogs(string userId);
        Task<IEnumerable<SubmitedFeedHistoryVM>> getSubmitedFeedHistory(string userId, string feedHistrytype);

        Task<string> AddSongsToPlayer(MusicCatalogVM model, string userId);
        Task<IEnumerable<SongsPlayListVM>> getSongsPlayList(string userId);

        Task<string> getUserSearchTags(string userId);

        //------Messages -------
        List<UserMessagesVM> GetMessageUsers(string userId);
        Task<List<UserMessagesVM>> getMessageUsersbyId(string FromUser, string ToUser);
        Task<string> deleteUserMessage(string usermsg, string UserId);

        Task<IEnumerable<PostFeedVM>> getVideos(string userId);
        Task<string> deleteUserFeed(string feedId, string UserId);
        Task<IEnumerable<SubmitedFeedHistoryVM>> getUserCatelogsById(string subuser,string feedId,string Type, string userId,string subHstry);
        Task<bool> getFollowingStatus(string profileUser, string UserId);

        Task<PostFeedVM> GetFeedById(string FeedId,string userId);

        Task<RegisterViewModel> editAccountInfo(RegisterViewModel model, string userId);

        Task<MainVM> feedSeachByUser(string seachType,string searchString, string userId);
        
    }
}
