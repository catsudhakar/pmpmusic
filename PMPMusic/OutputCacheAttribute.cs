﻿using System;

namespace PMPMusic.Controllers
{
    internal class OutputCacheAttribute : Attribute
    {
        public int Duration { get; set; }
        public bool NoStore { get; set; }
        public string VaryByParam { get; set; }
    }
}