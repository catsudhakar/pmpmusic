﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using PMPMusic.Models;
using Microsoft.EntityFrameworkCore;
using PMPMusic.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection.Extensions;
using PMPMusic.Repositories;


namespace PMPMusic
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            // Add framework services.
            services.AddMvc();

            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromMinutes(50);
                options.CookieName = ".MyApplication";
            });

            services.AddEntityFrameworkSqlServer()
                .AddDbContext<ApplicationDBContext>(options =>
                    options.UseSqlServer(Configuration["Data:DefaultConnection:ConnectionString"]));

            //services.AddIdentity<ApplicationUser, ApplicationRole>()
            //    .AddEntityFrameworkStores<ApplicationDBContext>()
            //    .AddDefaultTokenProviders();

            services.AddIdentity<ApplicationUser, ApplicationRole>(options =>
            {
                options.Password.RequireDigit = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequiredLength = 6;
            })
               .AddEntityFrameworkStores<ApplicationDBContext>()
               .AddDefaultTokenProviders();

            services.AddScoped<IEmailSender, AuthMessageSender>();
            services.AddScoped<ISmsSender, AuthMessageSender>();
            services.AddScoped<IPostFeedRepository, PostFeedRepository>();
            services.AddTransient<HTTPSClass, HTTPSClass>();
            services.AddScoped<ICommonRepository, CommonRepository>();
            services.AddScoped<IAdminRepository, AdminRepository>();

            services.AddScoped<ICloudService, AmazonAWS>();

            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();


            services.Configure<AmazonAWSOptions>(options =>
            {
                options.AccessKeyId = Configuration["Amazon:AccessKeyId"];
                options.SecretAccessKey = Configuration["Amazon:SecretAccessKey"];
            });

            services.Configure<FacebookKeys>(options =>
            {
                options.AppId = Configuration["Facebook:AppId"];
                options.AppSecret = Configuration["Facebook:AppSecret"];
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {

            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseIdentity();
            app.UseStaticFiles();

            app.UseSession();

            //app.UseFacebookAuthentication(new FacebookOptions
            //{
            //    AuthenticationScheme = "Facebook",
            //    AppId = Configuration["Facebook:AppId"],
            //    AppSecret = Configuration["Facebook:AppSecret"],
            //    SignInScheme = "ApplicationCookie"

            //});

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
