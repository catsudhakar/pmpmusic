﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace PMPMusic.Services
{
    public interface ICloudService
    {
        Task<string> UploadMedia(Stream stream, string mediaType,string key);
    }
}
