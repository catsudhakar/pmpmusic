﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace PMPMusic.Services
{
    public class AmazonAWS : ICloudService
    {

        private readonly AmazonAWSOptions Options;

        public AmazonAWS(IOptions<AmazonAWSOptions> options)
        {
            Options = options.Value;
        }
        async Task<string> ICloudService.UploadMedia(Stream stream, string mediaType, string key)
        {
            try
            {
                string dest = $"pmpmusicfeedpro/{mediaType.Split('/')[0]}";
                var formattedKey = $"{key}.{mediaType.Split('/')[1]}";
                var client = new AmazonS3Client(Options.AccessKeyId, Options.SecretAccessKey, RegionEndpoint.APSoutheast1);
                var url = client.GetPreSignedURL(new GetPreSignedUrlRequest
                {
                    BucketName = dest,
                    Key = formattedKey,
                    Expires = DateTime.Now.AddYears(1)
                });
                TransferUtility utility = new TransferUtility(client);
                await utility.UploadAsync(stream, dest, formattedKey);
                return url;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

    public class AmazonAWSOptions
    {
        public string AccessKeyId { get; set; }
        public string SecretAccessKey { get; set; }
    }
}
