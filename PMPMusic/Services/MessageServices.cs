﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
//using System.Net.Mail;
using MailKit;
using Microsoft.Extensions.Options;
using MimeKit;
using MailKit.Net.Smtp;
using Microsoft.AspNetCore.Http;

namespace PMPMusic.Services
{
    public class MessageServices
    {
    }

    public class HTTPSClass
    {
        private HttpContext _currentContext;
        public HTTPSClass(HttpContext currentContext)
        {
            _currentContext = currentContext;

        }
    }

    public class AuthMessageSender : IEmailSender, ISmsSender
    {
        //public AuthMessageSenderOptions Options { get; }

        //public AuthMessageSender(IOptions<AuthMessageSenderOptions> options)
        //{
        //    Options = options.Value;
        //}
        public Task SendEmailAsync(string email, string subject, string message)
        {
            // Plug in your email service here to send an email.
            //try
            //{
            //    using (SmtpClient client = new SmtpClient(Options.Host, Options.Port))
            //    {

            //        client.EnableSsl = true;
            //        client.Credentials = new System.Net.NetworkCredential(Options.User, Options.Password);
            //        using (MailMessage mailMessage = new MailMessage())
            //        {
            //            mailMessage.From = new MailAddress(Options.User);
            //            mailMessage.To.Add(email);
            //            mailMessage.IsBodyHtml = true;
            //            mailMessage.Subject = subject;
            //            mailMessage.Body = message;
            //            client.Send(mailMessage);
            //        }
            //    }
            //}
            //catch (System.Exception ex)
            //{

            //    throw ex;
            //}
            try
            {
                //From Address 
                string FromAddress = "pmpmusicfeedpro@gmail.com";
                string FromAdressTitle = "PMP Music Feed Pro";
                string fromPassword = "Friday$1";
                //To Address 
                string ToAddress = email;
                string ToAdressTitle = email;
                string Subject = subject;
                string BodyContent = message;

                //Smtp Server 
                string SmtpServer = "smtp.gmail.com";
                //Smtp Port Number 
                int SmtpPortNumber = 587;

                var mimeMessage = new MimeMessage();
                mimeMessage.From.Add(new MailboxAddress(FromAdressTitle, FromAddress));
                mimeMessage.To.Add(new MailboxAddress(ToAdressTitle, ToAddress));
                mimeMessage.Subject = Subject;
                mimeMessage.Body = new TextPart("html")
                {
                    Text = BodyContent

                };

                using (var client = new SmtpClient())
                {

                    client.Connect(SmtpServer, SmtpPortNumber, false);
                    // Note: only needed if the SMTP server requires authentication 
                    // Error 5.5.1 Authentication  
                    client.Authenticate(FromAddress, fromPassword);
                    client.Send(mimeMessage);
                    Console.WriteLine("The mail has been sent successfully !!");
                    Console.ReadLine();
                    client.Disconnect(true);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }


            return Task.FromResult(0);


        }

        public Task SendSmsAsync(string number, string message)
        {
            // Plug in your SMS service here to send a text message.
            return Task.FromResult(0);
        }
    }
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message);
    }

    public interface ISmsSender
    {
        Task SendSmsAsync(string number, string message);
    }
}



