﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMPMusic.ViewModel
{
    public class SongsPlayListVM
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public string Url { get; set; }

        public string Artist { get; set; }

        public DateTime CreatedOn { get; set; }
    }
}
