﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PMPMusic.ViewModel
{
    public class UserGalleryVM
    {
        public Guid Id { get; set; }
        public string FileUrl { get; set; }
        public string FileName { get; set; }

        public string FileType { get; set; }

        public virtual string CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        public string UpdatedBy { get; set; }

        public DateTime UpdatedOn { get; set; }

        public bool Status { get; set; }

        public string UserName { get; set; }
        public string profileUrl { get; set; }
    }
}
