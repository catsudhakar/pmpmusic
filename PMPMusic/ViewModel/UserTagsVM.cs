﻿using PMPMusic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PMPMusic.ViewModel
{
    public class UserTagsVM
    {
        public Guid Id { get; set; }

        public ApplicationUser User { get; set; }

        public string MainTag { get; set; }

        public string Tag1 { get; set; }

        public string Tag2 { get; set; }

        public string Tag3 { get; set; }

        public string UserBiography { get; set; }

        public int totalFeedUserLikes { get; set; }

        public int totalFeedViews { get; set; }

    }
}
