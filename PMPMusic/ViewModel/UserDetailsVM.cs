﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PMPMusic.ViewModel
{
    public class UserDetailsVM
    {

        public string Country { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ProfileLink { get; set; }
        public DateTime RegisteredOn { get; set; }
        public string Email { get; set; }
        public string UserId { get; set; }
        public bool IsFeaturedUser{ get; set; }
        public bool IsVerifyedUser { get; set; }
    }

    public class FeedItem
    {
        public string Link { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public DateTime PublishDate { get; set; }
        public Guid PostId { get; set; }
    }

    public class SelectedUserDetailsVM
    {
        public string UserId { get; set; }
        public bool IsFeaturedUser { get; set; }
        public string FeedId { get; set; }
        public bool IsVerifyedUser { get; set; }
    }
}
