﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PMPMusic.ViewModel
{
    public class MusicCatalogVM
    {
        public string Id { get; set; }

        public string CatalogId { get; set; }

        public string CatalogName { get; set; }

        public string CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        public string UpdatedBy { get; set; }

        public string UpdatedOn { get; set; }

        public string ImageName { get; set; }

        public string ImageLink { get; set; }

        public string SongName { get; set; }

        public string SongLink { get; set; }

        public string ImageSrc { get; set; }

        public string ImageType { get; set; }

        public string AudioSrc { get; set; }

        public string AudioType { get; set; }

        public string MusicTag1 { get; set; }

        public string MusicTag2 { get; set; }

        public string MusicTag3 { get; set; }
    }
}
