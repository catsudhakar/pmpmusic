﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMPMusic.ViewModel
{
    public class SubmitedFeedHistoryVM
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImageLink { get; set; }
        public string AudioLink { get; set; }
        public string VideoLink { get; set; }
        public string CreatedBy { get; set; }
        public DateTime ExpireDate { get; set; }
        public string ImageFileName { get; set; }
        public string AudioFileName { get; set; }
        public string VideoFileName { get; set; }

        public string UserId { get; set; }
        public string Firstname { get; set; }
        public string LastName { get; set; }
        public string ProfileUrl { get; set; }
        public string ProfilLink { get; set; }

        public string toUserId { get; set; }
        public string toUserName { get; set; }

        public Guid CategoryId { get; set; }
        public string MainFeedId { get; set; }
        public DateTime CreatedOn { get; set; }

        public string CatalogId { get; set; }
        public string CatalogName { get; set; }
        //public string ImageName { get; set; }
        //public string ImageSrc { get; set; }
        //public string SongName { get; set; }
        //public string SongSrc { get; set; }

        public string fileName { get; set; }
        public string fileUrl { get; set; }
        public string fileType { get; set; }
       
    }
}
