﻿using PMPMusic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PMPMusic.ViewModel
{
    public class MainVM
    {
        public List<PostFeedVM> PostFeeds { get; set; }

        public ApplicationUser UserDetails { get; set; }

        public List<PostFeedVM> PostFeedsLatest { get; set; }

        public List<PostFeedFollowedVM> FollowFeedDetails { get; set; }

        public List<PostFeedSavedVM> SavedFeedDetails { get; set; }
        
    }
}
