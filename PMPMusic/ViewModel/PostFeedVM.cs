﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PMPMusic.ViewModel
{
    public class PostFeedList
    {
        public List<PostFeedVM> PostFeeds { get; set; }
        public int TotalFeeds { get; set; }
    }
    public class PostFeedVM
    {

        public Guid Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string ImageLink { get; set; }

        public string AudioLink { get; set; }

        public string VideoLink { get; set; }

        public bool IsActive { get; set; }

        public bool IsLike { get; set; }

        public int IsLikes { get; set; }

        public int LikeCount { get; set; }

        public int IsFollow { get; set; }

        public string CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        public string UpdatedBy { get; set; }

        public DateTime UpdatedOn { get; set; }

        public DateTime ExpireDate { get; set; }


        public string ImageSrc { get; set; }

        public string ImageType { get; set; }

        public string AudioSrc { get; set; }

        public string AudioType { get; set; }

        public string VideoSrc { get; set; }

        public string VideoType { get; set; }

        public string ImageFileName { get; set; }

        public string AudioFileName { get; set; }

        public string VideoFileName { get; set; }

        public string FeedExpireIN { get; set; }

        public bool SaveFeed { get; set; }

        public string UserId { get; set; }

        public string CreatedUserName { get; set; }

        public string UserProfileUrl { get; set; }

        public string UserProfile { get; set; }

        public bool isTrending { get; set; }

        public int SaveFeedStatus { get; set; }
    }

    public class PostCommentVM
    {

        public Guid Id { get; set; }

        public Guid PostId { get; set; }

        public string PostComment { get; set; }

        public string CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        public string Title { get; set; }


    }

    public class FollowFeedVM
    {

        public Guid Id { get; set; }

        public Guid PostId { get; set; }

        public Guid FollowingUserId { get; set; }

        public bool IsFollow { get; set; }

        public string FollowingUser { get; set; }

        public DateTime FollowDate { get; set; }

    }


    public class PostFeedFollowedVM
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string ImageLink { get; set; }

        public string AudioLink { get; set; }

        public string VideoLink { get; set; }

        public bool IsActive { get; set; }

        public string CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        public string UpdatedBy { get; set; }

        public DateTime UpdatedOn { get; set; }

        public DateTime ExpireDate { get; set; }

        public string ImageSrc { get; set; }

        public string ImageType { get; set; }

        public string AudioSrc { get; set; }

        public string AudioType { get; set; }

        public string VideoSrc { get; set; }

        public string VideoType { get; set; }

        public string ImageFileName { get; set; }

        public string AudioFileName { get; set; }

        public string VideoFileName { get; set; }

        public string FeedExpireIN { get; set; }

        public bool SaveFeed { get; set; }

        public Guid FollowedId { get; set; }

        public Guid PostId { get; set; }

        public string FollowingUser { get; set; }

        public string FollowingUserPUrl { get; set; }

        public string FollowingUserPLink { get; set; }
        
        public DateTime FollowDate { get; set; }

        public int IsFollow { get; set; }

        public string FollowingUserId { get; set; }

    }

    public class PostFeedSavedVM
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string ImageLink { get; set; }

        public string AudioLink { get; set; }

        public string VideoLink { get; set; }

        public bool IsActive { get; set; }

        public string CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        public string UpdatedBy { get; set; }

        public DateTime UpdatedOn { get; set; }

        public DateTime ExpireDate { get; set; }

        public string ImageSrc { get; set; }

        public string ImageType { get; set; }

        public string AudioSrc { get; set; }

        public string AudioType { get; set; }

        public string VideoSrc { get; set; }

        public string VideoType { get; set; }

        public string ImageFileName { get; set; }

        public string AudioFileName { get; set; }

        public string VideoFileName { get; set; }

        public string FeedExpireIN { get; set; }

        public bool SaveFeed { get; set; }

        public Guid SavedFeedId { get; set; }

        public Guid PostId { get; set; }

        public string SavedFeedUser { get; set; }

        public string SavedFeedUserPUrl { get; set; }

        public string SavedFeedUserPLink { get; set; }

        public DateTime SavedFeedDate { get; set; }

        public DateTime UapdatedDate { get; set; }

        public int IsFollow { get; set; }
        
    }


    public class LikeFeedVM
    {

        public Guid Id { get; set; }

        public Guid PostId { get; set; }

        public string CreatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }

        public bool IsLike { get; set; }


    }

    public class MessageVM
    {

        public Guid Id { get; set; }

        public Guid PostId { get; set; }

        public string MessageFrom { get; set; }

        public string MessageTo { get; set; }

        public string Subject { get; set; }

        public DateTime MessageDate { get; set; }

        public string FromUserName { get; set; }

        public string ToUserName { get; set; }

        public string FeedTitle { get; set; }

        public string FeedId { get; set; }

    }

    public class SelectedCategoriesVM
    {

        public string Text { get; set; }

        public bool selected { get; set; }

    }












}
