﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMPMusic.ViewModel
{
    public class UserMessagesVM
    {
        public string Id { get; set; }

        public string PostId { get; set; }

        public string MessageFrom { get; set; }

        public string MessageTo { get; set; }

        public string FromUserId { get; set; }

        public string ToUserId { get; set; }

        public string Subject { get; set; }

        public int rmsgCount { get; set; }

        public int totalCount { get; set; }

        public string UserImage { get; set; }

        public string ToUserImage { get; set; }

        public DateTime MessageDate { get; set; }

        public string pullclass { get; set; }

        public string chatclass { get; set; }

        public string dateclass { get; set; }
    }
}
