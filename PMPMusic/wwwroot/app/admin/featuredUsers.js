﻿(function () {
    'use strict';

    angular
        .module('admin')
        .controller('featuredUsers', featuredUsers);

    featuredUsers.$inject = ['$scope', '$http','$mdDialog'];

    function featuredUsers($scope, $http, $mdDialog) {
        $scope.title = ':: Featured Users ::';

        $scope.usersList = [];
        $scope.usersListPartial = [];
        $scope.showSpinner = false;
        var basePath = "/";
        $scope.isSubmit = false;
        $scope.FeedComments = '';
        $scope.selectedUsers = [];
        $scope.selected = [];
        $scope.options = {
            rowSelection: true,
            multiSelect: true,
            autoSelect: true,
            decapitate: false,
            largeEditDialog: false,
            boundaryLinks: false,
            limitSelect: true,
            pageSelect: true
        };

        //$scope.selected = [];

        $scope.query = {
            order: 'name',
            limit: 5,
            page: 1
        };

        function success(desserts) {
            $scope.desserts = desserts;
        }

        $scope.getDesserts = function () {
            $scope.promise = $nutrition.desserts.get($scope.query, success).$promise;
        };

        activate();

        function activate() {

            $http({
                method: 'POST',
                url: basePath + 'admin/getAllUsers',
                headers: "application/json"
            }).then(function (data) {
                debugger;
                if (data.data.length > 0) {
                    $scope.usersList = data.data;
                    $scope.usersListPartial = data.data;

                } else {
                    $scope.FeedComments = 'Records not found';
                }
            }).catch(function (error) {
                console.log(error, " can't get data.");

            });
        }

        $scope.dialogBox = function (title, content) {
            $mdDialog.show(
             $mdDialog.alert()
            .parent(angular.element(document.querySelector('#popupContainer')))
            .clickOutsideToClose(true)
            .title(title)
            .textContent(content)
            .ariaLabel('Alert Dialog Demo')
            .ok('Ok')
             );
        };

        $scope.saveFeaturedUser = function () {
            $scope.selectedUsers = [];
             var i = 0;
             if ($scope.selected.length > 0) {

                 //angular.forEach($scope.selected, function (item) {
                 //    $scope.selectedUsers[i].userId = item.userId;
                 //    $scope.selectedUsers[i].isFeaturedUser = item.isFeaturedUser;
                 //    i++;

                 //});


                 $http({
                     method: 'POST',
                     url: basePath + 'admin/updateUser',
                     data: $scope.selected,
                     headers: "application/json"
                 }).then(function (data) {
                     debugger;
                     activate();

                 }, function (error) {
                     console.log(error, " can't get data.");

                 });
             } else {
                 $scope.dialogBox("Warn", "Please select atleast one user.");
             };
        }
    }
})();
