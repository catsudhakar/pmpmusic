﻿var adminModule = angular.module('admin', ['share','md.data.table'])
    .config(function ($routeProvider, $locationProvider) {
        $routeProvider.when('/admin', { templateUrl: '/app/admin/trendingFeeds.html', caseInsensitiveMatch: true });
        $routeProvider.when('/admin/trendingFeeds', { templateUrl: '/app/admin/trendingFeeds.html', caseInsensitiveMatch: true });
        $routeProvider.when('/admin/featuredUsers', { templateUrl: '/app/admin/featuredUsers.html', caseInsensitiveMatch: true });

        $routeProvider.otherwise({ redirectTo: '/admin' });
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
    });

adminModule.directive('validFile', function () {
    return {
        require: 'ngModel',
        link: function (scope, el, attrs, ngModel) {
            //change event is fired when file is selected
            el.bind('change', function () {
                scope.$apply(function () {
                    ngModel.$setViewValue(el.val());
                    ngModel.$render();
                });
            });
        }
    }
});
