﻿//(function () {
//    'use strict';

//    angular
//        .module('admin')
//        .controller('nutritionController', nutritionController);

//    nutritionController.$inject = ['$scope']; 

//    function nutritionController($scope) {
//        $scope.title = 'nutritionController';

//        activate();

//        function activate() { }
//    }
//})();


angular.module('admin').controller('nutritionController', ['$http', '$q', '$timeout', '$scope', function ($http,  $q, $timeout, $scope) {
    'use strict';

    $scope.options = {
        rowSelection: true,
        multiSelect: true,
        autoSelect: true,
        decapitate: false,
        largeEditDialog: false,
        boundaryLinks: false,
        limitSelect: true,
        pageSelect: true
    };

    $scope.selected = [];
    $scope.limitOptions = [5, 10, 15, {
        label: 'All',
        value: function () {
            return $scope.desserts ? $scope.desserts.count : 0;
        }
    }];

    $scope.query = {
        order: 'name',
        limit: 5,
        page: 1
    };

    // for testing ngRepeat
    $scope.columns = [{
        name: 'Dessert',
        orderBy: 'name',
        unit: '100g serving'
    }, {
        descendFirst: true,
        name: 'Type',
        orderBy: 'type'
    }, {
        name: 'Calories',
        numeric: true,
        orderBy: 'calories.value'
    }, {
        name: 'Fat',
        numeric: true,
        orderBy: 'fat.value',
        unit: 'g'
    }, /* {
    name: 'Carbs',
    numeric: true,
    orderBy: 'carbs.value',
    unit: 'g'
  }, */ {
      name: 'Protein',
      numeric: true,
      orderBy: 'protein.value',
      trim: true,
      unit: 'g'
  }, /* {
    name: 'Sodium',
    numeric: true,
    orderBy: 'sodium.value',
    unit: 'mg'
  }, {
    name: 'Calcium',
    numeric: true,
    orderBy: 'calcium.value',
    unit: '%'
  }, */ {
      name: 'Iron',
      numeric: true,
      orderBy: 'iron.value',
      unit: '%'
  }, {
      name: 'Comments',
      orderBy: 'comment'
  }];

    $scope.desserts=[
    {
        "name": "Frozen yogurt",
        "type": "Ice cream",
        "calories": { "value": 159.0 },
        "fat": { "value": 6.0 },
        "carbs": { "value": 24.0 },
        "protein": { "value": 4.0 },
        "sodium": { "value": 87.0 },
        "calcium": { "value": 14.0 },
        "iron": { "value": 1.0 },
        "comment": "Not as good as the real thing."
    },
    {
        "name": "Ice cream sandwich",
        "type": "Ice cream",
        "calories": { "value": 237.0 },
        "fat": { "value": 9.0 },
        "carbs": { "value": 37.0 },
        "protein": { "value": 4.3 },
        "sodium": { "value": 129.0 },
        "calcium": { "value": 8.0 },
        "iron": { "value": 1.0 }
    }]

   

    $scope.editComment = function (event, dessert) {
        event.stopPropagation();

        var dialog = {
            // messages: {
            //   test: 'I don\'t like tests!'
            // },
            modelValue: dessert.comment,
            placeholder: 'Add a comment',
            save: function (input) {
                dessert.comment = input.$modelValue;
            },
            targetEvent: event,
            title: 'Add a comment',
            validators: {
                'md-maxlength': 30
            }
        };

        //var promise = $scope.options.largeEditDialog ? $mdEditDialog.large(dialog) : $mdEditDialog.small(dialog);

        //promise.then(function (ctrl) {
        //    var input = ctrl.getInput();

        //    input.$viewChangeListeners.push(function () {
        //        input.$setValidity('test', input.$modelValue !== 'test');
        //    });
        //});
    };

    $scope.toggleLimitOptions = function () {
        $scope.limitOptions = $scope.limitOptions ? undefined : [5, 10, 15];
    };

    $scope.getTypes = function () {
        return ['Candy', 'Ice cream', 'Other', 'Pastry'];
    };

    $scope.onPaginate = function (page, limit) {
        console.log('Scope Page: ' + $scope.query.page + ' Scope Limit: ' + $scope.query.limit);
        console.log('Page: ' + page + ' Limit: ' + limit);

        $scope.promise = $timeout(function () {

        }, 2000);
    };

    $scope.deselect = function (item) {
        console.log(item.name, 'was deselected');
    };

    $scope.log = function (item) {
        console.log(item.name, 'was selected');
    };

    $scope.loadStuff = function () {
        $scope.promise = $timeout(function () {

        }, 2000);
    };

    $scope.onReorder = function (order) {

        console.log('Scope Order: ' + $scope.query.order);
        console.log('Order: ' + order);

        $scope.promise = $timeout(function () {

        }, 2000);
    };

}]);
