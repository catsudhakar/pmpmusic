﻿(function () {
    'use strict';

    angular
        .module('share')
        .controller('addTags', addTags);

    addTags.$inject = ['$scope', '$http', '$mdDialog'];

    function addTags($scope, $http, $mdDialog) {

        var basePath = '/';

        $scope.isSubmit = false;

        $scope.userTags = [];
        $scope.stepsModel = [];
        $scope.showSpinner = false;
        $scope.activated = false;

        $scope.title = 'Add / Edit Tags';

        activate();

        function activate() {

            //$http({
            //    method: 'POST',
            //    url: basePath + 'MainPage/getUserTags',
            //    data: $scope.userTags,
            //    headers: "application/json"
            //}).then(function (data) {
            //    $scope.userTags = data.data;


            //});
            $scope.SelectTagList = ['PRODUCER', 'ARTISTS', 'DJS', 'ENGINEER', 'SONGWRITER', 'GRAPHIC DESIGNER', 'MUSICIAN', 'MANAGEMENT', 'RECORD LABEL', 'VIDEOGRAPHER'];


        }



        function DialogController($scope, $mdDialog, message) {
            debugger
            $scope.message = message;

            $scope.hide = function () {
                $mdDialog.hide();
            };
            $scope.cancel = function () {
                $mdDialog.cancel();
            };
            $scope.answer = function (answer) {
                $mdDialog.hide(answer);
            };
        };

        $scope.editTags = function (frmRegister) {
            debugger;
            $scope.isSubmit = true;
            $scope.activated = true;
            var userTags = $scope.userTags;
            if (frmRegister.$valid) {
                $scope.showSpinner = true;

                //$http({
                //    method: 'POST',
                //    url: basePath + 'MainPage/addUserTags',
                //    data: $scope.userTags,
                //    headers: "application/json"
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: '../Account/Register',
                    parent: angular.element(document.body),
                    targetEvent: frmRegister,
                    clickOutsideToClose: false,
                    locals: {
                        message: angular.copy(frmRegister)
                    }
                }).then(function (result) {
                    if (result) {
                        $mdDialog.hide("Edit");
                        $scope.activated = false;
                        $scope.showSpinner = false;
                    }
                    else {
                        $mdDialog.hide("Cancel");
                        $scope.activated = false;
                        $scope.showSpinner = false;
                    }
                });
            };
        }

    }
})();
