﻿(function () {
    'use strict';

    angular
        .module('share')
        .controller('index', index);

    index.$inject = ['$scope', '$http', '$mdDialog'];

    function index($scope, $http, $mdDialog) {
        $scope.title = 'PMP Music';
        var basePath = "/";
        $scope.SelectCountryList = [];
        $scope.country = '';
        $scope.userTags = {};
        $scope.isSubmit = false;
        $scope.isSubmit1 = false;
        $scope.isSubmit2 = false;
        $scope.isSubmit3 = false;
        $scope.stepsModel = [];
        $scope.showSpinner = false;
        $scope.activated = false;
        $scope.register = {};
        $scope.isFocus = false;
        $scope.isError = false;
        $scope.errorMessage = '';
        $scope.userTags.paymentOption = 'Paypal';
        var vm = this;
        $scope.continueDisabled = false;
        $scope.isPaymentSussess = false;
        $scope.isPaymentCancelled = false;
        $scope.accessTokenObject = {};
        $scope.paymentSuccessObject = {};
        $scope.paymentSuccessObject.isNull = true;

        activate();

        function activate() {

            getPaypalAccessToken();
            $scope.SelectTagList = ['PRODUCER', 'ARTISTS', 'DJS', 'ENGINEER', 'SONGWRITER', 'GRAPHIC DESIGNER', 'MUSICIAN', 'MANAGEMENT', 'RECORD LABEL', 'VIDEOGRAPHER'];

            $scope.selectMonthList = ['01-JAN', '02-FEB', '03-MAR', '04-APR', '05-MAY', '06-JUN', '07-JUL', '08-AUG', '09-SEP', '10-OCT', '11-NOV', '12-DEC'];
            $scope.selectYearList = ['1980'];
            for (var i = 1981; i < 2045; i++) {
                $scope.selectYearList.push(i);
            }

            if ($scope.message != null) {
                //$scope.userTags = $scope.message;
                $scope.userTags.mainTag = $scope.message.mainTag.$modelValue;
                if (!$scope.message.Tag1.$modelValue) {

                    $scope.userTags.tag1 = "";
                }
                else {
                    $scope.userTags.tag1 = $scope.message.Tag1.$modelValue;
                }
                if (!$scope.message.Tag2.$modelValue) {
                    $scope.userTags.tag2 = "";
                } else {

                    $scope.userTags.tag2 = $scope.message.Tag2.$modelValue;
                }
                if (!$scope.message.Tag3.$modelValue) {
                    $scope.userTags.tag3 = "";
                } else {
                    $scope.userTags.tag3 = $scope.message.Tag3.$modelValue;

                }
            }
            $http.get(basePath + 'countryList/countryList.json').then(function (data) {
                $scope.SelectCountryList = data.data;
            });
        }
        function DialogController($scope, $mdDialog, message) {
            $scope.message1 = message;

            $scope.hide = function () {
                $mdDialog.hide();
            };
            $scope.cancel = function () {
                $mdDialog.cancel();
            };
            $scope.answer = function (answer) {
                $mdDialog.hide(answer);
            };
        };


        vm.gotoStep1 = function () {

            document.querySelector('core-animated-pages').selected = 0;
        }

        vm.gotoStep2 = function (form, currentStep) {


            if (currentStep == 1) {
                $scope.isSubmit1 = true;

                if (form.$valid && $scope.isSubmit1) {

                    document.querySelector('core-animated-pages').selected = 1;
                }
            }
            else {
                document.querySelector('core-animated-pages').selected = 1;
            }


        }
        vm.gotoStep3 = function (frmRegister2, currentStep) {
            var chkselct = $scope.userTags.termsandcontions;//check terms & conditions checkbox
            if (chkselct == false || chkselct == undefined) {
                $scope.valmsgtac = true;
                return false;
            }
            else {
                $scope.valmsgtac = false;
            }
            if (currentStep == 2) {
                $scope.isSubmit2 = true;
                if ($scope.isPaymentSussess === false) {

                    $scope.continueDisabled = true;
                } else {
                    $scope.continueDisabled = false;
                }


                if (frmRegister2.$valid && $scope.isSubmit2 && $scope.isFocus == false) {

                    document.querySelector('core-animated-pages').selected = 2;
                }
            }
            else {
                document.querySelector('core-animated-pages').selected = 2;
            }

        }

        vm.gotoStep4 = function (frmRegister3, currentStep) {
            if (currentStep == 3) {
                $scope.isSubmit3 = true;

                if (frmRegister3.$valid && $scope.isSubmit3) {

                    document.querySelector('core-animated-pages').selected = 3;
                }
            }
            else {
                document.querySelector('core-animated-pages').selected = 3;
            }

        }

        $scope.checkmail = function (email) {
            $scope.isFocus = false;
            if (email != undefined) {
                if (email != null || email.length > 0) {
                    $http({
                        method: 'POST',
                        url: basePath + 'Account/doesEmailExist?Email=' + email,
                        headers: "application/json"
                    }).then(function (data) {
                        if (data.data == 'User exist with this email') {
                            $scope.isFocus = true;
                        }
                    }, function (error) {
                        console.log(error, " can't get data.");

                    });
                }
            }
        }


        $scope.registerUser = function (registerForm) {
            //alert("Final Reached");

            //alert($scope.userTags);

            $scope.isError = false;
            $scope.errorMessage = '';


            $http({
                method: 'POST',
                url: basePath + 'Account/UserRegistration',
                data: $scope.userTags,
                headers: "application/json"
            }).then(function (data) {
                //if (data.data == 'User exist with this email' || data.data=='Dupicate User') 
                if (data.data != 'Your account is registered successfully, Please login with our emailid..') {
                    $scope.isError = true;
                    $scope.errorMessage = data.data;
                }
            }, function (error) {
                console.log(error, " can't get data.");

            });
            //debugger;
            //$scope.isSubmit = true;
            //$scope.activated = true;
            //if (registerForm.$valid) {
            //    $scope.showSpinner = true;

            //    //$http({
            //    //    method: 'POST',
            //    //    url: basePath + 'Account/Register',
            //    //    data: $scope.register,
            //    //    headers: "application/json"

            //    //}).then(function (result) {
            //    //    if (result) {
            //    //       // $mdDialog.hide("Edit");
            //    //        $scope.activated = false;
            //    //        $scope.showSpinner = false;
            //    //    }
            //    //    else {
            //    //        //$mdDialog.hide("Cancel");
            //    //        $scope.activated = false;
            //    //        $scope.showSpinner = false;
            //    //    }
            //    //});
            //    registerForm.userTags = $scope.userTags;
            //    registerForm.registerData = $scope.register;
            //    $mdDialog.show({
            //        controller: DialogController,
            //        templateUrl: '/app/register/payments.html',
            //        parent: angular.element(document.body),
            //        targetEvent: registerForm,
            //        clickOutsideToClose: false,
            //        locals: {
            //            message: angular.copy(registerForm)
            //        }
            //    }).then(function (result) {
            //        if (result) {
            //            //$mdDialog.hide("Edit");
            //            $scope.activated = false;
            //            $scope.showSpinner = false;
            //        }
            //        else {
            //            //$mdDialog.hide("Cancel");
            //            $scope.activated = false;
            //            $scope.showSpinner = false;
            //        }
            //    }).catch(() => {
            //        console.log("Cancelled");
            //    });
            //};
        }
        $scope.setupProfile = function () {
            alert("Profile clicked");
        }


        //$scope.client = {
        //    sandbox: 'ASn57Ju5TjRdxv62RTa5OWnf9nTC64mmTlAsYSEj2BYmLjvbFLCMC9PUEa2KBbOeblnyDMkChy0zVXOr',
        //    production: 'ASn57Ju5TjRdxv62RTa5OWnf9nTC64mmTlAsYSEj2BYmLjvbFLCMC9PUEa2KBbOeblnyDMkChy0zVXOr'
        //};

        //$scope.payment = function () {
        //    return actions.payment.create({
        //        payment: {
        //            transactions: [
        //                {
        //                    amount: { total: '1.00', currency: 'USD' }
        //                }
        //            ]
        //        }
        //    });
        //};

        //$scope.onAuthorize = function (data, action) {
        //    return actions.payment.execute().then(function (payment) {

        //        // The payment is complete!
        //        // You can now show a confirmation message to the customer
        //        window.alert("Payment Complete");

        //    });
        //};
        function isValid() {
            return document.querySelector('#check').checked;
        }

        function onChangeCheckbox(handler) {
            document.querySelector('#check').addEventListener('change', handler);
        }

        function toggleValidationMessage() {
            document.querySelector('#msg').style.display = (isValid() ? 'none' : 'block');
        }

        function toggleButton(actions) {
            return isValid() ? actions.enable() : actions.disable();
        }

        $scope.opts = {
            // Pass the client ids to use to create your transaction on sandbox and production environments
            client: {
                sandbox: 'Abeqj2EVujjQwryo7Vrk3TKGBza0UUPEEzZLELcXV_rxteEh7MEaazAs-GaT6XN8nG5dicb18OA6Ergf',
                production: 'ASn57Ju5TjRdxv62RTa5OWnf9nTC64mmTlAsYSEj2BYmLjvbFLCMC9PUEa2KBbOeblnyDMkChy0zVXOr'
            },
            // Pass the payment details for your transaction
            // See https://developer.paypal.com/docs/api/payments/#payment_create for the expected json parameters

            env: 'sandbox',
            payment: function () {
                console.log('payment');
                //console.log(this.props.env);
                //console.log(this.props.client);

                return paypal.rest.payment.create(this.props.env, this.props.client, {
                    transactions: [
                        {
                            amount: {
                                total: '4.99',
                                currency: 'USD'
                            }
                        }
                    ]
                });

            },
            // Display a "Pay Now" button rather than a "Continue" button
            commit: true,
            // Pass a function to be called when the customer completes the payment
            onAuthorize: function (data, actions) {
                console.log('authorize');
                console.log(data);
                console.log(actions);

                return actions.payment.execute().then(function (payment) {
                    console.log(payment);
                    // The payment is complete!
                    // You can now show a confirmation message to the customer
                    //window.alert("Payment Complete");
                    $scope.$apply(function () {
                        $scope.isPaymentSussess = true;
                        $scope.continueDisabled = false;
                        $scope.isPaymentCancelled = false;
                        $scope.paymentSuccessObject.isNull = false;
                        $scope.paymentSuccessObject.email = payment.payer.payer_info.email;
                        $scope.paymentSuccessObject.paymentMethod = payment.payer.payment_method;
                        $scope.paymentSuccessObject.amount = payment.transactions["0"].amount.total;
                        $scope.paymentSuccessObject.parentPayment = payment.transactions["0"].related_resources["0"].sale.parent_payment;
                        $scope.paymentSuccessObject.state = payment.transactions["0"].related_resources["0"].sale.state;
                        $scope.paymentSuccessObject.paymentId = payment.transactions["0"].related_resources["0"].sale.id;
                        $scope.paymentSuccessObject.currency = payment.transactions["0"].amount.currency;
                    });

                });
            },
            // Pass a function to be called when the customer cancels the payment
            onCancel: function (data) {
                $scope.$apply(function () {
                    console.log('cancel');
                    console.log(data);
                    console.log('The payment was cancelled!');
                    $scope.isPaymentSussess = false;
                    $scope.continueDisabled = true;
                    $scope.isPaymentCancelled = true;
                });

            },
            style: {
                label: 'pay',      // checkout | credit | pay
                size: 'medium',    // small | medium | responsive
                shape: 'pill',     // pill | rect
                color: 'gold'    // gold | blue | silver
            },
            validate: function (actions) {
                toggleButton(actions);

                onChangeCheckbox(function () {
                    toggleValidationMessage();
                    toggleButton(actions);
                });
            },

            onClick: function () {
                toggleValidationMessage();
            },
        };

        function getPaypalAccessToken() {
            var username = "Abeqj2EVujjQwryo7Vrk3TKGBza0UUPEEzZLELcXV_rxteEh7MEaazAs-GaT6XN8nG5dicb18OA6Ergf";
            var password = "EM_Tkst6lD3rUYiz7GlbOTaHa5kIIB6lcdRwU0bjf7e1_nXjrztBQS3tC57382F9AkFOut8FKVrNN_27";
            var url = "https://api.sandbox.paypal.com/v1/oauth2/token";
            var data = 'grant_type=client_credentials';
            var headers = {

                headers: {
                    "content-type": "application/x-www-form-urlencoded",
                    "authorization": 'Basic ' + btoa(username + ":" + password),
                    "cache-control": "no-cache",
                    "postman-token": "7a21dc81-11a8-bf34-b1dc-30a855d4f461",

                },

            };

            $http.post(url, data, headers).then(function (response) {
                console.log(response);
                $scope.accessTokenObject = response;
            })

        }

        $scope.makePayment = function (frmRegister3) {

            if (frmRegister3.$valid) {
                $scope.showSpinner = true;
                $scope.isPaymentSussess = false;
                $scope.continueDisabled = true;
                $scope.isPaymentCancelled = true;
                var access_token = $scope.accessTokenObject.data.access_token;

                var url = "https://api.sandbox.paypal.com/v1/payments/payment";

                var headers = {
                    headers: {
                        "Content-Type": "application/json",
                        "authorization": 'Bearer ' + access_token,
                        "cache-control": "no-cache",
                        "postman-token": "f13c0703-f5f9-4877-776a-a16620793204"
                    }
                }
                var expiry = $scope.userTags.expiry;
                var exp_month = expiry.substring(0, 2);
                var exp_year = expiry.substring(3, 7);

                var data = {
                    "intent": "sale",
                    "redirect_urls": {
                        "return_url": "http://example.com/your_redirect_url.html",
                        "cancel_url": "http://example.com/your_cancel_url.html"
                    },
                    "payer": {
                        "payment_method": "credit_card",
                        "funding_instruments": [{
                            "credit_card": {
                                "number": $scope.userTags.cardNumber,
                                "type": frmRegister3.cardNumber.$card.type,
                                "expire_month": exp_month,
                                "expire_year": exp_year,
                                "cvv2": $scope.userTags.cvvCode,
                                "first_name": $scope.userTags.cardName,
                                "last_name": "TEST",
                                "billing_address": {
                                    "line1": "111 First Street",
                                    "city": "Saratoga",
                                    "state": "CA",
                                    "postal_code": "95070",
                                    "country_code": "US"
                                }
                            }
                        }]
                    },
                    "transactions": [{
                        "amount": {
                            "total": "4.99",
                            "currency": "USD"
                        }
                    }]
                };

                $http.post(url, data, headers).then(function (response) {
                    console.log(response);
                    if (response.status == 201) {
                        $scope.isPaymentSussess = true;
                        $scope.continueDisabled = false;
                        $scope.isPaymentCancelled = false;
                        $scope.showSpinner = false;
                        $scope.paymentSuccessObject.isNull = false;
                        $scope.paymentSuccessObject.cardNumber = response.data.payer.funding_instruments["0"].credit_card.number;
                        $scope.paymentSuccessObject.paymentMethod = response.data.payer.payment_method;
                        $scope.paymentSuccessObject.amount = response.data.transactions["0"].amount.total;
                        $scope.paymentSuccessObject.parentPayment = response.data.transactions["0"].related_resources["0"].sale.parent_payment;
                        $scope.paymentSuccessObject.state = response.data.transactions["0"].related_resources["0"].sale.state;
                        $scope.paymentSuccessObject.paymentId = response.data.transactions["0"].related_resources["0"].sale.id;
                        $scope.paymentSuccessObject.currency = response.data.transactions["0"].amount.currency;

                    } else if (response.status === 200) {
                        $scope.isPaymentSussess = true;
                        $scope.continueDisabled = false;
                        $scope.isPaymentCancelled = false;
                        $scope.showSpinner = false;
                        $scope.paymentSuccessObject.isNull = false;
                        $scope.paymentSuccessObject.cardNumber = response.data.payer.funding_instruments["0"].credit_card.number;
                        $scope.paymentSuccessObject.paymentMethod = response.data.payer.payment_method;
                        $scope.paymentSuccessObject.amount = response.data.transactions["0"].amount.total;
                        $scope.paymentSuccessObject.parentPayment = response.data.transactions["0"].related_resources["0"].sale.parent_payment;
                        $scope.paymentSuccessObject.state = response.data.transactions["0"].related_resources["0"].sale.state;
                        $scope.paymentSuccessObject.paymentId = response.data.transactions["0"].related_resources["0"].sale.id;
                        $scope.paymentSuccessObject.currency = response.data.transactions["0"].amount.currency;

                    } else if (response.status === 202) {

                        $scope.isPaymentSussess = true;
                        $scope.continueDisabled = false;
                        $scope.isPaymentCancelled = false;
                        $scope.showSpinner = false;

                        $scope.paymentSuccessObject.isNull = false;
                        $scope.paymentSuccessObject.cardNumber = response.data.payer.funding_instruments["0"].credit_card.number;
                        $scope.paymentSuccessObject.paymentMethod = response.data.payer.payment_method;
                        $scope.paymentSuccessObject.amount = response.data.transactions["0"].amount.total;
                        $scope.paymentSuccessObject.parentPayment = response.data.transactions["0"].related_resources["0"].sale.parent_payment;
                        $scope.paymentSuccessObject.state = response.data.transactions["0"].related_resources["0"].sale.state;
                        $scope.paymentSuccessObject.paymentId = response.data.transactions["0"].related_resources["0"].sale.id;
                        $scope.paymentSuccessObject.currency = response.data.transactions["0"].amount.currency;
                    } else {
                        $scope.paymentSuccessObject.isNull = true;
                        $scope.isPaymentSussess = false;
                        $scope.continueDisabled = true;
                        $scope.isPaymentCancelled = true;
                        $scope.showSpinner = false;
                    }

                });
                $scope.fillDetialsMessage = "Please fill the card details";
            }

        }





    }

})();
