﻿
(function () {
    'use strict';

    angular
    .module('share')
    .controller('basicSignUp', basicSignUp);

    basicSignUp.$inject = ['$scope', '$http', '$mdDialog'];


    function basicSignUp($scope, $http, $mdDialog) {
        $scope.title = 'PMP Music';
        var basePath = "/";
        $scope.SelectCountryList = [];
        $scope.country = '';
        $scope.userTags = {};
        $scope.isSubmit = false;
        $scope.isSubmit1 = false;
        $scope.isSubmit2 = false;
        $scope.isSubmit3 = false;
        $scope.stepsModel = [];
        $scope.showSpinner = false;
        $scope.activated = false;
        $scope.register = {};
        $scope.isFocus = false;
        $scope.isError = false;
        $scope.errorMessage = '';
        $scope.valmsgtac = false;
        $scope.nameLength = false;

        $scope.isFinsh = false;


        var vm = this;

        activate();

        function activate() {
            $scope.SelectTagList = ['PRODUCER', 'ARTISTS', 'DJS', 'ENGINEER', 'SONGWRITER', 'GRAPHIC DESIGNER', 'MUSICIAN', 'MANAGEMENT', 'RECORD LABEL', 'VIDEOGRAPHER'];

            $scope.selectMonthList = ['01-JAN', '02-FEB', '03-MAR', '04-APR', '05-MAY', '06-JUN', '07-JUL', '08-AUG', '09-SEP', '10-OCT', '11-NOV', '12-DEC'];
            $scope.selectYearList = ['1980'];
            for (var i = 1981; i < 2045; i++) {
                $scope.selectYearList.push(i);
            }
            $scope.userTags.paymentOption = 'visaMaestro';
            if ($scope.message != null) {
                //$scope.userTags = $scope.message;
                $scope.userTags.mainTag = $scope.message.mainTag.$modelValue;
                if (!$scope.message.Tag1.$modelValue) {

                    $scope.userTags.tag1 = "";
                }
                else {
                    $scope.userTags.tag1 = $scope.message.Tag1.$modelValue;
                }
                if (!$scope.message.Tag2.$modelValue) {
                    $scope.userTags.tag2 = "";
                } else {

                    $scope.userTags.tag2 = $scope.message.Tag2.$modelValue;
                }
                if (!$scope.message.Tag3.$modelValue) {
                    $scope.userTags.tag3 = "";
                } else {
                    $scope.userTags.tag3 = $scope.message.Tag3.$modelValue;

                }
            }
            $http.get(basePath + 'countryList/countryList.json').then(function (data) {
                $scope.SelectCountryList = data.data;
            });
        }
        function DialogController($scope, $mdDialog, message) {
            $scope.message1 = message;

            $scope.hide = function () {
                $mdDialog.hide();
            };
            $scope.cancel = function () {
                $mdDialog.cancel();
            };
            $scope.answer = function (answer) {
                $mdDialog.hide(answer);
            };
        };

        vm.gotoStep1 = function () {

            document.querySelector('core-animated-pages').selected = 0;
        }

        vm.gotoStep2 = function (form, currentStep) {


            if (currentStep == 1) {
                $scope.isSubmit1 = true;

                if (form.$valid && $scope.isSubmit1) {

                    document.querySelector('core-animated-pages').selected = 1;
                }
            }
            else {
                document.querySelector('core-animated-pages').selected = 1;
            }


        }

        vm.gotoStep3 = function (frmRegister2, currentStep) {

            if ($scope.userTags.firstName !== undefined) {
                if ($scope.userTags.firstName.length < 3 || $scope.userTags.firstName.length > 40) {
                    //alert("Name should be 3 to 40 characters");
                    $scope.nameLength = true;
                    return false
                }
                else {
                    $scope.nameLength = false;
                }
            }

            var chkselct = $scope.userTags.termsandcontions;//check terms & conditions checkbox
            if (chkselct == false || chkselct == undefined) {
                $scope.valmsgtac = true;
                return false;
            }
            else {
                $scope.valmsgtac = false;
            }

            if (currentStep == 2) {
                $scope.isSubmit2 = true;
                if (frmRegister2.$valid && $scope.isSubmit2 && $scope.isFocus == false) {

                    document.querySelector('core-animated-pages').selected = 2;
                }
            }
            else {
                document.querySelector('core-animated-pages').selected = 1;
            }

        }

        vm.gotoStep4 = function (frmRegister3, currentStep) {
            if (currentStep == 3) {
                $scope.isSubmit3 = true;

                if (frmRegister3.$valid && $scope.isSubmit3) {

                    document.querySelector('core-animated-pages').selected = 3;
                }
            }
            else {
                document.querySelector('core-animated-pages').selected = 3;
            }

        }

        $scope.checkmail = function (email) {
            $scope.isFocus = false;
            if (email != undefined) {
                if (email != null || email.length > 0) {
                    $http({
                        method: 'POST',
                        url: basePath + 'Account/doesEmailExist?Email=' + email,
                        headers: "application/json"
                    }).then(function (data) {
                        if (data.data == 'User exist with this email') {
                            $scope.isFocus = true;
                        }
                    }, function (error) {
                        console.log(error, " can't get data.");

                    });
                }
            }

        }


        $scope.registerUser = function (registerForm) {
            debugger;
            //alert("Final Reached");
            //alert($scope.userTags);
            $scope.isError = false;
            $scope.errorMessage = '';
            //var userNames = $scope.userTags.firstName;
            //$scope.userTags.firstName = userNames.replace(' ', '-');

            $http({
                method: 'POST',
                url: basePath + 'Account/UserRegistration',
                data: $scope.userTags,
                headers: "application/json"
            }).then(function (data) {
                if (data.data == 'Your account is registered successfully, Please login with our emailid..') {
                    //$scope.isError = true;
                    //$scope.errorMessage = data.data;
                    //$scope.isFinsh = true;
                    //$scope.dialogBox("Success", data.data);
                    window.location.href = "/mainpage";
                }
                else {
                    //if (data.data == 'User exist with this email' || data.data == 'Dupicate User')
                    $scope.isError = true;
                    $scope.errorMessage = data.data;
                }

            }, function (error) {
                console.log(error, " can't get data.");

            });

        }
        $scope.setupProfile = function () {
            alert("Profile clicked");
        }

        $scope.dialogBox = function (title, content) {
            $mdDialog.show(
             $mdDialog.alert()
            .parent(angular.element(document.querySelector('#popupContainer')))
            .clickOutsideToClose(true)
            .title(title)
            .textContent(content)
            .ariaLabel('Alert Dialog Demo')
            .ok('Ok')
             );

        };
    }
})();

