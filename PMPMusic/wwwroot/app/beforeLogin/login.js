﻿(function () {
    'use strict';

    angular
        .module('share')
        .controller('login', login);

    login.$inject = ['$scope', '$http', '$mdDialog', '$location', '$window'];

    function login($scope, $http, $mdDialog, $location, $window) {

        $scope.title = 'Login';
        $scope.loginDetails = {};
        $scope.isSubmit = false;
        $scope.error = '';
        $scope.iserror = false;
        var basePath = '/';

        activate();

        function activate() {

        }

        $scope.submit = function () {
            alert("testing");
            $scope.isSubmit = true;
        }

        $scope.submit = function (frmLogin) {
            $scope.isSubmit = true;
            $scope.iserror = false;

            var host = $window.location.host;
            var landingUrl = "http://" + host + "/mainpage";


            if (frmLogin.$valid) {

                $http({
                    method: 'POST',
                    url: basePath + 'Account/AjaxLogin',
                    data: $scope.loginDetails,
                    headers: "application/json"
                })
               .then(function (data) {

                   if (data.data == 'success') {
                       $mdDialog.hide();
                       // $location.path('/mainpage/');
                       $window.location.href = landingUrl;
                   }
                   else {
                       $scope.iserror = true;
                       $scope.error = data.data;
                   }
               });
            }
        };
    }
})();
