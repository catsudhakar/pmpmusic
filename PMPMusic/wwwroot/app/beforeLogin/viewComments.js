﻿(function () {
    'use strict';

    angular
        .module('share')
        .controller('viewComments', viewComments);

    viewComments.$inject = ['$scope', '$http', '$mdDialog'];

    function viewComments($scope, $http, $mdDialog) {
        $scope.title = 'View Comments';
        var basePath = "/";
        $scope.comments = [];
        $scope.feedTitle = {};
        $scope.FeedComments = '';

        activate();

        function activate() {

           // alert($scope.commentsFeedId);
            $http({
                method: 'POST',
                url: basePath + 'MainPage/getComments?feedId=' + $scope.commentsFeedId,
                //data: $scope.commentsFeedId,
                headers: "application/json"
            }).then(function (data) {
                debugger;
                if (data.data.length > 0) {
                    $scope.comments = data.data;
                    $scope.feedTitle = data.data[0];
                } else {
                    $scope.FeedComments = 'Records not found';
                }
            }, function (error) {
                // console.log(error, " can't get data.");
               
            });
        }
    }
})();
