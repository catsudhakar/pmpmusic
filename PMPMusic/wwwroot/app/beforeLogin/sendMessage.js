﻿(function () {
    'use strict';

    angular
        .module('share')
        .controller('sendMessage', sendMessage);

    sendMessage.$inject = ['$scope', '$http', '$mdDialog'];

    function sendMessage($scope, $http, $mdDialog) {
        $scope.title = 'Send Message';
        var basePath = "/";
        $scope.isSubmit = false;
        $scope.showSpinner = false;
        $scope.activated = false;
        $scope.messages = {};

        activate();

        function activate() {
            $scope.messages.postId = $scope.messageFeedId;
            $scope.messages.messageTo = $scope.toUserId;
           
        }

        $scope.sendMessage = function (frmsendmessage) {
            //alert($scope.messageFeedId);
            //alert($scope.toUserId);
            $scope.isSubmit = true;
            $scope.activated = true;
            if (frmsendmessage.$valid) {
                $scope.showSpinner = true;

                $http({
                    method: 'POST',
                    url: basePath + 'MainPage/sendMessage',
                    data: $scope.messages,
                    headers: "application/json"
                }).then(function (result) {
                    if (result) {
                        $mdDialog.hide("Edit");
                        $scope.activated = false;
                        $scope.showSpinner = false;
                    }
                    else {
                        $mdDialog.hide("Cancel");
                        $scope.activated = false;
                        $scope.showSpinner = false;
                    }
                });
            };
        }
    }
})();
