﻿(function () {
    'use strict';

    angular
        .module('share')
        .controller('submitofeed', submitofeed);

    submitofeed.$inject = ['$scope', '$http', '$mdDialog'];

    function submitofeed($scope, $http, $mdDialog) {
        $scope.title = 'SUBMIT TO THIS FEED';
        var basePath = "/";

        $scope.feedId = '';
        $scope.userId = '';

        $scope.musicCatelogs = [];
        $scope.imageGallery = [];
        $scope.getVideos = [];
        $scope.subFeeds = {};
        $scope.stepsModel = [];
        $scope.myImage = '';
        $scope.myCroppedImage = '';

        activate();

        function activate() {

            var result = $scope.commentsFeedId.split(',');
            $scope.feedId = result[0];
            $scope.userId = result[1];
            $scope.feedTitle = result[2];
            getMusicCatelog();
            getImageGallery();
            getVideos();
        }

        function getMusicCatelog() {
            $http({
                method: 'POST',
                url: basePath + 'MainPage/getUserCatelogs',
                //url: basePath + 'MainPage/getUserCatelogs?userId=' + $scope.userId,
                headers: "application/json"
            }).then(function (data) {
                $scope.musicCatelogs = data.data;
                for (var i = 0; i < $scope.musicCatelogs.length; i++) {
                    if (!$scope.musicCatelogs[i].songLink) {
                        $scope.autoplay = false;
                        $scope.musicCatelogs[i].audioPlayButton = $scope.autoplay;
                        $scope.musicCatelogs[i].noAudioLink = false;
                        $scope.musicCatelogs[i].isAudioPlaying = false;
                        $scope.musicCatelogs[i].playBtnImg = "playBtnImg" + i;
                    } else {
                        $scope.autoplay = true;
                        $scope.musicCatelogs[i].audioPlayButton = $scope.autoplay;
                        $scope.musicCatelogs[i].noAudioLink = true;
                        $scope.musicCatelogs[i].isAudioPlaying = false;
                        $scope.musicCatelogs[i].playBtnImg = "playBtnImg" + i;
                    }
                }
            }, function (error) {
                // console.log(error, " can't get data.");
            });
        }

        function getImageGallery() {
            $http({
                method: 'POST',
                url: basePath + 'MainPage/getUserGallery',
                headers: "application/json"
            }).then(function (data) {
                $scope.imageGallery = data.data;
            }, function (error) {
            });
        }

        function getVideos() {
            $http({
                method: 'POST',
                url: basePath + 'MainPage/getvideos',
                headers: "application/json"
            }).then(function (data) {
                $scope.getVideos = data.data;
            }, function (error) {
            });
        }


        $scope.submittofeedcatelog = function (catelogId, mlink, mname, fileType) {
            if (mlink == 'cropedimage') {
                mlink = $scope.myCroppedImage.split(',')[1];
            }
            if (mlink != undefined && mname != undefined) {
                $scope.isSubmit = true;
                $scope.activated = true;
                $scope.showSpinner = true;
                $scope.subFeeds = {};
                $scope.subFeeds.fileName = mname;
                $scope.subFeeds.fileUrl = mlink;
                $scope.subFeeds.fileType = fileType;
                $scope.subFeeds.id = $scope.userId;
                $scope.subFeeds.mainFeedId = $scope.feedId;
                $scope.subFeeds.categoryId = catelogId;

                $http({
                    method: 'POST',
                    url: basePath + 'MainPage/addsubfeed',
                    data: $scope.subFeeds,
                    headers: "application/json"
                })
               .then(function (result) {
                   if (result) {
                       $mdDialog.hide("Edit");
                       $scope.dialogBox("Submit", result.data);
                       $scope.activated = false; $scope.showSpinner = false;
                   }
                   else {
                       $mdDialog.hide("Cancel");
                   }
               });
            }
            else {
                alert("Please choose File !");
            }
        }

        $scope.dialogBox = function (title, content) {
            $mdDialog.show(
             $mdDialog.alert()
            .parent(angular.element(document.querySelector('#popupContainer')))
            .clickOutsideToClose(true)
            .title(title)
            .textContent(content)
            .ariaLabel('Alert Dialog Demo')
            .ok('Ok')
             );
        };

        $scope.imageIsLoaded = function (e) {
            $scope.$apply(function () {
                $scope.stepsModel.push(e.target.result);
                var imageSrc = e.target.result.split(',')[1];
                $scope.subFeeds.fileUrlI = imageSrc;
            });
        }

        $scope.imageUpload = function (element) {
            if (element.target.files.length > 0) {
                var reader = new FileReader();
                reader.onload = $scope.imageIsLoaded;
                reader.readAsDataURL(element.target.files[0]);
                $scope.subFeeds.fileNameI = element.target.files[0].name;
                $scope.subFeeds.fileTypeI = element.target.files[0].type;
                var handleFileSelect = function (element) {
                    var file = element.currentTarget.files[0];
                    var reader = new FileReader();
                    reader.onload = function (element) {
                        $scope.$apply(function ($scope) {
                            $scope.myImage = element.target.result;
                        });
                    };
                    reader.readAsDataURL(file);
                };
                angular.element(document.querySelector('#myImage')).on('change', handleFileSelect);
            }
        }

        $scope.audioIsLoaded = function (e) {
            $scope.$apply(function () {
                $scope.stepsModel.push(e.target.result);
                var audioSrc = e.target.result.split(',')[1];
                $scope.subFeeds.fileUrlA = audioSrc;
            });
        }

        $scope.audioUpload = function (element) {
            if (element.target.files.length > 0) {
                var reader = new FileReader();
                reader.onload = $scope.audioIsLoaded;
                reader.readAsDataURL(element.target.files[0]);
                $scope.subFeeds.fileNameA = element.target.files[0].name;
                $scope.subFeeds.fileTypeA = element.target.files[0].type;
            }
        }


        $scope.videoIsLoaded = function (e) {
            $scope.$apply(function () {
                $scope.stepsModel.push(e.target.result);
                var videoSrc = e.target.result.split(',')[1];
                $scope.subFeeds.fileUrlV = videoSrc;
            });
        }

        $scope.videoUpload = function (element) {
            if (element.target.files.length > 0) {
                var reader = new FileReader();
                reader.onload = $scope.videoIsLoaded;
                reader.readAsDataURL(element.target.files[0]);
                $scope.subFeeds.fileNameV = element.target.files[0].name;
                $scope.subFeeds.fileTypeV = element.target.files[0].type;
            }

        }

        $scope.num = 0;
        var audioElement = document.getElementById('audio');
        $scope.playMusic = function (index) {
            $scope.autoplay = $scope.musicCatelogs[index].audioPlayButton;
            if ($scope.musicCatelogs[index].songLink != null || $scope.musicCatelogs[index].songLink != undefined) {
                $scope.currentTrack = $scope.musicCatelogs[index].songLink;
            }
            $scope.currentIndex = index;
            audioElement.src = $scope.currentTrack;

            if ($scope.audioPaused) {
                audioElement.autoplay = false;
                $("#play_btn").show();
                $("#pause_btn").hide();
                $scope.audioPaused = false;
                $scope.musicCatelogs[index].isAudioPlaying = false;
            } else {
                audioElement.autoplay = $scope.autoplay;
                $scope.musicCatelogs[index].isAudioPlaying = true;
                $("#play_btn").hide();
                $("#pause_btn").show();
                $scope.audioPaused = true;
            }
        }



    }
})();