﻿/// <reference path="mainpage/editbiography.html" />
/// <reference path="mainpage/editbiography.js" />
var commonModule = angular.module('share',
    [
      'ngAnimate',        // animations
        'ngRoute',          // routing
        //'ngSanitize',       // sanitizes html bindings (ex: sidebar.js)
        'ngMaterial',
        // Custom modules
       'common',           // common functions, logger, spinner
       // 'common.bootstrap', // bootstrap dialog wrapper functions
       // // 3rd Party Modules        
       //'angular-loading-bar',
       // 'ui.bootstrap',      // ui-bootstrap (ex: carousel, pagination, dialog),
       // //'data-table',
       // 'md.data.table',
       // 'ngFileUpload',
       'ngImgCrop',
       // 'pascalprecht.translate',
       'infinite-scroll',
       'angularSoundManager',
       'paypal-button',
       'angularPayments',
       //'hm.readmore'
    ]);




var mainModule = angular.module('main', ['share']);

mainModule.filter('offset', function () {
    return function (input, start) {
        start = parseInt(start, 10);
        return input.slice(start);
    };
});

commonModule.factory('viewModelHelper', function ($http, $q, $window, $location) { return MyApp.viewModelHelper($http, $q, $window, $location); });

commonModule.factory('validator', function () { return valJs.validator(); });

mainModule.controller("indexViewModel",
function ($scope, $http, $q, $routeParams, $window, $location, viewModelHelper, $mdDialog, $rootScope, config) {

    var self = this;
    window.MyApp.$rootScope = $rootScope;
    $scope.categories = [];
    var basePath = "/";
    $scope.autoplay = false;

    $scope.userBiography = 'Not Added';
    $scope.userTags = [];
    $scope.keywordsList = [];
    $scope.keywords = [];
    $scope.categoriesLoaded = false;
    $scope.isBusy = false;
    $scope.customFullscreen = false;
   
    $scope.volume = 75;

    $scope.showSpinner = false;
    $scope.activated = false;


    $scope.busyMessage = "astala vista";
    $scope.spinnerOptions = {
        radius: 40,
        lines: 7,
        length: 0,
        width: 30,
        speed: 1.7,
        corners: 1.0,
        trail: 100,
        color: '#F58A00'
    };

    activate();
    var events = config.events;
    function toggleSpinner(on, msg) {
        $scope.isBusy = on;
        $scope.busyMessage = msg;
    }



    function activate() {

        var userId = '996d7e2f-280b-4d3d-9a57-dfde0df2a445';
        //$http.post(basePath + 'MainPage/getAudioList', '"' + userId + '"').then(function (data) {
        $http.post(basePath + 'MainPage/getAudioList').then(function (data) {
            $scope.audioList = data.data;
        });

        $http.post(basePath + 'MainPage/getUserTags').then(function (data) {
            $scope.userTags = data.data;
            if ($scope.userTags != undefined && $scope.userTags.userBiography != null)
                $scope.userBiography = $scope.userTags.userBiography;
        });
        $http.post(basePath + 'MainPage/getAllFeedDetails').then(function (data) {
            $scope.feedsList = data.data.postFeeds;
            $scope.userDetails = data.data.userDetails;
            if ($scope.userDetails != null || $scope.userDetails != undefined) {
                var date = new Date($scope.userDetails.registeredOn);
                var locale = "en-us";
                var rmonth = date.toLocaleString(locale, { month: "short" });
                var ryear = date.getFullYear();

                var DisplayDate = rmonth + " " + ryear;

                $scope.userDetails.registeredOn = DisplayDate;
            }

        });


    }

    function DialogController($scope, $mdDialog) {
        $scope.hide = function () {
            $mdDialog.hide();
        };
        $scope.cancel = function () {
            $mdDialog.cancel();
        };
        $scope.answer = function (answer) {

            $mdDialog.hide(answer);
        };
    };

    $scope.editAccountInfo = function (ev) {

        $mdDialog.show({
            controller: DialogController,
            templateUrl: '/app/mainpage/editAccountInfo.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: false,
        })
        .then(function (answer) {
            if (answer === 'Edit') {
                $scope.dialogBox("Success", "Your Account been updated successfully.");
                setTimeout(function () {
                    window.location.reload();
                }, 3000);
            } else if (answer === 'Cancel') {
                console.log("Cancelled");
            }

        }, function () {
            //$scope.dialogBox("Failure", "You")
            console.log("dialog Closed");
        });

    };


    $scope.editTags = function (ev) {

        $mdDialog.show({
            controller: DialogController,
            templateUrl: '/app/mainpage/editTags.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: false,
        })
        .then(function (answer) {
            if (answer === 'Edit') {
                $scope.dialogBox("Success", "Your Tags has been updated successfully.");
                setTimeout(function () {
                    window.location.reload();
                }, 3000);
            } else if (answer === 'Cancel') {
                console.log("Cancelled");
            }

        }, function () {
            //$scope.dialogBox("Failure", "You")
            console.log("dialog Closed");
        });
    };

    $scope.categories = function (ev) {

        $mdDialog.show({
            controller: DialogController,
            templateUrl: '/app/mainpage/editTags.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: false,
        })
        .then(function (answer) {
            if (answer === 'Edit') {
                $scope.dialogBox("Success", "categorie updated successfully.");
                setTimeout(function () {
                    window.location.reload();
                }, 3000);
            } else if (answer === 'Cancel') {
                console.log("Cancelled");
            }

        }, function () {
            //$scope.dialogBox("Failure", "You")
            console.log("dialog Closed");
        });
    };

    $scope.editProfile = function (ev) {
        $mdDialog.show({
            controller: DialogController,
            templateUrl: '/app/mainpage/editProfile.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: false,
        })
        .then(function (answer) {
            if (answer === 'Edit') {
                $scope.dialogBox("Success", "Your profile has been updated successfully.");
                setTimeout(function () {
                    window.location.reload();
                }, 3000);
            } else if (answer === 'Cancel') {
                console.log("Cancelled");
            }
        }, function () {
            //$scope.dialogBox("Failure", "You")
            console.log("dialog Closed");
        });
    };

    $scope.editSocialMedia = function (ev) {
        $mdDialog.show({
            controller: DialogController,
            templateUrl: '/app/mainpage/editSocialMedia.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: false,
        })
        .then(function (answer) {
            if (answer === 'Edit') {
                $scope.dialogBox("Success", "Your profile has been updated successfully.");
                setTimeout(function () {
                    window.location.reload();
                }, 3000);
            } else if (answer === 'Cancel') {
                console.log("Cancelled");
            }
        }, function () {
            //$scope.dialogBox("Failure", "You")
            console.log("dialog Closed");
        });
    };

    $scope.editEmailNotifications = function (ev) {
        $mdDialog.show({
            controller: DialogController,
            templateUrl: '/app/mainpage/emailNotifications.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: false,
        })
        .then(function (answer) {
            if (answer === 'Edit') {
                $scope.dialogBox("Success", "Your email notifications  has been updated successfully.");
                setTimeout(function () {
                    window.location.reload();
                }, 3000);
            } else if (answer === 'Cancel') {
                console.log("Cancelled");
            }
        }, function () {
            //$scope.dialogBox("Failure", "You")
            console.log("dialog Closed");
        });
    };

    $scope.editBiography = function (ev) {
        $mdDialog.show({
            controller: DialogController,
            templateUrl: '/app/mainpage/editBiography.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: false,
        })
        .then(function (answer) {
            if (answer === 'Edit') {
                $scope.dialogBox("Success", "Your profile has been updated successfully.");
                activate();
            } else if (answer === 'Cancel') {
                console.log("Cancelled");
            }

        }, function () {
            //$scope.dialogBox("Failure", "You")
            console.log("dialog Closed");
        });

    };

    $scope.editGallery = function (ev) {
        $mdDialog.show({
            controller: DialogController,
            templateUrl: '/app/mainpage/editGallery.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: false,
        })
        .then(function (answer) {
            if (answer === 'Edit') {
                $scope.dialogBox("Success", "Your Gallery has been updated successfully.");
                setTimeout(function () {
                    window.location.reload();
                }, 3000);
            } else if (answer === 'Cancel') {
                console.log("Cancelled");
            }

        }, function () {
            //$scope.dialogBox("Failure", "You")
            console.log("dialog Closed");
        });
    };

    $scope.editProfilepic = function (ev) {
        $mdDialog.show({
            controller: DialogController,
            templateUrl: '/app/mainpage/editProfilepic.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: false,
        })
        .then(function (answer) {
            if (answer === 'Edit') {
                $scope.dialogBox("Success", "Your profile pic has been updated successfully.");
                setTimeout(function () {
                    window.location.reload();
                }, 2000);
            } else if (answer === 'Cancel') {
                console.log("Cancelled");
            }
        }, function () {
            console.log("dialog Closed");
        });
    };

    $scope.getsubtofeed = function (ev) {
        window.location.href = "/mainpage/getsubtofeed";
    }
    $scope.getsubfromfeed = function (ev) {
        window.location.href = "/mainpage/getsubfromfeed";
    }

    var names = [];
    $('.editpic').on('change', '#edituserpic', function (event) {
        var $this = $(this);
        var files = event.target.files;
        if (files.length > 0) {
            $scope.showSpinner = true;
            $scope.activated = true;
            var file = files[0];
            names.push($this.get(0).files[0].name);
            if (file.type.match('image')) {
                var picReader = new FileReader();
                picReader.fileName = file.name
                picReader.addEventListener("load", function (event) {
                    var picFile = event.target;
                    $scope.userDetails.profileLink = picFile.result;
                   
                    $http({
                        method: 'POST',
                        url: basePath + 'MainPage/editProfilePic',
                        data: $scope.userDetails,
                        headers: "application/json"
                    })
                   .then(function (result) {
                       if (result) {
                           $scope.dialogBox("Success", "Your profile has been updated successfully.");
                           $scope.showSpinner = false;
                           $scope.activated = false;
                           setTimeout(function () {
                               window.location.reload();
                           }, 3000);
                       }
                   }, function () {
                       $scope.showSpinner = false;
                       $scope.activated = false;
                       $scope.dialogBox("Failure", "Failed! try again");
                   });
                });
            }
            picReader.readAsDataURL(file);
        }
    });


    $scope.viewMyFeeds = function (ev) {
        window.location.href = "/mainpage/myfeeds";
    }

    $scope.followedFeeds = function () {
        window.location.href = "/mainpage/followedFeeds";
    }

    $scope.savedFeeds = function () {
        window.location.href = "/mainpage/savedFeeds";
    }

    $scope.trendingFeeds = function () {
        window.location.href = "/admin/trendingFeeds";
    }

    $scope.featuredUsers = function () {
        window.location.href = "/admin/featuredUsers";
    }

    $scope.searchByUser = function (ev) {
        window.location.href = "/mainpage/searchByUser";
    }

    $scope.searchByFeed = function (ev) {
        window.location.href = "/mainpage/searchByFeed";
    }

    $scope.gohome = function () {
        window.location.href = "/";
    }

    $scope.gotoProfile = function () {
        window.location.href = "/mainpage";
    }


    $scope.dialogBox = function (title, content) {
        $mdDialog.show(
         $mdDialog.alert()
        .parent(angular.element(document.querySelector('#popupContainer')))
        .clickOutsideToClose(true)
        .title(title)
        .textContent(content)
        .ariaLabel('Alert Dialog Demo')
        .ok('Ok')
         );

    };

    $scope.num = 0;
    var audioElement = document.getElementById('audio');

    $scope.playMusic = function (index) {

        $scope.autoplay = true;
        $scope.currentTrack = $scope.audioList[index].audioLink;



        //$scope.changeVolume(audioElement);
        $scope.currentIndex = index;

        //audioElement.autoplay = $scope.autoplay;
        if (audioElement.src == $scope.currentTrack) {
            audioElement.src = $scope.currentTrack;
            $("#play_btn").hide();
            $("#pause_btn").show();
        } else {
            audioElement.src = $scope.currentTrack;
            if (!$scope.isAudioPlaying) {
                audioElement.autoplay = $scope.autoplay;
                $("#play_btn").hide();
                $("#pause_btn").show();
                $scope.audioPausable = true;
                $scope.isAudioPlaying = true;
            } else if ($scope.audioPausable) {
                audioElement.autoplay = $scope.autoplay;
                $("#play_btn").hide();
                $("#pause_btn").show();
                $scope.audioPausable = false;
                $scope.isAudioPlaying = true;
            } else {
                audioElement.autoplay = $scope.autoplay;
                $("#play_btn").hide();
                $("#pause_btn").show();
                $scope.audioPausable = true;
                $scope.isAudioPlaying = true;
            }

        }

        setTimeout(function () {
            setDuration();
        }, 1500);

    };

    function setDuration() {
        $scope.num = audioElement.duration * 1030;
        var num = Math.ceil($scope.num);
        setTimeout(function () {
            $scope.audioEnded()
        }, num);
    }

    $scope.audioEnded = function () {
        if (audioElement.ended) {
            $("#play_btn").show();
            $("#pause_btn").hide();
            $scope.num = 0;

        } else {
            $("#play_btn").hide();
            $("#pause_btn").show();
            $scope.num = 0;
        }

    };

    $scope.changeVolume = function () {
        audioElement.volume = $scope.volume / 100;
    }

    $scope.volumeMuteFull = function () {
        if (audioElement.volume != 0) {
            audioElement.volume = 0;
            $scope.volume = 0;
        } else {
            audioElement.volume = 1;
            $scope.volume = 100
        }
    }



    $scope.musicForwardBtn = function () {
        if ($scope.currentIndex >= 0 && $scope.currentIndex < $scope.audioList.length - 1) {
            $scope.currentIndex = $scope.currentIndex + 1;
            $scope.playMusic($scope.currentIndex);
        }
    }

    $scope.musicBackwardBtn = function () {
        if ($scope.currentIndex > 0 && $scope.currentIndex <= $scope.audioList.length - 1) {
            $scope.currentIndex = $scope.currentIndex - 1;
            $scope.playMusic($scope.currentIndex);
        }
    }



    $scope.gotoProfile1 = function () {

        window.location.href = "/mainpage";
    }

    $scope.gotoMainFeed1 = function () {
        window.location.href = "/mainpage/mainfeed";
    }

    $scope.gotoCustomise = function () {

        window.location.href = "/mainpage/customise";
    }

    $scope.gotoMessages = function () {

        window.location.href = "/mainpage/messages";
    }


    $scope.gotoMusicList = function () {
        window.location.href = "/mainpage/musicList";
    }

    function GetSiteRoot() {
        var rootPath = window.location.protocol + "//" + window.location.host + "/";
        //if (window.location.hostname == "localhost") {
        //    var path = window.location.pathname;
        //    if (path.indexOf("/") == 0) {
        //        path = path.substring(1);
        //    }
        //    path = path.split("/", 1);
        //    if (path != "") {
        //        rootPath = rootPath + path + "/";
        //    }
        //}
        return rootPath;
    }

    $scope.getProfileLink = function (firstName, lastName) {

        //window.location.href = "/mainpage";
        //alert(Name);
        var path = GetSiteRoot();

        path = path + "profile?ProfileName=" + firstName + '-' + lastName;
        $mdDialog.show(
             $mdDialog.alert()
            .parent(angular.element(document.querySelector('#popupContainer')))
            .clickOutsideToClose(true)
            .title('Profile Link')
            .textContent(path)
            .ariaLabel('Alert Dialog Demo')
            .ok('Ok')
             );
    }


    // $scope.manageProfile = function () {
    //   window.location.href = "/Manage";
    //}


    /**
     * Create filter function for a query string
     */




    $scope.redirectToAdminDashboard = function () {
        window.location.href = '/Admin';
    }

});
mainModule.config(['$qProvider', function ($qProvider) {
    $qProvider.errorOnUnhandledRejections(false);
}]);

(function (myApp) {
    var viewModelHelper = function ($http, $q, $window, $location) {

        var self = this;

        self.modelIsValid = true;
        self.modelErrors = [];

        self.resetModelErrors = function () {
            self.modelErrors = [];
            self.modelIsValid = true;
        }

        self.apiGetByParm = function (uri, params, success, failure, always) {
            self.modelIsValid = true;

            $http({
                method: 'GET',
                url: MyApp.rootPath + uri,
                params: params
            })
                .then(function (result) {
                    success(result);
                    if (always != null)
                        always();
                }, function (result) {
                    if (failure != null) {
                        failure(result);
                    }
                    else {
                        var errorMessage = result.status + ':' + result.statusText;
                        if (result.data != null && result.data.Message != null)
                            errorMessage += ' - ' + result.data.Message;
                        self.modelErrors = [errorMessage];
                        self.modelIsValid = false;
                    }
                    if (always != null)
                        always();
                });
        }

        self.apiGet = function (uri, success, failure, always) {
            self.modelIsValid = true;
            $http.get(MyApp.rootPath + uri)
                .then(function (result) {
                    success(result);
                    if (always != null)
                        always();
                }, function (result) {
                    if (failure != null) {
                        failure(result);
                    }
                    else {
                        var errorMessage = result.status + ':' + result.statusText;
                        if (result.data != null && result.data.Message != null)
                            errorMessage += ' - ' + result.data.Message;
                        self.modelErrors = [errorMessage];
                        self.modelIsValid = false;
                    }
                    if (always != null)
                        always();
                });
        }

        self.apiPost = function (uri, data, success, failure, always) {
            self.modelIsValid = true;
            $http.post(MyApp.rootPath + uri, data)
                .then(function (result) {
                    success(result);
                    if (always != null)
                        always();
                }, function (result) {
                    if (failure != null) {
                        failure(result);
                    }
                    else {
                        var errorMessage = result.status + ':' + result.statusText;
                        if (result.data != null && result.data.Message != null)
                            errorMessage += ' - ' + result.data.Message;
                        self.modelErrors = [errorMessage];
                        self.modelIsValid = false;
                    }
                    if (always != null)
                        always();
                });
        }

        self.goBack = function () {
            $window.history.back();
        }

        self.navigateTo = function (path) {

            $location.path(path);
        }

        self.refreshPage = function (path) {
            $window.location.href = MyApp.rootPath + path;
        }

        self.clone = function (obj) {
            return JSON.parse(JSON.stringify(obj))
        }

        return this;
    };
    myApp.viewModelHelper = viewModelHelper;
}(window.MyApp));
