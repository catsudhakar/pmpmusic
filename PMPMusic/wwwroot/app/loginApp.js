﻿/// <reference path="../../views/account/login.cshtml" />
(function () {
    'use strict';

    angular
        .module('main')
        .controller('loginHomeVeiwModel', loginHomeVeiwModel);



    loginHomeVeiwModel.$inject = ['$scope', '$http', '$mdDialog'];

    function loginHomeVeiwModel($scope, $http, $mdDialog, $window) {

        var basePath = "/";

        $scope.title = "loginHomeVeiwModel"
        $scope.busyMessage = "Testing";
        $scope.LoadPosts = 0;
        $scope.totalActiveFeeds = 0;
        $scope.totalActiveMainFeeds = 0;

        $scope.showSpinner = false;
        $scope.activated1 = false;
        $scope.activated = false;
        $scope.showSpinner1 = false;
        $scope.busy = false;
        $scope.musicTagsList = [];
        $scope.feedsList = [];
        $scope.feedsListPartial = [];
        $scope.searchMusicList = [];
        $scope.getSocialMediaLinks = [];
        $scope.subFeeds = [];
        $scope.featuredProfileuserData = [];
        $scope.selectedTag = [];

        $scope.dialogBox = function (title, content) {
            $mdDialog.show(
             $mdDialog.alert()
            .parent(angular.element(document.querySelector('#popupContainer')))
            .clickOutsideToClose(true)
            .title(title)
            .textContent(content)
            .ariaLabel('Alert Dialog Demo')
            .ok('Ok')
             );
        };

        activate();
        function activate(newPost) {
            $http.post(basePath + 'Home/getHomepageFeeds').then(function (data) {
                $scope.feedsList = data.data.postFeeds;
                $scope.totalActiveFeeds = data.data.postFeeds.length;
                $scope.totalActiveMainFeeds = data.data.totalFeeds;
                //Loading feeds partially (5 at each scroll)
                var condi = $scope.LoadPosts + 5;
                for (var i = $scope.LoadPosts; i < condi && i < $scope.totalActiveFeeds; i++) {
                    if ($scope.feedsList[i] != null || $scope.feedsList[i] != undefined) {
                        $scope.feedsList[i].songPlaying = false;
                        $scope.feedsListPartial.push($scope.feedsList[i]);
                    }
                    $scope.LoadPosts = i + 1;
                };
                $scope.activated1 = false;
                $scope.showSpinner1 = false;
                $scope.busy = false;

            }, function (error) {
                console.log(error, " can't get data.");
            });

            $http.post(basePath + 'MainPage/getSocialMedia').then(function (data) {
                $scope.getSocialMediaLinks = data.data;
            });

            $http.post(basePath + 'MainPage/userProfileData').then(function (data) {
                $scope.featuredProfileuserData = data.data;
            });
            $http.post(basePath + 'MainPage/getPlayerSongsList').then(function (data) {
                $scope.songs = data.data;
            });

            $http.get(basePath + 'countryList/musicTags.json').then(function (data) {
                $scope.musicTagsList = data.data;
            });

            //$http.post(basePath + 'MainPage/getTotalActiveFeeds?currentFeed=').then(function (data) {
            //    $scope.totalActiveMainFeeds = data.data.length;
            //});
        };

        var scroll = 0, wscroll = 0, temp = 0;
        $scope.loadMorePosts = function () {
            wscroll = $(window).scrollTop();
            scroll = wscroll - temp; temp = wscroll;
            if (scroll >= 150) {
                if ($scope.LoadPosts != 0) {
                    if ($scope.LoadPosts == $scope.totalActiveFeeds) {
                    } else {
                        $scope.activated1 = true;
                        $scope.busy = true;
                        $scope.showSpinner1 = true;
                        setTimeout(function () {
                            activate();
                        }, 3000);
                    }
                }
            }
        };



        function showLoginpage() {
            $mdDialog.show({
                controller: DialogController,
                templateUrl: '/app/beforeLogin/login.html',
                parent: angular.element(document.body),
                clickOutsideToClose: false,
                locals: {
                    commentsFeedId: ''
                }
            })
             .then(function (answer) {
                 if (answer === 'Edit') {
                     $scope.dialogBox("Success", "Your profile has been updated successfully.");
                 } else if (answer === 'Cancel') {
                     console.log("Cancelled");
                 }
             }, function () {
                 console.log("dialog Closed");
             });
        }


        function savedfeed(model) {
            $http({
                method: 'POST',
                url: '/MainPage/saveFeed',
                data: model
            }).then(function (response) {
                if (response.data == "saved") {
                    model.saveFeedStatus = 1;
                    $scope.dialogBox("Success", "This Feed has been saved successfully.");
                } else {
                    model.saveFeedStatus = 0;
                    $scope.dialogBox("Success", "This Feed has been unsaved successfully.");
                }
                //activate(true);
            }, function (error) {
                console.log(error, " can't get data.");
            });
        }


        function DialogController($scope, $mdDialog, commentsFeedId) {
            $scope.commentsFeedId = commentsFeedId;
            $scope.hide = function () {
                $mdDialog.hide();
            };
            $scope.cancel = function () {
                $mdDialog.cancel();
            };
            $scope.answer = function (answer) {
                $mdDialog.hide(answer);
            };
        };

        function MessageDialogController($scope, $mdDialog, messageFeedId, toUserId) {
            $scope.messageFeedId = messageFeedId;
            $scope.toUserId = toUserId;
            $scope.hide = function () {
                $mdDialog.hide();
            };
            $scope.cancel = function () {
                $mdDialog.cancel();
            };
            $scope.answer = function (answer) {
                $mdDialog.hide(answer);
            };
        };


        $scope.playMusic = function (feed) {
            var index = 0, feedid = feed.id, songid = '',
                newsongid = '', count = $scope.songs.length;;
            for (var item in $scope.songs) {
                songid = $scope.songs[item].id;
                if (songid != feedid) {
                    index = index + 1;
                }
                else {
                    newsongid = item;
                }
            }
            if (index == count) {
                var newSong = {};
                newSong.id = feed.id;
                newSong.title = feed.audioFileName;
                newSong.artist = feed.createdUserName;
                newSong.url = feed.audioLink;
                $scope.newSong = newSong;
                $scope.songs.push(newSong);
            }
            else {
                $scope.newSong = $scope.songs[Number(newsongid)];
            }
            setTimeout(function () {
                update();
            }, 1000);
        }

        function update() {
            for (var i = 0; i < $scope.feedsListPartial.length; i++) {
                if ($scope.feedsListPartial[i].id == $scope.newSong.id) {
                    $scope.feedsListPartial[i].songPlaying = true;
                }
                else {
                    $scope.feedsListPartial[i].songPlaying = false;
                }
            }
        }



        $scope.displayComments = function (feedId, index) {
            // alert(feedId);
            $mdDialog.show({
                controller: DialogController,
                templateUrl: '/app/beforeLogin/viewComments.html',
                parent: angular.element(document.body),
                //targetEvent: ev,
                clickOutsideToClose: true,

                locals: { commentsFeedId: angular.copy(feedId) }
            })
           .then(function (answer) {
               if (answer === 'Edit') {
                   $scope.dialogBox("Success", "Your profile has been updated successfully.");
                   //setTimeout(function () {
                   //    window.location.reload();
                   //}, 3000);
               } else if (answer === 'Cancel') {
                   console.log("Cancelled");
               }

           }, function () {
               //$scope.dialogBox("Failure", "You")
               console.log("dialog Closed");
           });
        }

        function isEmpty(val) {
            return (val === undefined || val == null || val.length <= 0) ? true : false;
        }

        function openMessageDialog(feedId, userId) {
            $mdDialog.show({
                controller: MessageDialogController,

                templateUrl: '/app/beforeLogin/sendMessage.html',
                parent: angular.element(document.body),
                //targetEvent: ev,
                clickOutsideToClose: false,
                locals: {
                    messageFeedId: angular.copy(feedId), toUserId: angular.copy(userId)
                }
            })
              .then(function (answer) {
                  if (answer === 'Edit') {
                      $scope.dialogBox("Success", "Your message has been sent successfully.");
                      //setTimeout(function () {
                      //    window.location.reload();
                      //}, 3000);
                  } else if (answer === 'Cancel') {
                      console.log("Cancelled");
                  }

              }, function () {
                  //$scope.dialogBox("Failure", "You")
                  console.log("dialog Closed");
              });
        }

        $scope.openLogin = function (username, feedId, index, type, userId, frmcomments) {
            $scope.iscomment = false;
            $scope.postComments = {};

            var value = isEmpty(username);
            //if (username !== undefined || username != "") {
            if (!value) {

                if (type == 'Post Comment') {
                    var comment = $('#comments' + index);
                    if (comment.val() === undefined || comment.val() == '') {
                        $scope.iscomment = true;
                        frmcomments.comments.$invalid = true;
                        return false;
                    }
                    else {
                        frmcomments.comments.$invalid = false;
                        $scope.iscomment = false;
                        $scope.postComments.postId = feedId
                        $scope.postComments.postComment = comment.val();

                        $http({
                            method: 'POST',
                            url: '/MainPage/AddNewComment',
                            data: $scope.postComments
                        }).then(function (response) {
                            comment.val('');
                            $scope.dialogBox("Success", "Your comment has been saved successfully.");
                        }, function (error) {
                            console.log(error, " can't get data.");
                        });
                    }
                }
                else if (type == 'Message') {

                    openMessageDialog(feedId, userId);
                }
                else if (type == 'Like Feed') {
                    $scope.likeFeeds = {};
                    $scope.likeFeeds.postId = feedId
                    $scope.likeFeeds.isLikes = $scope.feedsList[index].isLikes;

                    $http({
                        method: 'POST',
                        url: '/MainPage/likeFeed',
                        data: $scope.likeFeeds
                    }).then(function (response) {
                        //$scope.feedsList[index].isLike = false;
                        //$scope.dialogBox("Success", "Liked");

                        if (response.data == false) {
                            $scope.feedsList[index].isLikes = 0;
                            $scope.feedsList[index].likeCount = $scope.feedsList[index].likeCount - 1;
                        } else {
                            $scope.feedsList[index].isLikes = 1;
                            $scope.feedsList[index].likeCount = $scope.feedsList[index].likeCount + 1;
                        }
                    }).catch(function (error) {
                        console.log(error, " can't get data.");
                    });

                }
                else if (type == 'Follow') {
                    followFeed(userId, index);
                }
                else if (type == 'media') {
                    if (feedId != '#')
                        window.open(feedId, "_blank");
                    else {
                        $mdDialog.show($mdDialog.alert().title('alert message')
                        .textContent(userId + ' Social media link not added!')
                        .ok('OK').targetEvent(this));
                    }
                }
                else if (type == 'submitTofeed') {
                    submittothisfeed(feedId, userId, index);
                }
                else if (type == 'SAVE FEED') {
                    savedfeed(userId);
                }
            }
            else {
                showLoginpage();
            }

        }

        function submittothisfeed(feedId, userId, feedTitle) {
            $mdDialog.show({
                controller: DialogController,
                templateUrl: '/app/beforeLogin/submitTothisFeed.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                locals: { commentsFeedId: angular.copy(feedId + ',' + userId + ',' + feedTitle) }
            })
           .then(function (answer) {
               if (answer === 'Edit') {
               } else if (answer === 'Cancel') {
                   console.log("Cancelled");
               }
           }, function () {
               //$scope.dialogBox("Failure", "You")
           });
        }

        //$scope.openRegister = function () {
        //    // alert("Login PopUp");

        //    $mdDialog.show({
        //        controller: DialogController,
        //        templateUrl: '../Account/Register',
        //        parent: angular.element(document.body),
        //        //targetEvent: ev,
        //        clickOutsideToClose: false,

        //    })
        //  .then(function (answer) {
        //      if (answer === 'Edit') {
        //          $scope.dialogBox("Success", "Your profile has been updated successfully.");
        //          //setTimeout(function () {
        //          //    window.location.reload();
        //          //}, 3000);
        //      } else if (answer === 'Cancel') {
        //          console.log("Cancelled");
        //      }

        //  }, function () {
        //      //$scope.dialogBox("Failure", "You")
        //      console.log("dialog Closed");
        //  });

        //}

        function followFeed(userId, index) {
            //alert(feedDetails.title);
            // var title = $scope.feedsList[index].title;
            var feedFollow = $scope.feedsList[index];
            $http({
                method: 'POST',
                url: '/MainPage/FollowFeed?id=' + userId,
                data: userId
            }).then(function (response) {
                if (response.data == "followed") {
                    feedFollow.isFollow = 1;
                    $scope.dialogBox("Success", "success you are now following to " + feedFollow.createdUserName + ".");
                } else if (response.data == "unfollow") {
                    feedFollow.isFollow = 0;
                    $scope.dialogBox("", "You have unfollowed the username " + feedFollow.createdUserName + ".");
                }
            }, function (error) {
                console.log(error, " can't get data.");
            });
        }


        $scope.openRegister = function () {
            // alert("Login PopUp");

            $mdDialog.show({
                controller: DialogController,
                templateUrl: '/app/register/addTags.html',
                parent: angular.element(document.body),
                //targetEvent: ev,
                clickOutsideToClose: false,
                locals: {
                    commentsFeedId: ''
                }

            })
          .then(function (answer) {
              if (answer === 'Edit') {
                  $scope.dialogBox("Success", "Your profile has been updated successfully.");
                  //setTimeout(function () {
                  //    window.location.reload();
                  //}, 3000);
              } else if (answer === 'Cancel') {
                  console.log("Cancelled");
              }

          }, function () {
              //$scope.dialogBox("Failure", "You")
              console.log("dialog Closed");
          });

        }


        $scope.openBasicRegister = function () {
            // alert("Login PopUp");

            $mdDialog.show({
                controller: DialogController,
                templateUrl: '/app/register/basicSignUp.html',
                parent: angular.element(document.body),
                //targetEvent: ev,
                clickOutsideToClose: false,
                locals: {
                    commentsFeedId: ''
                }

            })
          .then(function (answer) {
              if (answer === 'Edit') {
                  $scope.dialogBox("Success", "Your profile has been updated successfully.");
                  //setTimeout(function () {
                  //    window.location.reload();
                  //}, 3000);
              } else if (answer === 'Cancel') {
                  console.log("Cancelled");
              }

          }, function () {
              //$scope.dialogBox("Failure", "You")
              console.log("dialog Closed");
          });

        }

        $scope.gotoMainpage = function () {
            window.location.href = "/mainpage";
        }

        $scope.removeTag = function (value) {

            $scope.selectedTag.splice($scope.selectedTag.indexOf(value), 1);

            $http.post(basePath + 'Home/getFeedsByTags', $scope.selectedTag)
              .then(function (data) {
                  $scope.searchMusicList = data.data;

                  searchListpaging();

              }).catch(function (error) {
                  console.log(error, " can't get data.");
              });
        }

        $scope.searchFeedsByTag = function (value) {
            if ($scope.selectedTag.length < 3) {
                for (var i = 0; i < $scope.selectedTag.length; i++) {
                    if ($scope.selectedTag[i] == value) {
                        return false;
                    }
                }
                //Find same tag
                $scope.selectedTag.push(value);
                //get feeds by tag
                $http.post(basePath + 'Home/getFeedsByTags', $scope.selectedTag)
               .then(function (data) {
                   $scope.searchMusicList = data.data;
                   searchListpaging();
               }).catch(function (error) {
                   console.log(error, " can't get data.");
               });
            }
            else {
                $scope.dialogBox("Alert", "Limit exceeded! only 3 search tags are allowed.");
            }
        }

        //-----------paging------------//
        $scope.FeedListPagep = [];
        $scope.feedsListPartialp = [];
        function searchListpaging() {
            $scope.FeedListPagep = GetPager($scope.searchMusicList.length);
            $scope.feedsListPartialp = $scope.searchMusicList.slice($scope.FeedListPagep.startIndex, $scope.FeedListPagep.endIndex + 1);
        }

        $scope.setPage = function (page) {
            if (page < 1 || page > $scope.FeedListPagep.totalPages) { return; }
            $scope.FeedListPagep = GetPager($scope.searchMusicList.length, page);
            $scope.feedsListPartialp = $scope.searchMusicList.slice($scope.FeedListPagep.startIndex, $scope.FeedListPagep.endIndex + 1);
        }

        $scope.getNumber = function (num) {
            return new Array(num);
        }

        function GetPager(totalItems, currentPage, pageSize) {
         
            currentPage = currentPage || 1;
            pageSize = pageSize || 6;

            var totalPages = Math.ceil(totalItems / pageSize);

            var startPage, endPage;
            if (totalPages <= 2) {
                startPage = 1;
                endPage = totalPages;
            } else {
                if (currentPage <= 6) {
                    startPage = 1;
                    endPage = 10;
                } else if (currentPage + 4 >= totalPages) {
                    startPage = totalPages - 9;
                    endPage = totalPages;
                } else {
                    startPage = currentPage - 5;
                    endPage = currentPage + 4;
                }
            }
            var startIndex = (currentPage - 1) * pageSize;
            var endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);
            return {
                totalItems: totalItems,
                currentPage: currentPage,
                pageSize: pageSize,
                totalPages: totalPages,
                startPage: startPage,
                endPage: endPage,
                startIndex: startIndex,
                endIndex: endIndex
            };
        }
        //-----------paging------------//

        //var names = [];
        //$('body').unbind("change").on('change', '#picupload', function (event) {
        //    var $this = $(this);
        //    var ulid = $this.data('ulid');
        //    var value = isEmpty(ulid);
        //    if (!value) {
        //        var files = event.target.files;
        //        if (files.length > 0) {
        //            for (var i = 0; i < files.length; i++) {
        //                var file = files[i];
        //                names.push($this.get(0).files[i].name);
        //                if (file.type.match('image') || file.type.match('audio') || file.type.match('video')) {
        //                    var picReader = new FileReader();
        //                    picReader.fileName = file.name;
        //                    picReader.addEventListener("load", function (event) {
        //                        var picFile = event.target;
        //                        var fid = $this.data('fid'); var cid = $this.data('cid');
        //                        $scope.subFeeds = {};
        //                        $scope.subFeeds.fileName = picFile.fileName;
        //                        $scope.subFeeds.fileUrl = picFile.result;
        //                        $scope.subFeeds.fileType = file.type;
        //                        $scope.subFeeds.createdBy = cid;
        //                        $scope.subFeeds.mainFeedId = fid;
        //                        $http({
        //                            method: 'POST', url: basePath + 'MainPage/addsubfeed',
        //                            data: $scope.subFeeds, headers: "application/json"
        //                        })
        //                       .then(function (result) {
        //                           if (result) {
        //                               $scope.dialogBox("Success", result.data);
        //                           }
        //                       }, function () {
        //                           $scope.dialogBox("Failure", "Failed! try again");
        //                       });
        //                    });
        //                }
        //                else {
        //                    $scope.dialogBox("Alert", "choose Video/Audio/Image file only");
        //                }
        //                picReader.readAsDataURL(file);
        //            }
        //            console.log(names);
        //        }
        //    } else {
        //        showLoginpage();
        //    }
        //});
    }
})();

//var loginModule = angular.module('main', ['share']);

