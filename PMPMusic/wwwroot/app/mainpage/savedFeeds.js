﻿
(function () {
    'use strict';

    angular
        .module('mainpage')
        .controller('saveFeeds', saveFeeds);

    saveFeeds.$inject = ['$scope', '$location', '$http', 'viewModelHelper', '$mdDialog', '$timeout'];

    function saveFeeds($scope, $location, $http, viewModelHelper, $mdDialog, $timeout) {

        $scope.title = 'Saved Feeds';
        var basePath = "/";

        $scope.postFeed = {};
        $scope.stepsModel = [];
        $scope.feedsList = [];

        $scope.showSpinner = false;
        $scope.activated = false;

        $scope.activated1 = false;
        $scope.showSpinner1 = false;
        $scope.busy = false;

        $scope.isCommentSubmit = false;
        $scope.isSubmit = false;
        $scope.autoplay = false;
        $scope.audioPlayButton = true;
        $scope.audioPaused = false;

        $scope.totalActiveFeeds = 0;
        $scope.LoadPosts = 0;

        $scope.postComments = {};

        $scope.feedsListPartial = [];
        $scope.getSocialMediaLinks = [];

        activate();

        function activate(newPost) {
            $http.post(basePath + 'MainPage/getSavedFeeds').then(function (data) {
                $scope.feedsList = data.data.savedFeedDetails;
                $scope.totalActiveFeeds = data.data.savedFeedDetails.length;
                for (var i = 0; i < $scope.totalActiveFeeds; i++) {
                    if (!$scope.feedsList[i].audioLink) {
                        $scope.autoplay = false;
                        $scope.feedsList[i].audioPlayButton = $scope.autoplay;
                        $scope.feedsList[i].noAudioLink = false;
                        $scope.feedsList[i].isAudioPlaying = false;
                        $scope.feedsList[i].playBtnImg = "playBtnImg" + i;
                    } else {
                        $scope.autoplay = true;
                        $scope.feedsList[i].audioPlayButton = $scope.autoplay;
                        $scope.feedsList[i].noAudioLink = true;
                        $scope.feedsList[i].isAudioPlaying = false;
                        $scope.feedsList[i].playBtnImg = "playBtnImg" + i;
                    }
                }
                getsavedfeedrecords();
            });
            getSocialmedialinks();
        }

        function getsavedfeedrecords() {
            var initial = $scope.LoadPosts;
            var condi = initial + 5;
            for (var i = initial; i < condi && i < $scope.totalActiveFeeds; i++) {
                if ($scope.feedsList[i] != null || $scope.feedsList[i] != undefined) {
                    $scope.feedsListPartial.push($scope.feedsList[i]);
                }
                initial = i + 1;
                $scope.LoadPosts = initial;
            };
            loderImageShow(false, 1);
        }

        $scope.loadMorePosts = function () {
            if ($scope.LoadPosts != 0) {
                if ($scope.LoadPosts == $scope.totalActiveFeeds) {
                } else {
                    loderImageShow(true, 1);
                    $timeout(function () {
                        getsavedfeedrecords();
                    }, 1000);
                }
            }
        };

        function loderImageShow(bool, num) {
            if (num == 1) {
                $scope.activated1 = bool;
                $scope.showSpinner1 = bool;
            } else {
                $scope.activated = bool;
                $scope.showSpinner = bool;
            }
            $scope.busy = bool;
        }

        function getSocialmedialinks() {
            $http.post(basePath + 'MainPage/getSocialMedia').then(function (data) {
                $scope.getSocialMediaLinks = data.data;
            });
        }

        $scope.dialogBox = function (title, content) {
            $mdDialog.show(
             $mdDialog.alert()
            .parent(angular.element(document.querySelector('#popupContainer')))
            .clickOutsideToClose(true)
            .title(title)
            .textContent(content)
            .ariaLabel('Alert Dialog Demo')
            .ok('Ok')
             );
        };

        $scope.feedLinks = function (feedId, index, type, userId) {
            $scope.iscomment = false;
            $scope.postComments = {};

            if (type == 'Message') {
                openMessageDialog(feedId, userId);
            }
            else if (type == 'Like Feed') {
                likeFeeds(feedId, index, userId);
            }
            else if (type == 'media') {
                if (feedId != '#')
                    window.open(feedId, "_blank");
                else {
                    $mdDialog.show($mdDialog.alert().title('alert message')
                    .textContent(userId + ' Social media link not added!')
                    .ok('OK').targetEvent(this));
                }
            }
            else if (type == 'submitTofeed') {
                submittothisfeed(feedId, userId, index);
            }
        }

        function submittothisfeed(feedId, userId, feedTitle) {
            $mdDialog.show({
                controller: DialogController,
                templateUrl: '/app/beforeLogin/submitTothisFeed.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                locals: { commentsFeedId: angular.copy(feedId + ',' + userId + ',' + feedTitle) }
            })
           .then(function (answer) {
               if (answer === 'Edit') {
               } else if (answer === 'Cancel') {
                   console.log("Cancelled");
               }
           }, function () {
               //$scope.dialogBox("Failure", "You")
           });
        }

        function DialogController($scope, $mdDialog, commentsFeedId) {
            $scope.commentsFeedId = commentsFeedId;
            $scope.hide = function () {
                $mdDialog.hide();
            };
            $scope.cancel = function () {
                $mdDialog.cancel();
            };
            $scope.answer = function (answer) {
                $mdDialog.hide(answer);
            };
        };

        $scope.num = 0;
        var audioElement = document.getElementById('audio');
        $scope.playMusic = function (index) {
            $scope.autoplay = $scope.feedsList[index].audioPlayButton;
            var playBtnImage = document.getElementById($scope.feedsList[index].playBtnImg);
            if ($scope.feedsList[index].audioLink != null || $scope.feedsList[index].audioLink != undefined) {
                $scope.currentTrack = $scope.feedsList[index].audioLink;
            }
            $scope.currentIndex = index;
            audioElement.src = $scope.currentTrack;
            if ($scope.audioPaused) {
                audioElement.autoplay = false;
                $("#play_btn").show();
                $("#pause_btn").hide();
                $scope.audioPaused = false;
                $scope.feedsList[index].isAudioPlaying = false;
            } else {
                audioElement.autoplay = $scope.autoplay;
                $scope.feedsList[index].isAudioPlaying = true;
                $("#play_btn").hide();
                $("#pause_btn").show();
                $scope.audioPaused = true;
            }
        }
    }
})();
