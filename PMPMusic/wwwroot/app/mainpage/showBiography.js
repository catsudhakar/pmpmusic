﻿
(function () {
    'use strict';

    angular
        .module('mainpage')
        .controller('showBiography', showBiography);

    showBiography.$inject = ['$scope', '$http', '$mdDialog', '$sce'];

    function showBiography($scope, $http, $mdDialog, $sce) {
        $scope.title = 'User Biography';
        var basePath = '/';
               
        activate();

        function activate() {
            if ($scope.message != null) {

                $scope.videoSrc = $scope.message;

            }
        }
    }
})();

