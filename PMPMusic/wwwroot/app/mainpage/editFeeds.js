﻿(function () {
    'use strict';
    angular
        .module('mainpage')
        .controller('editFeeds', editFeeds);

    editFeeds.$inject = ['$scope', '$http', '$mdDialog'];

    function editFeeds($scope, $http, $mdDialog) {

        var basePath = '/';

        $scope.userProfile = [];
        $scope.stepsModel = [];
        $scope.postFeed = {};

        $scope.isSubmit = false;
        $scope.showSpinner = false;
        $scope.activated = false;

        $scope.title = 'Edit Your Feed';
        $scope.myImage = '';
        $scope.myCroppedImage = '';
        activate();

        function activate() {
            var fedId = $scope.editFeedIds;
            $http.get(basePath + 'MainPage/getFeed?feedId=' + fedId).then(function (data) {
                $scope.postFeed = data.data;
                $scope.editFeedIds = '';
            });
        }

        $scope.imageIsLoaded = function (e) {
            $scope.$apply(function () {
                $scope.stepsModel.push(e.target.result);
                var imageSrc = e.target.result.split(',')[1];
                $scope.postFeed.imageLink = imageSrc;
            });
        }

        $scope.imageUpload = function (element) {
            if (element.target.files.length > 0) {
                var reader = new FileReader();
                reader.onload = $scope.imageIsLoaded;
                reader.readAsDataURL(element.target.files[0]);
                $scope.postFeed.imageName = element.target.files[0].name;
                $scope.postFeed.imageType = element.target.files[0].type;
                var handleFileSelect = function (element) {
                    var file = element.currentTarget.files[0];
                    var reader = new FileReader();
                    reader.onload = function (element) {
                        $scope.$apply(function ($scope) {
                            $scope.myImage = element.target.result;
                        });
                    };
                    reader.readAsDataURL(file);
                };
                angular.element(document.querySelector('#myImage')).on('change', handleFileSelect);
            }
        }

        $scope.audioUpload = function (element) {
            if (element.target.files.length > 0) {
                var reader = new FileReader();
                reader.onload = $scope.audioIsLoaded;
                reader.readAsDataURL(element.target.files[0]);
                $scope.postFeed.audioName = element.target.files[0].name;
                $scope.postFeed.audioType = element.target.files[0].type;
            }
        }

        $scope.audioIsLoaded = function (e) {
            $scope.$apply(function () {
                $scope.stepsModel.push(e.target.result);
                var audioSrc = e.target.result.split(',')[1];
                $scope.postFeed.audioLink = audioSrc;
            });
        }


        $scope.videoupload = function (element) {
            if (element.target.files.length > 0) {
                var reader = new FileReader();
                reader.onload = $scope.videoIsLoaded;
                reader.readAsDataURL(element.target.files[0]);
                $scope.postFeed.videoType = element.target.files[0].type;
                $scope.postFeed.videoName = element.target.files[0].name;
            }
        }

        $scope.videoIsLoaded = function (e) {
            $scope.$apply(function () {
                $scope.stepsModel.push(e.target.result);
                var videoSrc = e.target.result.split(',')[1];
                $scope.postFeed.videoLink = videoSrc;
            });
        }

        $scope.editpostFeed = function (formEditFeed) {
            var imgLink=$scope.postFeed.imageLink;
            if (!imgLink.includes("https://")) {
                $scope.postFeed.imageLink=$scope.myCroppedImage.split(',')[1];
            }
            $scope.isSubmit = true;
            $scope.activated = true;
            if (formEditFeed.$valid) {
                $scope.showSpinner = true;
                $http({
                    method: 'POST',
                    url: basePath + 'MainPage/editFeed',
                    data: $scope.postFeed,
                    headers: "application/json"
                })
               .then(function (result) {
                   if (result) {
                       $mdDialog.hide("Edit");
                       $scope.activated = false;
                       $scope.showSpinner = false;
                   }
                   else {
                       $mdDialog.hide("Cancel");
                   }
               });
            }
        };
    }
})();
