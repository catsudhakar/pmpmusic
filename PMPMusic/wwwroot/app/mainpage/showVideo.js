﻿(function () {
    'use strict';

    angular
        .module('mainpage')
        .controller('showVideo', showVideo);

    showVideo.$inject = ['$scope', '$http', '$mdDialog', '$sce'];

    function showVideo($scope, $http, $mdDialog, $sce) {
        $scope.title = 'Show Video';
        var basePath = '/';

        $scope.trustSrc = function (src) {
            return $sce.trustAsResourceUrl(src);
        }

        $scope.movie = { src: "http://www.youtube.com/embed/Lx7ycjC8qjE", title: "Egghead.io AngularJS Binding" };


        activate();

        function activate() {
            if ($scope.message != null) {
              
                $scope.videoSrc = $scope.message.videoLink;
              
            } 
        }
    }
})();
