﻿(function () {
    'use strict';
    angular
        .module('main')
        .controller('editSocialMedia', editSocialMedia);

    editSocialMedia.$inject = ['$scope', '$http', '$mdDialog'];

    function editSocialMedia($scope, $http, $mdDialog) {

        var basePath = '/';

        $scope.isSubmit = false;
       
        $scope.stepsModel = [];
        $scope.showSpinner = false;
        $scope.activated = false;
        $scope.SocialMedia = {};
        

        $scope.title = 'Add Social Media Details';
        activate();
        function activate() {
            //Media names
            // $scope.socialMediaNames = [{ "name": "Facebook", "code": "facebook" }, { "name": "Twitter", "code": "twitter" }, { "name": "Google+", "code": "google+" }, { "name": "Flag", "code": "flag" }];
            $scope.socialMediaNames = [{ "name": "Facebook", "code": "facebook" }, { "name": "Twitter", "code": "twitter" }, { "name": "Google+", "code": "google+" }];
        }


        $scope.editSocialMedia = function (frmRegister) {
            $scope.isSubmit = true;
            $scope.activated = true;
            if (frmRegister.$valid && $scope.SocialMedia.MediaName != undefined && $scope.SocialMedia.MediaName != 'Select Social Media') {
                $scope.showSpinner = true;
                $http({
                    method: 'POST',
                    url: basePath + 'MainPage/editSocialMedia',
                    data: $scope.SocialMedia,
                    headers: "application/json"
                })
               .then(function (result) {
                   if (result) {
                       $mdDialog.hide("Edit");
                       $scope.activated = false;
                       $scope.showSpinner = false;
                   }
                   else {
                       $mdDialog.hide("Cancel");
                   }
               });
            }
        };
        $scope.selectChanged = function () {
            var mediaNames = $scope.SocialMedia.MediaName
            if (mediaNames != undefined && mediaNames != 'Select Social Media') {
                $http({
                    method: 'POST',
                    url: basePath + 'MainPage/checkSocialMedia',
                    data: $scope.SocialMedia,
                    headers: "application/json"
                })
               .then(function (result) {
                   if (result) {
                       if (result.data != null)
                           $scope.SocialMedia.mediaLink = result.data.mediaLink;
                       else
                           $scope.SocialMedia.mediaLink = '';
                   }
                   else {
                       $scope.SocialMedia.mediaLink = '';
                   }
               });
            }
            else
                $scope.SocialMedia.mediaLink = '';
        };
    }
})();