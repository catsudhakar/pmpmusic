﻿(function () {
    'use strict';

    angular
        .module('main')
        .controller('editTags', editTags);

    editTags.$inject = ['$scope', '$http', '$mdDialog'];

    function editTags($scope, $http, $mdDialog) {

        var basePath = '/';

        $scope.isSubmit = false;

        $scope.userTags = [];
        $scope.stepsModel = [];
        $scope.showSpinner = false;
        $scope.activated = false;

        $scope.title = 'Add / Edit Categories';

        activate();

        function activate() {

            $http({
                method: 'POST',
                url: basePath + 'MainPage/getUserTags',
                data: $scope.userTags,
                headers: "application/json"
            }).then(function (data) {
                $scope.userTags = data.data;


            });
            $scope.SelectTagList = ['PRODUCER', 'ARTISTS', 'DJ;S', 'ENGINEER', 'SONGWRITER', 'GRAPHIC DESIGNER', 'MUSICIAN', 'MANAGEMENT', 'RECORD LABEL', 'VIDEOGRAPHER'];
            for (var i = 0; i < $scope.SelectTagList.length; i++) {
                if ($scope.userTags.mainTag == $scope.SelectTagList[i]) {
                    $scope.userTags.mainTag = $scope.SelectTagList[i];
                } else if ($scope.userTags.tag1 == $scope.SelectTagList[i]) {
                    $scope.userTags.tag1 = $scope.SelectTagList[i];
                } else if ($scope.userTags.tag2 == $scope.SelectTagList[i]) {
                    $scope.userTags.tag2 = $scope.SelectTagList[i];
                } else if ($scope.userTags.tag3 == $scope.SelectTagList[i]) {
                    $scope.userTags.tag3 = $scope.SelectTagList[i];
                }

            }

        }





        $scope.editTags = function (frmRegister) {
            $scope.isSubmit = true;
            $scope.activated = true;
            if (frmRegister.$valid) {
                $scope.showSpinner = true;

                $http({
                    method: 'POST',
                    url: basePath + 'MainPage/addUserTags',
                    data: $scope.userTags,
                    headers: "application/json"
                }).then(function (result) {
                    if (result) {
                        $mdDialog.hide("Edit");
                        $scope.activated = false;
                        $scope.showSpinner = false;
                    }
                    else {
                        $mdDialog.hide("Cancel");
                        $scope.activated = false;
                        $scope.showSpinner = false;
                    }
                });
            };
        }

    }
})();
