﻿(function () {
    'use strict';

    angular
        .module('mainpage')
        .controller('mainLayout', mainLayout);

    mainLayout.$inject = ['$scope']; 

    function mainLayout($scope) {
        $scope.title = 'mainLayout';

        activate();

        function activate() { }
    }
})();
