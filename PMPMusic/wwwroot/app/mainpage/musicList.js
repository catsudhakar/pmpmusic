﻿(function () {
    'use strict';

    angular
        .module('mainpage')
        .controller('musicList', musicList);

    musicList.$inject = ['$scope']; 

    function musicList($scope) {
        $scope.title = 'musicList';

        activate();

        function activate() { }
    }
})();
