﻿(function () {
    'use strict';
    angular
        .module('main')
        .controller('editAccountInfo', editAccountInfo);

    editAccountInfo.$inject = ['$scope', '$http', '$mdDialog'];

    function editAccountInfo($scope, $http, $mdDialog) {

        var basePath = '/';

        $scope.isSubmit = false;

        $scope.userProfile = [];
        $scope.stepsModel = [];
        $scope.showSpinner = false;
        $scope.activated = false;

        $scope.title = 'Add / Edit Account Details';

        activate();

        function activate() {
            
            $http({
                method: 'POST',
                url: basePath + 'MainPage/editProfile',
                data: $scope.userProfile,
                headers: "application/json"
            }).then(function (data) {
                $scope.userProfile = data.data;
                if ($scope.userProfile != null && $scope.userProfile!=undefined)
                    $scope.userProfile.confirmPassword = $scope.userProfile.password;
            });
        }

        $scope.checkmail = function (email) {
            $scope.isFocus = false;
            if (email !== undefined || email != null || email.length > 0) {
                $http({
                    method: 'POST',
                    url: basePath + 'Account/doesEmailExist?Email=' + email,
                    headers: "application/json"
                }).then(function (data) {
                    if (data.data == 'User exist with this email') {
                        $scope.isFocus = true;
                    }
                }, function (error) {
                    console.log(error, " can't get data.");

                });
            }
        }

        $scope.editAccountInfo = function (frmRegister) {
            $scope.isSubmit = true;
            $scope.activated = true;
            if (frmRegister.$valid && $scope.userProfile.password == $scope.userProfile.confirmPassword) {
                $scope.showSpinner = true;
                $http({
                    method: 'POST',
                    url: basePath + 'MainPage/editAccountInfo',
                    data: $scope.userProfile,
                    headers: "application/json"
                })
               .then(function (result) {
                   if (result) {
                       debugger;
                       $mdDialog.hide("Edit");
                       $scope.activated = false;
                       $scope.showSpinner = false;
                   }
                   else {
                       $mdDialog.hide("Cancel");
                   }
               });
            }
        };
    }
})();
