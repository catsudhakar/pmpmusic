﻿(function () {
    'use strict';
    angular
        .module('main')
        .controller('editProfile', editProfile);

    editProfile.$inject = ['$scope', '$http', '$mdDialog'];

    function editProfile($scope, $http, $mdDialog) {

        var basePath = '/';

        $scope.isSubmit = false;

        $scope.userProfile = [];
        $scope.stepsModel = [];
        $scope.showSpinner = false;
        $scope.activated = false;

        $scope.title = 'Add / Edit Profile Details';

        activate();

        function activate() {
            $http.get(basePath + 'countryList/countryList.json').then(function (data) {
                $scope.SelectCountryList = data.data;

            });

            $http({
                method: 'POST',
                url: basePath + 'MainPage/editProfile',
                data: $scope.userProfile,
                headers: "application/json"
            }).then(function (data) {
                debugger;
                $scope.userProfile = data.data;
            });

        }

        $scope.imageIsLoaded = function (e) {
            $scope.$apply(function () {
                // alert(e.target.result)
                $scope.stepsModel.push(e.target.result);
                var imageSrc = e.target.result.split(',')[1];
                //alert(imageSrc);
                $scope.userProfile.imageSrc = imageSrc;

                //$scope.postFeed.imageType = reader.readAsDataURL(element.target.files[0].type);
            });
        }

        $scope.imageUpload = function (element) {
            if (element.target.files.length > 0) {
                var reader = new FileReader();
                reader.onload = $scope.imageIsLoaded;
                //console.log(element.target.files[0])
                reader.readAsDataURL(element.target.files[0]);
                $scope.userProfile.imageFileName = element.target.files[0].name;
                $scope.userProfile.imageType = element.target.files[0].type;
                readURL(element.target);
            }
        }

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function () {
                    $('#blah')
                        .attr('src', reader.result)
                        .width(150)
                        .height(200);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        $scope.editProfile = function (frmRegister) {
            $scope.isSubmit = true;
            $scope.activated = true;
            if (frmRegister.$valid) {
                $scope.showSpinner = true;
                $http({
                    method: 'POST',
                    url: basePath + 'MainPage/editProfile',
                    data: $scope.userProfile,
                    headers: "application/json"
                })
               .then(function (result) {
                   if (result) {
                       $mdDialog.hide("Edit");
                       $scope.activated = false;
                       $scope.showSpinner = false;
                   }
                   else {
                       $mdDialog.hide("Cancel");
                   }
               });
            }
        };
    }
})();
