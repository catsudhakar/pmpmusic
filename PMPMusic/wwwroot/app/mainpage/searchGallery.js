﻿(function () {
    'use strict';

    angular
        .module('mainpage')
        .controller('searchGallery', searchGallery);

    searchGallery.$inject = ['$scope', '$http', '$mdDialog'];

    function searchGallery($scope, $http, $mdDialog) {
        var basePath = '/';
        $scope.isSubmit = false;
        $scope.getGallery = {};
        $scope.UserGallery = {};
        $scope.FileName = "";

        $scope.showSpinner = false;
        $scope.activated = false;
        $scope.showSpinner1 = false;
        $scope.activated1 = false;

        $scope.title = 'User Gallery';

        activate();
        function activate() {
            $scope.UserGallery = {};
            $scope.getGallery = $scope.message;
        }
    }
})();
