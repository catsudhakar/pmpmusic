﻿/// <reference path="editfeeds.js" />
/// <reference path="editbiography.js" />
/// <reference path="editbiography.html" />
/// <reference path="editfeeds.js" />
/// <reference path="editfeeds.js" />
/// <reference path="editfeeds.html" />
/// <reference path="editfeeds.js" />
/// <reference path="editfeeds.html" />
/// <reference path="editfeeds.js" />
/// <reference path="editfeeds.html" />
/// <reference path="editfeeds.js" />
(function () {
    'use strict';

    angular
        .module('mainpage')
        .controller('viewMyFeeds', viewMyFeeds);

    viewMyFeeds.$inject = ['$scope', '$location', '$http', 'viewModelHelper', '$mdDialog', '$timeout'];

    function viewMyFeeds($scope, $location, $http, viewModelHelper, $mdDialog, $timeout) {

        $scope.title = 'View My Feed';
        var basePath = "/";
        $scope.isSubmit = false;
        $scope.autoplay = false;
        $scope.audioPlayButton = true;
        $scope.audioPaused = false;
        $scope.activated = false;
        $scope.postFeed = {};
        $scope.stepsModel = [];
        $scope.feedsList = [];
        $scope.showSpinner = false;
        $scope.postComments = {};
        $scope.isCommentSubmit = false;
        $scope.totalActiveFeeds = 0;

        $scope.activated1 = false;
        $scope.showSpinner1 = false;

        $scope.feedsListPartial = [];
        $scope.getSocialMediaLinks = [];
        $scope.LoadPosts = 0;
        $scope.busy = false;

        activate();
        function activate(newPost) {

            $http.post(basePath + 'MainPage/getFeeds').then(function (data) {
                $scope.feedsList = data.data;
                $scope.totalActiveFeeds = data.data.length;
                for (var i = 0; i < $scope.totalActiveFeeds; i++) {
                    if (!$scope.feedsList[i].audioLink) {
                        $scope.autoplay = false;
                        $scope.feedsList[i].audioPlayButton = $scope.autoplay;
                        $scope.feedsList[i].noAudioLink = false;
                        $scope.feedsList[i].isAudioPlaying = false;
                        $scope.feedsList[i].playBtnImg = "playBtnImg" + i;
                    } else {
                        $scope.autoplay = true;
                        $scope.feedsList[i].audioPlayButton = $scope.autoplay;
                        $scope.feedsList[i].noAudioLink = true;
                        $scope.feedsList[i].isAudioPlaying = false;
                        $scope.feedsList[i].playBtnImg = "playBtnImg" + i;
                    }
                }
                getmyfeedrecords();
            });
            getSocialmedialinks();
        }

        function getmyfeedrecords() {
            var initial = $scope.LoadPosts;
            var condi = initial + 5;
            for (var i = initial; i < condi && i < $scope.totalActiveFeeds; i++) {
                if ($scope.feedsList[i] != null || $scope.feedsList[i] != undefined) {
                    $scope.feedsListPartial.push($scope.feedsList[i]);
                }
                initial = i + 1;
                $scope.LoadPosts = initial;
            };
            loderImageShow(false, 1);
        }

        $scope.loadMorePosts = function () {
            if ($scope.LoadPosts != 0) {
                if ($scope.LoadPosts == $scope.totalActiveFeeds) {
                } else {
                    loderImageShow(true, 1);
                    $timeout(function () {
                        getmyfeedrecords();
                    }, 2000);
                }
            }
        };

        function getSocialmedialinks() {
            $http.post(basePath + 'MainPage/getSocialMedia').then(function (data) {
                $scope.getSocialMediaLinks = data.data;
            });
        }

        function loderImageShow(bool, num) {
            if (num == 1) {
                $scope.activated1 = bool;
                $scope.showSpinner1 = bool;
            } else {
                $scope.activated = bool;
                $scope.showSpinner = bool;
            }
            $scope.busy = bool;
        }

        $scope.editFeeds = function (ev, feedId) {
            $mdDialog.show({
                controller: DialogController,
                templateUrl: '/app/mainpage/editFeeds.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: false,
                locals: { editFeedId: angular.copy(feedId) }
            })
            .then(function (answer) {
                if (answer === 'Edit') {
                    $scope.dialogBox("Success", "Your Feed has been updated successfully.");
                    setTimeout(function () {
                        window.location.reload();
                    }, 2000);
                } else if (answer === 'Cancel') {
                    console.log("Cancelled");
                }
            }, function () {
                console.log("dialog Closed");
            });
        };

        $scope.showConfirm = function (ev, feedId) {
           
            var confirm = $mdDialog.confirm()
                  .title('Would you like to delete your feed?')
                  .ariaLabel('Lucky day').targetEvent(ev)
                  .ok('CONFIRM').cancel('CANCEL');
            $mdDialog.show(confirm).then(function () {

                $http({
                    method: 'POST',
                    url: '/MainPage/deleteFeed?FeedId=' + feedId,
                    data: feedId
                }).then(function (response) {
                    if (response.data == "success") {
                        $scope.dialogBox("", "Feed deleted successfully.");
                       setTimeout(function () {
                           window.location.reload();
                        }, 2000);
                    } else if (response.data == "unfollow") {
                        $scope.dialogBox("", "Feed deletion failed! ");
                    }
                }, function (error) {
                    console.log(error, " can't get data.");
                });

            }, function () {
                console.log(error, " can't get data.");
            });

        };

        $scope.deleteFeeds = function (ev, feedId) {
            $scope.showConfirm(ev, feedId);
        }

        function DialogController($scope, $mdDialog, editFeedId) {
            $scope.editFeedIds = editFeedId;
            $scope.hide = function () {
                $mdDialog.hide();
            };
            $scope.cancel = function () {
                $mdDialog.cancel();
            };
            $scope.answer = function (answer) {
                $mdDialog.hide(answer);
            };
        };

        function DialogControllerSTF($scope, $mdDialog, commentsFeedId) {
            $scope.commentsFeedId = commentsFeedId;
            $scope.hide = function () {
                $mdDialog.hide();
            };
            $scope.cancel = function () {
                $mdDialog.cancel();
            };
            $scope.answer = function (answer) {
                $mdDialog.hide(answer);
            };
        };


        $scope.imageIsLoaded = function (e) {
            $scope.$apply(function () {
                $scope.stepsModel.push(e.target.result);
                var imageSrc = e.target.result.split(',')[1];
                $scope.postFeed.imageSrc = imageSrc;
            });
        }

        $scope.imageUpload = function (element) {
            if (element.target.files.length > 0) {
                var reader = new FileReader();
                reader.onload = $scope.imageIsLoaded;
                reader.readAsDataURL(element.target.files[0]);
                $scope.postFeed.imageFileName = element.target.files[0].name;
                $scope.postFeed.imageType = element.target.files[0].type;
            }
        }

        $scope.audioIsLoaded = function (e) {
            $scope.$apply(function () {
                $scope.stepsModel.push(e.target.result);
                var audioSrc = e.target.result.split(',')[1];
                $scope.postFeed.audioSrc = audioSrc;
            });
        }

        $scope.audioUpload = function (element) {
            if (element.target.files.length > 0) {
                var reader = new FileReader();
                reader.onload = $scope.audioIsLoaded;
                reader.readAsDataURL(element.target.files[0]);
                $scope.postFeed.audioType = element.target.files[0].type;
                $scope.postFeed.audioFileName = element.target.files[0].name;
            }
        }


        $scope.videoIsLoaded = function (e) {
            $scope.$apply(function () {
                $scope.stepsModel.push(e.target.result);
                var videoSrc = e.target.result.split(',')[1];
                $scope.postFeed.videoSrc = videoSrc;
            });
        }

        $scope.videoUpload = function (element) {
            if (element.target.files.length > 0) {
                var reader = new FileReader();
                reader.onload = $scope.videoIsLoaded;
                reader.readAsDataURL(element.target.files[0]);
                $scope.postFeed.videoType = element.target.files[0].type;
                $scope.postFeed.videoFileName = element.target.files[0].name;
            }

        }

        $scope.followFeed = function (feedDetails) {
            $http({
                method: 'POST',
                url: '/MainPage/FollowFeed?id=' + feedDetails.createdBy,
                data: feedDetails.createdBy
            }).then(function (response) {
                if (response.data == "followed") {
                    feedDetails.isFollow = 1;
                    $scope.dialogBox("Success", "success you are now following to " + feedDetails.createdUserName + ".");
                } else if (response.data == "unfollow") {
                    feedDetails.isFollow = 0;
                    $scope.dialogBox("", "You have unfollowed the username " + feedDetails.createdUserName + ".");
                }
            }, function (error) {
                console.log(error, " can't get data.");
            });
        }

        $scope.feedLinks = function (feedId, index, type, userId) {
            $scope.iscomment = false;
            $scope.postComments = {};

            if (type == 'Message') {
                openMessageDialog(feedId, userId);
            }
            else if (type == 'Like Feed') {
                likeFeeds(feedId, index, userId);
            }
            else if (type == 'media') {
                if (feedId != '#')
                    window.open(feedId, "_blank");
                else {
                    $mdDialog.show($mdDialog.alert().title('alert message')
                    .textContent(userId + ' Social media link not added!')
                    .ok('OK').targetEvent(this));
                }
            }
            else if (type == 'submitTofeed') {
                submittothisfeed(feedId, userId, index);
            }
        }

        function submittothisfeed(feedId, userId, feedTitle) {
            $mdDialog.show({
                controller: DialogControllerSTF,
                templateUrl: '/app/beforeLogin/submitTothisFeed.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                locals: { commentsFeedId: angular.copy(feedId + ',' + userId + ',' + feedTitle) }
            })
           .then(function (answer) {
               if (answer === 'Edit') {
               } else if (answer === 'Cancel') {
                   console.log("Cancelled");
               }
           }, function () {
               //$scope.dialogBox("Failure", "You")
           });
        }

        $scope.SaveComment = function (frmComment, index) {
            $scope.isCommentSubmit = true;
            $scope.postComments.postId = $scope.feedsList[index].id
            $scope.postComments.postComment = $scope.feedsList[index].Comment;
            $scope.postComments.CreatedBy = '996d7e2f-280b-4d3d-9a57-dfde0df2a445';

            if (frmComment.$valid) {

                $http({
                    method: 'POST',
                    url: '/MainPage/AddNewComment',
                    data: $scope.postComments
                }).then(function (response) {
                    $scope.feedsList[index].Comment = "";
                    frmComment.Comment.$setUntouched();
                    $scope.dialogBox("Success", "commented successfully.");
                }, function (error) {
                    $scope.dialogBox("Failed !", "commented Failed.");
                });
            }
            else {
                frmComment.Comment.$setTouched();
            }
        }
        $scope.num = 0;
        var audioElement = document.getElementById('audio');

        $scope.playMusic = function (index) {
            $scope.autoplay = $scope.feedsList[index].audioPlayButton;
            var playBtnImage = document.getElementById($scope.feedsList[index].playBtnImg);

            if ($scope.feedsList[index].audioLink != null || $scope.feedsList[index].audioLink != undefined) {
                $scope.currentTrack = $scope.feedsList[index].audioLink;
            }
            $scope.currentIndex = index;
            audioElement.src = $scope.currentTrack;
            if ($scope.audioPaused) {
                audioElement.autoplay = false;
                $("#play_btn").show();
                $("#pause_btn").hide();
                $scope.audioPaused = false;
                $scope.feedsList[index].isAudioPlaying = false;
            } else {
                audioElement.autoplay = $scope.autoplay;
                $scope.feedsList[index].isAudioPlaying = true;
                $("#play_btn").hide();
                $("#pause_btn").show();
                $scope.audioPaused = true;
            }
        }

        $scope.savedFeed = function (model) {
            if (model.saveFeed == true) {
                model.saveFeed = false;
            } else {
                model.saveFeed = true;
            }
            $http({
                method: 'POST',
                url: '/MainPage/saveFeed',
                data: model
            }).then(function (response) {
                if (response.data.saveFeed == true) {
                    $scope.dialogBox("Success", "This Feed has been saved successfully.");
                } else {
                    $scope.dialogBox("Success", "This Feed has been unsaved successfully.");
                }

                activate(true);

            }, function (error) {
                console.log(error, " can't get data.");
            });
        }

        $scope.PostFeed = function (formPostFeed) {
            $scope.isSubmit = true;
            if (formPostFeed.$valid) {
                $http({
                    method: 'POST',
                    url: 'MainPage/AddNewFeed',
                    data: $scope.postFeed
                })
                    .success(function (data) {
                        // $mdDialog.hide(data);
                        alert("sucesss");
                    }).error(function () {

                    });
            }
        }
        $scope.dialogBox = function (title, content) {
            $mdDialog.show(
             $mdDialog.alert()
            .parent(angular.element(document.querySelector('#popupContainer')))
            .clickOutsideToClose(true)
            .title(title)
            .textContent(content)
            .ariaLabel('Alert Dialog Demo')
            .ok('Ok')
             );
        };


    }
})();



