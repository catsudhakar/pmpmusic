﻿(function () {
    'use strict';

    angular
        .module('mainpage')
        .controller('mainfeed', mainfeed);

    mainfeed.$inject = ['$scope', '$location', '$http', 'viewModelHelper', '$mdDialog', '$timeout'];

    function mainfeed($scope, $location, $http, viewModelHelper, $mdDialog, $timeout) {

        $scope.title = 'mainfeed';
        var basePath = "/";
        $scope.isSubmit = false;
        $scope.autoplay = false;
        $scope.audioPlayButton = true;
        $scope.audioPaused = false;
        $scope.activated = false;
        $scope.postFeed = {};
        $scope.stepsModel = [];
        $scope.feedsList = [];
        $scope.showSpinner = false;
        $scope.postComments = {};
        $scope.isCommentSubmit = false;
        $scope.totalActiveFeeds = 0;

        $scope.activated1 = false;
        $scope.showSpinner1 = false;

        $scope.feedsListPartial = [];
        $scope.getSocialMediaLinks = [];
        $scope.LoadPosts = 0;
        $scope.busy = false;
        $scope.currentFeed = '';

        $scope.showVideo = false;

        $scope.isValid = false;

        activate();
        //$scope.loadMorePosts();
        function activate(newPost) {
            Initialvalues();
            loderImageShow(true, 1);
            $http.post(basePath + 'MainPage/getTotalActiveFeeds?currentFeed=' + $scope.currentFeed).then(function (data) {
                $scope.feedsList = data.data;
                $scope.totalActiveFeeds = data.data.length;
                for (var i = 0; i < $scope.totalActiveFeeds; i++) {
                    if (!$scope.feedsList[i].audioLink) {
                        $scope.autoplay = false;
                        $scope.feedsList[i].audioPlayButton = $scope.autoplay;
                        $scope.feedsList[i].noAudioLink = false;
                        $scope.feedsList[i].isAudioPlaying = false;
                        $scope.feedsList[i].playBtnImg = "playBtnImg" + i;
                    } else {
                        $scope.autoplay = true;
                        $scope.feedsList[i].audioPlayButton = $scope.autoplay;
                        $scope.feedsList[i].noAudioLink = true;
                        $scope.feedsList[i].isAudioPlaying = false;
                        $scope.feedsList[i].playBtnImg = "playBtnImg" + i;
                    }
                }
                //Loading feeds partially (5 at each scroll)
                getmainfeedrecords();
            });
            getSocialmedialinks();
        }

        function Initialvalues() {
            $scope.feedsListPartial = [];
            $scope.feedsList = [];
            $scope.totalActiveFeeds = 0;
            $scope.LoadPosts = 0;
        }

        function getmainfeedrecords() {
            var condi = $scope.LoadPosts + 5;
            for (var i = $scope.LoadPosts; i < condi && i < $scope.totalActiveFeeds; i++) {
                if ($scope.feedsList[i] != null || $scope.feedsList[i] != undefined) {
                    $scope.feedsListPartial.push($scope.feedsList[i]);
                }
                $scope.LoadPosts = i + 1;
            };
            loderImageShow(false, 1);
        }



        function getSocialmedialinks() {
            $http.post(basePath + 'MainPage/getSocialMedia').then(function (data) {
                $scope.getSocialMediaLinks = data.data;
            });
        }

        $scope.loadMorePosts = function () {
            if ($scope.LoadPosts != 0) {
                if ($scope.LoadPosts == $scope.totalActiveFeeds) {

                } else {
                    loderImageShow(true, 1);
                    $timeout(function () {
                        getmainfeedrecords();
                    }, 1000);
                }

            }
        };

        function loderImageShow(bool, num) {
            if (num == 1) {
                $scope.activated1 = bool;
                $scope.showSpinner1 = bool;
            } else {
                $scope.activated = bool;
                $scope.showSpinner = bool;
            }
            $scope.busy = bool;
        }


        $scope.imageIsLoaded = function (e) {
            $scope.$apply(function () {
                $scope.stepsModel.push(e.target.result);
                var imageSrc = e.target.result.split(',')[1];
                $scope.postFeed.imageSrc = imageSrc;
            });
        }

        $scope.imageUpload = function (element) {
            if (element.target.files.length > 0) {
                var reader = new FileReader();
                reader.onload = $scope.imageIsLoaded;
                reader.readAsDataURL(element.target.files[0]);
                $scope.postFeed.imageFileName = element.target.files[0].name;
                $scope.postFeed.imageType = element.target.files[0].type;
            }
        }

        $scope.audioIsLoaded = function (e) {
            $scope.$apply(function () {
                $scope.stepsModel.push(e.target.result);
                var audioSrc = e.target.result.split(',')[1];
                $scope.postFeed.audioSrc = audioSrc;
            });
        }

        $scope.audioUpload = function (element) {
            if (element.target.files.length > 0) {
                var reader = new FileReader();
                reader.onload = $scope.audioIsLoaded;
                reader.readAsDataURL(element.target.files[0]);
                $scope.postFeed.audioType = element.target.files[0].type;
                $scope.postFeed.audioFileName = element.target.files[0].name;
            }
        }


        $scope.videoIsLoaded = function (e) {
            $scope.$apply(function () {
                $scope.stepsModel.push(e.target.result);
                var videoSrc = e.target.result.split(',')[1];
                $scope.postFeed.videoSrc = videoSrc;
            });
        }

        $scope.videoUpload = function (element) {
            if (element.target.files.length > 0) {
                var reader = new FileReader();
                reader.onload = $scope.videoIsLoaded;
                reader.readAsDataURL(element.target.files[0]);
                $scope.postFeed.videoType = element.target.files[0].type;
                $scope.postFeed.videoFileName = element.target.files[0].name;
            }

        }

        $scope.followFeed = function (feedDetails) {
            $http({
                method: 'POST',
                url: '/MainPage/FollowFeed?id=' + feedDetails.createdBy,
                data: feedDetails.createdBy
            }).then(function (response) {
                if (response.data == "followed") {
                    feedDetails.isFollow = 1;
                    $scope.dialogBox("Success", "success you are now following to " + feedDetails.createdUserName + ".");
                } else if (response.data == "unfollow") {
                    feedDetails.isFollow = 0;
                    $scope.dialogBox("", "You have unfollowed the username " + feedDetails.createdUserName + ".");
                }
                bindfollowrecords(response.data, feedDetails.createdBy);
            }, function (error) {

                console.log(error, " can't get data.");
            });
        }


        function bindfollowrecords(response, userId) {
            for (var i = 0; i < $scope.feedsList.length; i++) {
                if ($scope.feedsList[i].createdBy == userId) {
                    var len = $scope.feedsListPartial.length;
                    if (response == "followed") {
                        if (len > i)
                            $scope.feedsListPartial[i].isFollow = 1;
                        $scope.feedsList[i].isFollow = 1;
                    }
                    else if (response == "unfollow") {
                        if (len > i)
                            $scope.feedsListPartial[i].isFollow = 0;
                        $scope.feedsList[i].isFollow = 0;
                    }
                }
            }
        }

        function videoValidations(url) {
            //alert(url);
            //var regYoutube = '/^.*((youtu.be/)|(v/)|(/u/w/)|(embed/)|(watch?))??v?=?([^#&?]*).*/';
            //var regVimeo = '/^.*(vimeo.com/)((channels/[A-z]+/)|(groups/[A-z]+/videos/))?([0-9]+)/';
            //var regDailymotion = '/^.+dailymotion.com/(video|hub)/([^_]+)[^#]*(#video=([^_&]+))?/';
            //var regMetacafe = '/^.*(metacafe.com)(/watch/)(d+)(.*)/i';

            $scope.validString = '';

            var youtubeUrl = url.match(/watch\?v=([a-zA-Z0-9\-_]+)/);
            var vimeoUrl = url.match(/^http:\/\/(www\.)?vimeo\.com\/(clip\:)?(\d+).*$/);
            var dailyUrl = url.match(/^.+dailymotion.com\/(video|hub)\/([^_]+)[^#]*(#video=([^_&]+))?/);

            // var MetacafeUrl = /^.*(metacafe.com)\/(watch)\/(d+)(.*)/i;

            //http://www.dailymotion.com/video/x5x6ecf
            //https://www.youtube.com/watch?v=hS5CfP8n_js
            //https://vimeo.com/229856130
            //http://www.metacafe.com/

            if (youtubeUrl != null) {
                var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=|\?v=)([^#\&\?]*).*/;
                var match = url.match(regExp);
                if (match && match[2].length == 11) {
                    // Do anything for being valid
                    // if need to change the url to embed url then use below line
                    $scope.validString = 'https://www.youtube.com/embed/' + match[2];
                    return true;
                }

            } else if (vimeoUrl != null) {

            }
            else if (dailyUrl != null) {


            } //else if (MetacafeUrl != null) {

                //}
            else {
                return false;
            }

        }

        function matchYoutubeUrl(url) {
            var yUrl = /^(?:https?:\/\/)?(?:m\.|www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
            if (url.match(yUrl)) {
                return $scope.isValid = true;
            }
            return $scope.isValid = false;
        }

        $scope.SaveFeed = function (formPostFeed) {

            $scope.isSubmit = true;
            $scope.isValid = true;
            var vUrl = $('#videoUrl').val();
            if (vUrl != undefined) {
                matchYoutubeUrl(vUrl);
                if ($scope.isValid == false) {
                    return false;
                }
            }
            
            if (formPostFeed.$valid && $scope.isValid) {
                loderImageShow(true, 0);
                $http({
                    method: 'POST',
                    url: '/MainPage/AddNewFeed',
                    data: $scope.postFeed
                }).then(function (response) {
                    console.log(response, 'res');
                    loderImageShow(false, 0);
                    $scope.isSubmit = false;

                    $scope.dialogBox("Success", "Your feed has been posted successfully.");
                    $scope.currentFeed = 'current';
                    activate(true);
                    $scope.postFeed = {};
                    formPostFeed.$setPristine();
                    formPostFeed.$setUntouched();

                }, function (error) {
                    console.log(error, " can't get data.");
                    loderImageShow(false, 0);
                    $scope.isSubmit = false;
                    $scope.dialogBox("Failure", "Something went wrong.");
                    formPostFeed.$setPristine();
                    formPostFeed.$setUntouched();
                });
            }
        }

        $scope.SaveComment = function (frmComment, index) {

            $scope.isCommentSubmit = true;
            $scope.postComments.postId = $scope.feedsList[index].id
            $scope.postComments.postComment = $scope.feedsList[index].Comment;
            $scope.postComments.CreatedBy = '996d7e2f-280b-4d3d-9a57-dfde0df2a445';
            if (frmComment.$valid) {
                $http({
                    method: 'POST',
                    url: '/MainPage/AddNewComment',
                    data: $scope.postComments
                }).then(function (response) {
                    $scope.feedsList[index].Comment = "";
                    frmComment.Comment.$setUntouched();
                    $scope.dialogBox("Success", "commented successfully.");
                }, function (error) {
                    $scope.dialogBox("Failed !", "commented Failed.");
                });
            }
            else {
                frmComment.Comment.$setTouched();
            }
        }

        $scope.num = 0;
        var audioElement = document.getElementById('audio');
        $scope.playMusic = function (index) {
            $scope.autoplay = $scope.feedsList[index].audioPlayButton;
            if ($scope.feedsList[index].audioLink != null || $scope.feedsList[index].audioLink != undefined) {
                $scope.currentTrack = $scope.feedsList[index].audioLink;
            }
            $scope.currentIndex = index;
            audioElement.src = $scope.currentTrack;

            if ($scope.audioPaused) {
                audioElement.autoplay = false;
                $("#play_btn").show();
                $("#pause_btn").hide();
                $scope.audioPaused = false;
                $scope.feedsList[index].isAudioPlaying = false;
            } else {
                audioElement.autoplay = $scope.autoplay;
                $scope.feedsList[index].isAudioPlaying = true;
                $("#play_btn").hide();
                $("#pause_btn").show();
                $scope.audioPaused = true;
            }
        }

        function setDuration(i) {
            $scope.num = audioElement.duration * 1030;
            var num = Math.ceil($scope.num);
            setTimeout(function () {
                $scope.audioEnded(i)
            }, num);
        }

        $scope.audioEnded = function (i) {
            var playBtnImage = document.getElementById($scope.feedsList[i].playBtnImg);

            if ($scope.feedsList[i].isAudioPlaying != true) {
                playBtnImage.src = "../img/bh_img_12_pause.png";
            } else {
                playBtnImage.src = "../img/bh_img_12.png";
            }
            if (audioElement.ended) {
                $("#play_btn").show();
                $("#pause_btn").hide();
                $scope.num = 0;

            } else {
                $("#play_btn").hide();
                $("#pause_btn").show();
                $scope.num = 0;
            }

        };



        $scope.savedFeed = function (model) {

            $http({
                method: 'POST',
                url: '/MainPage/saveFeed',
                data: model
            }).then(function (response) {
                if (response.data == "saved") {
                    model.saveFeedStatus = 1;
                    $scope.dialogBox("Success", "This Feed has been saved successfully.");
                } else {
                    model.saveFeedStatus = 0;
                    $scope.dialogBox("Success", "This Feed has been unsaved successfully.");
                }
                //activate(true);
            }, function (error) {
                console.log(error, " can't get data.");
            });
        }

        $scope.PostFeed = function (formPostFeed) {
            $scope.isSubmit = true;
            if (formPostFeed.$valid) {
                $http({
                    method: 'POST',
                    url: 'MainPage/AddNewFeed',
                    data: $scope.postFeed
                })
                    .success(function (data) {
                        alert("sucesss");
                    }).error(function () {
                    });
            }
        }

        $scope.dialogBox = function (title, content) {
            $mdDialog.show(
             $mdDialog.alert()
            .parent(angular.element(document.querySelector('#popupContainer')))
            .clickOutsideToClose(true)
            .title(title)
            .textContent(content)
            .ariaLabel('Alert Dialog Demo')
            .ok('Ok')
             );
        };

        $scope.displayComments = function (feedId, index) {
            $mdDialog.show({
                controller: DialogController,
                templateUrl: '/app/beforeLogin/viewComments.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                locals: { commentsFeedId: angular.copy(feedId) }
            })
           .then(function (answer) {
               if (answer === 'Edit') {
                   //$scope.dialogBox("Success", "");
               } else if (answer === 'Cancel') {
                   console.log("Cancelled");
               }
           }, function () {
               console.log("dialog Closed");
           });
        }

        function DialogController($scope, $mdDialog, commentsFeedId) {
            $scope.commentsFeedId = commentsFeedId;
            $scope.hide = function () {
                $mdDialog.hide();
            };
            $scope.cancel = function () {
                $mdDialog.cancel();
            };
            $scope.answer = function (answer) {
                $mdDialog.hide(answer);
            };
        };

        function MessageDialogController($scope, $mdDialog, messageFeedId, toUserId) {
            $scope.messageFeedId = messageFeedId;
            $scope.toUserId = toUserId;
            $scope.hide = function () {
                $mdDialog.hide();
            };
            $scope.cancel = function () {
                $mdDialog.cancel();
            };
            $scope.answer = function (answer) {
                $mdDialog.hide(answer);
            };
        };

        $scope.feedLinks = function (feedId, index, type, userId) {
            $scope.iscomment = false;
            $scope.postComments = {};

            if (type == 'Message') {
                openMessageDialog(feedId, userId);
            }
            else if (type == 'Like Feed') {
                likeFeeds(feedId, index, userId);
            }
            else if (type == 'media') {
                if (feedId != '#')
                    window.open(feedId, "_blank");
                else {
                    $mdDialog.show($mdDialog.alert().title('alert message')
                    .textContent(userId + ' Social media link not added!')
                    .ok('OK').targetEvent(this));
                }
            }
            else if (type == 'submitTofeed') {
                submittothisfeed(feedId, userId, index);
            }
        }

        function likeFeeds(feedId, index, userId) {
            $scope.likeFeeds = {};
            $scope.likeFeeds.postId = feedId
            $scope.likeFeeds.isLikes = $scope.feedsList[index].isLikes;
            $http({
                method: 'POST',
                url: '/MainPage/likeFeed',
                data: $scope.likeFeeds
            }).then(function (response) {
                if (response.data == true) {
                    $scope.feedsList[index].isLikes = 1;
                    $scope.feedsList[index].likeCount = $scope.feedsList[index].likeCount + 1;
                } else {
                    $scope.feedsList[index].isLikes = 0;
                    $scope.feedsList[index].likeCount = $scope.feedsList[index].likeCount - 1;
                }
            }).catch(function (error) {
                console.log(error, " can't get data.");
            });
        }

        function submittothisfeed(feedId, userId, feedTitle) {
            $mdDialog.show({
                controller: DialogController,
                templateUrl: '/app/beforeLogin/submitTothisFeed.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                locals: { commentsFeedId: angular.copy(feedId + ',' + userId + ',' + feedTitle) }
            })
           .then(function (answer) {
               if (answer === 'Edit') {
               } else if (answer === 'Cancel') {
                   console.log("Cancelled");
               }
           }, function () {
               //$scope.dialogBox("Failure", "You")
           });
        }

        function openMessageDialog(feedId, userId) {
            $mdDialog.show({
                controller: MessageDialogController,
                templateUrl: '/app/beforeLogin/sendMessage.html',
                parent: angular.element(document.body),
                clickOutsideToClose: false,
                locals: {
                    messageFeedId: angular.copy(feedId), toUserId: angular.copy(userId)
                }
            })
              .then(function (answer) {
                  if (answer === 'Edit') {
                      $scope.dialogBox("Success", "Your message has been sent successfully.");
                  } else if (answer === 'Cancel') {
                      console.log("Cancelled");
                  }
              }, function () {
                  console.log("dialog Closed");
              });
        }

        $scope.displayVideo = function () {
            if ($scope.showVideo == false) {
                $scope.showVideo = true;
            }
            else {
                $scope.showVideo = false;
            }
        }

    }
})();
