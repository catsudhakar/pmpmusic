﻿

(function () {
    'use strict';
    angular
        .module('mainpage')
        .controller('editMusicCatalog', editMusicCatalog);

    editMusicCatalog.$inject = ['$scope', '$http', '$mdDialog'];

    function editMusicCatalog($scope, $http, $mdDialog) {

        var basePath = '/';

        $scope.musictagErrorMessage = "You need to select at least one tag";

        $scope.isSubmit = false;
        $scope.isSubmit1 = false;
        $scope.userMusicCatalog = {};
        $scope.stepsModel = [];
        $scope.showSpinner = false;
        $scope.activated = false;
        $scope.userMusicCatalogList = [];
        $scope.songToDelete = {};
        $scope.iseditable = false;


        activate();

        function activate() {

            if ($scope.message != null) {
                $scope.title = "Track :: " + $scope.message.catalogName;
                $scope.userMusicCatalog = $scope.message;
                $scope.userMusicCatalog.newCatalogName = $scope.userMusicCatalog.catalogName;
               
            } else {
                $scope.title = 'Add Music Track';
            }

            $http.get(basePath + 'countryList/musicTags.json').then(function (data) {
                $scope.musicTagsList = data.data;
            });

            //$http({
            //    method: 'POST',
            //    url: basePath + 'MainPage/getUserMusicCatalogs',
            //    data: $scope.userMusicCatalog,
            //    headers: "application/json"
            //}).then(function (data) {
            //    $scope.userMusicCatalogList = data.data;
            //    for (var i = 0; i < $scope.userMusicCatalogList.length; i++) {
            //        if (!$scope.userMusicCatalogList[i].songLink) {
            //            $scope.autoplay = false;
            //            $scope.userMusicCatalogList[i].audioPlayButton = $scope.autoplay;
            //            $scope.userMusicCatalogList[i].noAudioLink = false;
            //            $scope.userMusicCatalogList[i].isAudioPlaying = false;
            //            $scope.userMusicCatalogList[i].playBtnImg = "playBtnImg" + i;

            //        } else {
            //            $scope.autoplay = true;
            //            $scope.userMusicCatalogList[i].audioPlayButton = $scope.autoplay;
            //            $scope.userMusicCatalogList[i].noAudioLink = true;
            //            $scope.userMusicCatalogList[i].isAudioPlaying = false;
            //            $scope.userMusicCatalogList[i].playBtnImg = "playBtnImg" + i;

            //        }

            //    }
            //});

        }

        $scope.AddToPlayerList = function (catalogId, songLink, songName) {
            debugger;

            $scope.activated = true;
            $scope.showSpinner = true;
            $scope.userMusicCatalog = {};
            $scope.userMusicCatalog.catalogId = catalogId;
            $scope.userMusicCatalog.songLink = songLink;
            $scope.userMusicCatalog.songName = songName;
            $http({
                method: 'POST',
                url: basePath + 'MainPage/addSongsToPlayer',
                data: $scope.userMusicCatalog,
                headers: "application/json"
            }).then(function (answer) {
                if (answer) {
                    // $mdDialog.hide("Edit");
                    $scope.dialogBox("Succes", songName + " " + answer.data);
                    $scope.activated = false;
                    $scope.showSpinner = false;
                } else {
                    //$mdDialog.hide("cancel");
                }
            }, function () {
                console.log("dialog Closed");
            });

        }

        $scope.imageIsLoaded = function (e) {
            $scope.$apply(function () {
                // alert(e.target.result)
                $scope.stepsModel.push(e.target.result);
                var imageSrc = e.target.result.split(',')[1];
                //alert(imageSrc);
                $scope.userMusicCatalog.imageSrc = imageSrc;

                //$scope.postFeed.imageType = reader.readAsDataURL(element.target.files[0].type);

                //$scope.showSpinner = true;
                //$http({
                //    method: 'POST',
                //    url: basePath + 'MainPage/editMusicCatalog',
                //    data: $scope.userMusicCatalog,
                //    headers: "application/json"
                //}).then(function (answer) {
                //    if (answer) {
                //        $mdDialog.hide("Edit");
                //        $scope.activated = false;
                //        $scope.showSpinner = false;
                //        activate();
                //    } else {
                //        $mdDialog.hide("cancel");
                //    }

                //}, function () {
                //    //$scope.dialogBox("Failure", "You")
                //    console.log("dialog Closed");
                //});
            });
        }

        $scope.imageUpload = function (element) {
            debugger;
            if (element.target.files.length > 0) {
                var reader = new FileReader();
                reader.onload = $scope.imageIsLoaded;
                //console.log(element.target.files[0])
                reader.readAsDataURL(element.target.files[0]);

                if ($scope.userMusicCatalog.imageName === element.target.files[0].name) {
                    $scope.userMusicCatalog.imageName = element.target.files[0].name;

                } else {
                    $scope.userMusicCatalog.imageName = element.target.files[0].name;
                    $scope.userMusicCatalog.imageName1 = element.target.files[0].name;
                }
                $scope.userMusicCatalog.imageType = element.target.files[0].type;




                readURL(element.target);
            }
        }


        $scope.audioIsLoaded = function (e) {
            $scope.$apply(function () {
                // alert(e.target.result)
                $scope.stepsModel.push(e.target.result);
                var audioSrc = e.target.result.split(',')[1];
                //alert(imageSrc);
                $scope.userMusicCatalog.audioSrc = audioSrc;

                //$scope.postFeed.imageType = reader.readAsDataURL(element.target.files[0].type);
            });
        }

        $scope.audioUpload = function (element) {
            if (element.target.files.length > 0) {
                var reader = new FileReader();
                reader.onload = $scope.audioIsLoaded;
                //console.log(element.target.files[0])
                reader.readAsDataURL(element.target.files[0]);
                if ($scope.userMusicCatalog.songName === element.target.files[0].name) {
                    $scope.userMusicCatalog.songName = element.target.files[0].name;

                } else {
                    $scope.userMusicCatalog.songName = element.target.files[0].name;
                    $scope.userMusicCatalog.songName1 = element.target.files[0].name;
                }
                $scope.userMusicCatalog.audioType = element.target.files[0].type;
                //readURL(element.target);
            }
        }

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function () {
                    $('#blah')
                        .attr('src', reader.result)
                        .width(150)
                        .height(200);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

       

        $scope.deletSong = function (musicDetails, index) {
            $scope.songToDelete = musicDetails;
            var confirm = $mdDialog.confirm()
                 .title('Do you want to delete song "' + musicDetails.songName + '" of Catalog "' + musicDetails.catalogName + '" ?')
                 .ariaLabel('Do you want to proceed?')
                 .ok('Ok')
                 .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                $http({
                    method: 'POST',
                    url: basePath + 'MainPage/deleteSongFromCatalog',
                    data: $scope.songToDelete
                }).then(function (res) {
                    if (res !== null) {
                        $scope.dialogBox("Succes", "Song " + musicDetails.songName + ' of Catalog "' + musicDetails.catalogName + '" deleted successfully');
                        activate();
                    }
                });
            }).catch(() => {
                console.log("Cancelled");
            });
        };


        $scope.dialogBox = function (title, content) {
            $mdDialog.show(
             $mdDialog.alert()
            .parent(angular.element(document.querySelector('#popupContainer')))
            .clickOutsideToClose(true)
            .title(title)
            .textContent(content)
            .ariaLabel('Alert Dialog Demo')
            .ok('Ok')
             );

        };

       

      

        function DialogController($scope, $mdDialog, message) {

            $scope.message1 = message;
            $scope.hide = function () {
                $mdDialog.hide();
            };
            $scope.cancel = function () {
                $mdDialog.cancel();
            };
            $scope.answer = function (answer) {

                $mdDialog.hide(answer);
            };
        };


        $scope.addMusicCatalog = function (frmMusicCatalog) {

            //$scope.myCroppedImage = $scope.myCroppedImage.split(',')[1];
            //$scope.userMusicCatalog.imageSrc = $scope.myCroppedImage;
            $scope.isSubmit = true;
            $scope.IsmusictagError = false;
            if ($scope.userMusicCatalog.musicTag1 === undefined && $scope.userMusicCatalog.musicTag2 === undefined && $scope.userMusicCatalog.musicTag3 === undefined) {
                $scope.IsmusictagError = true;
            }

            $scope.activated = true;
            if (frmMusicCatalog.$valid && $scope.IsmusictagError == false) {
                $scope.showSpinner = true;
                $http({
                    method: 'POST',
                    url: basePath + 'MainPage/addMusicCatalog',
                    data: $scope.userMusicCatalog,
                    headers: "application/json"
                })
               .then(function (result) {
                   if (result) {
                       $mdDialog.hide("Edit");
                       $scope.activated = false;
                       $scope.showSpinner = false;
                   }
                   else {
                       $mdDialog.hide("Cancel");
                   }
               });
            }
        };

    }
})();

