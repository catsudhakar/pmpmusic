﻿(function () {
    'use strict';

    angular
        .module('mainpage')
        .controller('getSubmitedFromFeed', getSubmitedFromFeed);

    getSubmitedFromFeed.$inject = ['$scope', '$http', '$mdDialog'];

    function getSubmitedFromFeed($scope, $http, $mdDialog) {
        var basePath = '/';
        $scope.title = ':: Submited From feed History ::';
        $scope.stepsModel = [];

        $scope.isSubmit = false;
        $scope.showSpinner = false;
        $scope.activated = false;
        $scope.showSpinner1 = false;
        $scope.activated1 = false;
        $scope.audioPlayButton = true;
       
        $scope.LoadPosts = 0;
        $scope.SubmitedFromFeed = [];
        $scope.feedsListPartial = [];
        $scope.totalSubmitedFromFeed = 0;

        activate();
        function activate() {
            getSubfeedHistory();
        }

        function getSubfeedHistory() {
            $scope.showSpinner = true;
            $scope.activated = true;
            $http({
                method: 'POST',
                url: basePath + 'MainPage/getSubFeedHistory?FeedSubType=subFromHistry',
                headers: "application/json"
            }).then(function (data) {
                $scope.totalSubmitedFromFeed = data.data.length;
                $scope.feedsListPartial = data.data;
                for (var i = 0; i < $scope.totalSubmitedFromFeed; i++) {
                    if (!$scope.feedsListPartial[i].fileType) {
                        $scope.autoplay = false;
                        $scope.feedsListPartial[i].audioPlayButton = $scope.autoplay;
                        $scope.feedsListPartial[i].noAudioLink = false;
                        $scope.feedsListPartial[i].isAudioPlaying = false;
                        $scope.feedsListPartial[i].playBtnImg = "playBtnImg" + i;
                    } else {
                        $scope.autoplay = true;
                        $scope.feedsListPartial[i].audioPlayButton = $scope.autoplay;
                        $scope.feedsListPartial[i].noAudioLink = true;
                        $scope.feedsListPartial[i].isAudioPlaying = false;
                        $scope.feedsListPartial[i].playBtnImg = "playBtnImg" + i;
                    }
                    $scope.feedsListPartial[i].indexs = i;
                }

                loadrecords();
                $scope.showSpinner = false;
                $scope.activated = false;
            });
        }


        function loadrecords() {
            var condi = $scope.LoadPosts + 5;
            for (var i = $scope.LoadPosts; i < condi && i < $scope.totalSubmitedFromFeed; i++) {
                if ($scope.feedsListPartial[i] != null || $scope.feedsListPartial[i] != undefined) {
                    $scope.SubmitedFromFeed.push($scope.feedsListPartial[i]);
                }
                $scope.LoadPosts = i + 1;
            };
            $scope.activated1 = false;
            $scope.showSpinner1 = false;
        }

        $scope.loadMorePosts = function () {
            if ($scope.LoadPosts != 0) {
                if ($scope.LoadPosts == $scope.totalSubmitedFromFeed) {
                } else {
                    $scope.activated1 = true;
                    $scope.showSpinner1 = true;
                    setTimeout(function () {
                        getSubfeedHistory();
                    }, 2000);
                }
            }
        };

        $scope.num = 0;
        var audioElement = document.getElementById('audio');
        $scope.playMusic = function (index) {
            $scope.autoplay = $scope.SubmitedFromFeed[index].audioPlayButton;
            if ($scope.SubmitedFromFeed[index].fileUrl != null || $scope.SubmitedFromFeed[index].fileUrl != undefined) {
                $scope.currentTrack = $scope.SubmitedFromFeed[index].fileUrl;
            }
            $scope.currentIndex = index;
            audioElement.src = $scope.currentTrack;

            if ($scope.audioPaused) {
                audioElement.autoplay = false;
                $("#play_btn").show();
                $("#pause_btn").hide();
                $scope.audioPaused = false;
                $scope.SubmitedFromFeed[index].isAudioPlaying = false;
            } else {
                audioElement.autoplay = $scope.autoplay;
                $scope.SubmitedFromFeed[index].isAudioPlaying = true;
                $("#play_btn").hide();
                $("#pause_btn").show();
                $scope.audioPaused = true;
            }
        }

        function DialogController($scope, $mdDialog, commentsFeedId) {
            $scope.commentsFeedId = commentsFeedId;
            $scope.hide = function () {
                $mdDialog.hide();
            };
            $scope.cancel = function () {
                $mdDialog.cancel();
            };
            $scope.answer = function (answer) {
                $mdDialog.hide(answer);
            };
        };

        
        $scope.editViewSubmissions = function (ev, feedId, userId, feedTitle) {
            var subHistry = 'FromHstry';
            $mdDialog.show({
                controller: DialogController,
                templateUrl: '/app/mainpage/showsubmitedfromuser.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                locals: { commentsFeedId: angular.copy(feedId + ',' + userId + ',' + subHistry) + ',' + feedTitle }
            })
           .then(function (answer) {
               if (answer === 'Edit') {
               } else if (answer === 'Cancel') {
                   console.log("Cancelled");
               }
           }, function () {
               //$scope.dialogBox("Failure", "You")
           });
        };

    }
})();