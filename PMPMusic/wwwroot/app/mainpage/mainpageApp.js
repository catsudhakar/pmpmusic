﻿/// <reference path="savedfeeds.html" />
/// <reference path="savedfeeds.js" />
var mainpageModule = angular.module('mainpage', ['share'])
    .config(function ($routeProvider, $locationProvider) {
        $routeProvider.when('/mainpage', { templateUrl: '/app/mainpage/index.html', caseInsensitiveMatch: true });
        $routeProvider.when('/mainpage/mainfeed', { templateUrl: '/app/mainpage/mainfeed.html', caseInsensitiveMatch: true });
        $routeProvider.when('/mainpage/musicList', { templateUrl: '/app/mainpage/musicList.html', caseInsensitiveMatch: true });
        $routeProvider.when('/mainpage/customise', { templateUrl: '/app/mainpage/customise.html', caseInsensitiveMatch: true });
        $routeProvider.when('/mainpage/messages', { templateUrl: '/app/mainpage/messages.html', caseInsensitiveMatch: true });
      
        $routeProvider.when('/profile', { templateUrl: '/app/profile/index.html', caseInsensitiveMatch: true });
      
       
        $routeProvider.when('/mainpage/editProfile', { templateUrl: '/app/mainpage/editProfile.html', caseInsensitiveMatch: true });
        $routeProvider.when('/mainpage/myfeeds', { templateUrl: '/app/mainpage/viewMyFeeds.html', caseInsensitiveMatch: true });
        $routeProvider.when('/mainpage/followedFeeds', { templateUrl: '/app/mainpage/followedFeeds.html', caseInsensitiveMatch: true });
        $routeProvider.when('/mainpage/savedFeeds', { templateUrl: '/app/mainpage/savedFeeds.html', caseInsensitiveMatch: true });

        $routeProvider.when('/mainpage/getsubtofeed', { templateUrl: '/app/mainpage/getsubtofeed.html', caseInsensitiveMatch: true });
        $routeProvider.when('/mainpage/getsubfromfeed', { templateUrl: '/app/mainpage/getsubfromfeed.html', caseInsensitiveMatch: true });

        $routeProvider.when('/mainpage/searchByUser', { templateUrl: '/app/mainpage/searchByUser.html', caseInsensitiveMatch: true });
        $routeProvider.when('/mainpage/searchByFeed', { templateUrl: '/app/mainpage/searchByFeed.html', caseInsensitiveMatch: true });

        $routeProvider.otherwise({ redirectTo: '/mainpage' });
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
    });

mainpageModule.directive('validFile', function () {
    return {
        require: 'ngModel',
        link: function (scope, el, attrs, ngModel) {
            //change event is fired when file is selected
            el.bind('change', function () {
                scope.$apply(function () {
                    ngModel.$setViewValue(el.val());
                    ngModel.$render();
                });
            });
        }
    }
});
