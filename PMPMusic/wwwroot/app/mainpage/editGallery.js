﻿(function () {
    'use strict';

    angular
        .module('main')
        .controller('editGallery', editGallery);

    editGallery.$inject = ['$scope', '$http', '$mdDialog'];

    function editGallery($scope, $http, $mdDialog) {
        var basePath = '/';
        $scope.isSubmit = false;
        $scope.getGallery = [];
        $scope.UserGallery = {};
        $scope.FileName = "";

        $scope.showSpinner = false;
        $scope.activated = false;
        $scope.showSpinner1 = false;
        $scope.activated1 = false;

        $scope.title = 'User Gallery';

        activate();
        function activate() {
            $http({
                method: 'POST',
                url: basePath + 'MainPage/getUserGallery',
                headers: "application/json"
            }).then(function (data) {
                $scope.getGallery = data.data;
            });

        }

        var names = [];
        $('body').unbind("change").on('change', '.picupload', function (event) {
            var getAttr = $(this).attr('click-type');
            var files = event.target.files;
            var output = document.getElementById("media-list");
            if (getAttr == 'type2') {
                for (var i = 0; i < files.length; i++) {
                    var file = files[i];
                    names.push($(this).get(0).files[i].name);
                    if (file.type.match('image')) {
                        var picReader = new FileReader();
                        picReader.fileName = file.name;
                        picReader.addEventListener("load", function (event) {
                            var picFile = event.target;
                            $scope.UserGallery = {};
                            $scope.UserGallery.fileName = picFile.fileName;
                            $scope.UserGallery.fileUrl = picFile.result;
                            $scope.UserGallery.fileType = file.type;
                            $scope.isSubmit = true;
                            $scope.activated = true;
                            $scope.showSpinner = true;

                            $http({
                                method: 'POST',
                                url: basePath + 'MainPage/addUserGallery',
                                data: $scope.UserGallery,
                                headers: "application/json"
                            })
                           .then(function (result) {
                               if (result) {
                                   activate();
                                   $scope.activated = false;
                                   $scope.showSpinner = false;
                               }
                           }, function () {
                               $scope.dialogBox("Failure", "Failed! try again");
                               $scope.activated = false;
                               $scope.showSpinner = false;
                               console.log("dialog Closed");
                           });
                        });
                    }
                    picReader.readAsDataURL(file);
                }
                console.log(names);
            }
        });

        //----------------------Gallery Image Editing---------------
        $('body').on('change', '.editpicupload', function (event) {
            var $this = $(this);
            var getAttr = $this.attr('click-type');
            var gid = $this.attr('data-gid');
            var imgsrc = $this.parent().parent().parent().parent();
            var files = event.target.files;
            var output = document.getElementById("media-list");
            if (getAttr == 'type2' && gid != '') {
                for (var i = 0; i < files.length; i++) {
                    var file = files[i];
                    names.push($this.get(0).files[i].name);
                    if (file.type.match('image')) {
                        var picReader = new FileReader();
                        picReader.fileName = file.name
                        picReader.addEventListener("load", function (event) {
                            var picFile = event.target;

                            $scope.UserGallery.fileName = picFile.fileName;
                            $scope.UserGallery.fileUrl = picFile.result;
                            $scope.UserGallery.fileType = file.type;
                            $scope.UserGallery.id = gid;
                            $scope.activated1 = true;
                            $scope.showSpinner1 = true;

                            $http({
                                method: 'POST',
                                url: basePath + 'MainPage/editUserGallery',
                                data: $scope.UserGallery,
                                headers: "application/json"
                            })
                           .then(function (result) {
                               if (result) {
                                   imgsrc.prev('img').attr("src", result.data.fileUrl);
                                   $scope.activated1 = false;
                                   $scope.showSpinner1 = false;
                                   imgsrc.prev().prev('.overlay').children().text(result.data.fileName.slice(0, -4));
                               }
                           }, function () {
                               $scope.dialogBox("Failure", "Failed! try again");
                               $scope.activated1 = false;
                               $scope.showSpinner1 = false;
                               console.log("dialog Closed");
                           });
                        });
                    }
                    picReader.readAsDataURL(file);
                }
            }
        });

        $scope.removeUserGallery = function (frmRegister, ev) {

            $scope.UserGallery = {};
            $scope.UserGallery.id = frmRegister;
            $scope.activated1 = true;
            $scope.showSpinner1 = true;
          
            $http({
                method: 'POST',
                data: $scope.UserGallery,
                url: basePath + 'MainPage/removeUserGallery',
            }).
                then(function (result) {
                    if (result) {
                        activate();
                        $scope.showSpinner1 = false;
                        $scope.activated1 = false;
                        $scope.dialogBox("Success", "Removed Successfuly");
                    }
                }, function () {
                    $scope.showSpinner1 = false;
                    $scope.activated1 = false;
                    // $scope.dialogBox("Failure", "Failed");
                });
        };
    }
})();
