﻿(function () {
    'use strict';

    angular
        .module('mainpage')
        .controller('messages', messages);

    messages.$inject = ['$scope', '$http', '$mdDialog', '$timeout'];

    function messages($scope, $http, $mdDialog, $timeout) {
        $scope.title = 'Messages';
        var basePath = "/";
        $scope.messages = [];
        $scope.usersMsgList = [];
        $scope.usersTotalMsgs = [];
        $scope.currentUser = "";
        $scope.currentUserName = "";
        $scope.totalmsgs = 0;
        activate();

        function activate() {

            //$http({
            //    method: 'POST',
            //    url: basePath + 'MainPage/GetMessageByUserId',
            //    //data: $scope.messages,
            //    headers: "application/json"
            //}).then(function (result) {
            //    if (result.data) {
            //       // $scope.messages = result.data;
            //    }
            //    else {}
            //});

            showusersMessges();
        }

       function calltoscrolldown(){
            $('#chat_area').stop().animate({
                scrollTop: $('#chat_area')[0].scrollHeight +1000
            }, 800);
        }

        $scope.userMsgs = function (user,uname) {
            $scope.currentUser = user;
            $scope.currentUserName = uname;
            getchatusersconversation(user);
            calltoscrolldown();
        }

        $scope.deleteMsg = function (msg) {
            $http({
                method: 'POST',
                url: '/MainPage/deleteUsermsg?userMessage=' + msg,
                data: msg,
                headers: "application/json"
            }).then(function (result) {
                getchatusersconversation($scope.currentUser);
                calltoscrolldown();
            });

        }
        
        $('#postMsg').keyup(function (event) {
            if (event.keyCode == 13) {
                sendmessage();
                return true;
            }
        });

        $scope.sendmsg = function (sendmsg) {
            sendmessage();
        }

        function sendmessage() {
            var recentuser = $scope.currentUser;
            var subject = $("#postMsg");

            var subjectdata=subject.val().trim();
            if (subjectdata  != "") {
                if (recentuser != "") {
                    $scope.messages = {};
                    $scope.messages.messageTo = recentuser;
                    $scope.messages.subject = subjectdata;
                    $http({
                        method: 'POST',
                        url: basePath + 'MainPage/sendMessage',
                        data: $scope.messages,
                        headers: "application/json"
                    }).then(function (result) {
                        if (result) {
                            subject.val("");
                            getchatusersconversation(recentuser);
                            calltoscrolldown();
                        }
                    });
                }
                else { $scope.dialogBox("WARNING !", "please choose user to send message.."); }
            }
            else {
                subject.val('');
                $("#postMsg").css("border", "1px solid #a5192a");
                $scope.dialogBox("WARNING !", "please enter message");
            }
        }

        function showusersMessges() {
            $http.post(basePath + 'MainPage/getMessageUsers').then(function (data) {
                $scope.usersMsgList = data.data;
                $scope.totalmsgs = data.data[0].totalCount;
                var messageLegth = data.data.length;
                for (var i = 0; i < messageLegth; i++) {
                   var time = $scope.usersMsgList[i].messageDate == "0001-01-01T00:00:00"?" ":getdateformate($scope.usersMsgList[i].messageDate);
                    $scope.usersMsgList[i].messageDate = time;
                }
            });
        }

        function getdateformate(msgDate) {
            var date = new Date(msgDate);
            //var time = date.getMonth() + "/" + date.getDay() + "/" + date.getFullYear() + " " + date.getHours() + ":" + date.getMinutes();
            var time = date.toDateString()+" | "+ date.toLocaleTimeString() ;
            return time;
        }

        function getchatusersconversation(user) {
            $http({
                method: 'POST',
                url: '/MainPage/getMessagesbyUser?FromUser=' + user,
                data: user,
                headers: "application/json"
            }).then(function (result) {
                $scope.usersTotalMsgs = result.data;
                for (var i = 0; i < result.data.length; i++) {

                    var time = getdateformate($scope.usersTotalMsgs[i].messageDate);
                    $scope.usersTotalMsgs[i].messageDate = time;

                    if (user == result.data[i].fromUserId) {
                        $scope.usersTotalMsgs[i].pullclass = "pull-left";
                        $scope.usersTotalMsgs[i].dateclass = "pull-right";
                        $scope.usersTotalMsgs[i].chatclass = "left clearfix";
                    } else {
                        $scope.usersTotalMsgs[i].pullclass = "pull-right";
                        $scope.usersTotalMsgs[i].dateclass = "pull-left";
                        $scope.usersTotalMsgs[i].chatclass = "left clearfix admin_chat";
                    }
                }
                showusersMessges();
            });
        }

        $scope.dialogBox = function (title, content) {
            $mdDialog.show(
             $mdDialog.alert()
            .parent(angular.element(document.querySelector('#popupContainer')))
            .clickOutsideToClose(true)
            .title(title)
            .textContent(content)
            .ariaLabel('Alert Dialog Demo')
            .ok('Ok')
             );
        };

        setInterval(function () {
            //if($scope.currentUser!="")
            //    getchatusersconversation($scope.currentUser);
            if ($scope.currentUser != "")
            showusersMessges();
        }, 30000)

    }
})();


