﻿(function () {
    'use strict';

    angular
    .module('mainpage')
    .controller('searchByFeed', searchByFeed);

    searchByFeed.$inject = ['$scope', '$http', '$mdDialog', '$timeout'];

    function searchByFeed($scope, $http, $mdDialog, $timeout) {

        var basePath = "/";

        $scope.title = ':: Custom Search ::';
        $scope.seachString = '';
        $scope.cseachStr = '';
        $scope.seachType = 'title';

        $scope.totalActiveFeeds = 0;
        $scope.LoadPosts = 0;

        $scope.data = {};
        $scope.postComments = {};
        $scope.feedsList = [];
        $scope.getSocialMediaLinks = [];

        $scope.activated1 = false;
        $scope.showSpinner1 = false;
        $scope.busy = false;

        activate();
        function activate() {
            getsearchedrecords();
        }

        $scope.dialogBox = function (title, content) {
            $mdDialog.show(
             $mdDialog.alert()
            .parent(angular.element(document.querySelector('#popupContainer')))
            .clickOutsideToClose(true)
            .title(title)
            .textContent(content)
            .ariaLabel('Alert Dialog Demo')
            .ok('Ok')
             );
        };

        function getsearchedrecords() {
            $scope.feedsList = [];
            $scope.feedsListPartial = [];
            $scope.LoadPosts = 0;
            $scope.totalActiveFeeds = 0;
            loderImageShow(false);
            getCustomSearchFeed();
            getSocialmedialinks();
            loderImageShow(true);
        }
        var con = 0;
        $scope.searchUsers = function () { 
            if ($scope.seachString == '' && con==0) {
                getsearchedrecords();
                con = con + 1;
            }
            if ($scope.seachString != '') {
                getsearchedrecords();
                con = 0;
            }
        }

        function loderImageShow(bool) {
            $scope.activated1 = bool;
            $scope.showSpinner1 = bool;
            $scope.busy = bool;
        }

        $scope.searchtChange = function () {
            getsearchedrecords();
        };

        function getCustomSearchFeed() {
            $http.post(basePath + 'MainPage/feedSeachByUser?seachType=' + $scope.seachType + '&searchString=' + $scope.seachString)
              .then(function (data) {
                  $scope.feedsListPartial = data.data;
                  if (data.data != null) {
                      $scope.totalActiveFeeds = data.data.length;
                      for (var i = 0; i < $scope.totalActiveFeeds; i++) {
                          if (!$scope.feedsListPartial[i].audioLink) {
                              $scope.autoplay = false;
                              $scope.feedsListPartial[i].audioPlayButton = $scope.autoplay;
                              $scope.feedsListPartial[i].noAudioLink = false;
                              $scope.feedsListPartial[i].isAudioPlaying = false;
                              $scope.feedsListPartial[i].playBtnImg = "playBtnImg" + i;
                          } else {
                              $scope.autoplay = true;
                              $scope.feedsListPartial[i].audioPlayButton = $scope.autoplay;
                              $scope.feedsListPartial[i].noAudioLink = true;
                              $scope.feedsListPartial[i].isAudioPlaying = false;
                              $scope.feedsListPartial[i].playBtnImg = "playBtnImg" + i;
                          }
                      }
                      loadrecords();
                  } else {
                      $scope.totalActiveFeeds = 0;
                  }
              }).catch(function (error) {
                  console.log(error, " can't get data.");
              });
        }

        function loadrecords() {

            var condi = $scope.LoadPosts + 5;
            for (var i = $scope.LoadPosts; i < condi && i < $scope.totalActiveFeeds; i++) {
                if ($scope.feedsListPartial[i] != null || $scope.feedsListPartial[i] != undefined) {
                    $scope.feedsList.push($scope.feedsListPartial[i]);
                }
                $scope.LoadPosts = i + 1;
            };
            loderImageShow(false);

        }

        $scope.loadMorePosts = function () {
            if ($scope.LoadPosts != 0) {
                if ($scope.LoadPosts == $scope.totalActiveFeeds) {
                } else {
                    loderImageShow(true);
                    $timeout(function () {
                        loadrecords();
                    }, 1000);
                }
            }
        };


        $scope.toggle = function (item, list) {
        };

        $scope.exists = function (item, list) {
            return list.indexOf(item) > -1;
        };


        function getSocialmedialinks() {
            $http.post(basePath + 'MainPage/getSocialMedia').then(function (data) {
                $scope.getSocialMediaLinks = data.data;
            });
        }

        $scope.displayComments = function (feedId, index) {
            $mdDialog.show({
                controller: DialogController,
                templateUrl: '/app/beforeLogin/viewComments.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                locals: { commentsFeedId: angular.copy(feedId) }
            })
           .then(function (answer) {
               if (answer === 'Edit') {
                   //$scope.dialogBox("Success", "");
               } else if (answer === 'Cancel') {
                   console.log("Cancelled");
               }
           }, function () {
               console.log("dialog Closed");
           });
        }

        function DialogController($scope, $mdDialog, commentsFeedId) {
            $scope.commentsFeedId = commentsFeedId;
            $scope.hide = function () {
                $mdDialog.hide();
            };
            $scope.cancel = function () {
                $mdDialog.cancel();
            };
            $scope.answer = function (answer) {
                $mdDialog.hide(answer);
            };
        };

        function MessageDialogController($scope, $mdDialog, messageFeedId, toUserId) {
            $scope.messageFeedId = messageFeedId;
            $scope.toUserId = toUserId;
            $scope.hide = function () {
                $mdDialog.hide();
            };
            $scope.cancel = function () {
                $mdDialog.cancel();
            };
            $scope.answer = function (answer) {
                $mdDialog.hide(answer);
            };
        };

        $scope.feedLinks = function (feedId, index, type, userId) {
            $scope.iscomment = false;
            $scope.postComments = {};

            if (type == 'Message') {
                openMessageDialog(feedId, userId);
            }
            else if (type == 'Like Feed') {
                likeFeeds(feedId, index, userId);
            }
            else if (type == 'media') {
                if (feedId != '#')
                    window.open(feedId, "_blank");
                else {
                    $mdDialog.show($mdDialog.alert().title('alert message')
                    .textContent(userId + ' Social media link not added!')
                    .ok('OK').targetEvent(this));
                }
            }
            else if (type == 'submitTofeed') {
                submittothisfeed(feedId, userId, index);
            }
        }

        function likeFeeds(feedId, index, userId) {
            $scope.likeFeeds = {};
            $scope.likeFeeds.postId = feedId
            $scope.likeFeeds.isLikes = $scope.feedsList[index].isLikes;
            $http({
                method: 'POST',
                url: '/MainPage/likeFeed',
                data: $scope.likeFeeds
            }).then(function (response) {
                if (response.data == true) {
                    $scope.feedsList[index].isLikes = 1;
                    $scope.feedsList[index].likeCount = $scope.feedsList[index].likeCount + 1;
                } else {
                    $scope.feedsList[index].isLikes = 0;
                    $scope.feedsList[index].likeCount = $scope.feedsList[index].likeCount - 1;
                }
            }).catch(function (error) {
                console.log(error, " can't get data.");
            });
        }

        function submittothisfeed(feedId, userId, feedTitle) {
            $mdDialog.show({
                controller: DialogController,
                templateUrl: '/app/beforeLogin/submitTothisFeed.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                locals: { commentsFeedId: angular.copy(feedId + ',' + userId + ',' + feedTitle) }
            })
           .then(function (answer) {
               if (answer === 'Edit') {
               } else if (answer === 'Cancel') {
                   console.log("Cancelled");
               }
           }, function () {
               //$scope.dialogBox("Failure", "You")
           });
        }

        function openMessageDialog(feedId, userId) {
            $mdDialog.show({
                controller: MessageDialogController,
                templateUrl: '/app/beforeLogin/sendMessage.html',
                parent: angular.element(document.body),
                clickOutsideToClose: false,
                locals: {
                    messageFeedId: angular.copy(feedId), toUserId: angular.copy(userId)
                }
            })
              .then(function (answer) {
                  if (answer === 'Edit') {
                      $scope.dialogBox("Success", "Your message has been sent successfully.");
                  } else if (answer === 'Cancel') {
                      console.log("Cancelled");
                  }
              }, function () {
                  console.log("dialog Closed");
              });
        }

        $scope.followFeed = function (feedDetails) {
            $http({
                method: 'POST',
                url: '/MainPage/FollowFeed?id=' + feedDetails.createdBy,
                data: feedDetails.createdBy
            }).then(function (response) {
                if (response.data == "followed") {
                    feedDetails.isFollow = 1;
                    $scope.dialogBox("Success", "success you are now following to " + feedDetails.createdUserName + ".");
                } else if (response.data == "unfollow") {
                    feedDetails.isFollow = 0;
                    $scope.dialogBox("", "You have unfollowed the username " + feedDetails.createdUserName + ".");
                }
                bindfollowrecords(response.data, feedDetails.createdBy);
            }, function (error) {
                console.log(error, " can't get data.");
            });
        }
        function bindfollowrecords(response, userId) {
            for (var i = 0; i < $scope.feedsListPartial.length; i++) {
                if ($scope.feedsListPartial[i].createdBy == userId) {
                    var len = $scope.feedsList.length;
                    if (response == "followed") {
                        if (len > i)
                            $scope.feedsList[i].isFollow = 1;
                        $scope.feedsListPartial[i].isFollow = 1;
                    }
                    else if (response == "unfollow") {
                        if (len > i)
                            $scope.feedsList[i].isFollow = 0;
                        $scope.feedsListPartial[i].isFollow = 0;
                    }
                }
            }
        }


        $scope.savedFeed = function (model) {
            if (model.saveFeedStatus == 1) {
                model.saveFeedStatus = 0;
            } else {
                model.saveFeedStatus = 1;
            }
            $http({
                method: 'POST',
                url: '/MainPage/saveFeed',
                data: model
            }).then(function (response) {
                if (response.data == "saved") {
                    $scope.dialogBox("Success", "This Feed has been saved successfully.");
                } else {
                    $scope.dialogBox("Success", "This Feed has been unsaved successfully.");
                }
                //activate(true);
            }, function (error) {
                console.log(error, " can't get data.");
            });
        }

        $scope.num = 0;
        var audioElement = document.getElementById('audio');
        $scope.playMusic = function (index) {
            $scope.autoplay = $scope.feedsList[index].audioPlayButton;
            if ($scope.feedsList[index].audioLink != null || $scope.feedsList[index].audioLink != undefined) {
                $scope.currentTrack = $scope.feedsList[index].audioLink;
            }
            $scope.currentIndex = index;
            audioElement.src = $scope.currentTrack;

            if ($scope.audioPaused) {
                audioElement.autoplay = false;
                $("#play_btn").show();
                $("#pause_btn").hide();
                $scope.audioPaused = false;
                $scope.feedsList[index].isAudioPlaying = false;
            } else {
                audioElement.autoplay = $scope.autoplay;
                $scope.feedsList[index].isAudioPlaying = true;
                $("#play_btn").hide();
                $("#pause_btn").show();
                $scope.audioPaused = true;
            }
        }

        $scope.SaveComment = function (frmComment, index) {

            $scope.isCommentSubmit = true;
            $scope.postComments.postId = $scope.feedsList[index].id
            $scope.postComments.postComment = $scope.feedsList[index].Comment;
            $scope.postComments.CreatedBy = '996d7e2f-280b-4d3d-9a57-dfde0df2a445';

            if (frmComment.$valid) {

                $http({
                    method: 'POST',
                    url: '/MainPage/AddNewComment',
                    data: $scope.postComments
                }).then(function (response) {
                    $scope.feedsList[index].Comment = "";
                    frmComment.Comment.$setUntouched();
                    $scope.dialogBox("Success", "commented successfully.");
                }, function (error) {
                    $scope.dialogBox("Failed", "commented Failed.");
                });
            }
            else {
                frmComment.Comment.$setTouched();
            }
        }
    }
})();
