﻿(function () {
    'use strict';

    angular
        .module('mainpage')
        .controller('showFeedDetails', showFeedDetails);

    showFeedDetails.$inject = ['$scope', '$http', '$mdDialog'];

    function showFeedDetails($scope, $http, $mdDialog) {

        var basePath = "/";
        $scope.title = 'showFeedDetails';
        $scope.feedDetails = [];
        $scope.stepsModel = [];
        $scope.postComments = {};
        $scope.feedComments = '';

        activate();

        function activate() {
            if ($scope.message != null) {
              
                $scope.Id = $scope.message.id;
                $scope.title = $scope.message.title;//+  ":: Feed Details";
                $http({
                    method: 'POST',
                    url: '/MainPage/getFeedByFeedId?FeedID=' + $scope.message.id,
                    headers: "application/json"
                }).then(function (data) {
                    if (data.data != null) {
                        $scope.feedDetails = data.data;
                            if (!$scope.feedDetails.audioLink) {
                                $scope.autoplay = false;
                                $scope.feedDetails.audioPlayButton = $scope.autoplay;
                                $scope.feedDetails.noAudioLink = false;
                                $scope.feedDetails.isAudioPlaying = false;
                                $scope.feedDetails.playBtnImg = "playBtnImg1";
                            } else {
                                $scope.autoplay = true;
                                $scope.feedDetails.audioPlayButton = $scope.autoplay;
                                $scope.feedDetails.noAudioLink = true;
                                $scope.feedDetails.isAudioPlaying = false;
                                $scope.feedDetails.playBtnImg = "playBtnImg1";
                            }
                    } else {
                        $scope.feedComments = 'Records not found';
                    }
                }).catch(function (error) {
                    console.log(error, " can't get data.");
                });
            }
            getSocialmedialinks();
        }

        function getSocialmedialinks() {
            $http.post(basePath + 'MainPage/getSocialMedia').then(function (data) {
                $scope.getSocialMediaLinks = data.data;
            });
        }

        $scope.imageIsLoaded = function (e) {
            $scope.$apply(function () {
                $scope.stepsModel.push(e.target.result);
                var imageSrc = e.target.result.split(',')[1];
                $scope.feedDetails.imageSrc = imageSrc;
            });
        }

        $scope.imageUpload = function (element) {
            if (element.target.files.length > 0) {
                var reader = new FileReader();
                reader.onload = $scope.imageIsLoaded;
                reader.readAsDataURL(element.target.files[0]);
                $scope.feedDetails.imageFileName = element.target.files[0].name;
                $scope.feedDetails.imageType = element.target.files[0].type;
            }
        }

        $scope.audioIsLoaded = function (e) {
            $scope.$apply(function () {
                $scope.stepsModel.push(e.target.result);
                var audioSrc = e.target.result.split(',')[1];
                $scope.feedDetails.audioSrc = audioSrc;
            });
        }

        $scope.audioUpload = function (element) {
            if (element.target.files.length > 0) {
                var reader = new FileReader();
                reader.onload = $scope.audioIsLoaded;
                reader.readAsDataURL(element.target.files[0]);
                $scope.feedDetails.audioType = element.target.files[0].type;
                $scope.feedDetails.audioFileName = element.target.files[0].name;
            }
        }


        $scope.videoIsLoaded = function (e) {
            $scope.$apply(function () {
                $scope.stepsModel.push(e.target.result);
                var videoSrc = e.target.result.split(',')[1];
                $scope.feedDetails.videoSrc = videoSrc;
            });
        }

        $scope.videoUpload = function (element) {
            if (element.target.files.length > 0) {
                var reader = new FileReader();
                reader.onload = $scope.videoIsLoaded;
                reader.readAsDataURL(element.target.files[0]);
                $scope.feedDetails.videoType = element.target.files[0].type;
                $scope.feedDetails.videoFileName = element.target.files[0].name;
            }

        }

        $scope.followFeed = function (feedDetails) {
            $http({
                method: 'POST',
                url: '/MainPage/FollowFeed?id=' + feedDetails.createdBy,
                data: feedDetails.createdBy
            }).then(function (response) {
                if (response.data == "followed") {
                    feedDetails.isFollow = 1;
                    $scope.dialogBox("Success", "success you are now following to " + feedDetails.createdUserName + ".");
                } else if (response.data == "unfollow") {
                    feedDetails.isFollow = 0;
                    $scope.dialogBox("", "You have unfollowed the username " + feedDetails.createdUserName + ".");
                }
            }, function (error) {

                console.log(error, " can't get data.");
            });
        }

        $scope.SaveComment = function (frmComment) {
            $scope.isCommentSubmit = true;
            $scope.postComments.postId = $scope.feedDetails.id
            $scope.postComments.postComment = frmComment.Comment.$modelValue;
            $scope.postComments.CreatedBy = '996d7e2f-280b-4d3d-9a57-dfde0df2a445';
            if (frmComment.$valid) {
                $http({
                    method: 'POST',
                    url: '/MainPage/AddNewComment',
                    data: $scope.postComments
                }).then(function (response) {
                    frmComment.Comment.$modelValue = "";
                    frmComment.Comment.$setUntouched();
                    $scope.dialogBox("Success", "commented successfully.");
                }, function (error) {
                    $scope.dialogBox("Failed !", "commented Failed.");
                });
            }
            else {
                frmComment.Comment.$setTouched();
            }
        }

        $scope.num = 0;
        var audioElement = document.getElementById('audio');
        $scope.playMusic = function (index) {
            $scope.autoplay = $scope.feedDetails.audioPlayButton;
            if ($scope.feedDetails.audioLink != null || $scope.feedDetails.audioLink != undefined) {
                $scope.currentTrack = $scope.feedDetails.audioLink;
            }
            $scope.currentIndex = index;
            audioElement.src = $scope.currentTrack;

            if ($scope.audioPaused) {
                audioElement.autoplay = false;
                $("#play_btn").show();
                $("#pause_btn").hide();
                $scope.audioPaused = false;
                $scope.feedDetails.isAudioPlaying = false;
            } else {
                audioElement.autoplay = $scope.autoplay;
                $scope.feedDetails.isAudioPlaying = true;
                $("#play_btn").hide();
                $("#pause_btn").show();
                $scope.audioPaused = true;
            }
        }


        $scope.savedFeed = function (model) {
            $http({
                method: 'POST',
                url: '/MainPage/saveFeed',
                data: model
            }).then(function (response) {
                if (response.data == "saved") {
                    model.saveFeedStatus = 1;
                    $scope.dialogBox("Success", "This Feed has been saved successfully.");
                } else {
                    model.saveFeedStatus = 0;
                    $scope.dialogBox("Success", "This Feed has been unsaved successfully.");
                }
                //activate(true);
            }, function (error) {
                console.log(error, " can't get data.");
            });
        }

        $scope.PostFeed = function (formPostFeed) {
            $scope.isSubmit = true;
            if (formPostFeed.$valid) {
                $http({
                    method: 'POST',
                    url: 'MainPage/AddNewFeed',
                    data: $scope.postFeed
                })
                    .success(function (data) {
                        alert("sucesss");
                    }).error(function () {
                    });
            }
        }

        $scope.dialogBox = function (title, content) {
            $mdDialog.show(
             $mdDialog.alert()
            .parent(angular.element(document.querySelector('#popupContainer')))
            .clickOutsideToClose(true)
            .title(title)
            .textContent(content)
            .ariaLabel('Alert Dialog Demo')
            .ok('Ok')
             );
        };

        $scope.displayComments = function (feedId, index) {
            $mdDialog.show({
                controller: DialogController,
                templateUrl: '/app/beforeLogin/viewComments.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                locals: { commentsFeedId: angular.copy(feedId) }
            })
           .then(function (answer) {
               if (answer === 'Edit') {
                   //$scope.dialogBox("Success", "");
               } else if (answer === 'Cancel') {
                   console.log("Cancelled");
               }
           }, function () {
               console.log("dialog Closed");
           });
        }

        function DialogController($scope, $mdDialog, commentsFeedId) {
            $scope.commentsFeedId = commentsFeedId;
            $scope.hide = function () {
                $mdDialog.hide();
            };
            $scope.cancel = function () {
                $mdDialog.cancel();
            };
            $scope.answer = function (answer) {
                $mdDialog.hide(answer);
            };
        };

        function MessageDialogController($scope, $mdDialog, messageFeedId, toUserId) {
            $scope.messageFeedId = messageFeedId;
            $scope.toUserId = toUserId;
            $scope.hide = function () {
                $mdDialog.hide();
            };
            $scope.cancel = function () {
                $mdDialog.cancel();
            };
            $scope.answer = function (answer) {
                $mdDialog.hide(answer);
            };
        };

        $scope.feedLinks = function (feedId, index, type, userId) {
            $scope.iscomment = false;
            $scope.postComments = {};

            if (type == 'Message') {
                openMessageDialog(feedId, userId);
            }
            else if (type == 'Like Feed') {
                likeFeeds(feedId, index, userId);
            }
            else if (type == 'media') {
                if (feedId != '#')
                    window.open(feedId, "_blank");
                else {
                    $mdDialog.show($mdDialog.alert().title('alert message')
                    .textContent(userId + ' Social media link not added!')
                    .ok('OK').targetEvent(this));
                }
            }
            else if (type == 'submitTofeed') {
                submittothisfeed(feedId, userId, index);
            }
        }

        function likeFeeds(feedId, index, userId) {

            $scope.likeFeeds = {};
            $scope.likeFeeds.postId = feedId
            $scope.likeFeeds.isLikes = $scope.feedDetails.isLikes;
            $http({
                method: 'POST',
                url: '/MainPage/likeFeed',
                data: $scope.likeFeeds
            }).then(function (response) {
                if (response.data == true) {
                    $scope.feedDetails.isLikes = 1;
                    $scope.feedDetails.likeCount = $scope.feedDetails.likeCount + 1;
                } else {
                    $scope.feedDetails.isLikes = 0;
                    $scope.feedDetails.likeCount = $scope.feedDetails.likeCount - 1;
                }
            }).catch(function (error) {
                console.log(error, " can't get data.");
            });
        }

        function submittothisfeed(feedId, userId, feedTitle) {
            $mdDialog.show({
                controller: DialogController,
                templateUrl: '/app/beforeLogin/submitTothisFeed.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                locals: { commentsFeedId: angular.copy(feedId + ',' + userId + ',' + feedTitle) }
            })
           .then(function (answer) {
               if (answer === 'Edit') {
               } else if (answer === 'Cancel') {
                   console.log("Cancelled");
               }
           }, function () {
               //$scope.dialogBox("Failure", "You")
           });
        }

        function openMessageDialog(feedId, userId) {
            $mdDialog.show({
                controller: MessageDialogController,
                templateUrl: '/app/beforeLogin/sendMessage.html',
                parent: angular.element(document.body),
                clickOutsideToClose: false,
                locals: {
                    messageFeedId: angular.copy(feedId), toUserId: angular.copy(userId)
                }
            })
              .then(function (answer) {
                  if (answer === 'Edit') {
                      $scope.dialogBox("Success", "Your message has been sent successfully.");
                  } else if (answer === 'Cancel') {
                      console.log("Cancelled");
                  }
              }, function () {
                  console.log("dialog Closed");
              });
        } 
    }
})();
