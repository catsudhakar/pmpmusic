﻿(function () {
    'use strict';

    angular
        .module('mainpage')
        .controller('index', index);


    index.$inject = ['$scope', '$http', '$location', '$mdDialog'];

    function index($scope, $http, $location, $mdDialog) {
        $scope.title = 'PMP Music';
        var basePath = "/";
        $scope.SelectCountryList = [];
        $scope.country = '';
        $scope.feedsList = [];
        $scope.userDetails = {};
        $scope.feedsListLatest = [{ 'title': 'No records Found', 'Id': '' }];
        $scope.userTags = [];
        $scope.userBiography = 'Biography not updated';

        $scope.userBiography1 = 'Biography not updated';
        $scope.musicCatalogs = [];
        $scope.getSocialMediaLinks = [];

        $scope.getGallery = [];
        $scope.UserGallery = {};
        $scope.userStatus = {};
        $scope.showSpinner = false;
        $scope.activated = false;
        $scope.showSpinner1 = false;
        $scope.activated1 = false;
        $scope.isMore = false;

        activate();

        function activate() {
            $scope.isMore = false;
            getUserLoginprofileDetails();
            $http.post(basePath + 'MainPage/getAllFeedDetails').then(function (data) {
                $scope.feedsList = data.data.postFeeds;
                if (data.data.postFeedsLatest != '')
                    $scope.feedsListLatest = data.data.postFeedsLatest;
                $scope.userDetails = data.data.userDetails;
                //$scope.totalActiveFeeds = data.data.length;
            });
            $http.post(basePath + 'MainPage/getUserTags').then(function (data) {
                $scope.userTags = data.data;
                if ($scope.userTags != undefined && $scope.userTags.userBiography != null)
                    $scope.userBiography = $scope.userTags.userBiography;
                if ($scope.userBiography.length > 250) {

                    $scope.userBiography1 = $scope.userBiography.substring(0, 250);
                    $scope.isMore = true;
                }
                else {
                    $scope.userBiography1 = $scope.userBiography;
                    $scope.isMore = false;
                }
            });

            $http.post(basePath + 'MainPage/getAllMusicCatalogs').then(function (data) {
                $scope.musicCatalogs = data.data;
            });

            $http.post(basePath + 'MainPage/getUserGallery').then(function (data) {
                $scope.getGallery = data.data;
            });

            $http.post(basePath + 'MainPage/getSocialMedia').then(function (data) {
                $scope.getSocialMediaLinks = data.data;
            });

        }

        function getUserLoginprofileDetails() {
            $http.post(basePath + 'MainPage/getLoginprofileUserDetails').then(function (data) {
                $scope.userStatus = data.data;
            });
        }

        $scope.gotoMainFeed = function () {
            $location.path('/mainpage/mainfeed/');
        }

        function DialogController($scope, $mdDialog, message) {

            $scope.message = message;
            $scope.hide = function () {
                $mdDialog.hide();
            };
            $scope.cancel = function () {
                $mdDialog.cancel();
            };
            $scope.answer = function (answer) {

                $mdDialog.hide(answer);
            };
        };


        $scope.addMusicCatalog = function (ev) {

            $mdDialog.show({
                controller: DialogController,
                templateUrl: '/app/mainpage/AddMusicCatalog.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: false,
                locals: {
                    message: angular.copy(ev.targetEvent)
                }
            }).then(function (answer) {
                if (answer === 'Edit') {
                    $scope.dialogBox("Success", "Your Catalog has been Added successfully.");
                    setTimeout(function () {
                        //    window.location.reload();
                    }, 3000);
                    activate();

                } else if (answer === 'Cancel') {
                    console.log("Cancelled");
                }

            }, function () {
                //$scope.dialogBox("Failure", "You")
                console.log("dialog Closed");
            });
        };

        $scope.dialogBox = function (title, content) {
            $mdDialog.show(
             $mdDialog.alert()
            .parent(angular.element(document.querySelector('#popupContainer')))
            .clickOutsideToClose(true)
            .title(title)
            .textContent(content)
            .ariaLabel('Alert Dialog Demo')
            .ok('Ok')
             );

        };

        $scope.editCatalog = function (catalog) {
            $mdDialog.show({
                controller: DialogController,
                templateUrl: '/app/mainpage/editMusicCatalog.html',
                parent: angular.element(document.body),
                targetEvent: catalog,
                clickOutsideToClose: false,
                locals: {
                    message: angular.copy(catalog)
                }
            })
                .then(function (answer) {
                    if (answer === 'Edit') {
                        $scope.dialogBox("Success", "Your Catalog has been Edited successfully.");
                        activate();
                    } else if (answer === 'Cancel') {
                        console.log("Cancelled");
                    }

                }, function () {
                    //$scope.dialogBox("Failure", "You")
                    console.log("dialog Closed");
                });
        }

        //-------------------------------------------------showvideo--------------------

        $scope.showVideo = function (feed) {
            $mdDialog.show({
                controller: DialogController,
                templateUrl: '/app/mainpage/showVideo.html',
                parent: angular.element(document.body),
                targetEvent: feed,
                clickOutsideToClose: false,
                locals: {
                    message: angular.copy(feed)
                }
            })
                .then(function (answer) {
                    if (answer === 'Edit') {
                        $scope.dialogBox("Success", "Your Catalog has been Edited successfully.");
                        //activate();
                    } else if (answer === 'Cancel') {
                        console.log("Cancelled");
                    }

                }, function () {
                    //$scope.dialogBox("Failure", "You")
                    console.log("dialog Closed");
                });
        }

        //-------------------------------------------------show Biography--------------------

        $scope.showBiography = function (biography) {
            $mdDialog.show({
                controller: DialogController,
                templateUrl: '/app/mainpage/showBiography.html',
                parent: angular.element(document.body),
                targetEvent: biography,
                clickOutsideToClose: false,
                locals: {
                    message: angular.copy(biography)
                }
            })
                .then(function (answer) {
                    if (answer === 'Edit') {
                        $scope.dialogBox("Success", "Your Catalog has been Edited successfully.");
                        //activate();
                    } else if (answer === 'Cancel') {
                        console.log("Cancelled");
                    }

                }, function () {
                    //$scope.dialogBox("Failure", "You")
                    console.log("dialog Closed");
                });
        }

        //---------------------Gallery-----------------------------
        var names = [];
        $('body').unbind("change").on('change', '.picupload', function (event) {
            //alert('This file size is: ' + this.files[0].size / 1024 / 1024 + "MB");
            var getAttr = $(this).attr('click-type');
            var files = event.target.files;
            var output = document.getElementById("media-list");
            if (getAttr == 'type2') {
                for (var i = 0; i < files.length; i++) {
                    var file = files[i];
                    names.push($(this).get(0).files[i].name);
                    if (file.type.match('image')) {
                        var picReader = new FileReader();
                        if (file.size <= 1024 * 1024 * 3) {
                            picReader.fileName = file.name
                            picReader.addEventListener("load", function (event) {
                                var picFile = event.target;
                                $scope.UserGallery = {};
                                $scope.UserGallery.fileName = picFile.fileName;
                                $scope.UserGallery.fileUrl = picFile.result;
                                $scope.UserGallery.fileType = file.type;
                                $scope.activated = true;
                                $scope.showSpinner = true;

                                $http({
                                    method: 'POST',
                                    url: basePath + 'MainPage/addUserGallery',
                                    data: $scope.UserGallery,
                                    headers: "application/json"
                                })
                               .then(function (result) {
                                   if (result) {
                                       activate();
                                       $scope.activated = false;
                                       $scope.showSpinner = false;
                                   }
                               }, function () {
                                   $scope.dialogBox("Failure", "Failed! try again");
                                   $scope.activated1 = false;
                                   $scope.showSpinner1 = false;
                                   console.log("dialog Closed");
                               });
                            });
                        }
                        else {
                            $scope.dialogBox("Warning", "Uploaded image should be lessthen 3MB.");
                        }

                    }
                    picReader.readAsDataURL(file);
                }
                console.log(names);
            }
        });

        //----------------------Gallery Image Editing---------------
        $('body').on('change', '.editpicupload', function (event) {
            var $this = $(this);
            var getAttr = $this.attr('click-type');
            var gid = $this.attr('data-gid');
            var imgsrc = $this.parent().parent().parent().parent();
            var files = event.target.files;
            var output = document.getElementById("media-list");
            if (getAttr == 'type2' && gid != '') {
                for (var i = 0; i < files.length; i++) {
                    var file = files[i];
                    names.push($this.get(0).files[i].name);
                    if (file.type.match('image')) {
                        var picReader = new FileReader();
                        picReader.fileName = file.name
                        picReader.addEventListener("load", function (event) {
                            var picFile = event.target;

                            $scope.UserGallery.fileName = picFile.fileName;
                            $scope.UserGallery.fileUrl = picFile.result;
                            $scope.UserGallery.fileType = file.type;
                            $scope.UserGallery.id = gid;
                            $scope.activated1 = true;
                            $scope.showSpinner1 = true;

                            $http({
                                method: 'POST',
                                url: basePath + 'MainPage/editUserGallery',
                                data: $scope.UserGallery,
                                headers: "application/json"
                            })
                           .then(function (result) {
                               if (result) {
                                   imgsrc.prev('img').attr("src", result.data.fileUrl);
                                   $scope.activated1 = false;
                                   $scope.showSpinner1 = false;
                                   imgsrc.prev().prev('.overlay').children().text(result.data.fileName.slice(0, -4));
                               }
                           }, function () {
                               $scope.dialogBox("Failure", "server busy");
                               $scope.activated1 = false;
                               $scope.showSpinner1 = false;
                               console.log("dialog Closed");
                           });
                        });
                    }
                    picReader.readAsDataURL(file);
                }
            }
        });
        $scope.removeUserGallery = function (frmRegister, ev) {

            $scope.UserGallery = {};
            $scope.UserGallery.id = frmRegister;
            $scope.activated1 = true;
            $scope.showSpinner1 = true;
            $http({
                method: 'POST',
                data: $scope.UserGallery,
                url: basePath + 'MainPage/removeUserGallery',
            }).
                then(function (result) {
                    if (result) {
                        activate();
                        $scope.showSpinner1 = false;
                        $scope.activated1 = false;
                        $scope.dialogBox("Success", "Removed Successfuly");
                    }
                }, function () {
                    $scope.showSpinner1 = false;
                    $scope.activated1 = false;
                    $scope.dialogBox("Failure", "Failed! try again");
                });
        };

        $scope.usermsgfollow = function (userId, userLogin, check, ev) {

            if (userLogin != '' && userId != '') {
                if (check == 'Message') {
                    sendMsg(userId, ev);
                }
                else if (check == 'Follow') {
                    followUser(userId, userLogin);
                }
            } else {
                showLoginpage();
            }
        }


        $scope.showFeed = function (feed) {
            $mdDialog.show({
                controller: DialogController,
                templateUrl: '/app/mainpage/showFeedDetails.html',
                parent: angular.element(document.body),
                targetEvent: feed,
                clickOutsideToClose: false,
                locals: {
                    message: angular.copy(feed)
                }
            })
                .then(function (answer) {
                    if (answer === 'Edit') {
                        $scope.dialogBox("Success", "Your Catalog has been Edited successfully.");
                        activate();
                    } else if (answer === 'Cancel') {
                        console.log("Cancelled");
                    }

                }, function () {
                    //$scope.dialogBox("Failure", "You")
                    console.log("dialog Closed");
                });
        }
        function sendMsg(userId, ev) {
            var feedId = '00000000-0000-0000-0000-000000000000';
            $mdDialog.show({
                controller: MessageDialogController,
                templateUrl: '/app/beforeLogin/sendMessage.html',
                parent: angular.element(document.body),
                clickOutsideToClose: false,
                locals: {
                    messageFeedId: angular.copy(feedId), toUserId: angular.copy(userId)
                }
            }).then(function (answer) {
                if (answer === 'Edit') {
                    $scope.dialogBox("Success", "Your message has been sent successfully.");
                } else if (answer === 'Cancel') {
                    console.log("Cancelled");
                }
            }, function () {
                console.log("dialog Closed");
            });
        }

        function followUser(userId, userLogin) {
            var user = $scope.userDetails.firstName + ' ' + $scope.userDetails.lastName
            $http({
                method: 'POST',
                url: '/MainPage/FollowFeed?id=' + userId,
                data: userId
            }).then(function (response) {
                if (response.data == "followed") {
                    $scope.userStatus.followStatus = true;
                    $scope.dialogBox("Success", "success you are now following to " + user);
                } else if (response.data == "unfollow") {
                    $scope.userStatus.followStatus = false;
                    $scope.dialogBox("", "You have unfollowed the usernameto " + user);
                }
            }, function (error) {
                console.log(error, " can't get data.");
            });
        }

        function showLoginpage() {
            $mdDialog.show({
                controller: LoginDialogController,
                templateUrl: '/app/beforeLogin/login.html',
                parent: angular.element(document.body),
                clickOutsideToClose: false,
                locals: {
                    commentsFeedId: ''
                }
            }).then(function (answer) {
                if (answer === 'Edit') {
                    $scope.dialogBox("Success", "Your profile has been updated successfully.");
                } else if (answer === 'Cancel') {
                    console.log("Cancelled");
                }
            }, function () {
                console.log("dialog Closed");
            });
        }

        function MessageDialogController($scope, $mdDialog, messageFeedId, toUserId) {
            $scope.messageFeedId = messageFeedId;
            $scope.toUserId = toUserId;
            $scope.hide = function () {
                $mdDialog.hide();
            };
            $scope.cancel = function () {
                $mdDialog.cancel();
            };
            $scope.answer = function (answer) {
                $mdDialog.hide(answer);
            };
        };

        function LoginDialogController($scope, $mdDialog, commentsFeedId) {
            $scope.commentsFeedId = commentsFeedId;
            $scope.hide = function () {
                $mdDialog.hide();
            };
            $scope.cancel = function () {
                $mdDialog.cancel();
            };
            $scope.answer = function (answer) {
                $mdDialog.hide(answer);
            };
        };

        $scope.showGallery = function (ev, isrc) {
            $mdDialog.show({
                controller: DialogController,
                templateUrl: '/app/mainpage/searchGallery.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                locals: {
                    message: angular.copy(isrc)
                }
            })
            .then(function (answer) {
                if (answer === 'Edit') {
                } else if (answer === 'Cancel') {
                    console.log("Cancelled");
                }

            }, function () {
                console.log("dialog Closed");
            });
        };

    }
})();
