﻿(function () {
    'use strict';
    angular
        .module('mainpage')
        .controller('addMusicCatalog', addMusicCatalog);

    addMusicCatalog.$inject = ['$scope', '$http', '$mdDialog'];

    function addMusicCatalog($scope, $http, $mdDialog) {

        var basePath = '/';

        $scope.isSubmit = false;
        $scope.musictagErrorMessage = "You need to select at least one tag";

        $scope.userMusicCatalog = {};
        $scope.stepsModel = [];
        $scope.showSpinner = false;
        $scope.activated = false;

        if ($scope.message1 != null) {
            $scope.title = 'Edit Music Catalog ' + $scope.message1.catalogName;
            $scope.userMusicCatalog = $scope.message1;
            //activate();
        } else {
            $scope.title = 'Add Music Catalog';

            $http.get(basePath + 'countryList/musicTags.json').then(function (data) {
                $scope.musicTagsList = data.data;
            });
        }

        $scope.myImage = '';
        $scope.myCroppedImage = '';





        function activate() {
            $http({
                method: 'POST',
                url: basePath + 'MainPage/editMusicCatalog',
                data: $scope.userMusicCatalog,
                headers: "application/json"
            }).then(function (data) {
                $scope.userMusicCatalog = data.data;
            });

        }


        $scope.imageIsLoaded = function (e) {
            $scope.$apply(function () {
                // alert(e.target.result)
                $scope.stepsModel.push(e.target.result);
                var imageSrc = e.target.result.split(',')[1];
                //alert(imageSrc);
                $scope.userMusicCatalog.imageSrc = imageSrc;

                //$scope.postFeed.imageType = reader.readAsDataURL(element.target.files[0].type);
            });
        }

        $scope.imageUpload = function (element) {
            if (element.target.files.length > 0) {
                var reader = new FileReader();
                reader.onload = $scope.imageIsLoaded;
                //console.log(element.target.files[0])
                reader.readAsDataURL(element.target.files[0]);
                $scope.userMusicCatalog.imageName = element.target.files[0].name;
                $scope.userMusicCatalog.imageType = element.target.files[0].type;
                debugger;
                var handleFileSelect = function (element) {
                    var file = element.currentTarget.files[0];
                    var reader = new FileReader();
                    reader.onload = function (element) {
                        $scope.$apply(function ($scope) {
                            $scope.myImage = element.target.result;
                            //alert($scope.myCroppedImage);


                        });
                    };
                    reader.readAsDataURL(file);
                };
                angular.element(document.querySelector('#myImage')).on('change', handleFileSelect);
                //readURL(element.target);


            }
        }


        $scope.audioIsLoaded = function (e) {
            $scope.$apply(function () {
                // alert(e.target.result)
                $scope.stepsModel.push(e.target.result);
                var audioSrc = e.target.result.split(',')[1];
                //alert(imageSrc);
                $scope.userMusicCatalog.audioSrc = audioSrc;

                //$scope.postFeed.imageType = reader.readAsDataURL(element.target.files[0].type);
            });
        }

        $scope.audioUpload = function (element) {
            if (element.target.files.length > 0) {
                var reader = new FileReader();
                reader.onload = $scope.audioIsLoaded;
                //console.log(element.target.files[0])
                reader.readAsDataURL(element.target.files[0]);
                $scope.userMusicCatalog.songName = element.target.files[0].name;
                $scope.userMusicCatalog.audioType = element.target.files[0].type;
                //readURL(element.target);
            }
        }

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function () {
                    $('#blah')
                        .attr('src', reader.result)
                        .width(150)
                        .height(200);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        $scope.addMusicCatalog = function (frmMusicCatalog) {

            $scope.myCroppedImage = $scope.myCroppedImage.split(',')[1];
            $scope.userMusicCatalog.imageSrc = $scope.myCroppedImage;
            $scope.isSubmit = true;
            $scope.IsmusictagError = false;
            if ($scope.userMusicCatalog.musicTag1 === undefined && $scope.userMusicCatalog.musicTag2 === undefined && $scope.userMusicCatalog.musicTag3 === undefined) {
                $scope.IsmusictagError = true;
            }
           
            $scope.activated = true;
            if (frmMusicCatalog.$valid && $scope.IsmusictagError==false) {
                $scope.showSpinner = true;
                $http({
                    method: 'POST',
                    url: basePath + 'MainPage/addMusicCatalog',
                    data: $scope.userMusicCatalog,
                    headers: "application/json"
                })
               .then(function (result) {
                   if (result) {
                       $mdDialog.hide("Edit");
                       $scope.activated = false;
                       $scope.showSpinner = false;
                   }
                   else {
                       $mdDialog.hide("Cancel");
                   }
               });
            }
        };
    }
})();
