﻿(function () {
    'use strict';
    angular
        .module('main')
        .controller('editProfilepic', editProfilepic);
    editProfilepic.$inject = ['$scope', '$http', '$mdDialog'];

    function editProfilepic($scope, $http, $mdDialog) {
        var basePath = '/';
        $scope.title = 'Edit Profile-pic';

        $scope.isSubmit = false;
        $scope.showSpinner = false;
        $scope.activated = false;

        $scope.editImages = {};
        $scope.userDetails = {};

        $scope.myImage = '';
        $scope.myCroppedImage = '';
        $scope.stepsModel = [];
        activate();
        function activate() {

        }

        $scope.imageIsLoaded = function (e) {
            $scope.$apply(function () {
                $scope.stepsModel.push(e.target.result);
                var imageSrc = e.target.result.split(',')[1];
                $scope.editImages.fileUrl = imageSrc;
            });
        }

        $scope.imageUpload = function (element) {
            if (element.target.files.length > 0) {
                var reader = new FileReader();
                reader.onload = $scope.imageIsLoaded;
                reader.readAsDataURL(element.target.files[0]);
                $scope.editImages.fileName = element.target.files[0].name;
                $scope.editImages.fileType = element.target.files[0].type;
                var file = element.currentTarget.files[0];
                var reader = new FileReader();
                reader.onload = function (element) {
                    $scope.$apply(function ($scope) {
                        $scope.myImage = element.target.result;
                    });
                };
                reader.readAsDataURL(file);
            }
        }
      
        $scope.editUserProfilepic = function () {
            var plink = $scope.myCroppedImage.split(',')[1];
            if (plink != '' && plink != undefined) {
                $scope.showSpinner = true;
                $scope.activated = true;
                $scope.userDetails.profileLink = plink;
                $http({
                    method: 'POST',
                    url: basePath + 'MainPage/editProfilePic',
                    data: $scope.userDetails,
                    headers: "application/json"
                })
               .then(function (result) {
                   if (result) {
                       $scope.dialogBox("Success", "Your profile has been updated successfully.");
                       $scope.showSpinner = false;
                       $scope.activated = false;
                       setTimeout(function () {
                           window.location.reload();
                       }, 3000);
                   }
               }, function () {
                   $scope.showSpinner = false;
                   $scope.activated = false;
                   $scope.dialogBox("Failure", "Failed! try again");
               });
            }
        }

        $scope.dialogBox = function (title, content) {
            $mdDialog.show(
             $mdDialog.alert()
            .parent(angular.element(document.querySelector('#popupContainer')))
            .clickOutsideToClose(true)
            .title(title)
            .textContent(content)
            .ariaLabel('Alert Dialog Demo')
            .ok('Ok')
             );
        };

    }
})();
