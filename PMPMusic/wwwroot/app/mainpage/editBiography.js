﻿(function () {
    'use strict';

    angular
        .module('main')
        .controller('editBiography', editBiography);

    editBiography.$inject = ['$scope', '$http', '$mdDialog'];

    function editBiography($scope, $http, $mdDialog) {
        var basePath = '/';
        $scope.isSubmit = false;
        $scope.UserBiography = [];
        $scope.stepsModel = [];
        $scope.showSpinner = false;
        $scope.activated = false;
        $scope.title = 'User Biography';

        activate();
        function activate() {
            $http({
                method: 'POST',
                url: basePath + 'MainPage/getUserTags',
                headers: "application/json"
            }).then(function (data) {
                $scope.UserBiography = data.data;
            });
        }

        $scope.editBiography = function (frmRegister) {
            $scope.isSubmit = true;
            $scope.activated = true;
            if (frmRegister.$valid) {
                $scope.showSpinner = true;
                $http({
                    method: 'POST',
                    url: basePath + 'MainPage/editBiography',
                    data: $scope.UserBiography,
                    headers: "application/json"
                })
               .then(function (result) {
                   if (result) {
                       $mdDialog.hide("Edit");
                       $scope.activated = false;
                       $scope.showSpinner = false;
                   }
                   else {
                       $mdDialog.hide("Cancel");
                   }
               });
            }
        };
    }
})();
