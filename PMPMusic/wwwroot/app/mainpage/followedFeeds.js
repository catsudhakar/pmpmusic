﻿
(function () {
    'use strict';
    angular
        .module('mainpage')
        .controller('followedFeeds', followedFeeds);
    followedFeeds.$inject = ['$scope', '$location', '$http', 'viewModelHelper', '$mdDialog', '$timeout'];
    function followedFeeds($scope, $location, $http, viewModelHelper, $mdDialog, $timeout) {

        $scope.title = 'Followed Users';
        var basePath = "/";

        $scope.LoadPosts = 0;
        $scope.totalActiveFeeds = 0;

        $scope.showSpinner = false;
        $scope.activated = false;
        $scope.activated1 = false;
        $scope.showSpinner1 = false;
        $scope.busy = false;

        $scope.isSubmit = false;
        $scope.autoplay = false;
        $scope.audioPlayButton = true;
        $scope.audioPaused = false;
        $scope.isCommentSubmit = false;

        $scope.postComments = {};
        $scope.postFeed = {};

        $scope.stepsModel = [];
        $scope.feedsList = [];
        $scope.feedsListPartial = [];

        activate();

        function activate(newPost) {
           
            $http.post(basePath + 'MainPage/getFollowedFeeds').then(function (data) {
                $scope.feedsListPartial = data.data.followFeedDetails;
                $scope.totalActiveFeeds = data.data.followFeedDetails.length;
                for (var i = 0; i < $scope.totalActiveFeeds; i++) {
                    var time = getdateformate($scope.feedsListPartial[i].followDate);
                    $scope.feedsListPartial[i].followDate = time;
                }
            });
         
        }

        function getdateformate(msgDate) {
            var date = new Date(msgDate);
            var time = date.toDateString() + " | " + date.toLocaleTimeString();
            return time;
        }

    }
})();




