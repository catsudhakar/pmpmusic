﻿(function () {
    'use strict';

    angular
        .module('mainpage')
        .controller('showSubmitedFromUserFeed', showSubmitedFromUserFeed);

    showSubmitedFromUserFeed.$inject = ['$scope', '$http', '$mdDialog'];

    function showSubmitedFromUserFeed($scope, $http, $mdDialog) {
        $scope.title = 'SHOWING SUBMIT TO THIS FEED DETAILS';
        var basePath = "/";

        $scope.feedId = '';
        $scope.userId = '';

        $scope.musicCatelogs = [];
        $scope.imageGallery = [];
        $scope.getVideos = [];
        $scope.subFeeds = {};
        $scope.stepsModel = [];
        $scope.myImage = '';
        $scope.myCroppedImage = '';
        $scope.feedTitle = '';
        activate();

        function activate() {
            var result = $scope.commentsFeedId.split(',');
            $scope.feedId = result[0];
            $scope.userId = result[1];
            $scope.subHstry = result[2];
            $scope.feedTitle = result[3];
            getMusicCatelog();
            getImageGallery();
            getVideos();
        }

        function getMusicCatelog() {
            $http({
                method: 'POST',
                url: basePath + 'MainPage/getUserCatelogsById?userId=' + $scope.userId + '&feedId=' + $scope.feedId + '&type=audio&subHstry=' + $scope.subHstry,
                headers: "application/json"
            }).then(function (data) {
                $scope.musicCatelogs = data.data;
                for (var i = 0; i < $scope.musicCatelogs.length; i++) {
                    if (!$scope.musicCatelogs[i].fileUrl) {
                        $scope.autoplay = false;
                        $scope.musicCatelogs[i].audioPlayButton = $scope.autoplay;
                        $scope.musicCatelogs[i].noAudioLink = false;
                        $scope.musicCatelogs[i].isAudioPlaying = false;
                        $scope.musicCatelogs[i].playBtnImg = "playBtnImg" + i;
                    } else {
                        $scope.autoplay = true;
                        $scope.musicCatelogs[i].audioPlayButton = $scope.autoplay;
                        $scope.musicCatelogs[i].noAudioLink = true;
                        $scope.musicCatelogs[i].isAudioPlaying = false;
                        $scope.musicCatelogs[i].playBtnImg = "playBtnImg" + i;
                    }
                }
            }, function (error) {
                // console.log(error, " can't get data.");
            });
        }

        function getImageGallery() {
            $http({
                method: 'POST',
                url: basePath + 'MainPage/getUserCatelogsById?userId=' + $scope.userId + '&feedId=' + $scope.feedId + '&type=image&subHstry=' + $scope.subHstry,
                headers: "application/json"
            }).then(function (data) {
                $scope.imageGallery = data.data;
            }, function (error) {
            });
        }

        function getVideos() {
            $http({
                method: 'POST',
                url: basePath + 'MainPage/getUserCatelogsById?userId=' + $scope.userId + '&feedId=' + $scope.feedId + '&type=video&subHstry=' + $scope.subHstry,
                headers: "application/json"
            }).then(function (data) {
                $scope.getVideos = data.data;
            }, function (error) {
            });
        }

        $scope.dialogBox = function (title, content) {
            $mdDialog.show(
             $mdDialog.alert()
            .parent(angular.element(document.querySelector('#popupContainer')))
            .clickOutsideToClose(true)
            .title(title)
            .textContent(content)
            .ariaLabel('Alert Dialog Demo')
            .ok('Ok')
             );
        };

        $scope.imageIsLoaded = function (e) {
            $scope.$apply(function () {
                $scope.stepsModel.push(e.target.result);
                var imageSrc = e.target.result.split(',')[1];
                $scope.subFeeds.fileUrlI = imageSrc;
            });
        }

        $scope.num = 0;
        var audioElement = document.getElementById('audio');
        $scope.playMusic = function (index) {
            $scope.autoplay = $scope.musicCatelogs[index].audioPlayButton;
            if ($scope.musicCatelogs[index].fileUrl != null || $scope.musicCatelogs[index].fileUrl != undefined) {
                $scope.currentTrack = $scope.musicCatelogs[index].fileUrl;
            }
            $scope.currentIndex = index;
            audioElement.src = $scope.currentTrack;

            if ($scope.audioPaused) {
                audioElement.autoplay = false;
                $("#play_btn").show();
                $("#pause_btn").hide();
                $scope.audioPaused = false;
                $scope.musicCatelogs[index].isAudioPlaying = false;
            } else {
                audioElement.autoplay = $scope.autoplay;
                $scope.musicCatelogs[index].isAudioPlaying = true;
                $("#play_btn").hide();
                $("#pause_btn").show();
                $scope.audioPaused = true;
            }
        }



    }
})();