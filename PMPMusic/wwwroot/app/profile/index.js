﻿(function () {
    'use strict';

    angular
        .module('profile')
        .controller('index', index);

    index.$inject = ['$scope', '$http', '$location'];

    function index($scope, $http, $location) {
        $scope.title = 'PMP Music';
        var basePath = "/";
        $scope.SelectCountryList = [];
        $scope.country = '';
        $scope.feedsList = [];
        $scope.userDetails = {};
        $scope.profileVisitor = {};
        $scope.feedsListLatest = [{ 'title': 'No records Found', 'Id': '' }];
        $scope.getSocialMediaLinks = [];

        activate();

        function activate() {
          
           // var userId = '996d7e2f-280b-4d3d-9a57-dfde0df2a445';
            $http.post(basePath + 'MainPage/getAllFeedDetails').then(function (data) {
                $scope.feedsList = data.data.postFeeds;
                if (data.data.postFeedsLatest != '')
                    $scope.feedsListLatest = data.data.postFeedsLatest;
                $scope.userDetails = data.data.userDetails;
                $scope.profileVisitor = data.data.profileUser;
                //$scope.totalActiveFeeds = data.data.length;
            });

            $http.post(basePath + 'MainPage/getSocialMedia').then(function (data) {
                $scope.getSocialMediaLinks = data.data;
            });
        }

    }
})();



