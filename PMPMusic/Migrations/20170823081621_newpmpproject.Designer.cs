﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using PMPMusic.Models;

namespace PMPMusic.Migrations
{
    [DbContext(typeof(ApplicationDBContext))]
    [Migration("20170823081621_newpmpproject")]
    partial class newpmpproject
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.1")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("RoleId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<string>", b =>
                {
                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserToken<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("LoginProvider");

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("PMPMusic.Models.ApplicationRole", b =>
                {
                    b.Property<string>("Id");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Name")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("NormalizedName")
                        .HasAnnotation("MaxLength", 256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .HasName("RoleNameIndex");

                    b.ToTable("AspNetRoles");
                });

            modelBuilder.Entity("PMPMusic.Models.ApplicationUser", b =>
                {
                    b.Property<string>("Id");

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Country");

                    b.Property<string>("Email")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<string>("FirstName");

                    b.Property<bool>("IsActive");

                    b.Property<bool>("IsFeaturedUser");

                    b.Property<bool>("IsVerifyedUser");

                    b.Property<string>("LastName");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("NormalizedEmail")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("NormalizedUserName")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<string>("ProfileLink");

                    b.Property<DateTime>("RegisteredOn");

                    b.Property<string>("SecurityStamp");

                    b.Property<bool>("TermsAndContions");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<string>("UserName")
                        .HasAnnotation("MaxLength", 256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasName("UserNameIndex");

                    b.ToTable("AspNetUsers");
                });

            modelBuilder.Entity("PMPMusic.Models.ContactUs", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Email")
                        .IsRequired();

                    b.Property<string>("Message")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 1000);

                    b.Property<string>("Name")
                        .IsRequired();

                    b.HasKey("Id");

                    b.ToTable("ContactUs");
                });

            modelBuilder.Entity("PMPMusic.Models.FollowFeed", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy")
                        .IsRequired();

                    b.Property<DateTime>("FollowDate");

                    b.Property<Guid>("FollowingUserId");

                    b.Property<bool>("IsFollow");

                    b.Property<Guid>("PostId");

                    b.HasKey("Id");

                    b.HasIndex("CreatedBy");

                    b.ToTable("FollowFeed");
                });

            modelBuilder.Entity("PMPMusic.Models.LikeFeed", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy")
                        .IsRequired();

                    b.Property<bool>("IsLike");

                    b.Property<Guid>("PostId");

                    b.Property<DateTime>("UpdatedDate");

                    b.HasKey("Id");

                    b.HasIndex("CreatedBy");

                    b.ToTable("LikeFeed");
                });

            modelBuilder.Entity("PMPMusic.Models.Message", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("MessageDate");

                    b.Property<string>("MessageFrom")
                        .IsRequired();

                    b.Property<string>("MessageTo");

                    b.Property<Guid>("PostId");

                    b.Property<string>("Subject");

                    b.Property<bool>("isDelete");

                    b.Property<bool>("isSeenStatus");

                    b.HasKey("Id");

                    b.HasIndex("MessageFrom");

                    b.ToTable("Message");
                });

            modelBuilder.Entity("PMPMusic.Models.MusicCatalog", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CatalogId")
                        .IsRequired();

                    b.Property<string>("CatalogName")
                        .IsRequired();

                    b.Property<string>("CreatedBy")
                        .IsRequired();

                    b.Property<DateTime>("CreatedOn");

                    b.Property<string>("ImageLink");

                    b.Property<string>("ImageName");

                    b.Property<string>("MusicTag1");

                    b.Property<string>("MusicTag2");

                    b.Property<string>("MusicTag3");

                    b.Property<string>("SongLink");

                    b.Property<string>("SongName");

                    b.Property<string>("UpdatedBy");

                    b.Property<DateTime>("UpdatedOn");

                    b.HasKey("Id");

                    b.HasIndex("CreatedBy");

                    b.ToTable("MusicCatalog");
                });

            modelBuilder.Entity("PMPMusic.Models.PostComment", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Comment")
                        .IsRequired();

                    b.Property<Guid>("CreatedBy");

                    b.Property<DateTime>("CreatedOn");

                    b.Property<Guid>("PostId");

                    b.HasKey("Id");

                    b.ToTable("PostComment");
                });

            modelBuilder.Entity("PMPMusic.Models.PostFeed", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("AudioLink");

                    b.Property<string>("AudioName");

                    b.Property<string>("CreatedBy")
                        .IsRequired();

                    b.Property<DateTime>("CreatedOn");

                    b.Property<string>("Description");

                    b.Property<DateTime>("ExpireDate");

                    b.Property<string>("ImageLink");

                    b.Property<string>("ImageName");

                    b.Property<bool>("IsActive");

                    b.Property<bool>("IsTrendingFeed");

                    b.Property<bool>("SaveFeed");

                    b.Property<string>("Title");

                    b.Property<string>("UpdatedBy");

                    b.Property<DateTime>("UpdatedOn");

                    b.Property<string>("VideoLink");

                    b.Property<string>("VideoName");

                    b.HasKey("Id");

                    b.HasIndex("CreatedBy");

                    b.ToTable("PostFeed");
                });

            modelBuilder.Entity("PMPMusic.Models.SavedFeed", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy")
                        .IsRequired();

                    b.Property<DateTime>("CreatedOn");

                    b.Property<string>("PostId");

                    b.Property<bool>("SaveFeed");

                    b.Property<DateTime>("UpdatedOn");

                    b.HasKey("Id");

                    b.HasIndex("CreatedBy");

                    b.ToTable("SavedFeed");
                });

            modelBuilder.Entity("PMPMusic.Models.SearchSkillsTags", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy")
                        .IsRequired();

                    b.Property<DateTime>("SearchOn");

                    b.Property<string>("SearchTags");

                    b.HasKey("Id");

                    b.HasIndex("CreatedBy");

                    b.ToTable("SearchSkillsTags");
                });

            modelBuilder.Entity("PMPMusic.Models.SocialMedia", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy")
                        .IsRequired();

                    b.Property<string>("MediaLink");

                    b.Property<string>("MediaName");

                    b.HasKey("Id");

                    b.HasIndex("CreatedBy");

                    b.ToTable("SocialMedia");
                });

            modelBuilder.Entity("PMPMusic.Models.SongPlayList", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CatelogId")
                        .IsRequired();

                    b.Property<string>("CreatedBy")
                        .IsRequired();

                    b.Property<DateTime>("CreatedOn");

                    b.Property<string>("SongName")
                        .IsRequired();

                    b.Property<string>("SongUrl")
                        .IsRequired();

                    b.Property<string>("UpdatedBy");

                    b.Property<DateTime>("UpdatedOn");

                    b.HasKey("Id");

                    b.HasIndex("CreatedBy");

                    b.ToTable("SongPalyList");
                });

            modelBuilder.Entity("PMPMusic.Models.SubFeeds", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("CategoryId");

                    b.Property<string>("CreatedBy")
                        .IsRequired();

                    b.Property<DateTime>("CreatedOn");

                    b.Property<string>("FileName");

                    b.Property<string>("FileType");

                    b.Property<string>("FileUrl");

                    b.Property<bool>("IsActive");

                    b.Property<string>("MainFeedId");

                    b.HasKey("Id");

                    b.HasIndex("CreatedBy");

                    b.ToTable("SubFeeds");
                });

            modelBuilder.Entity("PMPMusic.Models.UserGallery", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy")
                        .IsRequired();

                    b.Property<DateTime>("CreatedOn");

                    b.Property<bool>("Status");

                    b.Property<string>("UpdatedBy");

                    b.Property<DateTime>("UpdatedOn");

                    b.Property<string>("fileName")
                        .IsRequired();

                    b.Property<string>("fileType");

                    b.Property<string>("fileUrl")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("CreatedBy");

                    b.ToTable("UserGallery");
                });

            modelBuilder.Entity("PMPMusic.Models.UserTags", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("MainTag");

                    b.Property<string>("Tag1");

                    b.Property<string>("Tag2");

                    b.Property<string>("Tag3");

                    b.Property<string>("UserBiography");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("UserTags");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<string>", b =>
                {
                    b.HasOne("PMPMusic.Models.ApplicationRole")
                        .WithMany("Claims")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<string>", b =>
                {
                    b.HasOne("PMPMusic.Models.ApplicationUser")
                        .WithMany("Claims")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<string>", b =>
                {
                    b.HasOne("PMPMusic.Models.ApplicationUser")
                        .WithMany("Logins")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<string>", b =>
                {
                    b.HasOne("PMPMusic.Models.ApplicationRole")
                        .WithMany("Users")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("PMPMusic.Models.ApplicationUser")
                        .WithMany("Roles")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("PMPMusic.Models.FollowFeed", b =>
                {
                    b.HasOne("PMPMusic.Models.ApplicationUser", "FollowingUser")
                        .WithMany()
                        .HasForeignKey("CreatedBy")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("PMPMusic.Models.LikeFeed", b =>
                {
                    b.HasOne("PMPMusic.Models.ApplicationUser", "FollowingUser")
                        .WithMany()
                        .HasForeignKey("CreatedBy")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("PMPMusic.Models.Message", b =>
                {
                    b.HasOne("PMPMusic.Models.ApplicationUser", "FormUser")
                        .WithMany()
                        .HasForeignKey("MessageFrom")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("PMPMusic.Models.MusicCatalog", b =>
                {
                    b.HasOne("PMPMusic.Models.ApplicationUser", "User")
                        .WithMany()
                        .HasForeignKey("CreatedBy")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("PMPMusic.Models.PostFeed", b =>
                {
                    b.HasOne("PMPMusic.Models.ApplicationUser", "User")
                        .WithMany()
                        .HasForeignKey("CreatedBy")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("PMPMusic.Models.SavedFeed", b =>
                {
                    b.HasOne("PMPMusic.Models.ApplicationUser", "User")
                        .WithMany()
                        .HasForeignKey("CreatedBy")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("PMPMusic.Models.SearchSkillsTags", b =>
                {
                    b.HasOne("PMPMusic.Models.ApplicationUser", "User")
                        .WithMany()
                        .HasForeignKey("CreatedBy")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("PMPMusic.Models.SocialMedia", b =>
                {
                    b.HasOne("PMPMusic.Models.ApplicationUser", "User")
                        .WithMany()
                        .HasForeignKey("CreatedBy")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("PMPMusic.Models.SongPlayList", b =>
                {
                    b.HasOne("PMPMusic.Models.ApplicationUser", "User")
                        .WithMany()
                        .HasForeignKey("CreatedBy")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("PMPMusic.Models.SubFeeds", b =>
                {
                    b.HasOne("PMPMusic.Models.ApplicationUser", "User")
                        .WithMany()
                        .HasForeignKey("CreatedBy")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("PMPMusic.Models.UserGallery", b =>
                {
                    b.HasOne("PMPMusic.Models.ApplicationUser", "User")
                        .WithMany()
                        .HasForeignKey("CreatedBy")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("PMPMusic.Models.UserTags", b =>
                {
                    b.HasOne("PMPMusic.Models.ApplicationUser", "User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
