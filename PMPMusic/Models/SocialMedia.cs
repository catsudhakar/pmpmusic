﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PMPMusic.Models
{


    [Table("SocialMedia")]
    public class SocialMedia
    {

        public Guid Id { get; set; }

       
        public string MediaLink { get; set; }

        public string MediaName { get; set; }

        public string CreatedBy { get; set; }

        [Required]
        [ForeignKey("CreatedBy")]
        public virtual ApplicationUser User { get; set; }

        


       
    }
}

