﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace PMPMusic.Models
{
    public class AccountViewModels
    {

    }
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }

        public ICollection<SelectListItem> Providers { get; set; }

        public string ReturnUrl { get; set; }

        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        public string Code { get; set; }

        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The password must be at least 6", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        //[Required]
        //[StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 8)]
        //[DataType(DataType.Password)]
        //[RegularExpression(@"(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[~!#$%^&*@_+-.,<>?|=:;{}/\()])[A-Za-z\d~!#$%^&*@_+-.,<>?|=:;{}/\()]{8,}", ErrorMessage = "Password should contain at least 8 characters with one number,one symbol,one uppercase,one lower case letter")]
        //public string Password { get; set; }

        //[DataType(DataType.Password)]
        //[Display(Name = "Confirm password")]
        //[Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        //public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {

        public string Id { get; set; }

        [Required]
        [Display(Name = "FirstName")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "LastName")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "Country")]
        public string Country { get; set; }


        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        [Microsoft.AspNetCore.Mvc.Remote("doesEmailExist", "Account", HttpMethod = "POST", ErrorMessage = "Email-Id already exists. Please enter a different Email-Id.")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 8)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        [RegularExpression(@"(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[~!#$%^&*@_+-.,<>?|=:;{}/\()])[A-Za-z\d~!#$%^&*@_+-.,<>?|=:;{}/\()]{8,}", ErrorMessage = "Password should contain at least 8 characters with one number,one symbol,one uppercase,one lower case letter")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        //[Required]
        [Display(Name = "Profile")]
        public string ProfileLink { get; set; }

        public ApplicationUser User { get; set; }

        public string ImageSrc { get; set; }

        public string ImageType { get; set; }

    }

    public class RoleViewModel
    {
        [Required]
        [Display(Name = "Role Name")]
        public string Name { get; set; }
    }

    public class ContactUsViewModel
    {
        [Required]
        [Display(Name = "Full Name")]
        public string Name { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        [Display(Name = "Message")]
        public string Message { get; set; }
    }

    public class UserRegistrationViewModel
    {

       

        [Required]
        [StringLength(40, ErrorMessage = "The Name must be at least 3 Characters", MinimumLength = 3)]
        [Display(Name = "FirstName")]
        public string FirstName { get; set; }

        //[Required]
        //[Display(Name = "LastName")]
        //public string LastName { get; set; }

        //[Required]
        //[Display(Name = "Country")]
        //public string Country { get; set; }


        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        [Microsoft.AspNetCore.Mvc.Remote("doesEmailExist", "Account", HttpMethod = "POST", ErrorMessage = "Email-Id already exists. Please enter a different Email-Id.")]
        public string Email { get; set; }



        //[StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 8)]
        //[RegularExpression(@"(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[~!#$%^&*@_+-.,<>?|=:;{}/\()])[A-Za-z\d~!#$%^&*@_+-.,<>?|=:;{}/\()]{8,}", ErrorMessage = "Password should contain at least 8 characters with one number,one symbol,one uppercase,one lower case letter")]
        [Required]
        [StringLength(100, ErrorMessage = "The password must be at least 6", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Tag1 { get; set; }

        [Required]
        public string MainTag { get; set; }

        public bool TermsAndContions { get; set; }

        public string Tag2 { get; set; }

        public string Tag3 { get; set; }

        public string CardName { get; set; }

        public string CardNumber { get; set; }

        public string cvvCode { get; set; }

        public string expMonth { get; set; }

        public string expYear { get; set; }

        public string PaymentOption { get; set; }

        


        ////[Required]
        //[Display(Name = "Profile")]
        //public string ProfileLink { get; set; }

        //public ApplicationUser User { get; set; }

        //public string ImageSrc { get; set; }

        //public string ImageType { get; set; }

    }

}
