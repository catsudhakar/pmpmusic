﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PMPMusic.Models
{
    [Table("PostComment")]
    public class PostComment
    {
        public Guid Id { get; set; }

        [Required]
        public string Comment { get; set; }

        [Required]
        public Guid PostId { get; set; }

        [Required]
        public Guid CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        

    }
}


