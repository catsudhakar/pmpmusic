﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PMPMusic.Models
{
    public class ApplicationDBContext : IdentityDbContext<ApplicationUser, ApplicationRole, string>
    {

        public ApplicationDBContext(DbContextOptions<ApplicationDBContext> options)
         : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }
        public DbSet<ContactUs> ContactUs { get; set; }

        public DbSet<PostFeed> PostFeed { get; set; }

        public DbSet<PostComment> PostComment { get; set; }

        public DbSet<ApplicationUser> AspNetUsers { get; set; }

        public DbSet<UserTags> UserTags { get; set; }

        public DbSet<FollowFeed> FollowFeeds { get; set; }

        public DbSet<MusicCatalog> MusicCatalog { get; set; }

        public DbSet<UserGallery> UserGallery { get; set; }

        public DbSet<SocialMedia> SocialMedia { get; set; }

        public DbSet<LikeFeed> LikeFeed { get; set; }

        public DbSet<Message> Message { get; set; }

        public DbSet<SubFeeds> SubFeeds { get; set; }

        public DbSet<SongPlayList> SongPlayList { get; set; }

        public DbSet<SearchSkillsTags> SearchSkillsTags { get; set; }

        public DbSet<SavedFeed> SavedFeeds { get; set; }
        
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }


    }

}
