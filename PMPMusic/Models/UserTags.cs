﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PMPMusic.Models
{
    [Table("UserTags")]
    public class UserTags
    {
        [Required]
        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }

        public Guid Id { get; set; }
        public string MainTag { get; set; }
        public string Tag1 { get; set; }
        public string Tag2 { get; set; }
        public string Tag3 { get; set; }
        public string UserBiography { get; set; }
    }
}
