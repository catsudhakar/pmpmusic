﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PMPMusic.Models
{


    [Table("Message")]
    public class Message
    {

        public Guid Id { get; set; }

        [Required]
        public Guid PostId { get; set; }

        public string MessageFrom { get; set; }

        [Required]
        [ForeignKey("MessageFrom")]
        public virtual ApplicationUser FormUser { get; set; }

        public string MessageTo { get; set; }

        public string Subject { get; set; }

        public DateTime MessageDate { get; set; }

        public bool isSeenStatus { get; set; }

        public bool isDelete { get; set; }

    }
}

