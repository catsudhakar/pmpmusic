﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMPMusic.Models
{
        [Table("SearchSkillsTags")]
        public class SearchSkillsTags
        {
            public Guid Id { get; set; }

            public string SearchTags { get; set; }

            public string CreatedBy { get; set; }

            [Required]
            [ForeignKey("CreatedBy")]
            public virtual ApplicationUser User { get; set; }
        
            public DateTime SearchOn { get; set; }
        }
}
