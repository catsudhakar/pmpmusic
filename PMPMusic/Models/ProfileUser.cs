﻿using System;

namespace PMPMusic.ViewModel
{
    public class ProfileUser
    {
        public bool showProfile { get; set; }
        public bool showProfileLinks { get; set; }
    }
}