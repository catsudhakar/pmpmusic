﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PMPMusic.Models
{
    [Table("FollowFeed")]
    public class FollowFeed
    {
        public Guid Id { get; set; }

        [Required]
        public Guid PostId { get; set; }

        public bool IsFollow { get; set; }

        [Required]
        public Guid FollowingUserId { get; set; }

        public string CreatedBy { get; set; }

        [Required]
        [ForeignKey("CreatedBy")]
        public virtual ApplicationUser FollowingUser { get; set; }

      
        public DateTime FollowDate { get; set; }
    }
}
