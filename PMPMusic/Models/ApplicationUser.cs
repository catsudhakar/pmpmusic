﻿
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PMPMusic.Models
{
    public class ApplicationUser : IdentityUser
    {
        public string Country { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ProfileLink { get; set; }
        public DateTime RegisteredOn { get; set; }

        public bool IsActive { get; set; }
        public bool IsFeaturedUser { get; set; }
        public bool IsVerifyedUser { get; set; }
        public bool TermsAndContions { get; set; }

        public static explicit operator ApplicationUser(string v)
        {
            throw new NotImplementedException();
        }
    }

}

