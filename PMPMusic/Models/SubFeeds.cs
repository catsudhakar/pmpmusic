﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PMPMusic.Models
{
    [Table("SubFeeds")]
    public class SubFeeds
    {
        public Guid Id { get; set; }

        public Guid CategoryId { get; set; }

        public string MainFeedId { get; set; }

        //[Required]
        //[ForeignKey("FeedId")]
        //public virtual PostFeed MainFeed { get; set; }


        public string FileName { get; set; }

        public string FileType { get; set; }

        public string FileUrl { get; set; }

        [Required]
        [ForeignKey("CreatedBy")]
        public virtual ApplicationUser User { get; set; }

        public DateTime CreatedOn { get; set; }

        public bool IsActive { get; set; }





    }
}


