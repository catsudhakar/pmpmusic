﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMPMusic.Models
{
    [Table("SongPalyList")]
    public class SongPlayList
    {
        public Guid Id { get; set; }

        [Required]
        public string SongName { get; set; }

        [Required]
        public string SongUrl { get; set; }

        [Required]
        public string CreatedBy { get; set; }

        [Required]
        [ForeignKey("CreatedBy")]
        public virtual ApplicationUser User { get; set; }

        [Required]
        public string CatelogId { get; set; }

        public DateTime CreatedOn { get; set; }
      
        public string UpdatedBy { get; set; }

        public DateTime UpdatedOn { get; set; }

    }
}
