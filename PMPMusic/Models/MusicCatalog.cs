﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PMPMusic.Models
{
    [Table("MusicCatalog")]
    public class MusicCatalog
    {
        [Required]
        public Guid Id { get; set; }

        [Required]
        public string CatalogId { get; set; }

        [Required]
        public string CatalogName { get; set; }
        [Required]

        public string CreatedBy { get; set; }

        [Required]
        [ForeignKey("CreatedBy")]
        public virtual ApplicationUser User { get; set; }

        public DateTime CreatedOn { get; set; }

        public string UpdatedBy { get; set; }

        public DateTime UpdatedOn { get; set; }

        public string ImageName { get; set; }

        public string ImageLink { get; set; }

        public string SongName { get; set; }

        public string SongLink { get; set; }

        public string MusicTag1 { get; set; }

        public string MusicTag2 { get; set; }

        public string MusicTag3 { get; set; }



    }
}
