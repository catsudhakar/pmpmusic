﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PMPMusic.Models
{
    [Table("UserGallery")]
    public class UserGallery
    {
        [Required]
        public Guid Id { get; set; }
        [Required]
        public string fileUrl { get; set; }
        [Required]
        public string fileName { get; set; }

        public string fileType { get; set; }

        public string CreatedBy { get; set; }

        [Required]
        [ForeignKey("CreatedBy")]
        public virtual ApplicationUser User { get; set; }

        public DateTime CreatedOn { get; set; }

        public string UpdatedBy { get; set; }

        public DateTime UpdatedOn { get; set; }

        public bool Status { get; set; }
    }
}
