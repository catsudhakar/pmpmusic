﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PMPMusic.Models
{
    [Table("PostFeed")]
    public class PostFeed
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string ImageLink { get; set; }

        public string AudioLink { get; set; }

        public string VideoLink { get; set; }

        public bool IsActive { get; set; }

        public bool IsTrendingFeed { get; set; }

        public string ImageName { get; set; }

        public string AudioName { get; set; }

        public string VideoName { get; set; }

        //public string CreatedBy { get; set; }

        [Required]
        [ForeignKey("CreatedBy")]
        public virtual ApplicationUser User { get; set; }

        public DateTime CreatedOn { get; set; }

        public string UpdatedBy { get; set; }

        public DateTime UpdatedOn { get; set; }

        public DateTime ExpireDate { get; set; }

        public bool SaveFeed { get; set; }


    }
}

