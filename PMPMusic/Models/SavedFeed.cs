﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMPMusic.Models
{
    [Table("SavedFeed")]
    public class SavedFeed
    {
        public Guid Id { get; set; }

        public string PostId { get; set; }

        public string CreatedBy { get; set; }

        [Required]
        [ForeignKey("CreatedBy")]
        public virtual ApplicationUser User { get; set; }

        public DateTime CreatedOn { get; set; }
        
        public DateTime UpdatedOn { get; set; }
        
        public bool SaveFeed { get; set; }
    }
}
