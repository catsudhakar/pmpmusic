﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PMPMusic.Models;
using PMPMusic.ViewModel;
using System.IO;
using PMPMusic.Services;
using PMPMusic.Repositories;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace PMPMusic.Controllers
{
    [Authorize]
    public class MainPageController : Controller
    {
        private readonly IPostFeedRepository _postFeedRepository;
        UserManager<ApplicationUser> _userManager;
        private ApplicationDBContext _applicationDbContext;

        public MainPageController(IPostFeedRepository postFeedRepository, UserManager<ApplicationUser> userManager, ApplicationDBContext applicationDbContext)
        {
            _postFeedRepository = postFeedRepository;
            _userManager = userManager;
            _applicationDbContext = applicationDbContext;
        }
        // GET: /<controller>/
        [Route("mainpage/{*catchall}")]
        public IActionResult Index()
        {
            HttpContext.Session.SetString("SessionprofileId", "");
            return View();
        }

        [Route("[controller]/getAudioList")]
        [AllowAnonymous]
        [HttpPost]
        //public async Task<IActionResult> getAudioList([FromBody] string userId)
        public async Task<IActionResult> getAudioList()
        {
            List<PostFeedVM> feeds = new List<PostFeedVM>();

            List<MusicCatalog> musics = new List<MusicCatalog>();
            try
            {
                var userId = HttpContext.Session.GetString("SessionprofileId");
                if (string.IsNullOrEmpty(userId))
                {
                    userId = _userManager.GetUserId(User);
                }
                var musicCatalogs = await _postFeedRepository.getAllMusicCatalogs(userId);
                //getting music from all catalogs
                foreach (var catalog in musicCatalogs)
                {
                    foreach (var item in catalog)
                    {
                        var songs = new PostFeedVM();

                        songs.AudioFileName = item.SongName;
                        songs.AudioLink = item.SongLink;
                        songs.CreatedOn = item.CreatedOn;

                        feeds.Add(songs);
                    }
                }
                //return Json(musicCatalogs);
                var feedsList = await _postFeedRepository.GetAllFeeds(_userManager.GetUserId(User));
                //getting music from feeds 
                foreach (var item in feedsList)
                {
                    if (item.AudioFileName != null)
                    {
                        feeds.Add(item);
                        feeds.OrderBy(x => x.CreatedOn);
                    }

                }

                return Json(feeds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [Route("[controller]/getFeeds")]
        [HttpPost]
        //public async Task<IActionResult> GetFeeds([FromBody] string userId)
        public async Task<IActionResult> GetFeeds()
        {
            try
            {
                var feedsList = await _postFeedRepository.GetAllFeeds(_userManager.GetUserId(User));
                return Json(feedsList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [Route("[controller]/getTotalActiveFeeds")]
        [HttpPost]
        public async Task<IActionResult> GetTotalActiveFeeds(string currentFeed)
        {
            try
            {
                var feedsList = await _postFeedRepository.getTotalActiveFeeds(_userManager.GetUserId(User));
                if (currentFeed != "current")
                {
                    List<PostFeedVM> sortList = new List<PostFeedVM>(feedsList);
                    Random rnd = new Random();
                    for (int i = sortList.Count() - 1; i > 0; --i)
                    {
                        int j = rnd.Next(i + 1);
                        PostFeedVM tmp = sortList[i];
                        sortList[i] = sortList[j];
                        sortList[j] = tmp;
                    }
                    feedsList = sortList;
                }
                return Json(feedsList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [Route("[controller]/getvideos")]
        [HttpPost]
        public async Task<IActionResult> GetVideos()
        {
            try
            {
                // getVideos
                var feedsList = await _postFeedRepository.getVideos(_userManager.GetUserId(User));
                return Json(feedsList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [Route("[controller]/getFollowedFeeds")]
        [HttpPost]
        public async Task<IActionResult> getFollowedFeeds()
        {
            try
            {
                var feedsList = await _postFeedRepository.GetFollowedFeedDetails(_userManager.GetUserId(User));
                return Json(feedsList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [Route("[controller]/getSavedFeeds")]
        [HttpPost]
        public async Task<IActionResult> getSavedFeeds()
        {
            try
            {
                var feedsList = await _postFeedRepository.GetSaveFeedDetails(_userManager.GetUserId(User));
                return Json(feedsList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [Route("[controller]/addUserTags")]
        [HttpPost]
        public async Task<IActionResult> addUserTags([FromBody] UserTagsVM model)
        {
            try
            {
                var userTag = await _postFeedRepository.addUserTags(model, _userManager.GetUserId(User));
                return Json(userTag);
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        [Route("[controller]/sendMessage")]
        [HttpPost]
        public async Task<IActionResult> SendMessage([FromBody] MessageVM model)
        {
            try
            {
                var userTag = await _postFeedRepository.SendMessage(model, _userManager.GetUserId(User));
                return Json(userTag);
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        [Route("[controller]/getMessageByUserId")]
        [HttpPost]
        public async Task<IActionResult> GetMessageByUserId()
        {
            try
            {
                var messages = await _postFeedRepository.GetMessageByUserId(_userManager.GetUserId(User));
                return Json(messages);
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }



        [Route("[controller]/getUserTags")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> getUserTags()
        {
            try
            {
                var userId = HttpContext.Session.GetString("SessionprofileId");
                if (string.IsNullOrEmpty(userId))
                {
                    userId = _userManager.GetUserId(User);
                }
                var userTag = await _postFeedRepository.getUserTags(userId);
                return Json(userTag);
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        [Route("[controller]/getLoginprofileUserDetails")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> getLoginprofileUserDetails()
        {
            bool status = false;
            try
            {
                var profileUser = HttpContext.Session.GetString("SessionprofileId");
                var loginUser = _userManager.GetUserId(User) == null ? "" : _userManager.GetUserId(User);
                if (profileUser != "" && loginUser != "")
                {
                    status = await _postFeedRepository.getFollowingStatus(profileUser, loginUser);
                }
                var result = new { ProfileUser = profileUser, LoginUser = loginUser, FollowStatus = status };
                return Json(result);
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }


        [Route("[controller]/deleteFeed")]
        [HttpPost]
        public async Task<IActionResult> deleteFeed(string FeedId)
        {
            try
            {
                var userTag = await _postFeedRepository.deleteUserFeed(FeedId, _userManager.GetUserId(User));
                return Json(userTag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [Route("[controller]/saveFeed")]
        [HttpPost]
        public async Task<IActionResult> SaveFeed([FromBody]PostFeedVM model)
        {
            string status = "notsaved";
            try
            {
                status = await _postFeedRepository.SaveFeed(model, _userManager.GetUserId(User));
                return Json(status);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        private void GetAllFeeds(string UserId)
        {

        }
        [Route("[controller]/AddNewFeed")]
        [HttpPost]
        public async Task<IActionResult> AddNewFeed([FromBody] PostFeedVM model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var post = await _postFeedRepository.AddPostFeed(model, _userManager.GetUserId(User));
                    return Json(model);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(model);
        }


        [Route("[controller]/AddNewComment")]
        [HttpPost]
        public async Task<IActionResult> AddNewComment([FromBody] PostCommentVM model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    model.CreatedBy = _userManager.GetUserId(User);
                    await _postFeedRepository.AddNewComment(model);
                    return Json(model);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(model);
        }


        [Route("[controller]/getAllFeedDetails")]
        [HttpPost]
        [AllowAnonymous]
        //public async Task<IActionResult> GetAllFeedDetails([FromBody] string userId)
        public async Task<IActionResult> GetAllFeedDetails()
        {
            //bool showProfileLinks = true; bool showProfile = true;
            MainVM mainVM = new MainVM();
            try
            {
                var userId = HttpContext.Session.GetString("SessionprofileId");
                if (!string.IsNullOrEmpty(userId))
                {
                    mainVM = await _postFeedRepository.GetAllFeedDetails(userId);
                    //showProfileLinks = false; showProfile = false;
                }
                else
                {
                    mainVM = await _postFeedRepository.GetAllFeedDetails(_userManager.GetUserId(User));
                    userId = _userManager.GetUserId(User);
                }
                //mainVM.ProfileUser = new ProfileUser()
                //{
                //    showProfileLinks = showProfileLinks,
                //    showProfile = showProfile,
                //};

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(mainVM);
        }


        [Route("[controller]/editProfile")]
        [HttpPost]
        public async Task<IActionResult> editProfile([FromBody]RegisterViewModel model)
        {

            RegisterViewModel regVM = new RegisterViewModel();
            try
            {
                regVM = await _postFeedRepository.editProfile(model, _userManager.GetUserId(User));

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return Json(regVM);
        }

        [Route("[controller]/editAccountInfo")]
        [HttpPost]
        public async Task<IActionResult> editAccountInfo([FromBody]RegisterViewModel model)
        {
            RegisterViewModel regVM = new RegisterViewModel();
            try
            {
                regVM = await _postFeedRepository.editAccountInfo(model, _userManager.GetUserId(User));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(regVM);
        }

        [Route("[controller]/editBiography")]
        [HttpPost]
        public async Task<IActionResult> editBiography([FromBody]UserTagsVM model)
        {
            UserTagsVM regVM = new UserTagsVM();
            try
            {
                regVM = await _postFeedRepository.editBiography(model, _userManager.GetUserId(User));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(regVM);
        }


        [Route("[controller]/FollowFeed")]
        [HttpPost]
        public async Task<JsonResult> FollowFeed(string id)
        {
            string retrunstring = string.Empty;
            FollowFeedVM model = new FollowFeedVM();
            model.FollowingUserId = new Guid(id);
            try
            {
                if (ModelState.IsValid)
                {
                    model.FollowingUser = _userManager.GetUserId(User);
                    retrunstring = await _postFeedRepository.FollowFeed(model);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(retrunstring);
        }

        [Route("[controller]/addMusicCatalog")]
        [HttpPost]
        public async Task<IActionResult> AddMusicCatalog([FromBody] MusicCatalogVM model)
        {
            MusicCatalogVM musicVM = new MusicCatalogVM();
            try
            {
                musicVM = await _postFeedRepository.addMusicCatalog(model, _userManager.GetUserId(User));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(musicVM);
        }

        [Route("[controller]/editMusicCatalog")]
        [HttpPost]
        public async Task<IActionResult> EditMusicCatalog([FromBody] MusicCatalogVM model)
        {
            MusicCatalogVM musicVM = new MusicCatalogVM();
            try
            {
                musicVM = await _postFeedRepository.editMusicCatalog(model, _userManager.GetUserId(User));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(musicVM);
        }


        [Route("[controller]/getAllMusicCatalogs")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> getAllMusicCatalogs()
        {
            try
            {
                var userId = HttpContext.Session.GetString("SessionprofileId");
                if (string.IsNullOrEmpty(userId))
                {
                    userId = _userManager.GetUserId(User);
                }
                var musicCatalogs = await _postFeedRepository.getAllMusicCatalogs(userId);
                return Json(musicCatalogs);
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }


        [Route("[controller]/getUserMusicCatalogs")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> getUserMusicCatalogs([FromBody] MusicCatalogVM model)
        {
            try
            {
                var userId = HttpContext.Session.GetString("SessionprofileId");
                if (string.IsNullOrEmpty(userId))
                {
                    userId = _userManager.GetUserId(User);
                }
                var musicCatalogs = await _postFeedRepository.getUserMusicCatalogs(model, userId);
                return Json(musicCatalogs);
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        [Route("[controller]/deleteSongFromCatalog")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> deleteSongFromCatalog([FromBody] MusicCatalogVM model)
        {
            try
            {
                var userId = HttpContext.Session.GetString("SessionprofileId");
                if (string.IsNullOrEmpty(userId))
                {
                    userId = _userManager.GetUserId(User);
                }
                var musicCatalogs = await _postFeedRepository.deleteSongFromCatalog(model, userId);
                return Json(musicCatalogs);
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
        [Route("[controller]/getUserGallery")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> getUserGallery()
        {
            try
            {
                var userId = HttpContext.Session.GetString("SessionprofileId");
                if (string.IsNullOrEmpty(userId))
                {
                    userId = _userManager.GetUserId(User);
                }
                var userGallery = await _postFeedRepository.getUserGalleryDetails(userId);
                return Json(userGallery);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [Route("[controller]/addUserGallery")]
        [HttpPost]
        public async Task<IActionResult> AddUserGallery([FromBody]UserGallery model)
        {
            UserGallery galleryVM = new UserGallery();
            try
            {
                galleryVM = await _postFeedRepository.addGallery(model, _userManager.GetUserId(User));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(galleryVM);
        }

        [Route("[controller]/editUserGallery")]
        [HttpPost]
        public async Task<IActionResult> EditUserGallery([FromBody]UserGalleryVM model)
        {
            UserGallery galleryVM = new UserGallery();
            try
            {
                galleryVM = await _postFeedRepository.editUserGallery(model, _userManager.GetUserId(User));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(galleryVM);
        }

        [Route("[controller]/removeUserGallery")]
        [HttpPost]

        public async Task<IActionResult> RemoveUserGallery([FromBody]UserGalleryVM model)
        {
            model.UpdatedBy = _userManager.GetUserId(User);
            string status = "success";
            try
            {
                await _postFeedRepository.removeUserGallery(model);
            }
            catch (Exception)
            {
                status = "Failure";
            }
            return Json(status);
        }

        [Route("[controller]/getComments")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> getComments(string feedId)
        {
            try
            {
                var comments = await _postFeedRepository.GetComments(feedId);
                return Json(comments);
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        [Route("[controller]/editSocialMedia")]
        [HttpPost]
        public async Task<IActionResult> EditSocialMedia([FromBody]SocialMedia model)
        {
            string status = "";
            try
            {
                var userId = HttpContext.Session.GetString("SessionprofileId");
                if (string.IsNullOrEmpty(userId))
                {
                    userId = _userManager.GetUserId(User);
                }
                model.CreatedBy = userId;
                await _postFeedRepository.socialMediaEdit(model);
                status = "Success";
            }
            catch (Exception)
            {
                status = "Failed";
            }
            return Json(status);
        }


        [Route("[controller]/getSocialMedia")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> GetSocialMedia()
        {
            List<SocialMedia> result = new List<SocialMedia>();
            try
            {
                var userId = HttpContext.Session.GetString("SessionprofileId");
                if (string.IsNullOrEmpty(userId))
                {
                    userId = _userManager.GetUserId(User);
                }
                result = await _postFeedRepository.getSocialMediaLinks(userId);
            }
            catch (Exception)
            {
                throw;
            }
            return Json(result);
        }

        [Route("[controller]/checkSocialMedia")]
        [HttpPost]
        public async Task<IActionResult> CheckSocialMedia([FromBody]SocialMedia model)
        {
            SocialMedia result = new SocialMedia();
            try
            {
                model.CreatedBy = _userManager.GetUserId(User);
                result = await _postFeedRepository.checkSocialMedia(model);
            }
            catch (Exception)
            {
                throw;
            }
            return Json(result);
        }


        [Route("[controller]/editProfilePic")]
        [HttpPost]
        public async Task<IActionResult> EditProfilePic([FromBody]ApplicationUser model)
        {
            string Status = "";
            try
            {
                model.Id = _userManager.GetUserId(User);
                Status = await _postFeedRepository.editProfileImage(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(Status);
        }
        [Route("[controller]/likeFeed")]
        [HttpPost]
        public async Task<IActionResult> LikeFeed([FromBody] LikeFeedVM model)
        {
            bool likeStatus = false;
            try
            {
                if (ModelState.IsValid)
                {
                    model.CreatedBy = _userManager.GetUserId(User);
                    likeStatus = await _postFeedRepository.LikeDislikeFeed(model);
                    return Json(likeStatus);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(likeStatus);
        }

        [Route("[controller]/getFeed")]
        [HttpGet]
        public async Task<IActionResult> GetFeed(string feedId)
        {
            PostFeed postFeed = new PostFeed();
            try
            {
                postFeed.Id = new Guid(feedId);
                var feedsList = await _postFeedRepository.getSingleFeed(postFeed);
                return Json(feedsList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [Route("[controller]/editFeed")]
        [HttpPost]
        public async Task<IActionResult> editPostFeed([FromBody] PostFeed model)
        {
            var feedsList = "";
            try
            {
                feedsList = await _postFeedRepository.editSingleFeed(model, _userManager.GetUserId(User));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(feedsList);
        }

        [Route("[controller]/addsubfeed")]
        [HttpPost]
        public async Task<IActionResult> AddsubmitedFeed([FromBody] SubFeeds model)
        {
            string subfeedsList = "";
            try
            {
                //subfeedsList = await _postFeedRepository.addSubfeeds(model,model.Id.ToString());
                subfeedsList = await _postFeedRepository.addSubfeeds(model, _userManager.GetUserId(User));
            }
            catch (Exception ex)
            {
                subfeedsList = "Failed! pls try again";
                throw ex;
            }
            return Json(subfeedsList);
        }

        [Route("[controller]/userProfileData")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> getuserProfileData()
        {
            try
            {
                var result = await _postFeedRepository.getFeaturebrowseusers();
                return Json(result);
            }
            catch (Exception)
            {
                return Json(null);
                throw;
            }
        }

        [Route("[controller]/getUserCatelogs")]
        [HttpPost]
        public async Task<IActionResult> GetUserCatelogs()
        {
            try
            {
                var musicCatalogs = await _postFeedRepository.getSubmitedfeedCatelogs(_userManager.GetUserId(User));
                return Json(musicCatalogs);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [Route("[controller]/getUserCatelogsById")]
        [HttpPost]
        public async Task<IActionResult> GetUserCatelogsById(string feedId, string userId, string type, string subHstry)
        {
            try
            {
                var subfeed = await _postFeedRepository.getUserCatelogsById(userId, feedId, type, _userManager.GetUserId(User), subHstry);
                return Json(subfeed);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [Route("[controller]/getCustomiseFeeds")]
        [HttpPost]
        public async Task<IActionResult> GetCustomiseFeeds([FromBody] List<SelectedCategoriesVM> selectedCat)
        {
            MainVM mainVM = new MainVM();
            try
            {
                mainVM = await _postFeedRepository.GetCustomiseFeeds(selectedCat, _userManager.GetUserId(User));
                List<PostFeedVM> sortList = new List<PostFeedVM>(mainVM.PostFeeds);
                Random rnd = new Random();
                for (int i = sortList.Count() - 1; i > 0; --i)
                {
                    int j = rnd.Next(i + 1);
                    PostFeedVM tmp = sortList[i];

                    sortList[i] = sortList[j];
                    sortList[j] = tmp;
                }
                var feedsList = sortList;
                return Json(feedsList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        

        [Route("[controller]/feedSeachByUser")]
        [HttpPost]
        public async Task<IActionResult> feedSeachByUser(string seachType, string searchString)
        {
            MainVM mainVM = new MainVM();
            try
            {
                mainVM = await _postFeedRepository.feedSeachByUser(seachType, searchString, _userManager.GetUserId(User));
                return Json(mainVM.PostFeeds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [Route("[controller]/getSubFeedHistory")]
        [HttpPost]
        public async Task<IActionResult> getSubFeedHistory(string FeedSubType)
        {
            try
            {
                var musicCatalogs = await _postFeedRepository.getSubmitedFeedHistory(_userManager.GetUserId(User), FeedSubType);
                return Json(musicCatalogs);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [Route("[controller]/addSongsToPlayer")]
        [HttpPost]
        public async Task<IActionResult> AddSongsToPlayer([FromBody] MusicCatalogVM model)
        {
            string status = "";
            try
            {
                status = await _postFeedRepository.AddSongsToPlayer(model, _userManager.GetUserId(User));
            }
            catch (Exception ex)
            {
                status = "Failed! pls try again";
                throw ex;
            }
            return Json(status);
        }
        [Route("[controller]/getPlayerSongsList")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> getPlayerSongsList()
        {
            try
            {
                var playList = await _postFeedRepository.getSongsPlayList(_userManager.GetUserId(User));
                return Json(playList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [Route("[controller]/getUserSearchTags")]
        [HttpPost]
        public async Task<IActionResult> GetUserSearchTags()
        {
            try
            {
                var userTags = await _postFeedRepository.getUserSearchTags(_userManager.GetUserId(User));
                return Json(userTags);
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        [Route("[controller]/getMessageUsers")]
        [HttpPost]
        public IActionResult GetMessageUsers()
        {
            try
            {
                var messages = _postFeedRepository.GetMessageUsers(_userManager.GetUserId(User));
                return Json(messages);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [Route("[controller]/getMessagesbyUser")]
        [HttpPost]
        public async Task<IActionResult> getMessagesbyUser(string FromUser)
        {
            try
            {
                var messages = await _postFeedRepository.getMessageUsersbyId(FromUser, _userManager.GetUserId(User));
                return Json(messages);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [Route("[controller]/deleteUsermsg")]
        [HttpPost]
        public async Task<IActionResult> deleteUsermsg(string userMessage)
        {
            try
            {
                var messages = await _postFeedRepository.deleteUserMessage(userMessage, _userManager.GetUserId(User));
                return Json(messages);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [Route("[controller]/getFeedByFeedId")]
        [HttpPost]
        [AllowAnonymous]
        //public async Task<IActionResult> GetAllFeedDetails([FromBody] string userId)
        public async Task<IActionResult> GetAllFeedDetails(string FeedID)
        {
            PostFeedVM mainVM = new PostFeedVM();
            try
            {
                mainVM = await _postFeedRepository.GetFeedById(FeedID, _userManager.GetUserId(User));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(mainVM);
        }

    }
}
