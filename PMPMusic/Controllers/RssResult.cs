﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using System.Xml.Linq;
using PMPMusic.ViewModel;
using PMPMusic.Models;
using System.Net;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace PMPMusic.Controllers
{
    public class RssResult : Controller
    {
        // GET: /<controller>/
        private readonly ApplicationDBContext _applicationDbContext;

        public RssResult(ApplicationDBContext applicationDbContext)

        //IHttpContextAccessor httpContextAccessor, ICustomerRepository customerManager)
        {
            _applicationDbContext = applicationDbContext;
        }

        public async Task<IActionResult> FeedDetails(string FeedId)
        {
            Guid GFeedId = new Guid(FeedId);

            var PostFeeds = _applicationDbContext.PostFeed.Where(x => x.Id == GFeedId).Select(f => new PostFeedVM
            {
                Id = f.Id,
                Title = f.Title,
                Description = f.Description,
                AudioLink = f.AudioLink,
                ImageLink = f.ImageLink,
                VideoLink = f.VideoLink,
                ExpireDate = f.ExpireDate,
                AudioFileName = f.AudioName,
                ImageFileName = f.ImageName,
                VideoFileName = f.VideoName,
                // FeedExpireIN = GetExpireTime(f.ExpireDate),
                CreatedOn = f.CreatedOn,
                SaveFeed = f.SaveFeed


            }).SingleOrDefault();
            return View(PostFeeds);
        }
        public async Task<IActionResult> Index()
        {

         
            List<FeedItem> Feeds = new List<FeedItem>();
            List<PostFeedVM> PostFeeds = new List<PostFeedVM>();

            PostFeeds = _applicationDbContext.PostFeed.Where(x=>x.ExpireDate >= DateTime.UtcNow).Select(f => new PostFeedVM
            {
                Id = f.Id,
                Title = f.Title,
                Description = f.Description,
                AudioLink = f.AudioLink,
                ImageLink = f.ImageLink,
                VideoLink = f.VideoLink,
                ExpireDate = f.ExpireDate,
                AudioFileName = f.AudioName,
                ImageFileName = f.ImageName,
                VideoFileName = f.VideoName,
               // FeedExpireIN = GetExpireTime(f.ExpireDate),
                CreatedOn = f.CreatedOn,
                SaveFeed = f.SaveFeed


            }).OrderByDescending(m => m.CreatedOn).ToList();

            //foreach(var f in PostFeeds)
            //{
            //    Feeds.Add(new FeedItem()
            //    {
            //        PostId=f.Id,
            //        Title = f.Title,
            //        Content = f.Description,
            //        Link = "http://localhost:56004/mainpage",
            //        PublishDate = DateTime.Now
            //    });
            //}
            

            return View(PostFeeds);

            var articles = new List<FeedItem>();
            // var feedUrl = "https://blogs.msdn.microsoft.com/martinkearn/feed/";
            var feedUrl = "http://feeds.abcnews.com/abcnews/topstories";
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(feedUrl);
                var responseMessage = await client.GetAsync(feedUrl);
                var responseString = await responseMessage.Content.ReadAsStringAsync();

                //extract feed items
                XDocument doc = XDocument.Parse(responseString);
                var feedItems = from item in doc.Root.Descendants().First(i => i.Name.LocalName == "channel").Elements().Where(i => i.Name.LocalName == "item")
                                select new FeedItem
                                {
                                    Content = item.Elements().First(i => i.Name.LocalName == "description").Value,
                                    Link = item.Elements().First(i => i.Name.LocalName == "link").Value,
                                    PublishDate = ParseDate(item.Elements().First(i => i.Name.LocalName == "pubDate").Value),
                                    Title = item.Elements().First(i => i.Name.LocalName == "title").Value
                                   
                                };
                articles = feedItems.ToList();
            }

            return View(articles);
        }

        private DateTime ParseDate(string date)
        {
            DateTime result;
            if (DateTime.TryParse(date, out result))
                return result;
            else
                return DateTime.MinValue;
        }
    }

 
}
