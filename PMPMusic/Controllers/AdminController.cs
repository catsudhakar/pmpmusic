﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using PMPMusic.Repositories;
using Microsoft.AspNetCore.Identity;
using PMPMusic.Models;
using PMPMusic.ViewModel;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace PMPMusic.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {

        private readonly IPostFeedRepository _postFeedRepository;
        private readonly IAdminRepository _adminRepository;
        UserManager<ApplicationUser> _userManager;
        private ApplicationDBContext _applicationDbContext;

        public AdminController(IAdminRepository adminRepository, IPostFeedRepository postFeedRepository, UserManager<ApplicationUser> userManager, ApplicationDBContext applicationDbContext)
        {
            _adminRepository = adminRepository;
            _postFeedRepository = postFeedRepository;
            _userManager = userManager;
            _applicationDbContext = applicationDbContext;
        }
        // GET: /<controller>/
        [Route("admin/{*catchall}")]
        public IActionResult Index()
        {

            return View();
        }

        [Route("[controller]/getFeeds")]
        [HttpPost]
        public async Task<IActionResult> GetFeeds()
        {

            try
            {
                var feedsList = await _postFeedRepository.GetAllFeeds(_userManager.GetUserId(User));
                return Json(feedsList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [Route("[controller]/getAllUsers")]
        [HttpPost]
        public async Task<IActionResult> GetAllUsers()
        {

            try
            {
                var users = await _adminRepository.GetUserDetails();
                return Json(users);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [Route("[controller]/updateUser")]
        [HttpPost]
        public async Task<IActionResult> UpdateUser([FromBody] List<SelectedUserDetailsVM> Users)
        {

            try
            {
                var users = _adminRepository.UpdateFeaturedUser(Users);
                return Json(users);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [Route("[controller]/updateFeed")]
        [HttpPost]
        public async Task<IActionResult> UpdateFeed([FromBody] List<PostFeedVM> Users)
        {

            try
            {
                var users = _adminRepository.UpdateTrendingFeed(Users);
                return Json(users);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
