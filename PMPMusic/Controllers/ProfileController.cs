﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PMPMusic.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace PMPMusic.Controllers
{
    public class ProfileController : Controller
    {
        private readonly ApplicationDBContext _applicationDbContext;

        public ProfileController(ApplicationDBContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        
        [Route("/profile/{*catchall}")]
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Index(string profileName)
        {
            // return View();
            if (!string.IsNullOrEmpty(profileName))
            {
             
                string[] userdetils = profileName.Split('-');

                if (userdetils.Length > 2)
                {
                    var user = await _applicationDbContext.AspNetUsers.Where(x => x.FirstName == profileName.TrimEnd('-')).SingleOrDefaultAsync();
                    if (user == null || user.EmailConfirmed == false) return RedirectToAction("Login", "Account");
                    HttpContext.Session.SetString("SessionprofileId", user != null ? user.Id : "");
                    return View();
                }
                else if (userdetils.Length == 2)
                {
                    //var user = await _applicationDbContext.AspNetUsers.Where(x => x.FirstName == userdetils[0] && x.LastName == userdetils[1]).SingleOrDefaultAsync();
                    var user = await _applicationDbContext.AspNetUsers.Where(x => x.FirstName == profileName).SingleOrDefaultAsync();
                    if (user == null || user.EmailConfirmed == false) return RedirectToAction("Login", "Account");
                    HttpContext.Session.SetString("SessionprofileId", user != null ? user.Id : "");
                    return View();
                }

                //if (userdetils.Length == 2)
                //{
                //    var user = await _applicationDbContext.AspNetUsers.Where(x => x.FirstName == userdetils[0] && x.LastName == userdetils[1]).SingleOrDefaultAsync();
                //    if (user == null || user.EmailConfirmed == false) return RedirectToAction("Login", "Account");
                //    HttpContext.Session.SetString("SessionprofileId", user != null ? user.Id : "");
                //    return View();
                //}

            }
            return View();
        }


        // GET: /<controller>/
        //[Route("/profile/")]
        //[HttpGet]
        //[AllowAnonymous]
        //public async Task<IActionResult> Index(string ProfileName)
        //{
        //    // return View();
        //    if (!string.IsNullOrEmpty(ProfileName))
        //    {
        //        string[] userdetils = ProfileName.Split('-');
        //        if (userdetils.Length == 2)
        //        {
        //            var user = await _applicationDbContext.AspNetUsers.Where(x => x.FirstName == userdetils[0] && x.LastName == userdetils[1]).SingleOrDefaultAsync();
        //            if (user == null) return RedirectToAction("Login", "Account");
        //            if (user.EmailConfirmed == false) return RedirectToAction("Login", "Account");
        //            //return RedirectToAction("Index", "MainPage");
        //            return View();
        //        }
        //    }
        //    return View();
        //}
    }
}
