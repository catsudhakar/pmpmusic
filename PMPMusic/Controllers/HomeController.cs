﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PMPMusic.Models;
using PMPMusic.Repositories;
using Microsoft.AspNetCore.Identity;

namespace PMPMusic.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationDBContext _applicationDbContext;
        private readonly ICommonRepository _commonManager;
        private readonly IPostFeedRepository _postFeedRepository;
        UserManager<ApplicationUser> _userManager;

        public HomeController
        (

                ApplicationDBContext applicationDbContext,
                ICommonRepository commonManager, IPostFeedRepository postFeedRepository, UserManager<ApplicationUser> userManager

        )
        {
            _applicationDbContext = applicationDbContext;
            _commonManager = commonManager; _postFeedRepository = postFeedRepository;
            _userManager = userManager;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }



        public IActionResult OurPolicies()
        {
            ViewData["Message"] = "Your OurPolicies page.";

            return View();
        }

        public IActionResult Advertise()
        {
            ViewData["Message"] = "Your Advertise page.";

            return View();
        }

        public IActionResult FAQ()
        {
            ViewData["Message"] = "FAQ.";

            return View();
        }

        public IActionResult Pricing()
        {
            ViewData["Message"] = "Pricing.";

            return View();
        }

        public IActionResult ContactUs()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Tutorials()
        {
            ViewData["Message"] = "Your Tutorials page.";

            return View();
        }

        public IActionResult TagCombos()
        {
            ViewData["Message"] = "Your TagCombos page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }

        public async Task<IActionResult> ContactUs1(ContactUsViewModel model)
        {
            _commonManager.EnsureDatabaseCreated(_applicationDbContext);

            if (ModelState.IsValid)
            {
                var contactUs = new ContactUs
                {
                    Name = model.Name,
                    Message = model.Message,
                    Email = model.Email
                };


                _applicationDbContext.ContactUs.Add(contactUs);

                await _applicationDbContext.SaveChangesAsync();

                ModelState.Clear();
                ModelState.AddModelError(string.Empty, "Your request has been submitted successfully.");

                return View("Contact");
            }

            return View("Contact");
        }

        [Route("[controller]/getFeeds")]
        [HttpPost]
        //public async Task<IActionResult> GetFeeds([FromBody] string userId)
        public async Task<IActionResult> GetFeeds()
        {

            try
            {
                var feedsList = await _postFeedRepository.GetAllFeeds();
                return Json(feedsList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [Route("[controller]/getHomepageFeeds")]
        [HttpPost]
        //public async Task<IActionResult> GetFeeds([FromBody] string userId)
        public async Task<IActionResult> getHomepageFeeds()
        {

            try
            {
                var feedsList = await _postFeedRepository.GetAllHomepageFeeds(_userManager.GetUserId(User));
                return Json(feedsList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [Route("[controller]/getFeedsByTags")]
        [HttpPost]
        public async Task<IActionResult> GetFeedsByTags([FromBody] List<string> selectedTags)
        {

            try
            {
                var musicCatalog = await _postFeedRepository.GetFeedsByTags(selectedTags);
                return Json(musicCatalog);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [Route("[controller]/getFeedsForTreding")]
        [HttpPost]
        public async Task<IActionResult> GetFeedsForTreding()
        {

            try
            {
                var feedsList = await _postFeedRepository.GetFeedsForTreding();
                return Json(feedsList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
