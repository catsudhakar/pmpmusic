﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using PMPMusic.Models;
using PMPMusic.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net.Http.Headers;

using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Authentication;
using PMPMusic.Repositories;
using System.IO;
using PMPMusic.ViewModel;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace PMPMusic.Controllers
{

    public class AccountController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        // private readonly RoleManager<ApplicationUser> _roleManager;
        private readonly SignInManager<ApplicationUser> _signInManager;

        private readonly IEmailSender _emailSender;
        private readonly ISmsSender _smsSender;

        private readonly ApplicationDBContext _applicationDbContext;
        //private static bool _databaseChecked;
        private IHostingEnvironment _envronment;

        private readonly ICommonRepository _commonManager;
        //private readonly IHttpContextAccessor _accessor;
        //private readonly ICustomerRepository _customerManager;
        private HttpContext _currentContext;

        private readonly ICloudService _amazonService;

        public AccountController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IEmailSender emailSender,
            ISmsSender smsSender,
            IHostingEnvironment envronment,
            ICommonRepository commonManager,
            IHttpContextAccessor httpContextAccessor,
            ApplicationDBContext applicationDbContext, ICloudService amazonService)

        //IHttpContextAccessor httpContextAccessor, ICustomerRepository customerManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = emailSender;
            _smsSender = smsSender;
            _applicationDbContext = applicationDbContext;
            _commonManager = commonManager;
            _currentContext = httpContextAccessor.HttpContext;
            //_customerManager = customerManager;
            _envronment = envronment;
            //_currentContext = currentContext;
            _amazonService = amazonService;
        }

        //
        // GET: /Account/Login
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Login(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (User.Identity.Name == null && User.Identity.IsAuthenticated == false)
            {
                if (Request.Cookies["UserName"] != null && Request.Cookies["Password"] != null)
                {
                    LoginViewModel model = new LoginViewModel();
                    model.Password = Request.Cookies["Password"];
                    model.Email = Request.Cookies["UserName"];
                    model.RememberMe = true;
                    return View(model);
                }
                else { ViewData["ReturnUrl"] = returnUrl; return View(); }
            }
            else
            {
                var user = await _applicationDbContext.AspNetUsers.Where(x => x.FirstName == User.Identity.Name && x.EmailConfirmed == true).SingleOrDefaultAsync();
                if (user == null) return View();
                return Redirect(returnUrl == null ? "/mainpage" : returnUrl);
            }
        }

        [HttpPost]
        public async Task<IActionResult> AjaxLogin([FromBody]LoginViewModel model)
        {
            // return RedirectToAction("Index", "MainPage");
            _commonManager.EnsureDatabaseCreated(_applicationDbContext);

            if (ModelState.IsValid)
            {
                //Require the user to have confirmed email before it can log on
                var user = await _userManager.FindByEmailAsync(model.Email);
                if (user != null)
                {
                    var roles = await _userManager.GetRolesAsync(user);
                    if (!await _userManager.IsEmailConfirmedAsync(user))
                    {
                        ModelState.AddModelError(string.Empty, "*** You must have confirmed your email to log in ***");
                        // return View(model);
                        return Json("*** You must have confirmed your email to log in ***");
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "*** Invalid login attempt. ***");
                    //return View(model);
                    return Json("*** Invalid login attempt.***");
                }

                // This doesn't count login failures towards account lockout
                // To enable password failures to trigger account lockout, set lockoutOnFailure: true
                var result = await _signInManager.PasswordSignInAsync(user, model.Password, model.RememberMe, lockoutOnFailure: false);
                if (result.Succeeded)
                {
                    CookieOptions options = new CookieOptions();
                    if (model.RememberMe)
                        options.Expires = DateTime.Now.AddDays(15);
                    else
                        options.Expires = DateTime.Now.AddDays(-1);
                    Response.Cookies.Append("UserName", model.Email, options);
                    Response.Cookies.Append("Password", model.Password, options);

                    bool isAdmin = await _userManager.IsInRoleAsync(user, "Admin");
                    //bool isCust = await _userManager.IsInRoleAsync(user, "Cust");
                    //return RedirectToLocal(returnUrl, isAdmin, isCust);
                    bool isCust = await _userManager.IsInRoleAsync(user, "user");
                    //return RedirectToAction("Index", "MainPage");
                    return Json("success");
                }
                //if (result.RequiresTwoFactor)
                //{
                //    return RedirectToAction(nameof(SendCode), new {RememberMe = model.RememberMe });
                //}
                if (result.IsLockedOut)
                {
                    //return View("Lockout");
                    return Json("Lockout");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "*** Invalid login attempt. ***");
                    //return View(model);
                    return Json("*** Invalid login attempt.***");
                }
            }

            // If we got this far, something failed, redisplay form
            // return View(model);
            return Json("*** Invalid login attempt.***");
        }
        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
        {
            // return RedirectToAction("Index", "MainPage");
            _commonManager.EnsureDatabaseCreated(_applicationDbContext);
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                //Require the user to have confirmed email before it can log on
                var user = await _userManager.FindByEmailAsync(model.Email);
                if (user != null)
                {
                    var roles = await _userManager.GetRolesAsync(user);
                    if (!await _userManager.IsEmailConfirmedAsync(user))
                    {
                        ModelState.AddModelError(string.Empty, "*** You must have confirmed your email to log in ***");
                        return View(model);
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "*** Invalid login attempt. ***");
                    return View(model);
                }

                // This doesn't count login failures towards account lockout
                // To enable password failures to trigger account lockout, set lockoutOnFailure: true
                var result = await _signInManager.PasswordSignInAsync(user, model.Password, model.RememberMe, lockoutOnFailure: false);
                if (result.Succeeded)
                {
                    CookieOptions options = new CookieOptions();
                    if (model.RememberMe)
                        options.Expires = DateTime.Now.AddDays(15);
                    else
                        options.Expires = DateTime.Now.AddDays(-1);
                    Response.Cookies.Append("UserName", model.Email, options);
                    Response.Cookies.Append("Password", model.Password, options);

                    bool isAdmin = await _userManager.IsInRoleAsync(user, "Admin");
                    //bool isCust = await _userManager.IsInRoleAsync(user, "Cust");
                    //return RedirectToLocal(returnUrl, isAdmin, isCust);
                    bool isCust = await _userManager.IsInRoleAsync(user, "user");
                    return RedirectToAction("Index", "MainPage");
                }
                if (result.RequiresTwoFactor)
                {
                    return RedirectToAction(nameof(SendCode), new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                }
                if (result.IsLockedOut)
                {
                    return View("Lockout");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "*** Invalid login attempt. ***");
                    return View(model);
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        // GET: /Account/Register
        [HttpGet]
        [AllowAnonymous]
        public IActionResult Register()
        {
            using (HttpClient client = new HttpClient())
            {
                var request = _currentContext.Request;

                var host = request.Host.ToUriComponent();

                var pathBase = request.PathBase.ToUriComponent();

                var paths = $"{request.Scheme}://{host}{pathBase}";

                var jsonPath = paths + "/countryList/countryList.json";
                client.BaseAddress = new Uri
                (jsonPath);
                MediaTypeWithQualityHeaderValue contentType =
                new MediaTypeWithQualityHeaderValue("application/json");
                client.DefaultRequestHeaders.Accept.Add(contentType);
                HttpResponseMessage response = client.GetAsync
                (jsonPath).Result;
                string stringData = response.Content.
                ReadAsStringAsync().Result;
                //List<CountryViewModel> data = JsonConvert.DeserializeObject
                //<List<CountryViewModel>>(stringData);
                //ViewBag.countryList = new SelectList(data, "code", "name");
                //ViewBag.countryListData = data;   
            }
            return View();
        }
        [HttpPost]
        public async Task<JsonResult> doesEmailExist(string Email)
        {
            var checkUser = await _userManager.FindByEmailAsync(Email);
            if (checkUser != null)
            { return Json("User exist with this email"); }
            else
            { return Json(true); }
        }


        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model, IFormFile ProfileLink)
        {
            try
            {
                _commonManager.EnsureDatabaseCreated(_applicationDbContext);
                //Register();

                var files = HttpContext.Request.Form.Files;
                foreach (var Image in files)
                {
                    if (Image != null && Image.Length > 0)
                    {
                        var file = Image;
                        if (file.Length > 0)
                        {
                            var fileName = ContentDispositionHeaderValue.Parse
                                (file.ContentDisposition).FileName.Trim('"');

                            using (var memoryStream = new MemoryStream())
                            {
                                await file.CopyToAsync(memoryStream);
                                //var a = memoryStream.ToArray(); "image/jpeg"
                                var imageUrl1 = await _amazonService.UploadMedia(memoryStream, file.ContentType, Guid.NewGuid().ToString());
                                model.ProfileLink = imageUrl1;
                            }
                        }
                    }
                }
                if (ModelState.IsValid)
                {
                    var user = new ApplicationUser
                    {
                        UserName = model.FirstName,
                        NormalizedUserName = model.Email,
                        Email = model.Email,
                        Country = model.Country,
                        FirstName = model.FirstName,
                        LastName = model.LastName,
                        ProfileLink = model.ProfileLink,
                        RegisteredOn = DateTime.UtcNow

                    };

                    var checkUser = await _userManager.FindByEmailAsync(user.Email);
                    if (checkUser != null)
                    {
                        ModelState.AddModelError(string.Empty, "*** User exist with this email ***");
                    }
                    else
                    {
                        var result = await _userManager.CreateAsync(user, model.Password);
                        if (result.Succeeded)
                        {
                            await _userManager.AddToRoleAsync(user, "User");
                            // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=532713
                            // Send an email with this link
                            var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                            var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Scheme);

                            string html = _commonManager.ReadHtmlTemplate("ConfirmRegistration", model.FirstName + " " + model.LastName, "", callbackUrl);
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
                            Task.Run(() =>
                            {
                                _emailSender.SendEmailAsync(model.Email, "Confirm your account", html);
                            });
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed

                            await _signInManager.SignInAsync(user, isPersistent: false);
                            // return RedirectToAction("Login", "Account");
                            ModelState.AddModelError(string.Empty, "Your account is registered successfully. Please verify your mail");
                            return RedirectToAction("RegisterConfirmation");
                        }
                        AddErrors(result);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [AllowAnonymous]
        public IActionResult RegisterConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/LogOff
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public IActionResult LogOff()
        //{
        //    _signInManager.SignOutAsync();
        //    return RedirectToAction("Login", "Account");
        //}

        public ActionResult LogOff()
        {
            _signInManager.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }


        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public IActionResult ExternalLogin(string provider, string returnUrl = null)
        {
            _commonManager.EnsureDatabaseCreated(_applicationDbContext);
            // Request a redirect to the external login provider.
            var redirectUrl = Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl });
            var properties = _signInManager.ConfigureExternalAuthenticationProperties(provider, redirectUrl);
            return new ChallengeResult(provider, properties);
        }


        // GET: /Account/ExternalLoginCallback
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ExternalLoginCallback(string returnUrl = null)
        {
            var info = await _signInManager.GetExternalLoginInfoAsync();
            if (info == null)
            {
                return RedirectToAction(nameof(Login));
            }

            // Sign in the user with this external login provider if the user already has a login.
            var result = await _signInManager.ExternalLoginSignInAsync(info.LoginProvider, info.ProviderKey, isPersistent: false);
            if (result.Succeeded)
            {
                var user = await _userManager.FindByLoginAsync(info.LoginProvider, info.ProviderKey);
                if (user != null)
                {
                    //await StoreFacebookAuthToken(user);
                    await SignInAsync(user, isPersist: true);
                }
                return RedirectToLocal(returnUrl, false, false);
            }
            if (result.RequiresTwoFactor)
            {
                return RedirectToAction(nameof(SendCode), new { ReturnUrl = returnUrl });
            }
            if (result.IsLockedOut)
            {
                return View("Lockout");
            }
            else
            {
                // If the user does not have an account, then ask the user to create an account.
                ViewData["ReturnUrl"] = returnUrl;
                ViewData["LoginProvider"] = info.LoginProvider;
                // var email = info.Principal.FindFirstValue(ClaimTypes.Email);
                // return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = email });
                return View();
            }
        }

        private Task SignInAsync(ApplicationUser user, bool isPersist)
        {
            return Task.FromResult(0);

        }

        //async private Task StoreFacebookAuthToken(ApplicationUser user)
        //{

        //    var userClaim = await _userManager.GetClaimsAsync(user);
        //    var accessToken = userClaim.FirstOrDefault(x => x.Type == "FacebookAccessToken");
        //    var fb = new FacebookClient();
        //    dynamic meInfo = fb.Get("/me");
        //    dynamic mePicture = fb.Get("/me?fields=picture");

        //    var profileInfo = new
        //    {
        //        Name = $"{meInfo["first_name"]} {meInfo["last_name"]}",
        //        Locale = meInfo["locale"],
        //        UpdatedTime = meInfo["updated_time"],
        //        PictureUrl = mePicture["data"]["url"]
        //    };

        //}

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl = null)
        {
            //if (User.IsSignedIn())
            //{
            //    return RedirectToAction(nameof(ManageController.Index),"Manage");
            //}

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await _signInManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await _userManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await _userManager.AddLoginAsync(user, info);
                    if (result.Succeeded)
                    {
                        await _signInManager.SignInAsync(user, isPersistent: false);
                        return RedirectToLocal(returnUrl, false, false);
                    }
                }
                AddErrors(result);
            }

            ViewData["ReturnUrl"] = returnUrl;
            return View(model);
        }

        // GET: /Account/ConfirmEmail
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                return View("Error");
            }
            var result = await _userManager.ConfirmEmailAsync(user, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        //
        // GET: /Account/ForgotPassword
        [HttpGet]
        [AllowAnonymous]
        public IActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(model.Email);
                if (user == null)
                {
                    ModelState.AddModelError(string.Empty, "User does not exist with this email");
                    // Don't reveal that the user does not exist or is not confirmed
                    return View();
                }
                if (!(await _userManager.IsEmailConfirmedAsync(user)))
                {
                    ModelState.AddModelError(string.Empty, "Please confirm your email first.");
                    // Don't reveal that the user does not exist or is not confirmed
                    return View();
                }
                // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=532713
                // Send an email with this link
                var code = await _userManager.GeneratePasswordResetTokenAsync(user);
                var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Scheme);

                string html = _commonManager.ReadHtmlTemplate("ForgotPassword", user.FirstName + " " + user.LastName, "", callbackUrl);
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
                Task.Run(() =>
                {
                    _emailSender.SendEmailAsync(model.Email, "Reset Password", html);
                });
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed

                return View("ForgotPasswordConfirmation");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [HttpGet]
        [AllowAnonymous]
        public IActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPassword(string code = null)
        {
            return code == null ? View("Error") : View();
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await _userManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                //return RedirectToAction(nameof(AccountController.ResetPasswordConfirmation), "Account");
                ModelState.AddModelError(string.Empty, "*** Invalid user name ***");
            }
            var result = await _userManager.ResetPasswordAsync(user, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction(nameof(AccountController.ResetPasswordConfirmation), "Account");
            }
            else if (result.ToString().Contains("Failed : InvalidToken"))
            {
                ModelState.AddModelError(string.Empty, "*** Reset password link was broken! please reset with valid link ***");
            }
            AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/SendCode
        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl = null, bool rememberMe = false)
        {
            var user = await _signInManager.GetTwoFactorAuthenticationUserAsync();
            if (user == null)
            {
                return View("Error");
            }
            var userFactors = await _userManager.GetValidTwoFactorProvidersAsync(user);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose });
            return View(new SendCodeViewModel { Providers = factorOptions.ToList(), ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            var user = await _signInManager.GetTwoFactorAuthenticationUserAsync();
            if (user == null)
            {
                return View("Error");
            }

            // Generate the token and send it
            var code = await _userManager.GenerateTwoFactorTokenAsync(user, model.SelectedProvider);
            if (string.IsNullOrWhiteSpace(code))
            {
                return View("Error");
            }

            var message = "Your security code is: " + code;
            if (model.SelectedProvider == "Email")
            {
                //await _emailSender.SendEmailAsync(await _userManager.GetEmailAsync(user), "Security Code", message);
            }
            else if (model.SelectedProvider == "Phone")
            {
                //await _smsSender.SendSmsAsync(await _userManager.GetPhoneNumberAsync(user), message);
            }

            return RedirectToAction(nameof(VerifyCode), new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
        }

        //
        // GET: /Account/VerifyCode
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> VerifyCode(string provider, bool rememberMe, string returnUrl = null)
        {
            // Require that the user has already logged in via username/password or external login
            var user = await _signInManager.GetTwoFactorAuthenticationUserAsync();
            if (user == null)
            {
                return View("Error");
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // The following code protects for brute force attacks against the two factor codes.
            // If a user enters incorrect codes for a specified amount of time then the user account
            // will be locked out for a specified amount of time.
            var result = await _signInManager.TwoFactorSignInAsync(model.Provider, model.Code, model.RememberMe, model.RememberBrowser);
            if (result.Succeeded)
            {
                return RedirectToLocal(model.ReturnUrl, false, false);
            }
            if (result.IsLockedOut)
            {
                return View("Lockout");
            }
            else
            {
                ModelState.AddModelError("", "Invalid code.");
                return View(model);
            }
        }

        public IActionResult AccessDenied()
        {
            ViewBag.UserName = _userManager.GetUserName
                (User);
            return View();
        }

        #region Helpers

        // The following code creates the database and schema if they don't exist.
        // This is a temporary workaround since deploying database through EF migrations is
        // not yet supported in this release.
        // Please see this http://go.microsoft.com/fwlink/?LinkID=615859 for more information on how to do deploy the database
        // when publishing your application.
        //private static void EnsureDatabaseCreated(ApplicationDBContext context)
        //{
        //    if (!_databaseChecked)
        //    {
        //        _databaseChecked = true;
        //        //context.Database.AsRelational().ApplyMigrations();
        //        context.Database.MigrateAsync();
        //    }
        //}

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                //if(error.Code=="DuplicateUserName")
                //{
                //    error.Description = "Given first name is already taken.";
                //}

                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        private async Task<ApplicationUser> GetCurrentUserAsync()
        {
            return await _userManager.GetUserAsync(User);
        }

        private IActionResult RedirectToLocal(string returnUrl, bool isAdmin, bool isCustomer)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                //if (isAdmin)
                //{
                //    return RedirectToAction(nameof(AdminController.Index), "Admin");
                //}
                //else if (isCustomer)
                //{
                //    return RedirectToAction(nameof(CustomerController.Index), "Customer");
                //}
                //else
                //{
                return RedirectToAction(nameof(HomeController.Index), "Home");
                //}



            }
        }



        #endregion


        public async Task<int> addUserTags(UserRegistrationViewModel model, ApplicationUser user)
        {
            try
            {
                var userTags = new UserTags();
                userTags.Id = Guid.NewGuid();
                userTags.MainTag = model.MainTag;
                userTags.Tag1 = model.Tag1;
                userTags.Tag2 = model.Tag2;
                userTags.Tag3 = model.Tag3;
                userTags.User = user;
                _applicationDbContext.Add(userTags);
                await _applicationDbContext.SaveChangesAsync();
                return await _applicationDbContext.SaveChangesAsync();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return 0;
        }

        [HttpPost]
        public async Task<IActionResult> UserRegistration([FromBody]UserRegistrationViewModel model)
        {
            string messages = string.Empty;
            try
            {
                if (ModelState.IsValid)
                {
                    string[] userName = model.FirstName.Split(' ');
                    string Fname = string.Empty,Lname = string.Empty;
                    int i = 0;
                    foreach (string users in userName)
                    {
                        if (users != "")
                        {
                            if (i == 0)
                                Fname = users;
                            else
                                Fname = Fname + "-" + users;
                            i++;
                        }
                    }
                    var user = new ApplicationUser
                    {
                        FirstName = Fname,
                        LastName="",
                        UserName = Fname,
                        NormalizedUserName = model.Email,
                        Email = model.Email,
                        RegisteredOn = DateTime.UtcNow,
                        EmailConfirmed=true,
                        TermsAndContions= model.TermsAndContions
                    };

                    var checkUser = await _userManager.FindByEmailAsync(user.Email);
                    if (checkUser != null)
                    {
                        return Json(" User exist with this email");
                    }
                    else
                    {
                        var result = await _userManager.CreateAsync(user, model.Password);
                        if (result.Succeeded)
                        {
                            //adding user tags
                            await addUserTags(model, user);

                            await _userManager.AddToRoleAsync(user, "User");
                            // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=532713
                            // Send an email with this link

                            //--------------------Email Verification--------------------//
                            //var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                            // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Scheme);
                            // string html = _commonManager.ReadHtmlTemplate("ConfirmRegistration", model.FirstName + " " + model.FirstName, "", callbackUrl);
                            //Task.Run(() =>
                            //{
                            //    _emailSender.SendEmailAsync(model.Email, "Confirm your account", html);
                            //});

#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
                            //Task.Run(() =>
                            //{
                            //    _emailSender.SendEmailAsync(model.Email, "Confirm your account", html);
                            //});

#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed

                            await _signInManager.SignInAsync(user, isPersistent: false);
                            //return RedirectToAction("Login", "Account");
                            //return Json("Your account is registered successfully. Please verify your mail");
                            return Json("Your account is registered successfully, Please login with our emailid..");

                            //ModelState.AddModelError(string.Empty, "Your account is registered successfully. Please verify your mail");
                            //return RedirectToAction("RegisterConfirmation");
                        }
                        else
                        {
                            AddErrors(result);
                              messages = string.Join("; ", ModelState.Values.SelectMany(x => x.Errors) .Select(x => x.ErrorMessage));
                             return Json("Error ! " + messages);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            // If we got this far, something failed, redisplay form
            //return Json(model);
             messages = string.Join("; ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
            return Json(messages);
        }

        public IActionResult FacebookLogin()
        {
            var authProperties = new AuthenticationProperties
            {
                RedirectUri = Url.Action("Index", "Home")
            };
            return Challenge(authProperties, "Facebook");
        }

        //public async Task<IActionResult> ExternalLoginCallback()
        //{
        //    ExternalLoginInfo info = await _signInManager.GetExternalLoginInfoAsync();
        //    //info.Principal //the IPrincipal with the claims from facebook
        //    //info.ProviderKey //an unique identifier from Facebook for the user that just signed in
        //    //info.LoginProvider //a string with the external login provider name, in this case Facebook

        //    //to sign the user in if there's a local account associated to the login provider
        //    //var result = await _signInManager.ExternalLoginSignInAsync(info.LoginProvider, info.ProviderKey, isPersistent: false);            
        //    //result.Succeeded will be false if there's no local associated account 

        //    //to associate a local user account to an external login provider
        //    //await _userInManager.AddLoginAsync(aUserYoullHaveToCreate, info);        
        //    return Redirect("~/");
        //}
    }
}
